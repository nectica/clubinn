<?php

namespace App\Http\Middleware;

use App\Models\Cliente;
use Closure;
use Illuminate\Http\Request;

class LoginApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
		$cliente = Cliente::where('email',$request->server('PHP_AUTH_USER') )->first();
		if ($cliente && $cliente->password !== $request->server('PHP_AUTH_PW') ) {
			return response()->json(['status' => 'error', 'msg' => 'Clave incorrecta.'], 403);

		}else if(!$cliente) {
			return response()->json(['status' => 'error', 'msg' => 'No existe el usuario.'], 403);
		}
        return $next($request);
    }
}
