<?php

namespace App\Http\Requests;

use App\Models\FormaPago;
use App\Models\Movimiento;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule ;

class StoreReciboRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$recibo = Movimiento::where('nombre', 'recibo')->first();
		$efectivo = FormaPago::where('nombre', 'efectivo')->first();
		return [
			'cliente' => 'required',
			'hotel' => 'required',
			'movimiento_id' => 'required',
			'importe' => 'required|numeric',
			'forma_pago_id' => 'required_if:movimiento_id,' . $recibo->id,
			'voucher' => [  Rule::requiredIf($this->movimiento_id == $recibo->id && $this->forma_pago_id != $efectivo->id),'mimes:png,jpg,pdf']
		];
	}



	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'forma_pago_id.required_if' => 'Es requerido cuando es un recibo',
		];
	}
}
