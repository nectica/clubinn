<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClienteAPiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			/* 'nombre' => 'required',
			'apellido' => 'required',
			'telefono' => 'required',
			'celular' => 'required',
			'email' => 'required|email',
			'nombre_conyugue' => 'required',
			'apellido_conyugue' => 'required',
			'celular_conyugue' => 'required|',
			'email_conyugue' => 'required|email', */
        ];
    }
}
