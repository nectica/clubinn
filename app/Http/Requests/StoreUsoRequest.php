<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StoreUsoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'semana' => 'nullable|numeric',
			'fraccionado' => 'nullable',
			'cliente_id' => 'required|numeric',
			'unidad_id' => 'required|numeric',
			'hotel_id' => 'required|numeric',
			'temporada_id' => 'required|numeric',

			'dias' => 'required|numeric',
			'anio' => 'required|numeric',
			'fecha_vencimiento' => 'required|date|after:yesterday',
        ];
    }
}
