<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCbuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'banco' => 'required',
			'titular' => 'required',
			'tipo_cuenta' => 'required',
			'nro_cuenta' => 'required',
			'cbu' => 'required',
			'cuil' => 'required',
        ];
    }
}
