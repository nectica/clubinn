<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
		$user = auth()->user();

        return $user->hasPermissionTo('cliente-edit') ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
            'identificacion' => 'required|numeric',
			'nombre' => 'required|',
			'apellido' => 'required',
			'telefono' => 'required',
			'celular' => 'required',
			'email' => 'required|email',
			'nombre_conyugue' => 'required',
			'apellido_conyugue' => 'required',
			'celular_conyugue' => 'required',
			'email_conyugue' => 'required|email',
/* 			'hotel' => 'required|numeric', */
			'dni' => 'required',
			'dni_conyugue' => 'required',
			'fecha_nacimiento' => 'required',
			'fecha_naciemiento_conyugue' => 'required',
			'domicilio' => 'required',
			'localidad' => 'required',
			'provincia' => 'required',
			'pais' => 'required',
			'cod_postal' => 'required',
			'email2' => 'required|email',
			'id_rci' => 'required',
			'id_interval' => 'required',
			'telefono_laboral' => 'required',
			'cuil' => 'required',
        ];
    }
}
