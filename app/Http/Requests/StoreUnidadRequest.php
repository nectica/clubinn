<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUnidadRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		$user = auth()->user();
		return $user->hasPermissionTo('unidad-create');
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nombre' => 'required', 'hotel' => 'required', 'capacidad' => 'required|lte:capacidad_maxima', 'capacidad_maxima' => 'required'
		];
	}
}
