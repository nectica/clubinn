<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClienteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$cuentasCorrientes = CuentaCorrienteResource::collection($this->full_cuenta_corrientes ) ;

		$saldo = $this->getSaldoCliente($cuentasCorrientes);
        return [
            'id' => $this->id,
            'email' => $this->email,
			'identificacion' => $this->identificacion, 'nombre' => $this->nombre,
			'apellido' => $this->apellido, 'telefono' => $this->telefono, 'celular' => $this->celular,
			'nombre_conyugue' => $this->nombre_conyugue, 'apellido_conyugue' => $this->apellido_conyugue,
			'celular_conyugue' => $this->celular_conyugue,
			'email_conyugue' => $this->email_conyugue, 'hotel' => $this->hotelR->nombre,
			'dni' => $this->dni, 'dni_conyugue' => $this->dni_conyugue,
			'fecha_nacimiento' => $this->fecha_nacimiento,
			'fecha_naciemiento_conyugue' => $this->fecha_naciemiento_conyugue,
			'domicilio' => $this->domicilio, 'localidad' => $this->localidad,
			'provincia' => $this->provincia, 'pais' => $this->pais, 'cod_postal' => $this->cod_postal,
			'email2' => $this->email2, 'id_rci' => $this->id_rci,
			'id_interval' => $this->id_interval, 'telefono_laboral'=> $this->telefono_laboral,
			'saldo' => $this->saldo,
			'cuentaCorriente' => $cuentasCorrientes/* CuentaCorrienteResource::collection($cuentasCorrientes )  */,
			'consultas' => ConsultaResource::collection($this->consultas) ,
			'recibos' => ReciboResource::collection( $this->full_recibos),
			'unidades' => UnidadResource::collection($this->unidades),
			'usos' => UsoAPiResource::collection($this->usos),
			'saldo_vencido' => $this->saldo_vencido,
			'calendario' => $this->when($this->hotelR->calendario, route('hotel.calendario', $this->hotel) ),

        ];
    }

	public function getSaldoCliente($cuentas){

		return  $cuentas->sum(function($cuenta){
			return $cuenta->importe * $cuenta->movimiento->signo;
		});
	}
}
