<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ConsultaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
			'id' => $this->id,
			'asunto' => $this->asunto,
			'consulta' => $this->consulta,
			'respuesta' => $this->respuesta,
			'estado' => $this->estado->nombre,
			'fecha' => $this->created_at->format('d/m/Y'),
			'cliente' => $this->cliente,
			'hotel' => $this->cliente->hotelR->nombre,
		];
    }
}
