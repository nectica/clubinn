<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SemanaTemporadaResource extends JsonResource
{

	protected $anio ;
	public function anio($anio){
		$anio = $anio;
	}
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		dd($this);
        return [
			'semana' =>$this->semana,
			'fechas' => $this->getStartDate

		];
    }
	public function getStartDate(){
		$date = Carbon::now();
		$date->setISODate($this->anio, $this->semana);

		return[ 'inicio' => $date->startOfWeek(Carbon::SATURDAY), 'fin' => $date->endOfWeek(Carbon::SATURDAY)];
	}
}
