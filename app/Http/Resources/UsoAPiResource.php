<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsoAPiResource extends JsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */


	public function toArray($request)
	{
		/* $temporada_semanas =   new SemanaTemporadaResource($this->temporada->semanas);
		$temporada_semanas->anio($this->anio); */
		return [
			'id' => $this->id,
			'dias' => $this->dias, 'dias_usado' => $this->dias_usado_f,
			'anio' => $this->anio, 'fecha_vencimiento' => $this->fecha_vencimiento,
			'fecha_uso' => $this->fecha_uso, 'semana' => $this->semana,
			'unidad' => $this->unidad->unidadR ? $this->unidad->unidadR->nombre : $this->unidad->nombre,
			'temporada' => $this->temporada->nombre,
			'temporada_fraccionada' => $this->temporada->fraccionado,
			'estado' => $this->estado->nombre,
			'temporada_semanas' => $this->temporada->semanas
		];
	}
}
