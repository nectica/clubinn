<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ReciboResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{

		return [
			'fecha_pago' => (new Carbon( $this->fecha_pago))->format('d/m/Y'),
			'forma_pago' => $this->forma_pago,
			'importe' => $this->importe,
			'estado_id' => $this->estado->nombre,
			'observaciones' => $this->observaciones,
			'voucher' => $this->voucher,
		];
	}
}
