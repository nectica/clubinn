<?php

namespace App\Http\Resources;

use App\Models\Temporada;
use Illuminate\Http\Resources\Json\JsonResource;

class UnidadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
			'nombre' => $this->nombre,
			'hotel' => $this->hotelR->nombre,
			'temporada' => Temporada::find($this->pivot->temporada),
		];
    }
}
