<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CbuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'banco' => $this->banco,
			'titular' => $this->titular,
			'tipo_cuenta' => $this->tipo_cuenta,
			'nro_cuenta' => $this->nro_cuenta,
			'cbu' => $this->cbu,
			'cuil' => $this->cuil,
		];
    }
}
