<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CuentaCorrienteResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		$full =  $this->attributesToArray();
		$full['importe'] *= $this->movimiento->signo;
		$full['movimiento'] = $this->getObservacion($this->movimiento, $this);
		$full['movimiento_abreviatura'] = $this->movimiento->abreviatura;
		$full['fecha'] =  $this->created_at->format('d/m/Y');
		$full['fecha_vencimiento'] =  $this->fecha_vencimiento ? (new Carbon($this->fecha_vencimiento))->format('d/m/Y') : '';
		if ($this->recibo_id ) {
			$full['factura_recibo'] = route('recibos.pdf.export', $this->recibo_id);
		}
		if ($this->recibo_id && $this->recibo->voucher) {
			$full['voucher'] = route('recibos.pdf', $this->recibo_id);
		}
		unset($full['created_at']);
		unset($full['updated_at']);
		return $full;
	}
	/**
	 * Get additional data that should be returned with the resource array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function with($request)
	{

		dd($request);
		return [
			'meta' => [
				'key' => 'value',
			],
		];
	}
	public function getObservacion($movimiento, $cc)
	{
		$observacion = $movimiento->abreviatura;
		if (strtolower($movimiento->nombre)  != 'recibo' && strtolower($movimiento->nombre) != 'Expensas') {
			$observacion .= ' ' . $cc->observacion;
		} else if ($cc->expensa) {
			$observacion .= ' ' . $cc->expensa->anio . ' ' . $cc->expensa->temporada->nombre;
		} else if ($cc->recibo) {
			$observacion .= ' N° ' . $cc->recibo->nro . ',' . $cc->recibo->formaPago->nombre;
		}
		return $observacion;
	}
}
