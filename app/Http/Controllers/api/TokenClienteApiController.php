<?php

namespace App\Http\Controllers\api;

use App\Events\ClienteResetPasswordEvent;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\TokenCliente;
use App\Notifications\ClientePasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TokenClienteApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function recoveryPassword(Request $request){
		$cliente = Cliente::where('email',$request->email )->first();
		if (!$cliente) {
			return response()->json(['status' => 'error', 'msg' => 'Cliente no existe'], 403);
		}
		$token = TokenCliente::create(['token'=> $this->getToken(), 'cliente_id' => $cliente->id]);
		ClienteResetPasswordEvent::dispatch($cliente);
		return response()->json(['status' => 'ok', 'msg' => 'Se ha un correo electronico a '. $cliente->email.' para resetear la contraseña.']);
	}

	public function getToken(){

		return Str::random(40);
	}
}
