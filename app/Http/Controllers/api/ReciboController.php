<?php

namespace App\Http\Controllers\api;

use App\Events\ReciboStored;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReciboApiRequest;
use App\Http\Resources\ReciboResource;
use App\Jobs\ReciboStoredJob;
use App\Models\Cliente;
use App\Models\CuentaCorriente;
use App\Models\Estado;
use App\Models\FormaPago;
use App\Models\Movimiento;
use App\Models\Recibo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReciboController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$recibos = Cliente::where('email', $request->server('PHP_AUTH_USER'))->recibos()->paginate(10);
		return ReciboResource::collection($recibos)->response();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreReciboApiRequest $request)
	{
		$cliente = Cliente::where('email', $request->server('PHP_AUTH_USER'))->first();
		$cliente_id = $cliente->id;
		$estado = Estado::where('nombre', 'Pendiente')->first();
		$estado_id = $estado->id;

		$forma_pago_id = FormaPago::where('nombre', 'like', '%transferencia%')->first()->id;
		$voucher = $this->storeFile($request, $cliente);
		$recibo_array = array_merge($request->except('voucher'), compact('cliente_id', 'voucher', 'estado_id', 'forma_pago_id'));
		$recibo = Recibo::Create($recibo_array);
		/* $this->insertCuentaCorriente($recibo); */

		ReciboStoredJob::dispatch($recibo);

		return response()->json(['status' => 'ok', 'data' => new ReciboResource($recibo)]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	public function getReciboPdf(Recibo $recibo)
	{

		try {
			return response()->json(base64_encode( Storage::get('voucher/'. $recibo->voucher) )  );
		} catch (\Exception $th) {
			dd($th);
		}
	}
	public function getPosiblesImputaciones(Request $request, Recibo $recibo){

		return response()->json(['status' => 'ok', 'data' =>  $recibo->posible_imputacion]);
		/* return response()->json(['status' => 'ok', 'data' =>  [
			[
				"id" => 247,
				"fecha_vencimiento" => "2021-10-31 00:00:00",
				"comprobante" =>"EXP",
				"anio" => 2022,
				"temporada" =>"Media Flotante",
				"importe" => "22600.00",
				"saldo" => "22600.00"
			],
			[
				"id" => 1108,
				"fecha_vencimiento" => "2021-12-03 00:00:00",
				"comprobante" =>"Exp.Ex.Parc",
				"anio" => null,
				"temporada" => null,
				"importe" => "43068.00",
				"saldo" => "43068.00"
				]
		]]); */
	}


	/* helper */
	public function storeFile($request, $cliente)
	{
		$originalName = pathinfo($request->file('voucher')->getClientOriginalName(), PATHINFO_FILENAME);
		$extension =  $request->file('voucher')->getClientOriginalExtension();
		$now = Carbon::now();
		$voucher = str_replace(' ', '_', $cliente->full_name) . '_' . $now->format('Y_m_d_h_H_i_s') . '.' . $extension;
		$request->file('voucher')->storeAs('voucher', $voucher);
		return $voucher;
	}

	public function insertCuentaCorriente($recibo)
	{
		$movimiento = Movimiento::where('nombre', 'Recibo')->first();
		$cuenta_array = [
			'movimiento_id' => $movimiento->id,
			'recibo_id' => $recibo->id,
			'importe' =>  $recibo->importe,
			'cliente_id' => $recibo->cliente_id
		];
		return CuentaCorriente::create($cuenta_array);
	}
}
