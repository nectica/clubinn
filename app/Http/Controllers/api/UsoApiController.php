<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UsoAPiResource;
use App\Jobs\UsoRequestJob;
use App\Models\EstadoSemanaUso;
use App\Models\FechaSemanaUso;
use App\Models\SemanaUso;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UsoApiController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$usos = SemanaUso::whereHas('cliente', function($query)use($request){
			return $query->where('email', $request->server('PHP_AUTH_USER'));
		})->orderBy('created_at', 'desc')->paginate(10);

		return UsoAPiResource::collection($usos)->response();

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, SemanaUso $uso)
	{

		if ($uso->estado->nombre == 'Pendiente' || ($uso->dias_usado < 7 && $uso->temporada->fraccionado == 1) ) {
			$dias =  7;

			$nombreEstado = $request->estado ?: 'Solicitado';

			if ($nombreEstado == 'Depositado' || ($this->checkWeek($uso->temporada->semanas, $request->semana) && $this->checkDate($uso->anio, $request->semana, $request->fecha_uso)) ) {
				$estado = EstadoSemanaUso::where('nombre', $nombreEstado)->first();
				$estado_semana_uso_id = $estado->id;
				$uso_array = $request->all();
				// sum days on SemanaUso
				$dias_usado = $uso->dias_usado +$request->dias_usado;
				$uso->update( array_merge($uso_array,compact('dias', 'estado_semana_uso_id')) );
				FechaSemanaUso::create( array_merge($uso_array, ['semana_uso_id' => $uso->id]));
				UsoRequestJob::dispatch($uso);
			} else {
				return response()->json(['status' => 'error', 'msg' => 'Error!!. fecha no puede ser seleccionada']);
			}
		} else {
			return response()->json(['status' => 'error', 'msg' => 'Ya ha sido solicitado este Uso. espere por respuesta.'], 423);
		}
		return response()->json(['status' => 'ok', 'data' => new UsoAPiResource($uso)]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	/*  helper */
	public function checkDate($anio, $semana, $fecha)
	{
		$fechaCarbon =new Carbon($fecha);
		$start = Carbon::now();
		$start->setISODate($anio, $semana);
		$end = Carbon::now();
		$end->setISODate($anio, $semana);
		$start_date = $start->startOfWeek(Carbon::SATURDAY);
		$end_date = $end->startOfWeek(Carbon::SATURDAY)->addDays(6);
		return $fechaCarbon >= $start_date && $fechaCarbon <= $end_date;
	}
	public function checkWeek($semanas, $semanaNro)
	{

		return $semanas->contains(function ($semana, $key) use ($semanaNro) {
			return $semana->semana == $semanaNro;
		});
	}
}
