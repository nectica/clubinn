<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateClienteAPiRequest;
use App\Http\Resources\ClienteResource;
use App\Models\Cliente;
use App\Models\ClienteTable;
use Illuminate\Http\Request;

class ClienteApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$cliente =  Cliente::where('email',$request->server('PHP_AUTH_USER'))->with('hotelR')->with('unidades')->first();
		/* dd($cliente->clienteMultiple, $cliente->full_cuenta_corrientes); */
		return response()->json(new ClienteResource($cliente) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClienteAPiRequest $request, ClienteTable $cliente)
    {
		$cliente->update($request->all());
        return response()->json(['status' => 'ok','data' => $cliente]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	//login
	public function showByHotel($hotelId){

		$clientes = Cliente::where('hotel', $hotelId)->get();
		return response()->json(['status' => 'ok','data' => $clientes]);
	}
	public function getCuentasCorrientesByDate(Request $cliente){

	}


}
