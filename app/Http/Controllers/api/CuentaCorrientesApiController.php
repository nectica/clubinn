<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\GetCuentasCorrientesApiRequest;
use App\Http\Resources\CuentaCorrienteResource;
use App\Models\Cliente;
use App\Models\CuentaCorriente;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CuentaCorrientesApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$cuentaCorrientes = CuentaCorriente::whereHas('cliente', function($query)use($request){
			return $query->where('email', $request->server('PHP_AUTH_USER'));
		})/* ->whereDate('created_at', '>=',$request->fecha) */->orderBy('created_at', 'desc')->paginate(2);

		return  CuentaCorrienteResource::collection($cuentaCorrientes)->response() ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	public function getCuentasCorrientesByDate(GetCuentasCorrientesApiRequest $request){
		$cliente = Cliente::where('email',$request->server('PHP_AUTH_USER'))->first();
		$cuentaCorrientes = $cliente->full_cuenta_corrientes/* ->whereBetween('created_at', [$request->desde, $request->hasta])->get() */;
		$cuentaCorrientes = $cuentaCorrientes->filter( function($cuenta, $key)use($request){

			return $cuenta->created_at >= $request->desde && $cuenta->created_at <= $request->hasta;

		});
		return response()->json(['status' => 'ok', 'data' => CuentaCorrienteResource::collection($cuentaCorrientes) ]);

	}
}
