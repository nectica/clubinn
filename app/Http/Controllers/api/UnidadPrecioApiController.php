<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PrecioTemporadaResource;
use App\Models\PrecioTemporadaAnio;
use Illuminate\Http\Request;

class UnidadPrecioApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function getPrecioUnidadByAnio(Request $request, $temporadaId ,  $unidadId, $anio){
		$precio = PrecioTemporadaAnio::where('temporada_id', $temporadaId)
		->where( 'anio', $anio )
		->where( 'anio', $anio )
		->where( 'unidad_id', $unidadId)
		->first();
		$data = null;
		if ($precio) {
			$data = new PrecioTemporadaResource($precio);

		}

		return response()->json(['status' => 'ok', 'data' => $data]);
	}
}
