<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\SemanaUso;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SemanaUsoApiController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, SemanaUso $uso)
	{



	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getDatesByWeek(SemanaUso $uso)
	{
		$semanas =  $uso->temporada->semanas;
		$anio = $uso->anio;
		$semanas_fechas = [];
		if($uso->semana ){
			$start = Carbon::now();
			$start->setISODate($anio, $uso->semana);
			$end = Carbon::now();
			$end->setISODate($anio, $uso->semana);
			array_push($semanas_fechas, [
				'semana' => $uso->semana,
				'inicio' => $start->startOfWeek(Carbon::SATURDAY),
				'fin' => $end->startOfWeek(Carbon::SATURDAY)->addDays(5)
			]);
		}else{

			foreach ($semanas as $semana) {
				$start = Carbon::now();
				$start->setISODate($anio, $semana->semana);
				$end = Carbon::now();;
				$end->setISODate($anio, $semana->semana);
				array_push($semanas_fechas, [
					'semana' => $semana->semana,
					'inicio' => $start->startOfWeek(Carbon::SATURDAY),
					'fin' => $end->startOfWeek(Carbon::SATURDAY)->addDays(6)
				]);

			}
		}
		return response()->json(['status' => 'ok', 'data' => $semanas_fechas]);
	}
}
