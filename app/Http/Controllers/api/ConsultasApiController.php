<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreConsultaRequest;
use App\Http\Requests\UpdateConsultaRequest;
use App\Http\Resources\ConsultaResource;
use App\Jobs\ConsultaResponseJob;
use App\Jobs\ConsultaStoreJob;
use App\Models\Cliente;
use App\Models\Consulta;
use App\Models\EstadoConsulta;
use Illuminate\Http\Request;

class ConsultasApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $consultas = Cliente::where('email', $request->server('PHP_AUTH_USER') )->first()->consultas()->paginate(10);
		return ConsultaResource::collection($consultas)->response();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreConsultaRequest $request)
    {
		$cliente = Cliente::where('email',$request->server('PHP_AUTH_USER') )->first();
		$estado = EstadoConsulta::where('nombre', 'pendiente')->first();
		$consulta = Consulta::create(array_merge($request->all(), ['cliente_id' => $cliente->id, 'estado_consulta_id' => $estado->id ]));


		ConsultaStoreJob::dispatch($consulta);
        return response()->json(['status' => 'ok', 'data' => $consulta]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Consulta $consulta)
    {

		return response()->json(['status' => 'ok','data' => new ConsultaResource($consulta) ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateConsultaRequest $request,Consulta $consulta)
    {
		$estado =  EstadoConsulta::where('nombre','Respondido')->first();
		$arrayTomerge = ['estado_consulta_id' => $estado->id];
		$consulta->update(array_merge($request->all(), $arrayTomerge));
		ConsultaResponseJob::dispatch($consulta);
		return response()->json(['status' =>  $consulta]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
