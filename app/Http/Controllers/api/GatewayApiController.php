<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\helpers\GatewayHelper;
use App\Http\Requests\StoreGatewayRequest;
use Illuminate\Http\Request;

class GatewayApiController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$gatewayHelper = new GatewayHelper();
		$gatewayHelper->startDecidir();
		$gatewayHelper->setCliente($request->server('PHP_AUTH_USER'));
		$gatewayHelper->createRecibo(544);
		dd($request->all(), $gatewayHelper);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreGatewayRequest $request)

	{
		$response = json_decode($request->response);
		$gatewayHelper = new GatewayHelper();
		$gatewayHelper->startDecidir();
		$gatewayHelper->setCliente($request->server('PHP_AUTH_USER'));
		$gatewayHelper->setResponseFrontend($response);
		$gatewayHelper->createRecibo(544);
		$gatewayHelper->decidirExecutePayments();

		if ($gatewayHelper->error) {
			return response()->json(['status' => 'error', 'msg' => 'Error!! la plataforma ha rechado el pago.'], 422);
		}
		dd($gatewayHelper->error, $request->server('PHP_AUTH_USER'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{		$gatewayHelper = new GatewayHelper();

		dd($gatewayHelper->getCardType($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	public function decidirPayment(StoreGatewayRequest $request)
	{
		$response = json_decode($request->response);
		$requestFrontend = json_decode($request->requestApi);

		$gatewayHelper = new GatewayHelper();
		$gatewayHelper->startDecidir();
		$gatewayHelper->setCliente($request->server('PHP_AUTH_USER'));
		$gatewayHelper->setResponseFrontend($response);
		$gatewayHelper->setRequestFrontendApi($requestFrontend);

		$gatewayHelper->createRecibo($requestFrontend->interes + $requestFrontend->amount);
		$gatewayHelper->decidirExecutePayments();
		$status = 'success';
		$msg = 'Hemos registrado su pago con exito.';
		$statusCode = 200;
		if ($gatewayHelper->error) {
			$status = 'error';
			$msg = $gatewayHelper->msgError;
			$statusCode = $gatewayHelper->statuCodeError?:500;
		}

		return response()->json(compact('status', 'msg'), $statusCode);
	}

	public function mercadoPagoPayment(StoreGatewayRequest $request)
	{
		$response = json_decode($request->response);
		$requestFrontend = json_decode($request->requestApi);
		$gatewayHelper = new GatewayHelper();
		$gatewayHelper->startMercadoPago();
		$gatewayHelper->setCliente($request->server('PHP_AUTH_USER'));
		$gatewayHelper->setResponseFrontend($response);
		$gatewayHelper->setRequestFrontendApi($requestFrontend);
		$gatewayHelper->createRecibo($requestFrontend->transactionAmount ?: 10);
		$gatewayHelper->mercadoPagoExecutePayment();
		$status = 'success';
		$msg = 'Hemos registrado su pago con exito.';
		$statusCode = 200;
		if ($gatewayHelper->error) {
			$status = 'error';
			$msg = $gatewayHelper->msgError;
			$statusCode = $gatewayHelper->statuCodeError;
		}

		return response()->json(compact('status', 'msg'), $statusCode);
	}
}
