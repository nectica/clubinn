<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Temporada;
use Illuminate\Http\Request;

class TemporadaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function showByHotel($idHotel){
		$temporadas = Temporada::where('hotel', $idHotel)->orderBy('nombre', 'asc')->get();

		return response()->json(['status' => 'ok', 'data' => $temporadas]);
	}
	public function showBycliente($idCliente){
		$temporadas = Temporada::WhereHas('unidadPorCliente', function($query)use($idCliente){
			return $query->where('cliente', $idCliente);
		})->get();
		return response()->json(['status' => 'ok', 'data' => $temporadas]);
	}
}
