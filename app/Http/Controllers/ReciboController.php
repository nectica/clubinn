<?php

namespace App\Http\Controllers;

use App\Events\ReciboRejectedEvent;
use App\Exports\ReciboExport;
use App\Http\Requests\StoreReciboRequest;
use App\Http\Requests\UpdateReciboRequest;
use App\Jobs\ReciboConfirmClienteJob;
use App\Jobs\ReciboRejectedJob;
use App\Models\Cbu;
use App\Models\Cliente;
use App\Models\CuentaCorriente;
use App\Models\Estado;
use App\Models\FormaPago;
use App\Models\Hotel;
use App\Models\Movimiento;
use App\Models\Recibo;
use App\Models\TarjetaCredito;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class ReciboController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$filterNombre = $request->cliente;
		$estados = Estado::orderBy('nombre', 'asc')->get();
		$estadoToFilter = $request->filter ?:  Estado::where('nombre', 'Pendiente')->first()->id ;
		$estadoAceptado = Estado::where('nombre', 'Aceptado')->first();
		Session::forget('recibo_estado');
		Session::put('recibo_estado', $estadoToFilter);
		$desde = $request->desde ? Carbon::parse($request->desde) :  Carbon::now()->subMonth();
		$hasta = $request->hasta ? Carbon::parse($request->hasta) : Carbon::now();
		$hasta->setTime(23, 59, 59, 59);
		$recibos = Recibo::sortable()->when($estadoToFilter != -1, function ($query) use ($estadoToFilter) {
			return $query->whereHas('estado', function ($query) use ($estadoToFilter) {
				return $query->where('id', $estadoToFilter);
			});
		})	->when($filterNombre, function ($query) use ($filterNombre) {

			return $query->orWhereHas('cliente', function ($query) use ($filterNombre) {
				return $query->where('nombre', 'like', '%' . $filterNombre . '%')
					->orWhere('identificacion',  $filterNombre )
					->orWhere('apellido', 'like', '%' . $filterNombre . '%')
					->orWhere('email', 'like', '%' . $filterNombre . '%')->orWhereHas('hotelR', function ($query) use ($filterNombre) {
						return $query->where('nombre', 'like', '%' . $filterNombre . '%');
					});

			});

		})->where('fecha_pago', '>=', $desde)->where('fecha_pago', '<=', $hasta)->orderBy('fecha_pago', 'desc')->paginate(10);

		return view('recibo.index', compact('recibos', 'estados', 'estadoToFilter', 'estadoAceptado', 'desde', 'hasta', 'filterNombre'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		/* $movimientos = Movimiento::where('nombre','like', '%Ajuste%')->orderBy('nombre', 'asc')->get(); */
		$movimientos = Movimiento::whereNotIn('nombre', ['recibo', 'Expensas'])->get();

		$recibo = Movimiento::where('nombre', 'like', '%Recibo%')->first();
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		$now = Carbon::now();
		$formasPagos = FormaPago::orderBy('nombre', 'asc')->get();
		$tcs = TarjetaCredito::orderBy('nombre', 'asc')->get();
		$cbus = Cbu::orderBy('banco', 'asc')->get();

		return view('recibo.store', compact('movimientos', 'hotels', 'now', 'recibo', 'formasPagos', 'tcs', 'cbus'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreReciboRequest $request)
	{
		$estado = Estado::where('nombre', 'Pendiente')->first();
		$estado_id = $estado->id;

		$recibo_array = $request->all();
		$recibo_array = array_merge($recibo_array, compact('estado_id'));
		$reciboMoviento = Movimiento::where('nombre', 'recibo')->first();
		$recibo = new recibo();
		if ($request->movimiento_id == $reciboMoviento->id) {
			if ($request->voucher) {
				$cliente = Cliente::find($request->cliente_id);
				$recibo_array['voucher'] = $this->storeFile($request, $cliente);
			}
			$recibo = Recibo::create($recibo_array);
		} else {
			$recibo = new Recibo($recibo_array);
			$this->createCuentaCorriente($recibo, $request, $request->movimiento_id);
		}


		return redirect()->route('recibos.index')->with('success', 'Se cargo el recibo(pago) correctamente.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Recibo $recibo)
	{
		$now = Carbon::now();
		$cliente = $recibo->cliente;
		return view('recibo.recibo-pdf', compact('recibo', 'now', 'cliente'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Recibo $recibo)
	{
		$estados = Estado::orderBy('nombre', 'asc')->get();
		Session::put('previous.recibo.edit', Session::get('_previous')['url'] );

		$formasPagos = FormaPago::orderBy('nombre', 'asc')->get();
		$tcs = TarjetaCredito::orderBy('nombre', 'asc')->get();
		$cbus = Cbu::orderBy('banco', 'asc')->get();
		return view('recibo.update', compact('recibo', 'estados', 'formasPagos', 'tcs' , 'cbus'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateReciboRequest $request, Recibo $recibo)
	{
		$imputaciones = json_decode($request->imputaciones) ;

		$estado = Estado::where('nombre', 'Aceptado')->first();
		$recibo->update($request->all());
		if ($recibo->estado_id == $estado->id && $recibo->wasChanged('estado_id')) {
			$movimiento = Movimiento::where('nombre', 'Recibo')->first();
			$cuentaCorriente = $this->createCuentaCorriente($recibo, $request, $movimiento->id);
			foreach($imputaciones as $imputar_id => $importe){
				DB::select("CALL imputacion($imputar_id, $cuentaCorriente->id, $importe)");
			}
			if ($request->notificacion) {
				ReciboConfirmClienteJob::dispatch($recibo);
			}
		} else if ($recibo->estado->nombre == 'Anulado' && $recibo->wasChanged('estado_id') && $request->notificacion) {
			ReciboRejectedJob::dispatch($recibo);
		}
		if ($request->hasFile('voucher')) {
			Storage::delete('voucher/' . $recibo->voucher);
			$recibo_array['voucher'] = $this->storeFile($request, $recibo->cliente);
			$recibo->update($recibo_array);
		}

		$route = Session::get('previous.recibo.edit') ?: URL::previous();
		return redirect($route)->with('success', 'Se actualizo el recibo correctamente.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	public function getReciboExport(Request $request)
	{

		$estadoToFilter = Session::get('recibo_estado') != -1 ? Session::get('recibo_estado') : 'null';
		$desde = $request->desde;
		$hasta =  $request->hasta ;
		$recibos = DB::select("CALL recibos_por_periodo('$desde', '$hasta', $estadoToFilter, 1, null )");
		/* $recibos = Recibo::when($estadoToFilter != -1, function ($query) use ($estadoToFilter) {
			return $query->whereHas('estado', function ($query) use ($estadoToFilter) {
				return $query->where('id', $estadoToFilter);
			});
		})->when($request->desde, function ($query) use ($request) {
			return $query->whereBetween('fecha_pago', [$request->desde, $request->hasta ?: Carbon::now()]);
		})->orderBy('created_at', 'desc')->get(); */
		return Excel::download(new ReciboExport($recibos), Carbon::now()->format('Y_m_d_s_') . 'recibos.xlsx');
	}
	public function getPdfExportRecibo(Recibo $recibo){
		$cliente = $recibo->cliente;
		$now = $recibo->fecha_pago;
		$pdf = PDF::loadView('recibo.recibo-pdf', compact('cliente', 'recibo', 'now'));
		return $pdf->download($cliente->nombre.$now->format('Y_m_d').'.pdf');

	}
	/* helper */
	public function getReciboPdf(Recibo $recibo)
	{
		try {
			return  Storage::download('voucher/' . $recibo->voucher);
		} catch (\Exception $th) {
			dd($th);
		}
	}
	public function createCuentaCorriente($recibo, $request, $movimiento_id)
	{
		return CuentaCorriente::create([
			'movimiento_id' => $movimiento_id,
			'recibo_id' => $recibo->id,
			'importe' =>  $recibo->importe,
			'cliente_id' => $recibo->cliente_id,
			'fecha_vencimiento' => $recibo->fecha_pago,
			'fecha' => $recibo->fecha_pago,
			'anio' => $recibo->fecha_pago->format('Y'),
			'observacion' => $recibo->observaciones
		]);
	}
	public function storeFile($request, $cliente)
	{
		$originalName = pathinfo($request->file('voucher')->getClientOriginalName(), PATHINFO_FILENAME);
		$extension =  $request->file('voucher')->getClientOriginalExtension();
		$now = Carbon::now();
		$voucher = str_replace(' ', '_', $cliente->full_name) . '_' . $now->format('Y_m_d_h_H_i_s') . '.' . $extension;
		$request->file('voucher')->storeAs('voucher', $voucher);
		return $voucher;
	}
}
