<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCbuRequest;
use App\Models\Cbu;
use Illuminate\Http\Request;

class CbuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$filter = $request->filter;
        $cbus  = Cbu::sortable()->orderBy('banco', 'asc')
			->when($filter , function ($query) use($filter ){
				return $query->where('banco', 'like', '%'.$filter  . '%')
					->orWhere('cbu', 'like', '%'.$filter . '%')
					->orWhere('nro_cuenta', 'like', '%'.$filter  . '%')
					->orWhere('tipo_cuenta', 'like', '%'.$filter  . '%')
					->orWhere('titular', 'like', '%'.$filter  . '%')
					;
			})
			->get();


		return view('cbu.index', compact('cbus', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$tipo_cuentas = [ ['nombre' => 'cuenta corriente']];
        return view('cbu.store', compact('tipo_cuentas') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCbuRequest $request)
    {
       $cbu = Cbu::create($request->all());

	   return redirect()->route('cbu.index')->with('success','Se ha creado la cbu '.$cbu->cbu.'.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Cbu $cbu)
    {
		$tipo_cuentas = [ ['nombre' => 'cuenta corriente']];

       return view('cbu.update', compact('cbu', 'tipo_cuentas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCbuRequest $request, Cbu $cbu)
    {
        $cbu->update($request->all());
		return redirect()->route('cbu.index')->with('success','Se ha actualizado la cbu '.$cbu->cbu.'.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cbu $cbu)
    {
        $cbu->delete();
		return redirect()->route('cbu.index')->with('success','Se ha borrado la cbu '.$cbu->cbu.'.');

    }
}
