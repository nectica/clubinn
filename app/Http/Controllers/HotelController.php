<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHotelRequest;
use App\Http\Requests\UpdateHotelRequest;
use App\Models\Hotel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::sortable()->orderBy('nombre', 'asc')->get();
		return view('hotel.index', compact('hotels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('hotel.store');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHotelRequest $request)
    {
        $hotel = Hotel::create($request->all());
		$msg = 'Hubo un error con la creacion, por favor intente nuevamente o contacte directamente al administrador.';
		$type = 'danger';
		if ( $hotel) {
			$msg = 'Se ha creado correctamente el hotel '.$hotel->nombre.' .';
			$type = 'success';
		}
		return redirect()->route('hotel.index')->with($type, $msg);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
		return view('hotel.update', compact('hotel'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHotelRequest $request, Hotel $hotel)
    {
		$hotel;
		if($request->hasFile('calendario')){
			Storage::delete('calendario/' . $hotel->calendario);
			$cliente_array['calendario'] = $this->storeFile($request, $hotel);

		}
		if ($hotel->update($cliente_array)) {
			$msg = 'Se ha actualizado correctamente el usuario ' . $hotel->nombre . ' .';
			$type = 'success';
		}
		return redirect()->route('hotel.index')->with($type, $msg);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();
    }

	public function getCalendario(Hotel $hotel){

		return Storage::download('calendario/' . $hotel->calendario);
	}
	public function storeFile($request, $hotel)
	{
		$extension =  $request->file('calendario')->getClientOriginalExtension();
		$now = Carbon::now();
		$voucher = str_replace(' ', '_', $hotel->nombre) . '_' . $now->format('Y_m_d_h_H_i_s') . '.' . $extension;
		$request->file('calendario')->storeAs('calendario', $voucher);
		return $voucher;
	}
}
