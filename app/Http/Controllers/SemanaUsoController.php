<?php

namespace App\Http\Controllers;

use App\Events\UsoConfirmEvent;
use App\Events\UsoRejected;
use App\Exports\UsosExport;
use App\Http\Requests\StoreUsoRequest;
use App\Jobs\SemanaUsoConfirmJob;
use App\Models\Cliente;
use App\Models\EstadoSemanaUso;
use App\Models\Hotel;
use App\Models\SemanaUso;
use App\Notifications\UsoRejectedCliente;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class SemanaUsoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$estadoToFilter = 'Solicitado';
		if ($request->filter) {
			$estadoToFilter = $request->filter;
		}
		$filterBytext = $request->filterText;
		Session::forget('semanaUsos-filtered');
		$estados = EstadoSemanaUso::orderBy('nombre')->get();
		$SemanaUsos = SemanaUso::sortable()->when($estadoToFilter != -1, function($query) use ($estadoToFilter){
			$query->whereHas('estado', function ($query) use ($estadoToFilter) {
				return $query->where('nombre', $estadoToFilter);
			});
		})->when($filterBytext, function ($query) use ($filterBytext) {
			return $query->where(function($query)use ($filterBytext){
				return $query->whereHas('cliente', function ($query) use ($filterBytext) {
					return $query->where('nombre', 'like', '%' . $filterBytext . '%')
						->orWhere('apellido', 'like', '%' . $filterBytext . '%')
						->orWhere('email', 'like', '%' . $filterBytext. '%')
						->orWhere('identificacion',  $filterBytext );
					})->orWhereHas('temporada', function ($query) use ($filterBytext) {
						return $query->where('nombre', 'like', '%' . $filterBytext . '%');
					})->orWhere('anio', $filterBytext);
			});
		})
		->orderBy('anio', 'desc')->paginate(10);
		$estadoConfirmar = EstadoSemanaUso::where('nombre', 'Confirmado')->first();
		$estadoDepositado = EstadoSemanaUso::where('nombre', 'Depositado confirmado')->first();
		$estadoRechazado = EstadoSemanaUso::where('nombre', 'Rechazado')->first();
		Session::put('semanaUsos-filtered', $SemanaUsos);
		return view('semanaUso.index', compact('filterBytext', 'SemanaUsos', 'estados', 'estadoToFilter', 'estadoConfirmar', 'estadoDepositado', 'estadoRechazado'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$now = Carbon::now();
		$anio = $now->format('Y');
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		$estados = EstadoSemanaUso::orderBy('nombre', 'asc')->get();
		/* $clientes = Cliente::orderBy('nombre', 'asc')->get(); */
		return view('semanaUso.store', compact('anio', 'hotels', 'now', 'estados'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreUsoRequest $request)
	{
		$array_uso = $request->request->all();
		$array_uso['estado_semana_uso_id'] = EstadoSemanaUso::where('nombre', 'Pendiente')->first()->id;

		if ($request->semana) {
			$date = Carbon::now();
			$date->setISODate($request->anio, $request->semana);
			$array_uso["fecha_uso"] = $date->startOfWeek(Carbon::SATURDAY);
		}
		/* $semanaUso ; */
		$uso = SemanaUso::create($array_uso);

		return redirect()->route('usos.index')->with('success', 'Se ha cargado correctamente el uso a ' . $uso->cliente->full_name . '.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(SemanaUso $uso)
	{
		return view('semanaUso.show', compact('uso') );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( SemanaUso $uso)
	{
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		$estados = EstadoSemanaUso::orderBy('nombre', 'asc')->get();
		$anio = Carbon::now()->format('Y');

		return view('semanaUso.update', compact('anio', 'uso', 'hotels', 'estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, SemanaUso $uso)
	{
		$estado = EstadoSemanaUso::find($request->estado_semana_uso_id);
		$uso->update($request->all());
		if ($estado->nombre == 'Rechazado' && $uso->wasChanged('estado_semana_uso_id') && $request->notificacion) {
			UsoRejected::dispatch($uso);
		}else if ( ($estado->nombre == 'Confirmado' || $estado->nombre == 'Depositado confirmado') && $uso->wasChanged('estado_semana_uso_id') && $request->notificacion) {
			SemanaUsoConfirmJob::dispatch($uso);
			/* UsoConfirmEvent::dispatch($uso); */
		}
		if ($request->redirect) {
			return redirect()->route($request->redirect)->with('success', 'Se ha actualizado el uso.');
		}
		return redirect()-> back()->with('success', 'Se ha actualizado el uso a ' . $estado->nombre . ' .');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function exportToExcel(Request $request){
		$semanaUsos = Session::get('semanaUsos-filtered');
			if ($semanaUsos->isEmpty()) {
				return redirect()->back();
			}
		return Excel::download(new UsosExport($semanaUsos ), Carbon::now()->format('Y_m_d_s_'). 'solicitudes.xlsx');
	}
}
