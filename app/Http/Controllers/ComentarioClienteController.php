<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreComentariosRequest;
use App\Models\Cliente;
use App\Models\ComentarioCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ComentarioClienteController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Cliente $cliente)
	{
		return view('comentario.store', compact('cliente'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreComentariosRequest $request)
	{
		$comentario_array = $request->all();
		$comentario_array['user_id'] = Auth::user()->id;
		$comentario = ComentarioCliente::create($comentario_array);

		return redirect()->route('cliente.show', $comentario->cliente_id)->with('success', 'Se ha agregado correctamente el comentario');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(ComentarioCliente $comentario, Cliente $cliente)
	{
		Session::put('previos.edit', Session::get('_previous')['url']);
		return view('comentario.update', compact('cliente', 'comentario'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(StoreComentariosRequest $request, ComentarioCliente $comentario, Cliente $cliente)
	{
		$comentario->update($request->all());
		return redirect()->route('cliente.show', $cliente->id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(ComentarioCliente $comentario)
	{
		$comentario->delete();
		return redirect()->back();
	}
}
