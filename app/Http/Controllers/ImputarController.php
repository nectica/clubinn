<?php

namespace App\Http\Controllers;

use App\Http\Controllers\helpers\ImputarHelper;
use App\Models\Cliente;
use Illuminate\Http\Request;

class ImputarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	protected $imputador;
	public function __construct(){
		$this->imputador = new ImputarHelper();
	}
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	public function imputarTodo(){
		$this->imputador->imputarTodo();
		return redirect()->back()->with('success', 'Se ha imputado los pagos a todos los clientes.');
	}
	public function imputarAutomatico($clienteId){
		$this->imputador->imputarPagoAutomatico($clienteId);

		return redirect()->back()->with('success', 'Se ha imputado los pagos.');
	}
	public function imputarIndividual(Cliente $cliente){

		$debe = $cliente->cuentasCorrientes()->where('saldo', '>', 0)->whereHas('movimiento', function ($query){
			return $query->where('signo', 1);
		})->get();
		$haber = $cliente->cuentasCorrientes()->where('saldo', '>', 0)->whereHas('movimiento', function ($query){
			return $query->where('signo', -1);
		})->get();
		return view('cliente.imputacion', compact('debe', 'haber', 'cliente') );
	}

	public function desimputador($id){
		$this->imputador->desimputar($id);
		return redirect()->back()->with('success', 'Se ha imputado los pagos.');
	}
}
