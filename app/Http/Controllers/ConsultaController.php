<?php

namespace App\Http\Controllers;

use App\Models\Consulta;
use App\Models\EstadoConsulta;
use Illuminate\Http\Request;

class ConsultaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$estados = EstadoConsulta::orderBy('nombre', 'asc')->get();
		$estadoToFilter = $request->filter ?:'Pendiente';
		$filterBytext = $request->filterText;

        $consultas = Consulta::sortable()->when($estadoToFilter != 'all', function ($query)use($estadoToFilter){
			return $query ->whereHas('estado', function($query) use($estadoToFilter) {
				return $query->where('nombre', $estadoToFilter);
			});
		})->when($filterBytext, function ($query) use ($filterBytext) {
					return $query->whereHas('cliente', function ($query) use ($filterBytext) {
						return $query->where('nombre', 'like', '%' . $filterBytext . '%')
							->orWhere('apellido', 'like', '%' . $filterBytext . '%')
							->orWhere('email', 'like', '%' . $filterBytext. '%')
							->orWhere('identificacion',  $filterBytext );

		});
	})->orderBy('updated_at', 'desc')->paginate(10);

		return view('consulta.index', compact('consultas', 'estados', 'estadoToFilter', 'filterBytext'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
