<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUnidadRequest;
use App\Http\Requests\UpdateUnidadRequest;
use App\Imports\HdcClienteUnidad;
use App\Imports\HdsClienteUnidadImport;
use App\Imports\UnidadImport;
use App\Models\Hotel;
use App\Models\Unidad;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class UnidadController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		/* viejos */
		/* Excel::import(new HdcClienteUnidad ,Storage::path('/public/aux_unidad.xlsx')); */
		/* Excel::import(new HdsClienteUnidadImport ,Storage::path('/public/cliente-unidad-hds1.xlsx')); */
		/* Excel::import(new HdsClienteUnidadImport ,Storage::path('/public/cliente-unidad-hds2.xlsx')); */
		/* Excel::import(new HdcClienteUnidad ,Storage::path('/public/cliente-unidad-hdc.xlsx')); */

		/* nuevos */
		/* Excel::import(new HdcClienteUnidad ,Storage::path('/public/cliente-unidad-hdc-sept.xlsx')); */
		/* Excel::import(new HdsClienteUnidadImport ,Storage::path('/public/cliente-unidad-hds1-sept.xlsx')); */
		/* Excel::import(new HdsClienteUnidadImport ,Storage::path('/public/cliente-unidad-hds2-sept.xlsx')); */
		$unidades = Unidad::Sortable()->orderBy('nombre')->paginate(10);
		return view('unidad.index', compact('unidades'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		return view('unidad.store', compact('hotels'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreUnidadRequest $request)
	{
		$unidad = Unidad::create($request->all());
		$type = 'danger';
		$msg= 'Hubo un error con la creacion, por favor intente nuevamente o contacte directamente al administrador.';
		if ($unidad) {
			$type = 'success';
			$msg = 'Se ha creado correctamente la unidad '.$unidad->nombre.' .';
		}
		return redirect()->route('unidad.index')->with($type, $msg);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Unidad $unidad)
	{
		dd($unidad->toArray(), $unidad->json());
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Unidad $unidad)
	{
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		return view('unidad.update', compact('unidad', 'hotels'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateUnidadRequest $request, Unidad $unidad)
	{
		$created = $unidad->update($request->all());
		$type = 'danger';
		$msg= 'Hubo un error con la actualizacion, por favor intente nuevamente o contacte directamente al administrador.';
		if ($created) {
			$type = 'success';
			$msg = 'Se ha actualizado correctamente la unidad '.$unidad->nombre.' .';
		}
		return redirect()->route('unidad.index')->with($type, $msg);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Unidad $unidad)
	{
		//
	}
}
