<?php

namespace App\Http\Controllers;

use App\Exports\ClienteContratos;
use App\Exports\ClienteSaldoExport;
use App\Exports\ComposicionSaldoExport;
use App\Http\Requests\StoreClienteRequest;
use App\Http\Requests\UpdateClientePasswordRequest;
use App\Http\Requests\UpdateClienteRequest;
use App\Imports\ClienteImport;
use App\Models\Cliente;
use App\Models\ClienteTable;
use App\Models\Estado;
use App\Models\EstadoSemanaUso;
use App\Models\Hotel;
use App\Models\Temporada;
use App\Models\TokenCliente;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Stmt\TryCatch;

class ClienteController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		/* Excel::import(new ClienteImport ,Storage::path('/public/cliente-1-2.xlsx')); */
		/* Excel::import(new ClienteImport ,Storage::path('/public/CLIENTE-2-SEPT.xlsx')); */
		$filter = $request->filter;
		$clientes = Cliente::sortable()
			->when($filter, function ($query) use ($filter) {
				return $query->where('nombre', 'like', '%' . $filter . '%')
					->orWhere('identificacion',  $filter )
					->orWhere('apellido', 'like', '%' . $filter . '%')
					->orWhere('email', 'like', '%' . $filter . '%')->orWhereHas('hotelR', function ($query) use ($filter) {
						return $query->where('nombre', 'like', '%' . $filter . '%');
					});
			})
			->paginate(10);
		return view('cliente.index', compact('clientes', 'filter'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		return view('cliente.store', compact('hotels'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreClienteRequest $request)
	{
		$cliente = new ClienteTable($request->all());
		if ($request->hasFile('calendario')) {
			$cliente->calendario = $this->storeFile($request, $cliente);
		}
		$cliente->save();
		/* $cliente = ClienteTable::create($request->all()); */

		return redirect()->route('cliente.index')->with('success', 'Se ha creado correctamente');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Cliente $cliente)
	{
		$temporadas = Temporada::all();
		$estadoConfirmar = EstadoSemanaUso::where('nombre', 'Confirmado')->first();
		$estadoDepositado = EstadoSemanaUso::where('nombre', 'Depositado confirmado')->first();
		$estadoRechazado = EstadoSemanaUso::where('nombre', 'Rechazado')->first();
		$estadoAceptado = Estado::where('nombre', 'Aceptado')->first();

		return view('cliente.show', compact('cliente', 'temporadas',  'estadoConfirmar', 'estadoDepositado', 'estadoRechazado', 'estadoAceptado'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(ClienteTable $cliente)
	{
		Session::put('previos.edit', Session::get('_previous')['url'] );
		$hotels = Hotel::orderBy('nombre', 'asc')->get();

		return view('cliente.update', compact('cliente', 'hotels'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateClienteRequest $request, ClienteTable $cliente)
	{
		$msg = 'Hubo un error con la actualizacion, por favor intente nuevamente o contacte directamente al administrador.';
		$type = 'danger';
		$cliente_array = $request->all();

		if ($cliente->update($cliente_array)) {
			$msg = 'Se ha actualizado correctamente el usuario ' . $cliente->email . ' .';
			$type = 'success';
		}

		return redirect( Session::get('previos.edit')=== url()->previous() ?  route('cliente.index'):Session::get('previos.edit'))->with($type, $msg);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Cliente $cliente)
	{
		$cliente->delete();
		return redirect()->route('cliente.index')->with('success', 'Se ha borrado correctamente');
	}
	public function exportClientesExcel(Request $request){
		$filter = $request->filter;
		$clientes = Cliente::with(['unidades', 'unidades.temporadas'])->when($filter, function ($query) use ($filter) {
			return $query->where('nombre', 'like', '%' . $filter . '%')
				->orWhere('identificacion',  $filter )
				->orWhere('apellido', 'like', '%' . $filter . '%')
				->orWhere('email', 'like', '%' . $filter . '%')->orWhereHas('hotelR', function ($query) use ($filter) {
					return $query->where('nombre', 'like', '%' . $filter . '%');
				});
		})->get();
		return Excel::download(new ClienteContratos($clientes ), Carbon::now()->format('Y_m_d_s_') . 'cliente.xlsx');

	}
	public function getSaldoclientes(Request $request)
	{

		$clientes = Cliente::sortable()->where('saldo', '>', 0)->paginate(10);
		return view('cliente.index-saldo', compact('clientes'));
	}

	public function getExcelSaldoclientes()
	{

		return Excel::download(new ClienteSaldoExport, Carbon::now()->format('Y_m_d_s_') . 'saldos.xlsx');
	}
	public function getClienteComposicionSaldo($clienteId)
	{
		$clientes = $this->getComposicionSaldoStored($clienteId);
		if (count($clientes) == 0) {
			return redirect()->back()->with('danger', 'No tiene registro para la composicion de saldo.');
		}
		return Excel::download(new ComposicionSaldoExport($clientes), Carbon::now()->format('Y_m_d_s_') . 'saldos.xlsx');
	}
	public function getClienteComposicionSaldoTodo()
	{
		try {
			$clientes = $this->getComposicionSaldoStored(-1);
		} catch (\Exception $e) {
			dd($e);
		}
		if (count($clientes) == 0) {
			return redirect()->back()->with('danger', 'No tiene registro para la composicion de saldo.');
		}
		return Excel::download(new ComposicionSaldoExport($clientes), Carbon::now()->format('Y_m_d_s_') . 'saldos.xlsx');
	}


	public function resetPasswordForm(Request $request, $token)
	{
		$tokenCliente = TokenCliente::where('token', $token)->first();
		$this->validarToken($request->token);
		return view('cliente.reset-password', compact('tokenCliente'));
	}

	public function resetPassword(UpdateClientePasswordRequest $request, ClienteTable $cliente)
	{
		$this->validarToken($request->token);
		$cliente->update($request->all());
		return redirect()->route('cliente.reset.get', $request->token)->with('success', 'Se ha actualizado la contraseña');
	}

	public function  validarToken($token)
	{
		$tokenCliente = TokenCliente::where('token', $token)->first();
		$now = Carbon::now();
		if (!$tokenCliente || $now->diffInHours($tokenCliente->created_at) > 24) {
			abort(response('token invalido', 500));
		}
	}

	public function getComposicionSaldoStored($clienteId)
	{
		try {

			return  DB::select("call composicion_saldo($clienteId)");
		} catch (\Exception $e) {
			dd('excepcion', $e);
		}
	}
	public function getCalendario(ClienteTable $cliente){

		return Storage::download('calendario/' . $cliente->calendario);
	}
	public function storeFile($request, $cliente)
	{
		$extension =  $request->file('calendario')->getClientOriginalExtension();
		$now = Carbon::now();
		$voucher = str_replace(' ', '_', $cliente->nombre) . '_' . $now->format('Y_m_d_h_H_i_s') . '.' . $extension;
		$request->file('calendario')->storeAs('calendario', $voucher);
		return $voucher;
	}

}
