<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreParametroRequest;
use App\Http\Requests\StoreparametroInteresRequest;
use App\Http\Requests\UpdateParametroUpdate;
use App\Models\Parametro;
use Illuminate\Http\Request;
use PhpParser\Builder\Param;

class ParametroController extends Controller
{


	public function __construct()
	{
		$this->middleware('permission:parametro-list|parametro-create|parametro-edit|parametro-delete', ['only' => ['index', 'store', 'show', 'filter']]);
		$this->middleware('permission:parametro-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:parametro-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:parametro-delete', ['only' => ['destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$parametros = Parametro::orderBy('nombre', 'asc')->get();
		return view('parametros.index', compact('parametros'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('parametros.store');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreParametroRequest $request)
	{
		Parametro::create($request->all());
		return redirect()->route('parametros.index')->with('success', 'Se creo correctamente el parametro.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Parametro $parametro)
	{
		return view('parametros.update', compact('parametro'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateParametroUpdate $request, Parametro $parametro)
	{
		$parametro->update($request->all());
		return redirect()->route('parametros.index')
			->with('success', 'Se actualizo correctamente el parametro.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{

	}
}
