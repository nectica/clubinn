<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTemporadaRequest;
use App\Models\Hotel;
use App\Models\Temporada;
use Illuminate\Http\Request;

class TemporadaController extends Controller
{

	public function __construct(){
		$this->middleware('permission:temporada-list|temporada-create|temporada-edit|temporada-delete', ['only' => ['index','show']]);
		$this->middleware('permission:temporada-create', ['only' => ['create','store']]);
		$this->middleware('permission:temporada-edit', ['only' => ['edit','update']]);
		$this->middleware('permission:temporada-delete', ['only' => ['destroy']]);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$temporadas = Temporada::sortable()->orderBy('id', 'desc')->get();

		return view('temporada.index', compact('temporadas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$hotels = Hotel::orderBy('nombre', 'asc')->get();

		return view('temporada.store', compact('hotels') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTemporadaRequest $request)
    {
		$temporada = Temporada::create($request->all());
		$type = 'danger';
		$msg= 'Hubo un error con la creacion, por favor intente nuevamente o contacte directamente al administrador.';
		if ($temporada) {
			$type = 'success';
			$msg = 'Se ha creado correctamente la temporada '.$temporada->nombre.' .';
		}
		return redirect()->route('temporada.index')->with($type, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Temporada $temporada)
    {
		$hotels = Hotel::orderBy('nombre', 'asc')->get();

      return view('temporada.update', compact('temporada', 'hotels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreTemporadaRequest $request, Temporada $temporada)
    {
        $temporada->update($request->all());
		$type = 'danger';
		$msg= 'Hubo un error con la actualizacion, por favor intente nuevamente o contacte directamente al administrador.';
		if ($temporada) {
			$type = 'success';
			$msg = 'Se ha creado correctamente la temporada '.$temporada->nombre.' .';
		}
		return redirect()->route('temporada.index')->with($type, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
