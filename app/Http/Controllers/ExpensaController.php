<?php

namespace App\Http\Controllers;

use App\Models\CuentaCorriente;
use App\Models\EstadoSemanaUso;
use App\Models\Expensa;
use App\Models\Hotel;
use App\Models\Movimiento;
use App\Models\PrecioTemporadaAnio;
use App\Models\SemanaUso;
use App\Models\Temporada;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ExpensaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$expensas = Expensa::sortable()
			->when($request->filter, function ($query) use ($request) {
				return $query->whereHas('cliente', function ($query) use ($request) {
					return $query->where('nombre', 'like', '%' . $request->filter . '%')
						->orWhere('apellido', 'like', '%' . $request->filter . '%')
						->orWhere('email', 'like', '%' . $request->filter . '%')
						->orWhere('identificacion',  $request->filter);
				})->orWhereHas('temporada', function ($query) use ($request) {
					return $query->where('nombre', 'like', '%' . $request->filter . '%');
				})->orWhere('anio', $request->filter);
			})
			->orderBy('created_at', 'desc')
			->paginate(10);
		return view('expensa.index', compact('expensas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$hotels = Hotel::orderBy('nombre', 'ASC')->get();
		$seasons = Temporada::orderBy('nombre', 'asc')->get();
		$now = Carbon::now();
		$anio = $now->format('Y');
		return view('expensa.preview', compact('hotels', 'seasons', 'anio', 'now'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$anio = $request->anio;
		$temporada_id = $request->temporada_id;
		$fecha = $request->fecha;
		$clientesPrecios =  $request->except('anio', '_token', 'fecha');

		$keys =  array_keys($clientesPrecios);
		$clientes = preg_grep('/^cliente_/', $keys);
		$expensas = [];
		$movimiento = Movimiento::where('nombre', 'Expensas')->first();
		$estadoSemana = EstadoSemanaUso::where('nombre', 'Pendiente')->first();

		foreach ($clientes as $cliente) {
			$ids = explode('_',  $cliente);
			$cliente_id = $ids[1];
			$unidades_por_cliente_id = $ids[2];
			$unidad_pivot = $ids[3];

			$fecha_vencimiento =  $request['fecha_vencimiento_' . $cliente_id . '_' . $unidades_por_cliente_id . '_' . $unidad_pivot];
			$importe = $clientesPrecios[$cliente_id . '_' . $unidades_por_cliente_id . '_' . $unidad_pivot];
			$arrayExpensa =  compact('cliente_id', 'anio', 'temporada_id');
			$arrayExpensa['unidades_por_cliente_id'] = $unidad_pivot;
			$expensa = Expensa::updateOrCreate($arrayExpensa, ['importe' => $importe]);
			$cuenta = CuentaCorriente::updateOrCreate(
				[
					'movimiento_id' => $movimiento->id,
					'expensa_id' => $expensa->id,
					'cliente_id' => $cliente_id,
				],
				[
					'fecha' => $fecha,
					'fecha_vencimiento' => $fecha_vencimiento,
					'importe' =>  $expensa->importe,
				]
			);
			$this->createSemanaUso($expensa, $cuenta, $estadoSemana);
		}

		return redirect()->route('expensas.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Expensa $expensa)
	{/*
		$clientes = Cliente::where('hotel', $expensa->unidad->hotel)
			->whereHas('unidades', function ($query) use ($expensa) {
				return $query->where('unidades.id', $expensa->unidad->unidadR->id)->where('temporada', $expensa->temporada_id);
			})->get(); */

		return view('expensa.update', compact('expensa'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Expensa $expensa)
	{
		$expensa->update($request->all());
		$cuentaCorriente =  $expensa->cuentaCorriente;
		$cuentaCorriente->importe =  $expensa->importe;
		$cuentaCorriente->update();

		return redirect()->route('expensas.index')->with('success', 'Se ha actualizado correctamente la expensa del cliente ' . $expensa->cliente->full_name);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function createUniqueExpensa()
	{

		$now = Carbon::now();
		$anio = $now->format('Y');
		$hotels = Hotel::orderBy('nombre', 'asc')->get();
		$estados = EstadoSemanaUso::orderBy('nombre', 'asc')->get();
		return view('expensa.create-expensa', compact('now', 'anio', 'hotels', 'estados'));
	}

	public function createUniqueExpensaStore(Request $request)
	{
		$expensa = Expensa::create($request->all() );
		$cc_array =
		[
			'expensa_id' => $expensa->id,
			'cliente_id' => $request->cliente_id,
			'fecha' => $request->fecha_vencimiento,
			'fecha_vencimiento' => $request->fecha_vencimiento,
			'importe' =>  $expensa->importe,
		];
		$this->createCuentaCorrienteRecord($cc_array);
		return redirect()->route('expensas.index')->with('success', 'Se ha cargardo la expensa');
	}
	public function createCuentaCorrienteRecord($cc_array)
	{

		$movimiento = Movimiento::where('nombre', 'Expensas')->first();
		$cc_array['movimiento_id'] = $movimiento->id;
		return 	 CuentaCorriente::create(
			$cc_array
		);
	}

	public function createPricesToUnidad(Request $request)
	{
		$hotel_id  = $request->hotel_id;
		$temporada_id = $request->temporada_id;
		$anio = $request->anio;
		$precios = $request->except(['_token', 'hotel_id', 'temporada_id', 'anio', 'fecha', 'fecha_vencimiento']);
		$unidades = array_keys($precios);
		$arrayToInsert = [];
		foreach ($unidades as $key => $unidad_id) {

			$arrayToPrecioPorTemp = compact('unidad_id', 'temporada_id', 'anio');
			PrecioTemporadaAnio::updateOrCreate($arrayToPrecioPorTemp, ['precio' => $precios[$unidad_id]]);
		}

		return redirect()->route('expensas.clientes', ['temporada' => $temporada_id, 'anio' => $anio, 'fecha' => $request->fecha, 'fecha_vencimiento' => $request->fecha_vencimiento]);
	}
	public function chooseTempAndHotel(Request $request)
	{
		$this->validatorExpensasMethodGet($request);
		$hotel = Hotel::find($request->hotel_id);
		$temporada = Temporada::where('id', $request->temporada_id)->where('hotel', $request->hotel_id)->first();
		$anio = $request->anio;
		$fecha = $request->fecha;
		$fecha_vencimiento = $request->fecha_vencimiento;

		return view('expensa.store', compact('hotel', 'temporada', 'anio', 'fecha', 'fecha_vencimiento'));
	}
	/* helper */
	public function generateExpensas(Temporada $temporada, $anio, $fecha, $fecha_vencimiento)
	{
		$preciosUnidad = PrecioTemporadaAnio::where('temporada_id', $temporada->id)->where('anio', $anio)->get();
		return view('expensa.clientes', compact('temporada', 'preciosUnidad', 'anio', 'fecha', 'fecha_vencimiento'));
	}
	function validatorExpensasMethodGet($request)
	{
		if ($request->temporada === null) {
			throw ValidationException::withMessages(['temporada_id' => 'La temporada no coincide con el hotel.']);
		}
		if ($request->anio === null) {
			throw ValidationException::withMessages(['anio' => 'el año es requerido']);
		}
		if ($request->fecha === null) {
			throw ValidationException::withMessages(['fecha' => 'el año es requerido']);
		}
		if ($request->fecha_vencimiento === null) {
			throw ValidationException::withMessages(['fecha_vencimiento' => 'el año es requerido']);
		}
	}
	public function createSemanaUso($expensa, $cuentaCorriente, $estado)
	{
		$arraySemanaUso = [
			'anio' => $expensa->anio,
			'semana' => $expensa->unidad()->first()->semana,
			'cliente_id' => $expensa->cliente_id,
			'unidad_id' => $expensa->unidad->unidadR->id,
			'unidad_por_cliente' => $expensa->unidad->id,
			'hotel_id' => $expensa->unidad->hotelR->id,
			'temporada_id' => $expensa->temporada_id,
			'estado_semana_uso_id' => $estado->id,
		];
		return SemanaUso::updateOrCreate(
			$arraySemanaUso,
			[
				'fecha_vencimiento' => $cuentaCorriente->fecha_vencimiento, 'fraccionado' => $expensa->temporada->fraccionado,
			]
		);
	}
}
