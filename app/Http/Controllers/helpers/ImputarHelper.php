<?php

namespace App\Http\Controllers\helpers;

use Illuminate\Support\Facades\DB;

class ImputarHelper {


	public function imputarTodo(){
		return DB::statement("CALL imputacion_automatica_todos_los_clientes()");
	}
	public function imputarPagoAutomatico ($clienteId){
		return DB::statement("CALL imputacion_automatica($clienteId)");
	}

	public function imputarPagoEspecifico($idDebe, $idHaber, $saldo){

		return DB::statement("CALL imputacion($idDebe, $idHaber, $saldo)");
	}

	public function desimputar($id){
		return DB::statement("CALL desimputacion($id)");
	}
	public function desimputarRecibo($reciboId){
		RETURN DB::statement("CALL desimputar_recibo($reciboId)");
	}
}
