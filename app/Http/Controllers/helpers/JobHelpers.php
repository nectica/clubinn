<?php

namespace App\Http\Controllers\helpers;

use App\Models\CuentaCorriente;
use App\Models\Movimiento;
use App\Models\Parametro;
use Carbon\Carbon;

class JobHelpers
{


	public function clienteInteres($clientes)
	{
		$movimiento_id = Movimiento::where('nombre', 'Interes por demora')->first()->id;
		$parametro = Parametro::where('nombre', 'tasa de interes')->first();
		foreach ($clientes as $cliente) {
			CuentaCorriente::create([
				'movimiento_id' => $movimiento_id,
				'importe' =>  $cliente->saldo_vencido * ($parametro->valor / 100),
				'cliente_id' => $cliente->id,
				'fecha_vencimiento' => Carbon::now(),
				'observacion' => 'Interes generado',
				'fecha' => Carbon::now(),
			]);
		}
	}
}
