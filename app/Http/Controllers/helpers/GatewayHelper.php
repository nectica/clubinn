<?php

namespace App\Http\Controllers\helpers;

use App\Events\ReciboConfirmEvent;
use App\Events\ReciboRejectedEvent;
use App\Models\ClienteTable;
use App\Models\CuentaCorriente;
use App\Models\Estado;
use App\Models\FormaPago;
use App\Models\FrontendPayment;
use App\Models\Gateway;
use App\Models\Movimiento;
use App\Models\PlanesDecidir;
use App\Models\Recibo;
use App\Models\TarjetaCredito;
use Carbon\Carbon;
use Decidir\Connector;
use Faker\Provider\Uuid;
use MercadoPago\Payment;
use MercadoPago\SDK;

class GatewayHelper
{

	protected $decidir2, $mercadoPago;
	public $recibo;
	protected $response, $requestFrontend, $planesDecidir;
	public $cliente;
	public $responseFrontend;
	public $gateway;
	public  $error = false, $statuCodeError = 400, $msgError = 'pago fue rechazado por la plataforma. Intente mas tarde.';

	public function __construct()
	{
	}

	public function startDecidir()
	{
		$keys_data = array(
			'public_key' =>  env('DECIDIR_PUBLIC_KEY'),
			'private_key' => env('DECIDIR_PRIVATE_KEY')
		);
		$this->decidir2 = new Connector($keys_data, ENV('DECIDIR_ENV'));
	}

	public function startMercadoPago()
	{
		SDK::setAccessToken(env('MP_KEY_PRIV_TEST'));
		$this->mercadoPago = new Payment();
	}

	public function decidirExecutePayments()
	{
		$this->getPlanDecir();
		$uiid = Uuid::uuid();
		$data = [
			"site_transaction_id" => $this->responseFrontend->id,
			"token" => $this->responseFrontend->id,
			"customer" => array(
				"id" => 'cliente_id-' . $this->cliente->id,
				"email" => $this->cliente->email,
			),
			"payment_method_id" => 1,
			"bin" => $this->responseFrontend->bin,
			"amount" => $this->recibo->importe,
			"currency" => "ARS",
			"installments" => $this->planesDecidir->cuotas_desde,
			"description" => "",
			"establishment_name" => "Clubinn",
			"payment_type" => "single",
			"sub_payments" => array(),
		];
		try {
			$response = $this->decidir2->payment()->ExecutePayment($data);
			if ($response->getStatus() == 'approved') {
				$this->confirmRecibo();
			} else if ($response->getStatus() == 'rejected') {
				$this->rejectedRecibo();
			}
			$this->createGateway($response, 'decidir');
			$this->createResponseFrontend();
		} catch (\Exception $e) {

			$this->msgError = $e->getMessage();
			$this->statuCodeError = $e->getCode();
			$this->gateway = Gateway::create([
				'cliente_id' => $this->cliente->id,
				'via' => 'decidir',
				'response_api' => json_encode(property_exists($e, 'data') ? $e->getData() :  $e->getMessage()),
				'numero_tarjeta' => $this->responseFrontend->last_four_digits,
				'nombre_titular_tarjeta' => $this->responseFrontend->cardholder->name,
				'dni_titular'  => $this->responseFrontend->cardholder->identification->number,
				'estado' => 'error',
				'recibo_id' => $this->recibo->id
			]);
			$this->createResponseFrontend();
			$this->rejectedRecibo();
		}
	}
	public function mercadoPagoExecutePayment()
	{
		try {
			$this->mercadoPago->transaction_amount = $this->requestFrontend->transactionAmount ?: 99;
			$this->mercadoPago->token = $this->responseFrontend->id;
			$this->mercadoPago->description = $this->requestFrontend->description;
			$this->mercadoPago->installments = $this->requestFrontend->installments;
			$this->mercadoPago->payment_method_id = $this->requestFrontend->paymentMethodId ?: 'visa';
			$this->mercadoPago->payer = array(
				"email" => $this->requestFrontend->email
			);
			$this->mercadoPago->save();
			if ($this->mercadoPago->status == 'approved') {
				$this->confirmRecibo();
				$this->createGatewayMP($this->mercadoPago->toArray(), 'mercadoPago');
			} else if ($this->mercadoPago->status == 'rejected') {
				$this->rejectedRecibo();
				$this->rejectPayment($this->mercadoPago->toArray(), 'mercadoPago');
			} else if ($this->mercadoPago->error) {
				$this->msgError = $this->mercadoPago->error->message;
				$this->statuCodeError = $this->mercadoPago->error->status;
				$this->rejectPayment($this->mercadoPago->error->causes, 'mercadoPago');
			}
		} catch (\Exception $e) {
			$this->rejectPayment($e, 'mercadoPago');
		}
	}

	public function createRecibo($importe)
	{
		$estado = Estado::where('nombre', 'Pendiente')->first();
		$formaDePago = FormaPago::where('nombre', 'Tarjeta de credito')->first();
		$tarjetaCredito = TarjetaCredito::where('nombre', $this->getCardType($this->requestFrontend->card_number))->first();
		$now = Carbon::now();
		$this->recibo = Recibo::create(
			[
				'estado_id' => $estado->id,
				'cliente_id' => $this->cliente->id,
				'importe' => $importe,
				'forma_pago_id' => $formaDePago->id,
				'fecha_pago' => $now,
				'nro_tarjeta' => $this->responseFrontend->last_four_digits,
				'tarjeta_credito_id' => $tarjetaCredito->id,
			]
		);
	}
	public function createResponseFrontend()
	{
		FrontendPayment::create([
			'response_frontend' => json_encode($this->responseFrontend),
			'gateway_id' => $this->gateway->id,
			'request_api' => json_encode($this->requestFrontend)
		]);
	}
	public function createGateway($response, $via)
	{
		if ($via == 'decidir' && $this->planesDecidir->cuotas_desde > 1) {
			$this->createInteresDecidirCuotas();
		}
		$this->gateway = Gateway::create([
			'cliente_id' => $this->cliente->id,
			'via' => $via,
			'response_api' => json_encode($response->getDataField()),
			'numero_tarjeta' => $this->responseFrontend->last_four_digits,
			'nombre_titular_tarjeta' => $this->responseFrontend->cardholder->name,
			'dni_titular'  => $this->responseFrontend->cardholder->identification->number,
			'estado' => $response->getStatus(),
			'recibo_id' => $this->recibo->id

		]);
	}
	public function createInteresDecidirCuotas()
	{
		$copiaRecibo = $this->recibo;
		$copiaRecibo->id = -1;
		$copiaRecibo->importe = $this->requestFrontend->interes;
		$movimiento = Movimiento::where('nombre', 'like', '%Interes de financiacion%')->first();
		$observacion = 'interes del plan: ' . $this->planesDecidir->nombre;
		$this->createCuentaCorriente($copiaRecibo, $movimiento->id, $observacion);
	}
	public function createGatewayMP($response, $via)
	{
		$this->gateway = Gateway::create([
			'cliente_id' => $this->cliente->id,
			'via' => $via,
			'response_api' => json_encode($response),
			'numero_tarjeta' => $this->responseFrontend->last_four_digits,
			'nombre_titular_tarjeta' => $this->responseFrontend->cardholder->name,
			'dni_titular'  => $this->responseFrontend->cardholder->identification->number,
			'estado' => $response["status"],
			'recibo_id' => $this->recibo->id

		]);
	}
	public function setCliente($email)
	{
		$this->cliente = ClienteTable::where('email', $email)->first();
	}
	public function setResponseFrontend($response)
	{
		$this->responseFrontend = $response;
	}
	public function setRequestFrontendApi($requestFrontend)
	{
		$this->requestFrontend =  $requestFrontend;
	}
	public function updateReciboStatus($status = 'Aceptado')
	{
		$estado = Estado::where('nombre', $status)->first();
		$this->recibo->update([
			'estado_id' => $estado->id
		]);
	}
	public function getPlanDecir()
	{
		$this->planesDecidir =  PlanesDecidir::where('codigo_plan', $this->requestFrontend->installments)->first();
	}
	public function rejectedRecibo()
	{
		$this->updateReciboStatus('Anulado');
		ReciboRejectedEvent::dispatch($this->recibo);
		$this->error = true;
	}
	public function confirmRecibo()
	{
		$movimiento = Movimiento::where('nombre', 'Recibo')->first();
		$this->createCuentaCorriente($this->recibo, $movimiento->id);
		$this->updateReciboStatus('Aceptado');
		ReciboConfirmEvent::dispatch($this->recibo);
	}

	public function rejectPayment($e, $via = 'decidir')
	{
		$this->error = true;


		$this->gateway = Gateway::create([
			'cliente_id' => $this->cliente->id,
			'via' => $via,
			'response_api' => json_encode($e),
			'numero_tarjeta' => $this->responseFrontend->last_four_digits,
			'nombre_titular_tarjeta' => $this->responseFrontend->cardholder->name,
			'dni_titular'  => $this->responseFrontend->cardholder->identification->number,
			'estado' => 'error',
			'recibo_id' => $this->recibo->id
		]);
		$this->createResponseFrontend();
		$this->rejectedRecibo();
	}


	public function createCuentaCorriente($recibo, $movimiento_id, $observacion = 'Gateway de pago')
	{
		$cuenta = CuentaCorriente::create([
			'movimiento_id' => $movimiento_id,
			'recibo_id' => $recibo->id,
			'importe' =>  $recibo->importe,
			'cliente_id' => $recibo->cliente_id,
			'fecha_vencimiento' => $recibo->fecha_pago,
			'fecha' => $recibo->fecha_pago,
			'anio' => $recibo->fecha_pago->format('Y'),
			'observacion' => $observacion,
		]);
		$this->imputarCliente();
		return $cuenta;
	}


	public function imputarCliente()
	{
		$imputar = new ImputarHelper();
		$imputar->imputarPagoAutomatico($this->cliente->id);
	}
	function getCardType($cardNumber)
	{
		// visa
		$brandCard = 'visa';
		if (preg_match("/^4/", $cardNumber)) {
			$brandCard = "visa";
		} else if (preg_match("/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/", $cardNumber)) {
			$brandCard = "master";
		} else if (preg_match("/^3[47]/", $cardNumber)) {
			$brandCard = "amex";
		}
		return  $brandCard;
	}
}
