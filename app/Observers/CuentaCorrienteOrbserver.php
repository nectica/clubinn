<?php

namespace App\Observers;

use App\Models\CuentaCorriente;

class CuentaCorrienteOrbserver
{

 /**
     * Handle the CuentaCorriente "creating" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
	public function creating(CuentaCorriente $cuentaCorriente){
		$cuentaCorriente->saldo = $cuentaCorriente->importe;
		$cuentaCorriente->fecha = $cuentaCorriente->fecha->setTime(0, 0, 0, 0);
		$cuentaCorriente->fecha_vencimiento = $cuentaCorriente->fecha_vencimiento->setTime(0, 0, 0, 0);
		if ($cuentaCorriente->expensa) {
			$cuentaCorriente->observacion = $cuentaCorriente->expensa->observaciones;
		}
	}
    /**
     * Handle the CuentaCorriente "created" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
    public function created(CuentaCorriente $cuentaCorriente)
    {
        //
    }

    /**
     * Handle the CuentaCorriente "updated" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
    public function updated(CuentaCorriente $cuentaCorriente)
    {
        //
    }

    /**
     * Handle the CuentaCorriente "deleted" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
    public function deleted(CuentaCorriente $cuentaCorriente)
    {
		if ($cuentaCorriente->expensa){
			$cuentaCorriente->expensa->delete();
		}
    }

    /**
     * Handle the CuentaCorriente "restored" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
    public function restored(CuentaCorriente $cuentaCorriente)
    {
        //
    }

    /**
     * Handle the CuentaCorriente "force deleted" event.
     *
     * @param  \App\Models\CuentaCorriente  $cuentaCorriente
     * @return void
     */
    public function forceDeleted(CuentaCorriente $cuentaCorriente)
    {
		if($cuentaCorriente->recibo){
			$cuentaCorriente->recibo->delete();
		}else if ($cuentaCorriente->expensa){
			$cuentaCorriente->expensa->delete();
		}

    }
}
