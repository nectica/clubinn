<?php

namespace App\Observers;

use App\Http\Controllers\helpers\ImputarHelper;
use App\Models\Recibo;

class ReciboObserver
{


	public function creating(Recibo $recibo)
	{
		$last = Recibo::latest()->first();
		$nro = $last ? $last->nro + 1 : 1;
		$recibo->nro = $nro;
		$recibo->saldo = $recibo->importe;
	}
	/**
	 * Handle the Recibo "created" event.
	 *
	 * @param  \App\Models\Recibo  $recibo
	 * @return void
	 */
	public function created(Recibo $recibo)
	{
	}

	/**
	 * Handle the Recibo "updated" event.
	 *
	 * @param  \App\Models\Recibo  $recibo
	 * @return void
	 */
	public function updated(Recibo $recibo)
	{
		if ($recibo->wasChanged('estado_id') && $recibo->estado->nombre == 'Aceptado'){
			$this->imputar($recibo);
		}

		if ( $recibo->wasChanged('estado_id') && ($recibo->estado->nombre == 'Anulado' || $recibo->estado->nombre == 'Pendiente')) {
			//desimputar aqui primero
			$this->desimputar($recibo);
			//borrar cuenta corriente
			if ($recibo->cuentaCorriente) {
				$recibo->cuentaCorriente->delete();
			}
		}
	}

	/**
	 * Handle the Recibo "deleted" event.
	 *
	 * @param  \App\Models\Recibo  $recibo
	 * @return void
	 */
	public function deleted(Recibo $recibo)
	{
		//
	}

	/**
	 * Handle the Recibo "restored" event.
	 *
	 * @param  \App\Models\Recibo  $recibo
	 * @return void
	 */
	public function restored(Recibo $recibo)
	{
		//
	}

	/**
	 * Handle the Recibo "force deleted" event.
	 *
	 * @param  \App\Models\Recibo  $recibo
	 * @return void
	 */
	public function forceDeleted(Recibo $recibo)
	{
		//
	}

	public function desimputar(Recibo $recibo)
	{
		$imputar = new ImputarHelper();
		$imputar->desimputarRecibo($recibo->id);
	}
	public function imputar(Recibo $recibo)
	{
		$imputar = new ImputarHelper();
		$imputar->imputarPagoAutomatico($recibo->cliente_id);
	}
}
