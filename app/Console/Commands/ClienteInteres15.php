<?php

namespace App\Console\Commands;

use App\Jobs\InteresVencidaEach15;
use App\Jobs\InteresVencidaEach15Job;
use Illuminate\Console\Command;

class ClienteInteres15 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cliente:interes15';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        InteresVencidaEach15Job::dispatch();
    }
}
