<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\Hotel;
use App\Models\Temporada;
use App\Models\Unidad;
use App\Models\UnidadPorCliente;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class HdsClienteUnidadImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
		try {
			if ($row['hotel'] == null) {
				return null;
			}
			$hotel = Hotel::where('nombre',$row['hotel'])->first();
			$cliente = Cliente::where('identificacion', $row['cliente_no'])->where('hotel', $hotel->id)->first();
			$temporada = Temporada::where('identificacion', $row['temporada'])->where('hotel',$hotel->id)->first();
			$unidad = Unidad::where('capacidad', $row['capacidad'])->where('hotel', $hotel->id)->first();
		} catch (\Exception $th) {
			dd($th,$row );
		}
		/* dd($row, $hotel, $cliente, $temporada, $unidad ); */
		try {
			return new UnidadPorCliente([
				'cliente' => $cliente->id,
				'unidad' => $unidad->id,
				'temporada' => $temporada->id,
				'capacidad' => $row['capacidad'],
				'hotel' => $hotel->id,
				'semana' => $row['semana_no'],
			]);



		} catch (\Exception $th) {
			dd($th   );
		}
        return new UnidadPorCliente([
            //
        ]);
    }
}
