<?php

namespace App\Imports;

use App\Models\Cliente;
use App\Models\ClienteTable;
use App\Models\Hotel;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;

class ClienteImport implements ToModel
{
	/**
	 * @param array isset($row


	 	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function model(array $row
	){
		$hotel = Hotel::where('nombre', $row[0])->first();
		if ( str_contains($row[0], 'HDS')) {
			$hotel = Hotel::where('nombre', 'HDS2')->first();
		}
		if($hotel === null ){
			/* return new Cliente(); */
			return;
		}


		if ( !str_contains($row[0], 'HDS')) {
			return ;
		}
		try {
			//code...
			return new ClienteTable(
				[
					'identificacion' => isset($row[1]) ?$row[1]: null,
					'nombre' => isset($row[3]) ?$row[3]: null, 'apellido' => isset($row[2]) ?$row[2]: null, 'telefono' => isset($row[15]) ?$row[15]: null, 'celular' => isset($row[17]) ?$row[17]: null,
					'email' => isset($row[18]) ?$row[18]: null, 'nombre_conyugue' => isset($row[7]) ?$row[7]: null, 'apellido_conyugue' => isset($row[6]) ?$row[6]: null, 'celular_conyugue' => isset($row[25]) ?$row[25]: null,
					'email_conyugue' => isset($row[26]) ?$row[26]: null,
					'password' => '', 'hotel' => $hotel->id?: 0,
					'dni' => isset($row[4]) ?$this->stringWithoutSignal($row[4]): null,
					'dni_conyugue' => isset($row[8]) ? $this->stringWithoutSignal($row[8]): null,
					'fecha_nacimiento' => isset($row[5]) ?   $this->validateDate($row[5]) : null, 'fecha_naciemiento_conyugue' => isset($row[9])  ?$this->validateDate($row[9]) : null,
					'domicilio' => isset($row[10]) ?$row[10]: null, 'localidad' => isset($row[11]) ?$row[11]: null, 'provincia' => isset($row[12]) ?$row[12]: null, 'pais' => isset($row[13]) ?$row[13]: null, 'cod_postal' => isset($row[14]) ?$row[14]: null,
					'email2' => isset($row[20]) ?$row[20]: null, 'id_rci' => isset($row[21]) ?$row[21]: null, 'id_interval' => isset($row[22]) ?$row[22]: null, 'telefono_laboral' => isset($row[16])? $row[16]: null,
				]
			);
		} catch (\Exception $th) {
			dd($th, $row);
		}

	}

	function validateDate($value){
		$return = $value ;

		if ($value === 0  || str_contains( $value ,'1900') ==! false  || str_contains( $value ,'1899') ) {

			$return = null;
		}

		return \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($return ) ;
	}
	function stringWithoutSignal($str, $signal = '.' ){

		return str_replace($signal, '', $str);
	}
}
