<?php

namespace App\Imports;

use App\Models\Hotel;
use App\Models\Unidad ;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class UnidadImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
		$capacidad = explode('/', $row['cap']);
		$hotel = Hotel::where('nombre', 'HDC')->first();
        return new Unidad([
			'nombre' => $row['dpto'] ,
			'hotel' => $hotel->id,
			'capacidad' => $capacidad[0],
			'capacidad_maxima' => $capacidad[1]
        ]);
    }
}
