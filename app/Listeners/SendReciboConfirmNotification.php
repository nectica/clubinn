<?php

namespace App\Listeners;

use App\Events\ReciboConfirmEvent;
use App\Notifications\ReciboConfirmClienteNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReciboConfirmNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReciboConfirmEvent  $event
     * @return void
     */
    public function handle(ReciboConfirmEvent $event)
    {
        $event->recibo->cliente->notify(new ReciboConfirmClienteNotification($event->recibo));
    }
}
