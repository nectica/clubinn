<?php

namespace App\Listeners;

use App\Events\UsoRequestEvent;
use App\Notifications\UsoRequestClienteNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUsoRequestClienteNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UsoRequestEvent  $event
     * @return void
     */
    public function handle(UsoRequestEvent $event)
    {
		$event->semanaUso;
		$cliente = $event->semanaUso->cliente;
		$cliente->notify(new UsoRequestClienteNotification($event->semanaUso));
    }
}
