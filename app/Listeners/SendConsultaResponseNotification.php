<?php

namespace App\Listeners;

use App\Models\User;
use App\Notifications\ConsultaResponseNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendConsultaResponseNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
		$usuario = $event->consulta->cliente;
		$usuario->notify(new ConsultaResponseNotification($event->consulta));
    }
}
