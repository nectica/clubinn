<?php

namespace App\Listeners;

use App\Events\ReciboStored;
use App\Models\User;
use App\Notifications\ReciboStoredNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendReciboStoredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ReciboStored $event)
    {
		/* $usuario = User::where('email', env('email', 'admin@admin.com' ))->first();
		$usuario->notify(new ReciboStoredNotification($event->recibo)); */
		//
		Notification::route('mail',env('MAIL_ADMIN', 'fordoqui@nectica.com'))->notify(new ReciboStoredNotification($event->recibo));

    }
}
