<?php

namespace App\Listeners;

use App\Events\ReciboRejectedEvent;
use App\Notifications\ReciboRejectedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReciboRejectedNotication
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReciboRejectedEvent  $event
     * @return void
     */
    public function handle(ReciboRejectedEvent $event)
    {
        $event->recibo->cliente->notify(new ReciboRejectedNotification($event->recibo));
    }
}
