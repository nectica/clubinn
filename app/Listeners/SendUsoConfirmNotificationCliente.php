<?php

namespace App\Listeners;

use App\Events\UsoConfirmEvent;
use App\Notifications\UsoConfirmNotificationCliente;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendUsoConfirmNotificationCliente
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UsoConfirmEvent $event)
    {
       $event->semanaUso;
	   $cliente = $event->semanaUso->cliente;
	   $cliente->notify(new UsoConfirmNotificationCliente($event->semanaUso));
    }
}
