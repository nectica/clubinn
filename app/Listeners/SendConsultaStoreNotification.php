<?php

namespace App\Listeners;

use App\Events\ConsultaStoreEvent;
use App\Notifications\ConsultaStoreNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendConsultaStoreNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConsultaStoreEvent  $event
     * @return void
     */
    public function handle(ConsultaStoreEvent $event)
    {
        Notification::route('mail',env('MAIL_ADMIN', 'fordoqui@nectica.com'))->notify(new ConsultaStoreNotification($event->consulta));
    }
}
