<?php

namespace App\Listeners;

use App\Events\UsoRejected;
use App\Notifications\UsoRejectedAdminNotification;
use App\Notifications\UsoRejectedCliente;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class SendRejectedUsoNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UsoRejected  $event
     * @return void
     */
    public function handle(UsoRejected $event)
    {
        $event->SemanaUso->cliente->notify(new UsoRejectedCliente($event->semanaUso));
		/* Auth::user()->notify(new UsoRejectedAdminNotification); */
    }
}
