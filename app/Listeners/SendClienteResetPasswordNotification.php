<?php

namespace App\Listeners;

use App\Events\ClienteResetPasswordEvent;
use App\Notifications\ClienteResetPasswordNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendClienteResetPasswordNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClienteResetPassword  $event
     * @return void
     */
    public function handle(ClienteResetPasswordEvent $event)
    {

        $event->cliente->notify(new ClienteResetPasswordNotification() );
    }
}
