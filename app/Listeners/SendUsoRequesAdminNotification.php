<?php

namespace App\Listeners;

use App\Events\UsoRequestEvent;
use App\Notifications\UsoRequestAdminNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;

class SendUsoRequesAdminNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UsoRequestEvent  $event
     * @return void
     */
	public function handle(UsoRequestEvent $event)
    {
		Notification::route('mail',env('MAIL_ADMIN', 'fordoqui@nectica.com'))->notify(new UsoRequestAdminNotification($event->semanaUso));
    }
}
