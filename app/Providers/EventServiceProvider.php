<?php

namespace App\Providers;

use App\Events\ClienteResetPasswordEvent;
use App\Events\ConsultaResponseEvent;
use App\Events\ConsultaStoreEvent;
use App\Events\ReciboConfirmEvent;
use App\Events\ReciboRejectedEvent;
use App\Events\ReciboStored;
use App\Events\UsoConfirmEvent;
use App\Events\UsoRejected;
use App\Events\UsoRequestEvent;
use App\Listeners\SendClienteResetPasswordNotification;
use App\Listeners\SendConsultaResponseNotification;
use App\Listeners\SendConsultaStoreNotification;
use App\Listeners\SendReciboConfirmNotification;
use App\Listeners\SendReciboRejectedNotication;
use App\Listeners\SendReciboStoredNotification;
use App\Listeners\SendRejectedUsoNotification;
use App\Listeners\SendUsoConfirmNotificationCliente;
use App\Listeners\SendUsoRequestClienteNotification;
use App\Models\CuentaCorriente;
use App\Models\Recibo;
use App\Observers\CuentaCorrienteOrbserver;
use App\Observers\ReciboObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
	/**
	 * The event listener mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		Registered::class => [
			SendEmailVerificationNotification::class
		],
		UsoRejected::class => [
			SendRejectedUsoNotification::class,
		],
		UsoConfirmEvent::class =>[
			SendUsoConfirmNotificationCliente::class,
		],
		UsoRequestEvent::class => [
			SendUsoRequestClienteNotification::class
		],
		ReciboStored::class => [
			SendReciboStoredNotification::class,
		],
		ReciboRejectedEvent::class => [
			SendReciboRejectedNotication::class
		],
		ReciboConfirmEvent::class => [
			SendReciboConfirmNotification::class
		],
		ConsultaStoreEvent::class => [
			SendConsultaStoreNotification::class,
		],
		ConsultaResponseEvent::class => [
			SendConsultaResponseNotification::class
		],
		ClienteResetPasswordEvent::class => [
			SendClienteResetPasswordNotification::class
		],
	];

	/**
	 * Register any events for your application.
	 *
	 * @return void
	 */
	public function boot()
	{
		Recibo::observe(ReciboObserver::class);
		CuentaCorriente::observe(CuentaCorrienteOrbserver::class);
	}
}
