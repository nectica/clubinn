<?php

namespace App\Exports;

use App\Models\Cliente;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ClienteSaldoExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {	$clientes = Cliente::orderBy('Nombre')->get();
        return View('cliente.export-saldo', compact('clientes'));
    }
}
