<?php

namespace App\Exports;

use App\Models\Recibo;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class ReciboExport implements FromView
{
	public $recibos;

	public function __construct($recibos){
		$this->recibos = $recibos;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  view(): View
    {
        $recibos =$this->recibos;
		return View('recibo.export', compact('recibos'));
    }
}
