<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;;

class ComposicionSaldoExport implements FromView
{
	protected $clientes ;
	public function __construct($clientes){
		$this->clientes = $clientes;
	}
    /**
    * @return \Illuminate\Support\Collection
    */
	public function view(): View
    {	$clientes = $this->clientes;
        return View('cliente.saldo-compuesto', compact('clientes'));
    }
}

