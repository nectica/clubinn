<?php

namespace App\Exports;

use App\Models\Cliente;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ClienteContratos implements FromView
{

	protected $clientes;

	public function __construct($clientes){
		$this->clientes = $clientes;
	}
	/**
	 * @return Illuminate\Contracts\View\View
	 */
	public function view(): View
	{
		$clientes = $this->clientes;
		return View('cliente.export-cliente', compact('clientes'));
	}
}
