<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class UsosExport implements FromView
{

	public $semanaUsos;
	public function __construct($usos)
	{
		$this->semanaUsos = $usos;
	}
	/**
	 * @return \Illuminate\Support\Collection
	 */
	public function  view(): View
	{
		$semanaUsos = $this->semanaUsos;
		return View('semanaUso.export', compact('semanaUsos'));
	}
}
