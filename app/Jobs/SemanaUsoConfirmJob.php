<?php

namespace App\Jobs;

use App\Events\UsoConfirmEvent;
use App\Models\SemanaUso;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SemanaUsoConfirmJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
	protected $semanaUso;
    public function __construct(SemanaUso $semanaUso)
    {
        $this->semanaUso = $semanaUso;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
		UsoConfirmEvent::dispatch($this->semanaUso);
    }
}
