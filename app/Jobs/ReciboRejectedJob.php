<?php

namespace App\Jobs;

use App\Events\ReciboRejectedEvent;
use App\Models\Recibo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ReciboRejectedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
	protected $recibo;
    public function __construct(Recibo $recibo)
    {
        $this->recibo = $recibo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       ReciboRejectedEvent::dispatch($this->recibo);
    }
}
