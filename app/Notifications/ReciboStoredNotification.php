<?php

namespace App\Notifications;

use App\Models\Recibo;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Storage;

class ReciboStoredNotification extends Notification
{
	use Queueable;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	protected $recibo;
	public function __construct(Recibo $recibo)
	{
		$this->recibo = $recibo;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		return (new MailMessage)
			->subject('Recibo de pago cargado Hostería del Cerro / Hostal del Sol.'.$this->recibo->cliente->hotelR->full_nombre)
			->greeting('Hola')
			->line('el cliente '. $this->recibo->cliente->full_name.', '. $this->recibo->cliente->email.', del hotel: '.$this->recibo->cliente->hotelR->full_nombre)
			->line('forma de pago: '.$this->recibo->formaPago->nombre)
			->line('Ha cargo un pago con fecha de ' . $this->recibo->fecha_pago->format('d/m/Y') . ', por el monto de ' . $this->recibo->importe)
			 ->attachData(Storage::get('voucher/'.$this->recibo->voucher), $this->recibo->voucher )
			;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
