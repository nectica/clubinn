<?php

namespace App\Notifications;

use App\Models\Recibo;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Barryvdh\DomPDF\Facade as PDF;

class ReciboRejectedNotification extends Notification
{
	use Queueable;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	protected $recibo;
	public function __construct(Recibo $recibo)
	{
		$this->recibo = $recibo;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$cliente = $notifiable;
		$recibo = $this->recibo;
		$now = Carbon::now();
		/* $pdf = PDF::loadView('recibo.recibo-pdf', compact('cliente', 'recibo', 'now')); */
		return (new MailMessage)
			->subject('Recibo de pago rechazado '.$cliente->hotelR->full_nombre)
			->greeting('Estimado/a  ')
			->line($notifiable->full_name)
			->line('el recibo correspondiente al pago realizado en el ' . $this->recibo->fecha_pago->format('d/m/Y')  . ', por el monto de ' . $this->recibo->importe)
			->line('forma de pago: '.$this->recibo->formaPago->nombre)
			->line('ha sido rechazado')
			->salutation('Saludos, '.$cliente->hotelR->full_nombre)

			/* ->attachData($pdf->output(), 'recibo.pdf') */;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
