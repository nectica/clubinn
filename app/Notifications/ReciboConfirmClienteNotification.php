<?php

namespace App\Notifications;

use App\Models\Recibo;
use Barryvdh\DomPDF\Facade as PDF;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReciboConfirmClienteNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
	protected $recibo;
    public function __construct(Recibo $recibo)
    {
        $this->recibo = $recibo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		$cliente = $notifiable;
		$recibo = $this->recibo;
		$now = Carbon::now();
		$pdf = PDF::loadView('recibo.recibo-pdf', compact('cliente', 'recibo', 'now'));

        return (new MailMessage)
		->subject('Recibo de pago Confimado '.$cliente->hotelR->full_nombre)
		->greeting('Estimado/a  ')
		->line($notifiable->full_name)
		->line('A continuación le enviamos el recibo correspondiente al pago realizado en el '.$this->recibo->fecha_pago->format('d/m/Y').' Saludos Cordiales, Clara Ciarmiello')
		->line('forma de pago: '.$this->recibo->formaPago->nombre)
		->salutation('Saludos, '.$cliente->hotelR->full_nombre)
		->attachData($pdf->output(), 'recibo.pdf')
		;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
