<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Barryvdh\DomPDF\Facade as PDF;

class UsoRejectedCliente extends Notification
{
	use Queueable;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	protected $semanaUso;
	public function __construct($uso)
	{
		$this->semanaUso = $uso;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$unidadNombre = $this->semanaUso->unidad->unidadR ? $this->semanaUso->unidad->unidadR->nombre : $this->semanaUso->unidad->nombre;
		$now = Carbon::now();
		/* $pdf = PDF::loadView(
			'semanaUso.uso-pdf-email',
			[
				'cliente' => $notifiable,
				'unidadNombre' => $unidadNombre,
				'now' => $now,
				'semanaUso' => $this->semanaUso
			]
		); */
		return (new MailMessage)
			->subject('Uso rechazado')
			->greeting('Hola')
			->line($notifiable->full_name)
			->line('el uso para el hotel ' . $this->semanaUso->hotel->nombre . ', para el año ' . $this->semanaUso->anio)
			->line('en la unidad ' . $unidadNombre)
			->line('Tu uso fue rechazado.')
			/* ->attachData($pdf->output(), 'uso.pdf') */;
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
