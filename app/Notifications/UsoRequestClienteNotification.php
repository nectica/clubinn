<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UsoRequestClienteNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
	protected $semanaUso;
	public function __construct($uso)
	{
		$this->semanaUso = $uso;
	}


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		$unidadNombre = $this->semanaUso->unidad->unidadR ? $this->semanaUso->unidad->unidadR->nombre : $this->semanaUso->unidad->nombre;
        return (new MailMessage)
		->subject('Uso solicitado')
				->greeting('Hola')
					->line($notifiable->full_name)
					->line('el uso para el hotel '.$this->semanaUso->hotel->nombre.', para el año '.$this->semanaUso->anio )
					->line('en la unidad '.$unidadNombre )
					->line('ha  sido solicitado ')
                    ->line('Gracias por confiar en nosotros!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
