<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CuentaCorriente extends Model
{
	use HasFactory, Sortable;
	protected $table = 'cuentas_corrientes',
		$fillable = [
			'fecha', 'importe', 'expensa_id',
			'recibo_id', 'movimiento_id',
			'cliente_id', 'fecha_vencimiento',
			'observacion'
			]
	/* ,$appends = ['importe', 'expensa_id', 'recibo_id', 'movimiento_id', 'cliente_id', 'importe'] */;
	protected $dates = ['fecha', 'fecha_vencimiento'];
	public function movimiento()
	{
		return $this->belongsTo(Movimiento::class,  'movimiento_id',   'id');
	}
	public function cliente()
	{
		return $this->belongsTo(Cliente::class);
	}
	public function expensa(){
		return $this->belongsTo(Expensa::class);
	}
	public function recibo(){
		return $this->belongsTo(Recibo::class);
	}

	/* public function getImporteAttribute()
	{
		try {
			$multiple = 1;
			if ($this->movimiento->nombre == 'Recibo' || $this->movimiento->nombre == ' Ajuste de saldo negativo') {
				$multiple = -1;
			}
			return $this->attributes['importe']  * $multiple;
		} catch (\Exception $th) {
			throw new \Exception();
		}
	}
	public function setImporteAttribute()
	{
		$multiple = 1;
		if ($this->importe < 0) {
			$multiple = -1;
		}
		return $this->importe * $multiple;
	} */
}
