<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrontendPayment extends Model
{
    use HasFactory;
	protected $table = 'frontend_payments';
	protected $fillable = ['gateway_id', 'response_frontend', 'request_api'];
	public function gateway(){
		return $this->belongsTo(Gateway::class, 'gateway_id');
	}
}
