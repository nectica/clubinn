<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Expensa extends Model
{
    use HasFactory, Sortable;
	protected $table = 'expensas', $fillable = ['temporada_id', 'unidades_por_cliente_id', 'cliente_id', 'anio', 'observaciones', 'importe'],
	$sortable = ['unidades_por_cliente_id', 'cliente_id', 'anio', 'observaciones', 'importe', 'temporada_id'];
	public function unidad(){
		return $this->belongsTo( UnidadPorCliente::class, 'unidades_por_cliente_id');
		//return $this->belongsToMany( Unidad::class, UnidadPorCliente::class,'id', 'unidad', 'unidades_por_cliente_id','id')->withPivot(['id','semana']);
	}
	/* public function unidad(){
		return $this->belongsTo(Unidad::class, 'unidades_por_cliente_id', 'id');
	} */
	public function cliente(){
		return $this->belongsTo(Cliente::class,  'cliente_id',   'id');
	}
	public function unidadPorCliente(){
		return $this->hasOne(UnidadPorCliente::class, 'unidad', 'unidades_por_cliente_id')->where('cliente', $this->cliente_id);
	}
	public function temporada(){
		return $this->belongsTo(Temporada::class);
	}
	public function CuentaCorriente(){
		return  $this->hasOne(CuentaCorriente::class);
	}
}
