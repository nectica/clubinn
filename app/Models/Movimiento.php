<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Movimiento extends Model
{
    use HasFactory, Sortable;
	protected $table = 'movimientos', $fillable = ['nombre'], $sortable = ['nombre'];

	public function CuentasCorrientes(){
		return $this->hasMany(CuentasCorrientes::class, 'movimiento_id', 'id');
	}
}
