<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanesDecidir extends Model
{
    use HasFactory;
	protected $table = 'planes_pago_decidir';
}
