<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class EstadoConsulta extends Model
{
    use HasFactory, Sortable;
	protected $table = 'estado_consulta', $fillable = ['nombre'];

	public function consultas(){
		return $this->hasMany(Consulta::class, 'estado_consulta_id', 'id');
	}
}
