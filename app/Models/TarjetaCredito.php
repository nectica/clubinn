<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TarjetaCredito extends Model
{
    use HasFactory;
	protected $table = 'tarjeta_creditos';
	protected $fillable = ['nombre'];

	public function recibos(){
		return $this->hasMany(Recibo::class, 'tarjeta_credito_id');
	}
}
