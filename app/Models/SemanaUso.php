<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SemanaUso extends Model
{
	use HasFactory, Sortable;
	protected $table = 'semanas_usos';
	protected $fillable = [
			'anio', 'fecha_vencimiento',
			'fecha_uso', 'semana', 'dias_usado', 'dias',
			'fraccionado', 'cliente_id', 'unidad_por_cliente', 'unidad_id',
			'hotel_id', 'temporada_id', 'estado_semana_uso_id', 'observacion'
		];
	protected $dates = ['fecha_vencimiento', 'fecha_uso'];

	public function getUnidadAttribute()
	{
		return  $this->unidadR  ?:$this->unidadPorCliente;
	}
	public function cliente(){
		return $this->belongsTo(Cliente::class);
	}
	public function temporada(){
		return $this->belongsTo(Temporada::class,'temporada_id',   'id');
	}
	public function estado(){
		return $this->belongsTo(EstadoSemanaUso::class, 'estado_semana_uso_id');
	}
	public function fechas(){
		return $this->hasMany(FechaSemanaUso::class, 'semana_uso_id');
	}
	public function getDiasUsadoFAttribute(){
		return $this->fechas->sum(function($fecha){
			return $fecha->dias_usado;
		});
	}
	public function unidadPorCliente(){
		return $this->belongsTo(UnidadPorCliente::class, 'unidad_por_cliente');
	}
	public function unidadR(){
		return $this->belongsTo(Unidad::class, 'unidad_id');

	}
	public function hotel(){
		return $this->belongsTo(Hotel::class);
	}
}
