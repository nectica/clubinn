<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Cliente extends Model
{
	use HasFactory, Sortable, Notifiable;
	protected $table = 'clientes_con_saldo',
		$fillable = [
			'identificacion', 'nombre', 'apellido', 'telefono', 'celular',
			'email', 'nombre_conyugue', 'apellido_conyugue', 'celular_conyugue',
			'email_conyugue', 'password', 'hotel',
			'dni', 'dni_conyugue', 'fecha_nacimiento', 'fecha_naciemiento_conyugue',
			'domicilio', 'localidad', 'provincia', 'pais', 'cod_postal',
			'email2', 'id_rci', 'id_interval', 'telefono_laboral','saldo', 'dias_mora', 'saldo_vencido', 'cuil'
		],
		$sortable  = [
			'identificacion', 'nombre', 'apellido', 'telefono', 'celular',
			'email', 'nombre_conyugue', 'apellido_conyugue', 'celular_conyugue',
			'email_conyugue', 'password', 'hotel',
			'dni', 'dni_conyugue', 'fecha_nacimiento', 'fecha_naciemiento_conyugue',
			'domicilio', 'localidad', 'provincia', 'pais', 'cod_postal',
			'email2', 'id_rci', 'id_interval', 'telefono_laboral','saldo', 'dias_mora', 'saldo_vencido', 'cuil'
		];
	public $timestamps = false;


	public function hotelR()
	{
		return $this->belongsTo(Hotel::class, 'hotel', 'id');
	}
	public function unidades()
	{
		return $this->belongsToMany(Unidad::class, 'unidades_por_cliente', 'cliente', 'unidad', 'id', 'id')
		->withPivot(['temporada']);
	}
	public function consultas(){
		return $this->hasMany(Consulta::class, 'cliente_id', 'id');
	}
	public function cuentasCorrientes(){
		return $this->hasMany(CuentaCorriente::class)->orderBy('created_at', 'desc');
	}
	public function recibos(){
		return $this->hasMany(Recibo::class);
	}

	/* accesors */
	public function getFullNameAttribute(){
		return $this->nombre.' '.$this->apellido;
	}
	public function clienteMultiple(){
		return $this->hasOne(Cliente::class,'email', 'email')->where('id', '!=', $this->id);
	}
	public function getFullCuentaCorrientesAttribute(){
		$collection = new Collection($this->cuentasCorrientes);
		if ($this->clienteMultiple && !$this->clienteMultiple->cuentasCorrientes->isEmpty()) {
			# code...monibea2015@gmail.com aleovio@gmail.com
			$collection = $collection->merge($this->clienteMultiple->cuentasCorrientes);
		}
		return $collection;
	}
	public function getFullRecibosAttribute(){
		$collection =  collect($this->recibos);
		if ($this->clienteMultiple) {
			# code...monibea2015@gmail.com
			$collection->merge($this->clienteMultiple->recibos);
		}
		return $collection;
	}


	public function usos(){
		return $this->hasMany(SemanaUso::class);
	}
	public function tokens(){
		return $this->hasMany(TokenCliente::class);
	}

	public function comentarios(){
		return $this->hasMany(ComentarioCliente::class);
	}
	/* public function routeNotificationForMail($notification){
		return env('MAIL_ADMIN', 'gorge2004@gmail.com') ;
	} */
	public function getCcStoreAttribute(){
		$now = Carbon::now();
		$now->setTime(0,0);
		$now->addYears(10);
		$now =  $now->format('Y-m-d');
		return DB::select("CALL consultar_imputaciones( $this->id,'1900-01-01', '$now')") ;
	}
}
