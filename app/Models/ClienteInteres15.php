<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteInteres15 extends Model
{
    use HasFactory;
	protected $table = 'cliente_saldo_vencido_15';

}
