<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteTable extends Model
{
	use HasFactory;
	protected $table = 'clientes';
	protected	$fillable = [
		'identificacion', 'nombre', 'apellido', 'telefono', 'celular',
		'email', 'nombre_conyugue', 'apellido_conyugue', 'celular_conyugue',
		'email_conyugue', 'password', 'hotel',
		'dni', 'dni_conyugue', 'fecha_nacimiento', 'fecha_naciemiento_conyugue',
		'domicilio', 'localidad', 'provincia', 'pais', 'cod_postal',
		'email2', 'id_rci', 'id_interval', 'telefono_laboral', 'saldo', 'cuil', 'calendario'

	];
	public $timestamps = false;
	public function hotelR()
	{
		return $this->belongsTo(Hotel::class, 'hotel', 'id');
	}
	public function unidades()
	{
		return $this->belongsToMany(Unidad::class, 'unidades_por_cliente', 'cliente', 'unidad', 'id', 'id')
			->withPivot(['temporada']);
	}
	public function consultas()
	{
		return $this->hasMany(Consulta::class, 'cliente_id', 'id');
	}
	public function cuentasCorrientes()
	{
		return $this->hasMany(CuentaCorriente::class)->orderBy('created_at', 'desc');
	}
	public function recibos()
	{
		return $this->hasMany(Recibo::class);
	}
	/* public function routeNotificationForMail($notification)
	{
		return env('MAIL_FROM_ADDRESS', 'gorge2004@gmail.com');
	} */
}
