<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Unidad extends Model
{
	use HasFactory, Sortable;
	protected $table = 'unidades', $fillable = ['nombre', 'hotel', 'capacidad', 'capacidad_maxima'];
	public $timestamps = false;


	public function hotelR()
	{
		return $this->belongsTo(Hotel::class, 'hotel', 'id');
	}
	public function clientes()
	{
		return $this->belongsToMany(Cliente::class, 'unidades_por_cliente', 'unidad', 'cliente', 'id', 'id')->withPivot(['temporada']);
	}
	public function temporadas()
	{
		return $this->belongsToMany(Temporada::class, 'unidades_por_cliente', 'unidad', 'temporada', 'id', 'id')->withPivot(['temporada']);
	}

	public function precioAnual()
	{
		return $this->hasMany(PrecioTemporadaAnio::class);
	}

	public function unidadPorCliente()
	{
		return $this->hasMany(UnidadPorCliente::class, 'unidad');
	}
}
