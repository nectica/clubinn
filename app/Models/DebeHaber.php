<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DebeHaber extends Model
{
    use HasFactory;
	protected $table = 'debe_rel_haber';
	protected $fillable = ['id_deber', 'id_haber', 'importe'];
}
