<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClienteInteres30 extends Model
{
    use HasFactory;
	protected $table = 'cliente_saldo_vencido_30';
}
