<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SemanaTemporada extends Model
{
    use HasFactory, Sortable;
	protected $table = 'semanas_temporadas';
	protected $fillable = ['temporada_id', 'semana'];
	protected $sortable = ['temporada_id', 'semana'];

	public function temporadas(){
		return $this->belongsTo(Temporada::class);
	}
}
