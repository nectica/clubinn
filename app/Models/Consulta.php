<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Consulta extends Model
{
    use HasFactory, Sortable;
	protected $table = 'consultas', $fillable = ['asunto', 'consulta', 'respuesta', 'estado_consulta_id', 'cliente_id'];

	public function cliente(){
		return $this->belongsTo(Cliente::class,  'cliente_id',   'id');
	}
	public function estado(){
		return $this->belongsTo(EstadoConsulta::class, 'estado_consulta_id', 'id');
	}

}
