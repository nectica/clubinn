<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class PrecioTemporadaAnio extends Model
{
    use HasFactory, Sortable;

	protected $table = 'precio_temporada_anios', $fillable = ['temporada_id', 'anio', 'precio', 'unidad_id'];

	public function temporada(){
		return $this->belongsTo(Temporada::class, 'temporada_id', 'id');
	}
	public function unidad(){
		return $this->belongsTo(Unidad::class);
	}
}
