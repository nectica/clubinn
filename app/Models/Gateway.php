<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    use HasFactory;
	protected $table = 'gateways';
	protected $fillable = ['cliente_id', 'via', 'response_api', 'numero_tarjeta', 'nombre_titular_tarjeta', 'dni_titular', 'estado', 'recibo_id'];

	public function cliente(){
		return $this->belongsTo(Cliente::class,  'cliente_id');
	}

	public function frontendResponse(){
		return $this->hasOne(FrontendPayment::class, 'gateway_id');
	}
	public function recibo(){
		return $this->belongsTo(Recibo::class, 'recibo_id');
	}
}
