<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class FechaSemanaUso extends Model
{
    use HasFactory, Sortable;
	protected $table = 'fechas_semanas_uso';
	protected $fillable = ['semana_uso_id', 'fecha_uso', 'dias_usado'];

	public function usos(){
		return $this->belongsTo(SemanaUso::class, 'semana_uso_id');
	}
}
