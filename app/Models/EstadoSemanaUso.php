<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class EstadoSemanaUso extends Model
{
    use HasFactory, Sortable;
	protected $table = 'estados_semana_uso', $fillable = ['nombre'], $sortable = ['nombre'] ;

	public function semanaUso(){
		return $this->hasMany(SemanaUso::class, 'estado_consulta_id');
	}
}
