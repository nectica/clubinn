<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Kyslik\ColumnSortable\Sortable;

class UnidadPorCliente extends Pivot
{
    use HasFactory;
    protected $table = 'unidades_por_cliente', $fillable= ['cliente', 'unidad', 'temporada', 'semana', 'capacidad', 'hotel'];
    public $timestamps = false;
	protected $appends = ['temporadaR'];

    public function hotelR(){
        return $this->belongsTo(Hotel::class,  'hotel',   'id');
    }
    public function unidadR(){
        return $this->belongsTo(Unidad::class, 'unidad', 'id');
    }
    public function clienteR(){
        return $this->belongsTo(Cliente::class,  'cliente',   'id');
    }
	public function temporadaR(){
		return $this->belongsTo(Temporada::class, 'temporada', 'id');
	}
}
