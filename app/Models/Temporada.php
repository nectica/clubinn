<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Temporada extends Model
{
    use HasFactory, Sortable;
    protected $table = 'temporadas', $fillable = ['identificacion', 'nombre', 'hotel', 'fraccionado'],
	$sortable = ['identificacion', 'nombre', 'hotel', 'fraccionado'];
    public $timestamps = false;

    public function hotelR(){
        return $this->belongsTo(Hotel::class, 'hotel', 'id');
    }
	public function semanas(){
		return $this->hasMany(SemanaTemporada::class);
	}
	public function unidadPorCliente(){
		return $this->hasMany(UnidadPorCliente::class, 'temporada');
	}
}
