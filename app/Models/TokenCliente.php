<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class TokenCliente extends Model
{
    use HasFactory;
	protected $table = 'tokens_clientes';
	protected $fillable = ['token', 'cliente_id'];

	public function cliente(){
		return $this->belongsTo(Cliente::class);
	}
}
