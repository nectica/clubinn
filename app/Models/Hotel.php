<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Hotel extends Model
{
    use HasFactory, Sortable;
    protected $table = 'hoteles', $fillable = ['nombre', 'calendario', 'full_nombre'];
    public $timestamps = false;

    public function habitaciones(){
        return $this->hasMany(Unidad::class, 'hotel', 'id');
    }
    public function temporadas(){
        return $this->hasMany(Temporada::class, 'hotel', 'id');
    }
    public function clientes(){
        return $this->hasMany(Cliente::class,'hotel', 'id');
    }

}
