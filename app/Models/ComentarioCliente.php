<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComentarioCliente extends Model
{
    use HasFactory;

	protected $table = 'comentarios_clientes';
	protected $fillable = ['observacion', 'cliente_id', 'user_id'];

	public function cliente(){
		return $this->belongsTo(Cliente::class);
	}
	public function user(){
		return $this->belongsTo(User::class);
	}
}
