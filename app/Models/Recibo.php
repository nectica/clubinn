<?php

namespace App\Models;

use Illuminate\Auth\Access\Gate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable\Sortable;

class Recibo extends Model
{
    use HasFactory, Sortable;
	protected $table = 'recibos',
	$fillable= [
		'fecha_pago', 'forma_pago_id', 'importe',
		'estado_id','cliente_id',
		'observaciones' ,'voucher', 'cbu_id',
		'cuotas', 'lote', 'cupon', 'tarjeta_credito_id', 'nro_tarjeta', 'saldo', 'nro'
	], $sortable = [
		'fecha_pago', 'forma_pago_id', 'importe',
		'estado_id','cliente_id',
		'observaciones' ,'voucher', 'cbu_id',
		'cuotas', 'lote', 'cupon', 'tarjeta_credito_id', 'nro_tarjeta', 'saldo', 'nro'
	],
	$dates = ['fecha_pago'];

	public function cliente(){
		return $this->belongsTo(Cliente::class,  'cliente_id');
	}
	public function cuentaCorriente(){
		return $this->hasOne(CuentaCorriente::class, 'recibo_id');
	}
	public function estado(){
		return $this->belongsTo(Estado::class);
	}
	public function formaPago(){
		return $this->belongsTo(FormaPago::class, 'forma_pago_id');
	}
	public function tarjetaCredito(){
		return $this->belongsTo(TarjetaCredito::class, 'tarjeta_credito_id',   'id');
	}
	public function gateway(){
		return $this->hasOne(Gateway::class,'recibo_id');
	}

	public function getImputacionesAttribute(){
		return DB::select("CALL imputaciones_recibo($this->id)");
	}
	public function getPosibleImputacionAttribute(){
		return DB::select("CALL comprobantes_pendientes_pago($this->cliente_id)");
	}

}
