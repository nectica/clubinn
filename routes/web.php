<?php

use App\Http\Controllers\api\GatewayApiController;
use App\Http\Controllers\CbuController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ComentarioClienteController;
use App\Http\Controllers\ConsultaController;
use App\Http\Controllers\CuentaCorrienteController;
use App\Http\Controllers\ExpensaController;
use App\Http\Controllers\GatewayController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\ImputarController;
use App\Http\Controllers\ParametroController;
use App\Http\Controllers\ReciboController;
use App\Http\Controllers\SemanaUsoController;
use App\Http\Controllers\TemporadaController;
use App\Http\Controllers\UnidadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('cliente/reset/{token}', [ClienteController::class, 'resetPasswordForm'])->name('cliente.reset.get');
Route::put('cliente/reset/{cliente}', [ClienteController::class, 'resetPassword'])->name('cliente.reset.update');
Route::get('recibos/pdf/{recibo}', [ReciboController::class, 'getReciboPdf'])->name('recibos.pdf');
Route::get('recibos/pdf/{recibo}/export', [ReciboController::class, 'getPdfExportRecibo'])->name('recibos.pdf.export');
Route::get('hotel/calendario/{hotel}', [HotelController::class, 'getCalendario'])->name('hotel.calendario');

Route::middleware(['auth'])->group(function () {
	Route::get('cliente/saldo/especifico', [ClienteController::class, 'getClienteComposicionSaldoTodo'])->name('cliente.saldo.compuesto.todos');
	Route::get('cliente/saldo/composicion/{clienteId}', [ClienteController::class, 'getClienteComposicionSaldo'])->name('cliente.saldo.compuesto');
	Route::get('cliente/exportar/excel/{filter?}', [ClienteController::class ,'exportClientesExcel'])->name('cliente.exportar.contratos');
	Route::resource('cliente', ClienteController::class);

	Route::resource('temporada', TemporadaController::class);

	Route::resource('hotel', HotelController::class);

	Route::resource('unidad', UnidadController::class);

	Route::resource('consultas', ConsultaController::class);

	Route::resource('recibos', ReciboController::class);
	Route::get('recibo/export', [ReciboController::class, 'getReciboExport'])->name('recibos.export');

	Route::get('expensas/choose', [ExpensaController::class, 'chooseTempAndHotel'])->name('expensas.choose');
	Route::post('expensas/unidad', [ExpensaController::class, 'createPricesToUnidad'])->name('expensas.unidad');
	Route::get('expensas/clientes/{temporada}/{anio}/{fecha}/{fecha_vencimiento}', [ExpensaController::class, 'generateExpensas'])->name('expensas.clientes');
	Route::resource('expensas', ExpensaController::class);
	Route::get('expensas/unica/cliente', [ExpensaController::class, 'createUniqueExpensa'])->name('expensa.unica');
	Route::post('expensas/unica/cliente', [ExpensaController::class, 'createUniqueExpensaStore'])->name('expensa.unica.store');

	Route::get('usos/excel/download', [SemanaUsoController::class, 'exportToExcel'])->name('usos.excel.download');
	Route::resource('usos', SemanaUsoController::class);

	Route::get('saldo/cliente', [ClienteController::class, 'getSaldoclientes'])->name('cliente.saldo');
	Route::get('saldo/cliente/export', [ClienteController::class, 'getExcelSaldoclientes'])->name('cliente.saldo.export');

	Route::get('comentario/cliente/{cliente}', [ComentarioClienteController::class, 'create'])->name('comentario.create');
	Route::post('comentario/cliente/create', [ComentarioClienteController::class, 'store'])->name('comentario.store');
	Route::get('comentario/cliente/{comentario}/{cliente}', [ComentarioClienteController::class, 'edit'])->name('comentario.edit');
	Route::put('comentario/cliente/update/{comentario}/{cliente}', [ComentarioClienteController::class, 'update'])->name('comentario.update');
	Route::delete('comentario/cliente/delete/{comentario}', [ComentarioClienteController::class, 'destroy'] )->name('comentario.destroy');

	Route::resource('parametros', ParametroController::class);

	Route::resource('gateway', GatewayController::class);

	Route::resource('cbu', CbuController::class);

	Route::resource('CuentaCorriente', CuentaCorrienteController::class);

	Route::get('imputar/cliente/{clienteId}', [ImputarController::class, 'imputarAutomatico'])->name('imputar.cliente');
	Route::get('imputar/desimputar/pago/{idImputacion}', [ImputarController::class, 'desimputador'])->name('desimputar.pago');
	Route::get('imputar/lista/cliente/{cliente}', [ImputarController::class, 'imputarIndividual'])->name('imputar.cliente.recibos');
	Route::get('imputar/todo', [ImputarController::class, 'imputarTodo'])->name('imputar.todo');

	Route::get('credicard/{id}', [GatewayApiController::class, 'show']);
});
Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
