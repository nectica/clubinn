<?php

use App\Http\Controllers\api\CbuApiController;
use App\Http\Controllers\api\ClienteApiController;
use App\Http\Controllers\api\ConsultasApiController;
use App\Http\Controllers\api\CuentaCorrientesApiController;
use App\Http\Controllers\api\GatewayApiController;
use App\Http\Controllers\api\ImputarApiController;
use App\Http\Controllers\api\PlanesDecidirApiController;
use App\Http\Controllers\api\ReciboController;
use App\Http\Controllers\api\SemanaUsoApiController;
use App\Http\Controllers\api\TemporadaController;
use App\Http\Controllers\api\TokenClienteApiController;
use App\Http\Controllers\api\UnidadApiController;
use App\Http\Controllers\api\UnidadPrecioApiController;
use App\Http\Controllers\api\UsoApiController;
use App\Http\Controllers\ImputarController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('login.api')->group(function(){
	Route::resource('cliente', ClienteApiController::class);
	Route::resource('consulta', ConsultasApiController::class)->names('consulta')->except('show', 'update');
	Route::post('recibo/create', [ReciboController::class, 'store']);
	Route::post('cuentas/corrientes/cliente', [CuentaCorrientesApiController::class, 'getCuentasCorrientesByDate'])->name('cuentas.cliente');
	route::get('semana/uso/{uso}', [SemanaUsoApiController::class, 'getDatesByWeek']);
	Route::resource('uso', UsoApiController::class)->except('index');
	Route::get('usos', [UsoApiController::class, 'index'])->name('usos.index');
	Route::resource('cuenta/corriente', CuentaCorrientesApiController::class);
	Route::post('gateway/mercadoPago', [GatewayApiController::class, 'mercadoPagoPayment']);
	Route::post('gateway/decidir', [GatewayApiController::class, 'decidirPayment']);
	Route::resource('gateway', GatewayApiController::class);
	Route::resource('cbu', CbuApiController::class);
	Route::resource('precioUnidad', UnidadPrecioApiController::class);
	Route::resource('planes/decidir', PlanesDecidirApiController::class);

});
Route::get('recibos/voucher/{recibo}', [ReciboController::class, 'getReciboPdf'])->name('recibos.pdf');
/* Route::resource('temporadas', TemporadaController::class);
*/
Route::get('/', function () {

})->name('url.base.api');
Route::get('precioUnidad/{temporadaId}/{unidadId}/{anio}', [UnidadPrecioApiController::class, 'getPrecioUnidadByAnio'])->name('precio.api');
Route::get('temporada/hotel/{idHotel}', [TemporadaController::class, 'showByHotel'])->name('temporada.hotel');
Route::get('temporada/cliente/{idCliente}', [TemporadaController::class, 'showBycliente'])->name('temporada.cliente');
Route::get('consulta/{consulta}',[ConsultasApiController::class,  'show'])->name('consultas.show');
Route::post('consulta/respuesta/{consulta}',[ConsultasApiController::class,  'update'])->name('consultas.update');
Route::get('cliente/hotel/{hotelId}', [ClienteApiController::class, 'showByHotel'])->name('cliente.hotel');
Route::get('unidad/hotel/{hotel}', [UnidadApiController::class, 'unidadByHotel'])->name('unidad.hotel.api');
Route::get('unidad/cliente/{clienteId}/{temporadaId}', [UnidadApiController::class, 'unidadByClienteAndTemporada'])->name('unidad.cliente.api');
Route::post('password/recovery', [TokenClienteApiController::class, 'recoveryPassword']);

Route::post('imputar/cliente/{debeId}/{haberId}', [ImputarApiController::class, 'imputarRecibo']);

Route::post('recibo/imputacionesPosible/{recibo}', [ReciboController::class, 'getPosiblesImputaciones']);
