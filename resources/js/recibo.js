class Recibo {

	constructor() {
		this.selectElement;
		this.detectValue = [];
		this.idSeccionesToShow = [];
		this.idElementsToClear = [];
	}
	addDetectValueToChange(value) {
		this.addValueToArray(this.detectValue, value.toLowerCase());
	}
	addIdSeccionesToShow(id) {
		this.addValueToArray(this.idSeccionesToShow, id);
	}
	addIdElementsToClear(id) {
		this.addValueToArray(this.idElementsToClear, id);
	}
	addValueToArray(array, value) {
		array.push(value);
	}
	addChangesListener(id) {
		this.selectElement = document.querySelector('#' + id);
		this.selectElement.addEventListener('change', this.handlerChangeEvent);
	}
	logVar() {
		console.log(this.selectElement)
	}
	handlerChangeEvent = (evt) => {
		const selected = evt.target.querySelector('option:checked').innerText.trim();
		console.log(this.detectValue, this.detectValue.includes(selected.toLowerCase()));
		if (this.detectValue.includes(selected.toLowerCase())) {
			//mostrar
			this.toggleSecciones();
		} else {
			//ocultar elementos
			this.toggleSecciones('none');

		}
	}
	toggleSecciones(display = 'flex') {
		for (const idSeccion of this.idSeccionesToShow) {
			const seccion = this.lookingForElement(idSeccion);
			seccion.style.display = display;
		}
		if (display == 'none') {
			for (const idElement of this.idElementsToClear) {
				const element = this.lookingForElement(idElement);
				element.value = '';
			}
		}
	}

	lookingForElement(id) {
		return document.querySelector('#' + id);
	}
	firstTime(){
		this.handlerChangeEvent({target: 	this.selectElement});
	}
}



document.addEventListener('DOMContentLoaded', function () {
	const reciboTc = new Recibo();
	//valor usado para detectar cuando mostrar elementos
	reciboTc.addDetectValueToChange('Tarjeta de credito');
	//select que cambia
	reciboTc.addChangesListener('forma_pago_id');
	//secciones que se mostraran
	reciboTc.addIdSeccionesToShow('tc_container');
	reciboTc.addIdSeccionesToShow('nro_tarjeta_container');
	reciboTc.addIdSeccionesToShow('cuotas_container');
	reciboTc.addIdSeccionesToShow('lote_container');
	reciboTc.addIdSeccionesToShow('cupon_container');
	//elementos para resetear cuando cambia el select de forma de pago desde tarjeta de credito a otro
	reciboTc.addIdElementsToClear('tarjeta_credito_id');
	reciboTc.addIdElementsToClear('nro_tarjeta');
	reciboTc.addIdElementsToClear('cuotas');
	reciboTc.addIdElementsToClear('lote');
	reciboTc.addIdElementsToClear('cupon');
	reciboTc.firstTime();


	const reciboTransferencia = new Recibo();
	//valor usado para detectar cuando mostrar elementos
	reciboTransferencia.addDetectValueToChange('transferencia');
	reciboTransferencia.addDetectValueToChange('deposito');
	//select que cambia
	reciboTransferencia.addChangesListener('forma_pago_id');
	//secciones que se mostrara
	reciboTransferencia.addIdSeccionesToShow('cbu_container');
	//elementos para resetear cuando cambia el select de forma de pago desde tarjeta de credito a otro
	reciboTc.addIdElementsToClear('cbu_id');
	reciboTransferencia.firstTime();





}
);
