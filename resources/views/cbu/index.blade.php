@extends('layouts.layout')
@section('title', 'Cbu')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col p-0 d-flex align-items-center gap-title">
            <i class="fas fa-user-circle fa-3x" style="color: #e8a02f !important; "></i>
            <h1 class="title-clubinn">Cbu</h1>
        </div>
        <div class="col-12 col-xl-8 p-0 d-flex justify-content-end">
            <form class="col-8 input-group" action="" method="get">
                <input placeholder="Filtrar por banco, cbu, tipo de cuenta o titular." name="filter" class="form-control"
                    type="text" value="{{ old('filter', $filter) }}">
                <div class="input-group-append"><button class="btn btn-success " type="submit">Filtrar</button></div>
            </form>
            <div class="col">
                <a href="{{ route('cbu.create') }}" type="button"
                    class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Crear cbu</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($cbus->isEmpty())
                <p class="lead">No existen cbu cargado.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('banco', 'Banco', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('tipo_cuenta', 'Tipo de cuenta', [], ['class'
                            => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('cbu', 'Cbu', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('nro_cuenta', 'Numero de cuenta', [], ['class'
                            => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('titular', 'Titular', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>

                        @foreach ($cbus as $cbu)
                            <tr>
                                <td class="text-gergal-color text-white">{{ $cbu->banco }}</td>
                                <td class="text-gergal-color text-white">{{ $cbu->tipo_cuenta }}</td>
                                <td class="text-gergal-color text-white">{{ $cbu->cbu }}</td>
                                <td class="text-gergal-color text-white">{{ $cbu->nro_cuenta }}</td>
                                <td class="text-gergal-color text-white">{{ $cbu->titular }}</td>

                                <td>


                                    @can('cbu-edit')
                                        <a href="{{ route('cbu.edit', $cbu->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('cbu-delete')
                                        <form style="display: inline-block;" name="form-delete"
                                            action="{{ route('cbu.destroy', $cbu->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($cbus, 'appends') ? $cbus->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
    <script src="{{ asset('js/delete.js') }}"></script>
    <script type="text/javascript">

    </script>
@endsection
