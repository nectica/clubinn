@extends('partials.form.form')

@section('form-title', 'Cbu')
@section('form-route', route('cbu.update', $cbu->id))
@section('form-content')
    @method('PUT')
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Banco</span>
            </div>
            <input id="banco" placeholder="Banco" type="text"
                class="form-control @error('banco') is-invalid @enderror" name="banco" value="{{ old('banco', $cbu->banco) }}">
            @error('banco')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                       color: white;
                                                                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Titular</span>
            </div>
            <input id="titular" placeholder="titular" type="text"
                class="form-control @error('titular') is-invalid @enderror" name="titular" value="{{ old('titular', $cbu->titular) }}">
            @error('titular')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                       color: white;
                                                                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Cuil</span>
            </div>
            <input id="cuil" placeholder="cuil" type="text"
                class="form-control @error('cuil') is-invalid @enderror" name="cuil" value="{{ old('cuil', $cbu->cuil) }}">
            @error('cuil')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                       color: white;
                                                                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Numero de cuenta</span>
            </div>
            <input id="nro_cuenta" placeholder="Nro de cuenta" type="text"
                class="form-control @error('nro_cuenta') is-invalid @enderror" name="nro_cuenta" value="{{ old('nro_cuenta',$cbu->nro_cuenta) }}">
            @error('nro_cuenta')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                       color: white;
                                                                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Cbu</span>
            </div>
            <input id="cbu" placeholder="Cbu" type="text"
                class="form-control @error('cbu') is-invalid @enderror" name="cbu" value="{{ old('cbu', $cbu->cbu) }}">
            @error('cbu')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                       color: white;
                                                                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row" >
        <div class="col-md-12 px-5">
            <select class="form-control  @error('tipo_cuenta') is-invalid @enderror" id="tipo_cuenta"
                name="tipo_cuenta" placeholder="">
                <option selected disabled>-- Seleccione tipo de cuenta --</option>
                @foreach ($tipo_cuentas as $tipo_cuenta)
                    <option value="{{ $tipo_cuenta['nombre'] }}"
                        {{ old('tipo_cuenta', $cbu->tipo_cuenta) == $tipo_cuenta['nombre'] ? 'selected' : '' }}>
                        {{ $tipo_cuenta['nombre'] }}</option>
                @endforeach
            </select>
            @error('tipo_cuenta')
                <span class="invalid-feedback" style="background-color: red;
                                                                                                   color: white;
                                                                                                   margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar Cbu
            </button>
        </div>
    </div>
@endsection
