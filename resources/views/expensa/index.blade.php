@extends('layouts.layout')
@section('title', 'expensas')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
	<div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<h1 class="title-clubinn">Expensas</h1>
		</div>
		<div class="col-12 col-xl-6 p-0 d-flex justify-content-end">
			<form  class="col-8 input-group" action="" method="get">
				<input placeholder="Filtrar por Año, temporado o cliente." name="filter" class="form-control h-100" type="text" aria-describedby="basic-addon2">
				<div class="input-group-append">
					<button class="btn btn-success " id="basic-addon2" type="submit">Filtrar</button>
				</div>
			</form>

			<div class="col">
				<a href="{{ route('expensas.create') }}" type="button"
					class="btn btn-block btn-clubinn-blue clubinn-blue-color">Generar expensa</a>
			</div>
			<div class="col">
				<a href="{{ route('expensa.unica') }}" type="button"
					class="btn btn-block btn-clubinn-blue clubinn-blue-color">Expensa</a>
			</div>
		</div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($expensas->isEmpty())
                <p class="lead">No existen Expensas.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('unidad', 'Unidad', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('cliente.identificacion', 'Identificacion', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('cliente.nombre', 'Cliente', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('importe', 'Importe', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('anio', 'Año', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('observaciones', 'Observaciones', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('temporada.nombre', 'Temporada', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('', 'Hotel', [],  ['class' => ' clubinn-th-text'])</th>

                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($expensas as $expensa)
                            <tr>

                                <td class="text-gergal-color">{{ $expensa->unidad->unidadR->nombre }}</td>
                                <td class="text-gergal-color">{{ $expensa->cliente->identificacion }}</td>
                                <td class="text-gergal-color">{{ $expensa->cliente->full_name }}</td>
                                <td class="text-gergal-color">{{ $expensa->importe}}</td>
                                <td class="text-gergal-color">{{ $expensa->anio}}</td>
                                <td class="text-gergal-color">{{ $expensa->observaciones }}</td>
                                <td class="text-gergal-color">{{ $expensa->temporada->nombre }}</td>
                                <td class="text-gergal-color">{{ $expensa->temporada->hotelR->nombre }}</td>
                                <td style="min-width: 100px;">
                                    @can('expensa-edit')
                                        <a href="{{ route('expensas.edit', $expensa->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('expensa-delete')
                                        <form style="display: inline-block;"  name="form-delete" action="{{ route('expensas.destroy', $expensa->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($expensas, 'appends') ? $expensas->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
@endsection
