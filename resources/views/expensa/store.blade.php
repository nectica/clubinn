@extends('partials.form.form')

@section('form-title', 'Generar precios por unidad')
@section('form-route', route('expensas.unidad'))

@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de creacion</span>
            </div>
			<input type="hidden"  name="fecha" value="{{ $fecha }}">
            <input type="date" class="form-control  @error('fecha') is-invalid @enderror" id="fecha"
                placeholder="Fecha de inicio" value="{{ $fecha }}"  disabled/>
        </div>
        @error('fecha')
            <span class="invalid-feedback" style="background-color: red;
            color: white;
            margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2" >Fecha de vencimiento</span>
            </div>
			<input type="hidden" name="fecha_vencimiento" value="{{ $fecha_vencimiento }}">
            <input type="date" class="form-control  @error('fecha_vencimiento') is-invalid @enderror" id="fecha"
                 placeholder="Fecha de vencimiento"
                value="{{ $fecha_vencimiento }}" disabled />
        </div>
        @error('fecha_vencimiento')
            <span class="invalid-feedback" style="background-color: red;
            color: white;
            margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">

            <input type="text" class="form-control @error('nombre') is-invalid @enderror" value="{{ $hotel->nombre }}"
                disabled>
            <input type="hidden" name="hotel_id" value="{{ $hotel->id }}">
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="text" class="form-control @error('nombre') is-invalid @enderror" value="{{ $temporada->nombre }}"
                disabled>
            <input type="hidden" name="temporada_id" value="{{ $temporada->id }}">
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="text" class="form-control @error('anio') is-invalid @enderror" value="{{ $anio }}"
                disabled>
            <input type="hidden" name="anio" value="{{ $anio }}">

            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <input type="number" class="form-control" max="100" placeholder="Aumentar por porcetanje" id="percent-input">
            <div class="input-group-prepend">
                <button class="btn btn-success" type="button" id="percent-button">Aumentar</button>

            </div>
        </div>
    </div>
    @foreach ($hotel->habitaciones as $habitacion)


        <div class="form-row row mb-1">
            <div class="col">
                <input type="text" class="form-control @error('nombre') is-invalid @enderror"
                    value="{{ $habitacion->nombre }}" disabled>
                @error('nombre')
                    <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            @php
                $precio = $habitacion->precioAnual
                    ->where('anio', $anio - 1)
                    ->Where('temporada_id', $temporada->id)
                    ->first();
            @endphp
            <div class="col">
                <input type="text" class="form-control precio-percent" name="{{ $habitacion->id }}"
                    placeholder="Precio anual" value="{{ $precio ? $precio->precio : '' }}" required>
                @error('nombre')
                    <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endforeach


    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Cargar Precio
            </button>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function() {
            console.log('loaded');
            const inputPercent = document.querySelector('#percent-input');
            const buttonPercent = document.querySelector('#percent-button');
            buttonPercent.addEventListener('click', handlerClickPercent);
            inputPercent.addEventListener('keypress', handlerKeyPercent);

            function handlerClickPercent(evt) {
                const percent = inputPercent.value / 100;
                evt.preventDefault();
                if (!percent) {
                    alert('Debe selecccionar un porcentaje valido');
                } else {
                    const inputs = document.querySelectorAll('.precio-percent');
                    console.log(Array.from(inputs), inputs);
                    for (const input of Array.from(inputs)) {
                        console.log(input);
                        input.value *= (1 + percent);
                    }
                }

            }

            function handlerKeyPercent(evt) {
                if (evt.keyCode === 13) {
                    evt.preventDefault();
                    handlerClickPercent(evt);
                }
            }
        }
    </script>
@endsection
