@extends('layouts.layout')
@section('title', 'preciosUnidad por cliente')
@section('content')
@if ($message = Session::get('success'))
    <div class="row justify-content-center">
        <div class=" col-6  mt-2 alert alert-success">
            <p>{{ $message }}</p>
        </div>
    </div>
@endif
<div class="row h-120-px align-items-center justify-content-between">
    <div class="col p-0 d-flex align-items-center gap-title">
        <h1 class="title-clubinn">preciosUnidad por cliente</h1>
    </div>
    <div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
        <input type="search" placeholder="Buscar..." class="form-control search-input" data-table="customers-list" />

    </div>
</div>

<form action="{{ route('expensas.store') }}" method="POST">
    @csrf
    <input type="hidden" name="fecha" value="{{ $fecha }}">
    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($preciosUnidad->isEmpty())
                <p class="lead">No existen preciosUnidad.</p>
            @else
                <table class="table table-bordered table-clubinn customers-list">
                    <thead>
                        <th class="form-inline "> <input type="checkbox" class="form-check-input" id="marca" checked />
                            Marcar </th>
                        <th>Temporada</th>
                        <th>Unidad</th>
                        <th>Hotel</th>
                        <th>Cliente</th>
                        <th>Vencimiento</th>
                        <th>Expensa a</th>
                    </thead>
                    <tbody>
                        <input type="hidden" name="anio" value="{{ $anio }}">
                        <input type="hidden" name="temporada_id" value="{{ $temporada->id }}">
                        @foreach ($preciosUnidad as $precioUnidad)

                            @foreach ($precioUnidad->unidad->clientes()->wherePivot('temporada', $precioUnidad->temporada_id)->withPivot('id')->get()
    as $cliente)
                                <tr>
                                    <td>
                                        <div class="form-inline ">
                                            <input
                                                name="{{ 'cliente_' . $cliente->id . '_' . $precioUnidad->unidad_id . '_' . $cliente->pivot->id }}"
                                                class="form-check-input" type="checkbox" checked>
                                        </div>
                                    </td>
                                    <td>{{ $precioUnidad->temporada->nombre }}</td>
                                    <td>{{ $precioUnidad->unidad->nombre }}</td>
                                    <td>{{ $precioUnidad->unidad->hotelR->nombre }}</td>
                                    <td>{{ $cliente->full_name }}</td>
                                    <td><input class="form-control" type="date"
                                            name="{{ 'fecha_vencimiento_' . $cliente->id . '_' . $precioUnidad->unidad_id . '_' . $cliente->pivot->id }}"
                                            value="{{ $fecha_vencimiento }}"></td>
                                    <td> <input
                                            name="{{ $cliente->id . '_' . $precioUnidad->unidad_id . '_' . $cliente->pivot->id }}"
                                            class="form-control" type="text" value="{{ $precioUnidad->precio }}">
                                    </td>
                                </tr>

                            @endforeach
                        @endforeach

                    </tbody>
                </table>


            @endif

        </div>
    </div>
    <div class="row d-flex justify-content-end">
        <div class="col-4 col-xl-2">
            <button type="submit" class="btn btn-primary">
                Cargar expensas
            </button>
        </div>
    </div>
</form>


<script>
    window.onload = function() {
        marca = document.querySelector('#marca');
        marca.addEventListener('change', handlerChangeMarca);

        function handlerChangeMarca(evt) {
            const inputs = document.querySelectorAll('input[type=checkbox]');
            for (const input of inputs) {
                input.checked = evt.target.checked;
            }
        }


        (function(document) {
            'use strict';

            var TableFilter = (function(myArray) {
                var search_input;

                function _onInputSearch(e) {
                    search_input = e.target;
                    var tables = document.getElementsByClassName(search_input.getAttribute(
                        'data-table'));
                    myArray.forEach.call(tables, function(table) {
                        myArray.forEach.call(table.tBodies, function(tbody) {
                            myArray.forEach.call(tbody.rows, function(row) {
                                var text_content = row.textContent
                            .toLowerCase();
                                var search_val = search_input.value
                                .toLowerCase();
                                row.style.display = text_content.indexOf(
                                    search_val) > -1 ? '' : 'none';
                            });
                        });
                    });
                }

                return {
                    init: function() {
                        var inputs = document.getElementsByClassName('search-input');
                        myArray.forEach.call(inputs, function(input) {
                            input.oninput = _onInputSearch;
                        });
                    }
                };
            })(Array.prototype);
			TableFilter.init();


        })(document);
    }
</script>
@endsection
