@extends('partials.form.form')

@section('form-title', 'Actualizar Expensas')
@section('form-route', route('expensas.update', $expensa->id))
@section('form-content')
    @method('PUT')
	<div class="form-group row">
		<div class="col-md-12 px-5 input-group ">
			<div class="input-group-prepend">
				<span class="input-group-text" id="basic-addon1">$</span>
			  </div>
            <input id="importe" placeholder="Importe" type="text"
                class="form-control @error('importe') is-invalid @enderror" name="importe"
                value="{{ old('importe', $expensa->importe) }}">
            @error('importe')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="observaciones" placeholder="Observaciones" type="text"
                class="form-control @error('observaciones') is-invalid @enderror" name="observaciones"
                value="{{ old('observaciones', $expensa->observaciones) }}">
            @error('observaciones')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
		<div class="col-md-12 px-5 input-group">
			<div class="input-group-prepend">
				<span class="input-group-text" >Año</span>
			  </div>
			<input id="anio" placeholder="Año" type="text"
				class="form-control @error('anio') is-invalid @enderror" name="anio"
				value="{{ old('anio', $expensa->anio) }}">
			@error('anio')
				<span class="invalid-feedback" style="background-color: red;
							   color: white;
							   margin-top: 0;" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
		</div>
	</div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input  placeholder="" type="text"
                class="form-control @error('hotel') is-invalid @enderror" name="observaciones"
                value="{{ old('hotel', $expensa->unidad->hotelR->nombre) }}" disabled>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input  placeholder="" type="text"
                class="form-control @error('') is-invalid @enderror" name=""
                value="{{ old('', $expensa->unidad->unidadR->nombre) }}" disabled>
            @error('')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('cliente_id') is-invalid @enderror" id="cliente_id" name="cliente_id" placeholder="Cliente" disabled>
                <option selected disabled>-- Seleccione Cliente --</option>
                <option selected disabled>{{  $expensa->unidad->clienteR->full_name}}</option>
              {{--   @foreach ($clientes as $cliente)
                    <option value="{{ $cliente->id }}" {{ old('cliente',$expensa->cliente_id) == $cliente->id ? 'selected' : ''}}>{{ $cliente->full_name }}</option>
                @endforeach --}}
            </select>
            @error('cliente_id')
                <span class="invalid-feedback" style="background-color: red;
                   color: white;
                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar expensa
            </button>
        </div>
    </div>
@endsection
