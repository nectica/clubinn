@extends('partials.form.form')

@section('form-title', 'Selecccionar Hotel y temporada')
@section('form-route', route('expensas.choose'))
@section('method', 'GET')
@section('form-content')

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
				<span class="input-group-text" id="basic-addon2">Fecha de creacion</span>
			  </div>
            <input type="date" class="form-control  @error('fecha') is-invalid @enderror" id="fecha" name="fecha"
                placeholder="Fecha de inicio" value="{{ $now->format('Y-m-d') }}" />
        </div>
        @error('fecha')
            <span class="invalid-feedback" style="background-color: red;
                     color: white;
                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
				<span class="input-group-text" id="basic-addon2">Fecha de vencimiento</span>
			  </div>
            <input type="date" class="form-control  @error('fecha_vencimiento') is-invalid @enderror" id="fecha"
                name="fecha_vencimiento" placeholder="Fecha de vencimiento"
                value="{{ $now->addDays(10)->format('Y-m-d') }}" />
        </div>
        @error('fecha_vencimiento')
            <span class="invalid-feedback" style="background-color: red;
                     color: white;
                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('hotel_id') is-invalid @enderror" id="hotel_id" name="hotel_id"
                placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel_id') == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                                                   color: white;
                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="hidden" name="temporada_id" id="temporada_id">
            <input list="temporada_select" class="form-control  @error('temporada_id') is-invalid @enderror"
                id="temporada_input" name="temporada" placeholder="Temporada" autocomplete="off" />
            <datalist id="temporada_select" placeholder="temporada" required>
                <option selected disabled>-- Seleccione temporada --</option>

            </datalist> @error('temporada')
                <span class="invalid-feedback" style="background-color: red;
                                                   color: white;
                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('anio') is-invalid @enderror" id="anio" name="anio" placeholder="Año">
                <option selected disabled>-- Seleccione Año --</option>

                @for ($i = $anio - 5; $i < $anio + 10; $i++)
                    <option value="{{ $i }}" {{ old('anio') == $i ? 'selected' : '' }}>{{ $i }}
                    </option>
                @endfor
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                                                   color: white;
                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Cargar precio unidades
            </button>
        </div>
    </div>
    <script type="text/javascript">
        const $url = "{{ route('temporada.hotel', '') }}"
        window.onload = function() {
            $selectHotel = document.querySelector('#hotel_id');
            $temporadaSelect = document.querySelector('#temporada_select');
            $temporadaHidden = document.querySelector('#temporada_id');
            $temporada_input = document.querySelector('#temporada_input');

            $selectHotel.addEventListener('change', handlerChangeHotel);
            $temporada_input.addEventListener('change', handlerChangeClient);
            $temporadasStorage = getStorage('temporadas');
            if ($temporadasStorage) {
                loadTemporadas($temporadasStorage);
            }

            function handlerChangeHotel(evt) {

                clearClient();
                fetch($url + '/' + evt.target.value)
                    .then(response => response.json())
                    .then(json => {
                        console.log('json', json);
                        loadTemporadas(json.data);
                    });


            }
            async function handlerChangeClient(evt) {
                $value = evt.target.value;
                $option = $temporadaSelect.querySelector(`option[value="${$value}"]`);
                $temporadaHidden.value = $option.dataset.id;
            }

            function loadTemporadas(temporadas) {
                while ($temporadaSelect.lastChild) {
                    $temporadaSelect.removeChild($temporadaSelect.lastChild);
                }
                setStorage('temporadas', temporadas, 60);
                for (const temporada of temporadas) {
                    const option = document.createElement('option');
                    option.dataset.id = temporada.id;
                    option.setAttribute('value',
                        temporada.identificacion + ' - ' + temporada.nombre);
                    $temporadaSelect.appendChild(option);
                }
            }

            function clearClient() {
                while ($temporadaSelect.lastChild) {
                    $temporadaSelect.removeChild($temporadaSelect.lastChild);
                }

            }

            function getStorage(keyName) {
                const data = localStorage.getItem(keyName);
                if (!data) { // if no value exists associated with the key, return null
                    return null;
                }
                const item = JSON.parse(data);
                // If TTL has expired, remove the item from localStorage and return null
                if (Date.now() > item.ttl) {
                    localStorage.removeItem(keyName);
                    return null;
                }
                // return data if not expired
                return item.value;
            };
            /**
             * @param {string} keyName - A key to identify the value.
             * @param {any} keyValue - A value associated with the key.
             * @param {number} ttl- Time to live in seconds.
             */
            function setStorage(keyName, keyValue, ttl) {
                const data = {
                    value: keyValue, // store the value within this object
                    ttl: Date.now() + (ttl * 1000), // store the TTL (time to live)
                }
                // store data in LocalStorage
                localStorage.setItem(keyName, JSON.stringify(data));
            }
        }
    </script>
@endsection
