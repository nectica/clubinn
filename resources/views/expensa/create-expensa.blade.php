@extends('partials.form.form')

@section('form-title', 'Expensa unica de cliente.')
@section('form-route', route('expensa.unica.store'))

@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Año para usar</span>
            </div>
            <select class="form-control  @error('anio') is-invalid @enderror" id="anio" name="anio" placeholder="Año">
                <option selected disabled>-- Seleccione Año --</option>

                @for ($i = $anio - 5; $i < $anio + 10; $i++)
                    <option value="{{ $i }}" {{ old('anio') == $i ? 'selected' : '' }}>{{ $i }}
                    </option>
                @endfor
            </select>
            @error('anio')
                <span class="invalid-feedback" style="background-color: red;
                                                                                   color: white;
                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Observacion</span>
            </div>
            <input class="form-control @error('observaciones')  is-invalid @enderror" type="text" name="observaciones"
                id="observaciones" placeholder="Observacion" value="{{ old('observacion') }}">
            @error('observaciones')
                <span class="invalid-feedback" style="background-color: red;
                                                                                             color: white;
                                                                                             margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Semana para uso</span>
            </div>
            <input class="form-control @error('semana')  is-invalid @enderror" type="number" name="semana" id="semana"
                placeholder="Semana" value="{{ old('semana') }}">
            @error('semana')
                <span class="invalid-feedback" style="background-color: red;
                                                                                     color: white;
                                                                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Dias de uso</span>
            </div>
            <input class="form-control @error('dias')  is-invalid @enderror" type="number" name="dias" id="dias"
                placeholder="Nro de dias " value="{{ old('dias') }}">
            @error('dias')
                <span class="invalid-feedback" style="background-color: red;
                                                                                     color: white;
                                                                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    {{-- <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
				<span class="input-group-text" id="basic-addon2">Fecha de uso</span>
			  </div>
            <input type="date" class="form-control  @error('fecha') is-invalid @enderror" id="fecha" name="fecha"
                placeholder="Fecha de inicio" value="{{ $now->format('Y-m-d') }}" />
        </div>
        @error('fecha')
            <span class="invalid-feedback" style="background-color: red;
                     color: white;
                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div> --}}
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de vencimiento</span>
            </div>
            <input type="date" class="form-control  @error('fecha_vencimiento') is-invalid @enderror" id="fecha_vencimiento"
                name="fecha_vencimiento" placeholder="Fecha de vencimiento"
                value="{{ old('fecha_vencimiento', $now->addDays(10)->format('Y-m-d')) }}" />
        </div>
        @error('fecha_vencimiento')
            <span class="invalid-feedback" style="background-color: red;
                                                                                     color: white;
                                                                                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Hotel</span>
            </div>
            <select class="form-control  @error('hotel_id') is-invalid @enderror" id="hotel_id" name="hotel_id"
                placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel_id') == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Asignado a</span>
            </div>
            <input type="hidden" name="cliente_id" id="cliente_hidden" value="{{ old('cliente_id') }}">
            <input class="form-control  @error('cliente_id') is-invalid @enderror" placeholder="Cliente" list="cliente_id"
                id="cliente_input" name="cliente" autocomplete="off" value="{{ old('cliente') }}">

            <datalist id="cliente_id" placeholder="Cliente" required>
                {{-- @foreach ($clientes as $cliente)
                	<option >{{ $cliente->identificacion.' - ' .$cliente->dni.' - '.$cliente->full_name}}</option>

				@endforeach --}}
            </datalist>
            @error('cliente_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                           color: white;
                                                                                                                                           margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Temporada para el uso </span>
            </div>
            <input type="hidden" name="temporada_id" id="temporada_id" value="{{ old('temporada_id') }}">
            <input list="temporada_select" class="form-control  @error('temporada_id') is-invalid @enderror"
                id="temporada_input" name="temporada" placeholder="Temporada" autocomplete="off"
                value="{{ old('temporada') }}" />
            <datalist id="temporada_select" placeholder="temporada" required>
                <option selected disabled>-- Seleccione temporada --</option>

            </datalist> @error('temporada')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Unidad a usar</span>
            </div>
            <input type="hidden" name="unidad_id" id="unidad_hidden" value="{{ old('unidad_id') }}">
            <input type="hidden" name="unidades_por_cliente_id" id="unidades_por_cliente_id" value="{{ old('unidades_por_cliente_id') }}">
            <input class="form-control  @error('unidades_por_cliente_id') is-invalid @enderror" placeholder="Unidad" list="unidad_select"
                id="unidad_input" name="unidad" autocomplete="off" value="{{ old('unidad') }}">

            <datalist id="unidad_select" placeholder="unidad" required>

            </datalist>
            @error('unidades_por_cliente_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                           color: white;
                                                                                                                                           margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Importe</span>
            </div>
            <input class="form-control @error('importe')  is-invalid @enderror" type="text" name="importe" id="importe"
                placeholder="Importe" value="{{ old('importe') }}">
            @error('importe')
                <span class="invalid-feedback" style="background-color: red;
                                                                                             color: white;
                                                                                             margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear expensa
            </button>
        </div>
    </div>
    <script type="text/javascript">
        const $urlClienteByHotel = '{{ route('cliente.hotel', '') }}';
        const $urlTemporadaByCliente = "{{ route('temporada.cliente', '') }}";
        /* const $urlUnidadByHotel = "{{ route('unidad.hotel.api', '') }}"; */
        const URLbase = "{{ route('url.base.api') }}";

        window.onload = function() {
            $selectHotel = document.querySelector('#hotel_id');
            $selectClient = document.querySelector('#cliente_id');
            $clienteHidden = document.querySelector('#cliente_hidden');
            $clienteInput = document.querySelector('#cliente_input');

            $selectHotel.addEventListener('change', handlerChangeHotel);
            $clienteInput.addEventListener('change', handlerChangeClient);
            $clientesStorage = getStorage('clientes');
            if ($clientesStorage) {
                loadClientes($clientesStorage);
            }

            function handlerChangeHotel(evt) {

                clearElement($selectClient);
                clearElement($temporadaSelect);
                fetch($urlClienteByHotel + '/' + evt.target.value)
                    .then(response => response.json())
                    .then(json => {
                        loadClientes(json.data);
                    });




            }
            async function handlerChangeClient(evt) {
                $value = evt.target.value;
                $option = $selectClient.querySelector(`option[value="${$value}"]`);
                $clienteHidden.value = $option.dataset.id;

                fetch($urlTemporadaByCliente + '/' +  $option.dataset.id )
                    .then(response => response.json())
                    .then(json => {
                        loadTemporadas(json.data);
                    });
            }

            function loadClientes(clientes) {
                clearElement($selectClient);
                setStorage('clientes', clientes, 60);
                for (const cliente of clientes) {
                    const option = document.createElement('option');
                    option.dataset.id = cliente.id;
                    option.setAttribute('value', cliente.identificacion + ' - ' + cliente.dni + ' - ' + cliente
                        .nombre + ' ' + cliente.apellido);
                    $selectClient.appendChild(option);
                }
            }

            function clearElement(element) {
                while (element.lastChild) {
                    element.removeChild(element.lastChild);
                }

            }

            function getStorage(keyName) {
                const data = localStorage.getItem(keyName);
                if (!data) { // if no value exists associated with the key, return null
                    return null;
                }
                const item = JSON.parse(data);
                // If TTL has expired, remove the item from localStorage and return null
                if (Date.now() > item.ttl) {
                    localStorage.removeItem(keyName);
                    return null;
                }
                // return data if not expired
                return item.value;
            };
            /**
             * @param {string} keyName - A key to identify the value.
             * @param {any} keyValue - A value associated with the key.
             * @param {number} ttl- Time to live in seconds.
             */
            function setStorage(keyName, keyValue, ttl) {
                const data = {
                    value: keyValue, // store the value within this object
                    ttl: Date.now() + (ttl * 1000), // store the TTL (time to live)
                }
                // store data in LocalStorage
                localStorage.setItem(keyName, JSON.stringify(data));
            }
            $temporadaSelect = document.querySelector('#temporada_select');
            $temporadaHidden = document.querySelector('#temporada_id');
            $temporada_input = document.querySelector('#temporada_input');
            $temporada_input.addEventListener('change', handlerChangeTemporada);
            $temporadasStorage = getStorage('temporadas');
            if ($temporadasStorage) {
                loadTemporadas($temporadasStorage);
            }
            async function handlerChangeTemporada(evt) {
                $value = evt.target.value;
                $option = $temporadaSelect.querySelector(`option[value="${$value}"]`);
                $temporadaHidden.value = $option.dataset.id;
				fetch(URLbase + '/unidad/cliente/'+$clienteHidden.value+'/' + $option.dataset.id)
                    .then(response => response.json())
                    .then(json => {
                        console.log('json', json);
                        loadUnidad(json.data);
                    });
            }

            function loadTemporadas(temporadas) {
                clearElement($temporadaSelect);

                setStorage('temporadas', temporadas, 60);
                for (const temporada of temporadas) {
                    const option = document.createElement('option');
                    option.dataset.id = temporada.id;
                    option.setAttribute('value',
                        temporada.identificacion + ' - ' + temporada.nombre);
                    $temporadaSelect.appendChild(option);
                }
            }
            $unidadSelect = document.querySelector('#unidad_select');
            $unidadHidden = document.querySelector('#unidad_hidden');
            $unidadPorClienteHidden = document.querySelector('#unidades_por_cliente_id');
            $unidad_input = document.querySelector('#unidad_input');
            $unidad_input.addEventListener('change', handlerChangeUnidad);
            $unidadtorage = getStorage('unidad');
            if ($unidadtorage) {
                loadUnidad($unidadtorage);
            }

            function handlerChangeUnidad(evt) {
                $value = evt.target.value;
                $option = $unidadSelect.querySelector(`option[value="${$value}"]`);
                $unidadPorClienteHidden.value = $option.dataset.id;
                $unidadHidden.value = $option.dataset.unidadId;
                const precio = new PrecioUnidad();
                precio.urlbase = URLbase;
                precio.anioId = 'anio';
                precio.unidadId = 'unidad_hidden';
                precio.temporadaId = 'temporada_id';
                precio.precioId = 'importe';

                precio.fetchPrecio();
            }

            function loadUnidad(unidades) {
                clearElement($unidadSelect);
                setStorage('unidad', unidades, 60);
                console.log('unidad loaded', unidades);
                for (const unidad of unidades) {
					for (const unidadPorCliente of unidad.unidad_por_cliente) {
						const option = document.createElement('option');
						option.dataset.id = unidadPorCliente.id;
						option.dataset.unidadId = unidad.id;
						option.setAttribute('value', unidadPorCliente.id+' ) Semana: '+unidadPorCliente.semana +' - '+unidad.nombre);
						$unidadSelect.appendChild(option);

					}
                }
            }
            const anio = document.querySelector('#anio');
            const fechaVencimiento = document.querySelector('#fecha_vencimiento');
            anio.addEventListener('change', handlerChangeAnio);

            function handlerChangeAnio(evt) {



                const newDate = evt.target.value + fechaVencimiento.value.slice(fechaVencimiento.value.indexOf('-'));
                fechaVencimiento.value = newDate;
                fechaVencimiento.min = newDate;
            }

            class PrecioUnidad {
                constructor() {
                    this.urlbase, this.temporadaId, this.anioId, this.unidadId, this.precioId;
                }

                getElementById(id) {
                    return document.querySelector('#' + id);
                }


                getInputValue(id) {

                    return this.getElementById(id).value;
                }
                setInputValue(id, value) {

                    this.getElementById(id).value = value;
                }
                getTemporada() {
                    return this.getInputValue(this.temporadaId);
                }
                getAnio() {
                    return this.getInputValue(this.anioId);
                }


                getUnidad() {
                    return this.getInputValue(this.unidadId);
                }

                fetchPrecio() {
                    const temporadaId = this.getTemporada(),
                        unidadId = this.getUnidad(),
                        anio = this.getAnio();
                    if (!temporadaId) {
                        alert('seleccione temporada.')
                        return false;
                    }
                    if (!unidadId) {
                        alert('seleccione unidad.')
                        return false;
                    }
                    if (!anio) {
                        alert('seleccione año.')
                        return false;
                    }
                    fetch(`${this.urlbase}/precioUnidad/${temporadaId}/${unidadId}/${anio}`)
                        .then(response => response.json())
                        .then(this.handlerFetch)
                }
                handlerFetch = (response) => {
                    if (response.status = 'ok' && response.data) {
                        this.setInputValue(this.precioId, response.data.precio);
                    }
                }
            }


        }
    </script>

@endsection
