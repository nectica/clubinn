@extends('layouts.layout')

@section('content')
    <div class="row pt-1  justify-content-center min-vh-75 align-items-center">
        <div class="col-10 col-md-6">
            <div class="card pt-5">
                <div class="d-flex pt-2  mb-2 justify-content-center h-50">
                    <h3 class="text-center">@yield('form-title')</h3>
                </div>
                <div class="card-body">


                    <form method="@yield('method', 'POST')" action="@yield('form-route')" enctype='multipart/form-data'>
                        @csrf
                        @yield('form-content')

                </div>
                </form>
            </div>
        </div>
    </div>

@endsection
