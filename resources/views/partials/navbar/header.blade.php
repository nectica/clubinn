<nav class="navbar  navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('storage/images/Logo.jpg') }}" height="80" alt="{{ config('app.name', 'Laravel') }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto gap-menu">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li
                        class="nav-item dropdown  {{ request()->route()->getName() == 'cliente.index'
    ? 'active-tab'
    : '' }}">
                        <a class="nav-link " href="{{ route('cliente.index') }}">
                            <strong>Clientes</strong>
                        </a>
                    </li>
                    <li
                        class="nav-item dropdown {{ request()->route()->getName() == 'recibos.index'
    ? 'active-tab'
    : '' }}">
                        <a class="nav-link " href="{{ route('recibos.index') }}">
                            <strong>Pagos</strong>
                        </a>
                    </li>
                    <li
                        class="nav-item dropdown {{ request()->route()->getName() == 'consultas.index'
    ? 'active-tab'
    : '' }}">
                        <a class="nav-link " href="{{ route('consultas.index') }}">
                            <strong>Consultas</strong>
                        </a>
                    </li>
                    @can('expensa-list')
                        <li
                            class="nav-item dropdown  {{ request()->route()->getName() == 'expensas.index'
    ? 'active-tab'
    : '' }}">

                            <a class="nav-link " href="{{ route('expensas.index') }}">
                                <strong>

                                    Expensas
                                </strong>
                            </a>
                        </li>
                    @endcan
                    @can('expensa-list')
                        <li
                            class="nav-item dropdown  {{ request()->route()->getName() == 'usos.index'
    ? 'active-tab'
    : '' }}">
                            <a class="nav-link" href="{{ route('usos.index') }}">
                                <strong>
                                    Usos
                                </strong>
                            </a>
                        </li>
                    @endcan
                    @can('unidad-list')
                        <li
                            class="nav-item dropdown  {{ request()->route()->getName() == 'cliente.saldo'
    ? 'active-tab'
    : '' }}">
                            <a class="nav-link"
                                {{ request()->route()->getName() == 'cliente.saldo'
    ? 'active-tab'
    : '' }}"
                                href="{{ route('cliente.saldo') }}">
                                <strong>

                                    Saldos
                                </strong>
                            </a>
                        </li>
                    @endcan
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle border-menu" href="#" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <strong>{{ Auth::user()->name }}</strong>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            @can('temporada-list')
                                <a class="dropdown-item  {{ request()->route()->getName() == 'temporada.index'
    ? 'active-tab'
    : '' }}"
                                    href="{{ route('temporada.index') }}">
                                    Temporadas
                                </a>
                            @endcan
                            @can('temporada-list')
                                <a class="dropdown-item  {{ request()->route()->getName() == 'cbu.index'
    ? 'active-tab'
    : '' }}"
                                    href="{{ route('cbu.index') }}">
                                    Cbu
                                </a>
                            @endcan
                            @can('unidad-list')
                                <a class="dropdown-item  {{ request()->route()->getName() == 'unidad.index'
    ? 'active-tab'
    : '' }}"
                                    href="{{ route('unidad.index') }}">
                                    Unidad
                                </a>
                            @endcan
                            @can('unidad-list')
                                <a class="dropdown-item  {{ request()->route()->getName() == 'hotel.index'
    ? 'active-tab'
    : '' }}"
                                    href="{{ route('hotel.index') }}">
                                    Calendario
                                </a>
                            @endcan
							@can('parametro-list')
                                <a class="dropdown-item  {{ request()->route()->getName() == 'parametros.index'
    ? 'active-tab'
    : '' }}"
                                    href="{{ route('parametros.index') }}">
                                    Parametro
                                </a>
                            @endcan
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
