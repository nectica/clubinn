@extends('layouts.layout')
@section('title', 'Consultas')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col p-0 d-flex align-items-center gap-title">
            <h1 class="title-clubinn"> Consultas</h1>
        </div>
        <div class="col-8 col-xl-6 p-0 d-flex justify-content-end">
            <form class="form-inline" action="" method="get">
                <div class="form-group mx-2">
                    <select class="form-control"  id="filterStatus" name="filter">
                        <option value="" selected disabled>Filtrar por estado</option>
                        <option value="all" {{ $estadoToFilter == 'all' ? 'selected' : '' }}>Todos</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->nombre }}"
                                {{ $estadoToFilter == $estado->nombre ? 'selected' : '' }}>
                                {{ $estado->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mx-2">
                    <input placeholder="Filtrar por Año o cliente." name="filterText" class="form-control h-100" type="text"
                        aria-describedby="basic-addon2" value="{{ $filterBytext}}">
                </div>
                <div class="form-group mx-2">
                    <button class="btn btn-block btn-clubinn-blue clubinn-blue-color" type="submit" class>Filtrar</button>
                </div>
            </form>
            </form>

        </div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($consultas->isEmpty())
                <p class="lead">No existen consultas.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('estado.nombre', 'Estado', [], ['class' => '
                            clubinn-th-text']) </th>
                        <th scope="col" class="text-nowrap ">@sortablelink('cliente.identificacion', 'Identificacion', [],
                            ['class' => '
                            clubinn-th-text']) </th>
                        <th scope="col" class="text-nowrap ">@sortablelink('cliente.nombre', 'Nombre', [], ['class' => '
                            clubinn-th-text']) </th>
                        <th scope="col" class="text-nowrap ">@sortablelink('cliente.apellido', 'Apellido', [], ['class' =>
                            '
                            clubinn-th-text']) </th>
                        <th scope="col" class="text-nowrap">@sortablelink('updated_at', 'Fecha', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('cliente.hotel', 'Hotel', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($consultas as $consulta)
                            <tr>
                                <td class="text-gergal-color"> <span
                                        class="status {{ $consulta->estado->nombre }}"></span>
                                    {{ $consulta->estado->nombre }}</td>
                                <td class="text-gergal-color">{{ $consulta->cliente->identificacion }}</td>
                                <td class="text-gergal-color">{{ $consulta->cliente->nombre }}</td>
                                <td class="text-gergal-color">{{ $consulta->cliente->apellido }}</td>
                                <td class="text-gergal-color">{{ $consulta->updated_at->format('d/m/Y') }}</td>
                                <td class="text-gergal-color">{{ $consulta->cliente->hotelR->nombre }}</td>

                                <td>
                                    @can('consulta-edit')
                                        <button {{-- href="{{ route('consultas.edit', $consulta->id) }}" --}} data-id="{{ $consulta->id }}" type="button"
                                            data-toggle="modal" data-target="#myModal"
                                            class="btn btn-outline-primary btn-sm  my-1 edit-button"> <i
                                                class="far fa-eye"></i>
                                        </button>
                                    @endcan
                                    @can('consulta-delete')
                                        {{-- <form style="display: inline-block;" name="form-delete"
                                            action="{{ route('consultas.destroy', $consulta->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form> --}}
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($consultas, 'appends') ? $consultas->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
    {{-- modal --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row table-responsive mx-0">
                        <table class="table table-bordered table-clubinn">
                            <thead>
                                <th scope="col" class="text-nowrap clubinn-th-text">Nombre</th>
                                <th scope="col" class="text-nowrap clubinn-th-text">Apellido</th>
                                <th scope="col" class="text-nowrap clubinn-th-text">Fecha</th>
                                <th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id="nombreTd"></td>
                                    <td id="apellidoTd"></td>
                                    <td id="fechaTD"></td>
                                    <td id="hotelTD"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row justify-content-center pb-5">
                        <div class="col-12 col-xl-10">
                            <p class="clubinn-th-text"> <strong id="titulo-consulta">Consultas</strong> </p>
                            <p class="text-dark" id="consulta"></p>
                        </div>
                    </div>
                    <div class="row justify-content-center ">
                        <div class="col-12 col-xl-10">
                            <p class="clubinn-th-text"> <strong id="titulo-respuesta"></strong> </p>
                            <p class="text-dark" id="info-respuesta"></p>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="form-group col-12 col-xl-10">
                            <label class="clubinn-th-text" for="respuesta"><strong>Respuesta</strong> </label>
                            <textarea class="form-control" name="respuesta" id="respuesta"></textarea>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="form-group col-12 col-xl-4">
                            <button type="button" class="btn btn-block btn-clubinn-blue clubinn-blue-color"
                                id="enviar">Enviar</button>
                        </div>
                    </div>
                </div>
                {{-- <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- endmodal --}}
    <script type="text/javascript">
        window.onload = function() {
            const route = '{{ route('consulta.index') }}';
            const buttons = document.querySelectorAll('.edit-button');
            const enviarButton = document.querySelector('#enviar');
            enviarButton.addEventListener('click', handlerEnviar);
            for (const button of buttons) {
                button.addEventListener('click', handlerClick);
            }

            function handlerClick(evt) {
                evt.preventDefault();
                clearText(['nombreTd', 'apellidoTd', 'fechaTD', 'hotelTD', 'titulo-consulta', 'consulta',
                    'titulo-respuesta', 'info-respuesta'
                ]);
                const input = document.querySelector('#respuesta');
                input.value = null;
                const button = evt.currentTarget;
                const idConsulta = button.dataset.id;
                fetch(`${route}/${idConsulta}`)
                    .then(response => response.json())
                    .then(handlerModal)
            }

            function handlerModal(data) {
                const consulta = data.data;
                const input = document.querySelector('#respuesta');
                input.dataset.id = consulta.id;
                input.disabled = false;
                renderText('#nombreTd', consulta.cliente.nombre);
                renderText('#apellidoTd', consulta.cliente.apellido);
                renderText('#fechaTD', consulta.fecha);
                renderText('#hotelTD', consulta.hotel);
                renderText('#titulo-consulta', consulta.asunto);
                renderText('#consulta', consulta.consulta);
                if (consulta.respuesta) {
                    renderText('#titulo-respuesta', 'Respuesta');
                    renderText('#info-respuesta', consulta.respuesta);
                    input.disabled = true;
                }
            }

            function clearText(ids) {
                for (const id of ids) {

                    renderText(`#${id}`, '');
                }

            }

            function handlerEnviar(evt) {
                const respuesta = document.querySelector('#respuesta');
                const idConsulta = respuesta.dataset.id;
                const endPoint = `${route}/respuesta/${idConsulta}`;
                let value = respuesta.value.trim();
                if (!value) {
                    alert('La respuesta no puede ser vacia.');
                    return;
                }
                const formData = new FormData();
                formData.append('respuesta', value);
                fetch(endPoint, {
                        method: 'POST',
                        body: formData,
                        headers: {
                            'Accept': 'application/json'
                        },
                    }).then(response => {
                        if (response.status == 422) {
                            /* alert("Error, vuelva intentar."); */
                            throw new Error(response.json());
                        }
                        return response.json();
                    })
                    .then((data) => {

                        location.reload();
                    }).catch(except => {
                        alert("Error, vuelva intentar.");
                    })
            }

            function renderText(id, text) {
                const element = document.querySelector(id);
                element.innerText = '';
                element.appendChild(document.createTextNode(text));
            }

            filter = document.querySelector('#filterStatus');
/*             filter.addEventListener('change', handlerChangeFilter);
 */            if (window.location.search) {
                const parameter = window.location.search
                const value = parameter.slice(parameter.indexOf('=') + 1);
                filter.querySelector(`option[value="${value}"]`).selected = true;
            }

            function handlerChangeFilter(evt) {
                window.location.assign(window.location.origin + window.location.pathname + '?filter=' + filter.value)
            }
        }
    </script>
@endsection
