@extends('layouts.layout')
@section('title', 'Parametros')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col-12 col-xl-2 p-0 d-flex align-items-center gap-title">
            <h1 class="title-clubinn">Parametros.</h1>
        </div>
        <div class="col row justify-content-end">
            <div class="col-8 col-xl-2 p-0 d-flex justify-content-end align-items-center">
                @can('parametro-create')
                    <a href="{{ route('parametros.create') }}" type="button"
                        class="btn btn-block btn-clubinn-blue clubinn-blue-color" style=" height: max-content;">Crear
                        parametros</a>
                @endcan
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($parametros->isEmpty())
                <p class="lead">No existen parametros cargados.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap clubinn-th-text">Nombre</th>
                        <th scope="col" class="text-nowrap clubinn-th-text">Valor</th>

                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>

                        @foreach ($parametros as $tasa)
                            <tr>
                                <td class="text-gergal-color text-white">{{ $tasa->nombre }}</td>
                                <td class="text-gergal-color text-white">{{ $tasa->valor }}</td>
                                <td>
                                    @can('parametro-edit')
                                        <a href="{{ route('parametros.edit', $tasa->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($parametros, 'appends') ? $parametros->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>

@endsection
