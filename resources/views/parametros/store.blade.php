@extends('partials.form.form')

@section('form-title', 'Parametros')
@section('form-route', route('parametros.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nombre</span>
            </div>
            <input id="nombre" placeholder="Nombre" type="text" class="form-control @error('nombre') is-invalid @enderror"
                name="nombre" value="{{ old('nombre') }}">
            @error('nombre')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                               color: white;
                                                                                                                               margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Valor</span>
            </div>
            <input id="valor" placeholder="Valor" type="text" class="form-control @error('valor') is-invalid @enderror"
                name="valor" value="{{ old('valor') }}">
            @error('valor')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                               color: white;
                                                                                                                               margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear parametro
            </button>
        </div>
    </div>


@endsection
