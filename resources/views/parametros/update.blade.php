@extends('partials.form.form')

@section('form-title', 'parametos')
@section('form-route', route('parametros.update', $parametro->id))
@section('form-content')
    @method('PUT')
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">{{$parametro->nombre}}</span>
            </div>
            <input id="valor" placeholder="valor" type="text" class="form-control @error('valor') is-invalid @enderror"
                name="valor" value="{{ old('valor',  $parametro->valor) }}">
            @error('valor')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                               color: white;
                                                                                                                               margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar Parametro
            </button>
        </div>
    </div>
@endsection
