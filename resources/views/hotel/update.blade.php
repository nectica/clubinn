@extends('partials.form.form')

@section('form-title', 'Calendario')
@section('form-route', route('hotel.update', $hotel->id))
@section('form-content')
    @method('PUT')
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input
			class="form-control "

                value="{{ $hotel->nombre }}" disabled>
            @error('calendario')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="calendario" placeholder="calendario" type="file"
                class="form-control @error('calendario') is-invalid @enderror" name="calendario"
                value="{{ old('calendario') }}">
            @error('calendario')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>


    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar calendario
            </button>
        </div>
    </div>
@endsection
