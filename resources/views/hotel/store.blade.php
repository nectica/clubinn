@extends('partials.form.form')

@section('form-title', 'Hotel')
@section('form-route', route('hotel.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="nombre" placeholder="nombre" type="text"
                class="form-control @error('nombre') is-invalid @enderror" name="nombre"
                value="{{ old('nombre') }}">
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear
            </button>
        </div>
    </div>
@endsection
