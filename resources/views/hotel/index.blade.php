@extends('layouts.layout')
@section('title', 'Hoteles')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif

    <div class="row mt-5 justify-content-between">
        <h1>Hoteles</h1>
        <div class="col-4">
           {{--  <a href="{{ route('hotel.create') }}" type="button" class="btn btn-outline-success btn-sm  my-1"> Crear
                Hotel</a> --}}
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-11 table-responsive">
            @if ($hotels->isEmpty())
                <p class="lead">No existen Hoteles.</p>
            @else
                <table class="table table-bordered">
                    <thead>
                        <th scope="col">@sortablelink('nombre', 'Nombre')</th>
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($hotels as $hotel)
                            <tr>
                                <td>{{ $hotel->nombre }}</td>

                                <td>
                                    @can('hotel-edit')
                                        <a href="{{ route('hotel.edit', $hotel->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1">Cargar Calendario <i
                                                class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('hotel-edit')
									@if ($hotel->calendario)

									<a href="{{ route('hotel.calendario', $hotel->id) }}" type="button"
										class="btn btn-outline-primary btn-sm  my-1">Calendario <i
										class="far fa-eye"></i></a>
									@endif
                                    @endcan
                                    @can('hotel-delete')
                                        {{-- <form style="display: inline-block;"  name="form-delete" action="{{ route('hotel.destroy', $hotel->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form> --}}
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($hotels, 'appends') ? $hotels->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
@endsection
