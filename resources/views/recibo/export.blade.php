<table class="table table-bordered table-clubinn">
	<thead>
		<tr>
			<th scope="col" class="text-nowrap">Nro</th>
			<th scope="col" class="text-nowrap">Fecha del pago</th>
			<th scope="col" class="text-nowrap">Cliente</th>
			<th scope="col" class="text-nowrap">cuil</th>
			<th scope="col" class="text-nowrap">Importe</th>
			<th scope="col" class="text-nowrap">Estado</th>
			<th scope="col" class="text-nowrap">comprobante</th>
			<th scope="col" class="text-nowrap">Año</th>
			<th scope="col" class="text-nowrap">Temporada</th>
			<th scope="col" class="text-nowrap">Metodo de pago</th>
			<th scope="col" class="text-nowrap">Tarjeta</th>
			<th scope="col" class="text-nowrap">cuotas</th>

		</tr>
	</thead>
	<tbody>

		@foreach ($recibos as $recibo)
			<tr>
				<td class="text-gergal-color text-white">{{ $recibo->nro}}</td>
				<td class="text-gergal-color text-white">{{ $recibo->fecha_pago}}</td>
				<td class="text-gergal-color text-white">{{ $recibo->cliente }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->cuil }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->importe ?$recibo->importe. ' $': '-' }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->estado }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->comprobante }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->anio }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->temporada }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->nombre_forma_pago }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->tarjeta_credito ?: '-' }}</td>
				<td class="text-gergal-color text-white">{{ $recibo->cuotas }}</td>


			</tr>
		@endforeach
	</tbody>
</table>
