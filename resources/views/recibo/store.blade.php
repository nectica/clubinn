@extends('partials.form.form')

@section('form-title', 'Recibo o Ajuste')
@section('form-route', route('recibos.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">$</span>
            </div>
            <input id="importe" placeholder="Importe" type="text" class="form-control @error('importe') is-invalid @enderror"
                name="importe" value="{{ old('importe') }}">
            @error('importe')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                       color: white;
                                                                                                                                       margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de pago</span>
            </div>
            <input type="date" class="form-control  @error('fecha_pago') is-invalid @enderror" id="fecha_pago"
                name="fecha_pago" placeholder="Fecha de vencimiento" value="{{ $now->addDays(10)->format('Y-m-d') }}" />
        </div>
        @error('fecha_pago')
            <span class="invalid-feedback" style="background-color: red;
                                                             color: white;
                                                             margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">

            <input type="hidden" class="form-control " name="" value="Ajuste de saldo">
            @error('anio')
                <span class="invalid-feedback" style="background-color: red;
                                                                                                               color: white;
                                                                                                               margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="observaciones" placeholder="Observaciones" type="text"
                class="form-control @error('observaciones') is-invalid @enderror" name="observaciones"
                value="{{ old('observaciones') }}">
            @error('observaciones')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                               color: white;
                                                                                                                               margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('movimiento_id') is-invalid @enderror" id="movimiento_id"
                name="movimiento_id" placeholder="movimiento_id">
                <option selected disabled>-- Seleccione Ajuste --</option>
                <option value="{{ $recibo->id }}" {{ old('movimiento_id') == $recibo->id ? 'selected' : '' }}>
                    {{ $recibo->nombre }}</option>
                @foreach ($movimientos as $movimiento)
                    <option value="{{ $movimiento->id }}"
                        {{ old('movimiento_id') == $movimiento->id ? 'selected' : '' }}>{{ $movimiento->nombre }}
                    </option>
                @endforeach
            </select>
            @error('movimiento_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="recibo-container" style="display: none;">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="voucher">Recibo</span>
            </div>
            <input type="file" class="form-control  @error('voucher') is-invalid @enderror" id="voucher" name="voucher"
                placeholder="voucher" />
            @error('voucher')
                <span class="invalid-feedback" style="background-color: red;
                       color: white;
                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="pago-container">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('forma_pago_id') is-invalid @enderror" id="forma_pago_id"
                name="forma_pago_id" placeholder="">
                <option selected disabled>-- Seleccione Forma de pago --</option>
                @foreach ($formasPagos as $FormaPago)
                    <option value="{{ $FormaPago->id }}"
                        {{ old('forma_pago_id') == $FormaPago->id ? 'selected' : '' }}>
                        {{ $FormaPago->nombre }}</option>
                @endforeach
            </select>
            @error('forma_pago_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="cbu_container" style="display:none">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('cbu_id') is-invalid @enderror" id="cbu_id" name="cbu_id"
                placeholder="cbu_id">
                <option selected disabled>-- Seleccione Cbu --</option>

                @foreach ($cbus as $cbu)
                    <option value="{{ $cbu->id }}" {{ old('cbu_id') == $cbu->id ? 'selected' : '' }}>
                        {{ $cbu->banco . ' - ' . $cbu->cbu }}
                    </option>
                @endforeach
            </select>
            @error('cbu_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="tc_container" style="display:none">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('tarjeta_credito_id') is-invalid @enderror" id="tarjeta_credito_id"
                name="tarjeta_credito_id" placeholder="tarjeta_credito_id">
                <option selected disabled>-- Seleccione tipo de Tarjeta --</option>

                @foreach ($tcs as $tc)
                    <option value="{{ $tc->id }}" {{ old('tarjeta_credito_id') == $tc->id ? 'selected' : '' }}>
                        {{ $tc->nombre }}
                    </option>
                @endforeach
            </select>
            @error('tarjeta_credito_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="nro_tarjeta_container" style="display:none">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Ultimos 4 digitos de la tarjeta</span>
            </div>
            <input id="nro_tarjeta" placeholder="N° de tarjeta" type="number"
                class="form-control @error('nro_tarjeta') is-invalid @enderror" name="nro_tarjeta"
                value="{{ old('nro_tarjeta') }}">
            @error('nro_tarjeta')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                       color: white;
                                                                                                                                       margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="cuotas_container" style="display:none">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Cuotas</span>
            </div>
            <input id="cuotas" placeholder="Cuotas" type="number" class="form-control @error('cuotas') is-invalid @enderror"
                name="cuotas" value="{{ old('cuotas') }}">
            @error('cuotas')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                       color: white;
                                                                                                                                       margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="lote_container" style="display:none">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Lote</span>
            </div>
            <input id="lote" placeholder="lote" type="text" class="form-control @error('lote') is-invalid @enderror"
                name="lote" value="{{ old('lote') }}">
            @error('lote')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                       color: white;
                                                                                                                                       margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="cupon_container" style="display:none">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Cupon</span>
            </div>
            <input id="cupon" placeholder="cupon" type="text" class="form-control @error('cupon') is-invalid @enderror"
                name="cupon" value="{{ old('cupon') }}">
            @error('cupon')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                       color: white;
                                                                                                                                       margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('hotel') is-invalid @enderror" id="hotel" name="hotel" placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel') == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="hidden" name="cliente_id" id="cliente_hidden" value="{{ old('cliente_id') }}">
            <input class="form-control  @error('cliente') is-invalid @enderror" placeholder="Cliente" list="cliente_id"
                id="cliente_input" name="cliente" autocomplete="off" value="{{ old('cliente') }}">

            <datalist id="cliente_id" placeholder="Cliente" required>
                <option selected disabled>-- Seleccione Cliente --</option>

            </datalist>
            @error('cliente')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                   color: white;
                                                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear recibo
            </button>
        </div>
    </div>


@endsection
<script src=" {{ asset('js/recibo.js') }}"></script>
<script type="text/javascript">
    $url = '{{ route('cliente.hotel', '') }}';
    window.onload = function() {
        $selectHotel = document.querySelector('#hotel');
        $selectClient = document.querySelector('#cliente_id');
        $clienteHidden = document.querySelector('#cliente_hidden');
        $clienteInput = document.querySelector('#cliente_input');


        $selectHotel.addEventListener('change', handlerChangeHotel);
        $clienteInput.addEventListener('change', handlerChangeClient);
        $clientesStorage = getStorage('clientes');
        if ($clientesStorage) {
            loadClientes($clientesStorage);
        }

        function handlerChangeHotel(evt) {

            clearClient();
            fetch($url + '/' + evt.target.value)
                .then(response => response.json())
                .then(json => {
                    loadClientes(json.data);
                });


        }
        async function handlerChangeClient(evt) {
            $value = evt.target.value;
            $option = $selectClient.querySelector(`option[value="${$value}"]`);
            $clienteHidden.value = $option.dataset.id;
        }

        function loadClientes(clientes) {
            while ($selectClient.lastChild) {
                $selectClient.removeChild($selectClient.lastChild);
            }
            setStorage('clientes', clientes, 600);
            for (const cliente of clientes) {
                const option = document.createElement('option');
                option.dataset.id = cliente.id;
                option.setAttribute('value', cliente.identificacion + ' - ' + cliente.dni + ' - ' + cliente
                    .nombre + ' ' + cliente.apellido);
                $selectClient.appendChild(option);
            }
        }

        function clearClient() {
            while ($selectClient.lastChild) {
                $selectClient.removeChild($selectClient.lastChild);
            }
            /*   const option = document.createElement('option');
              option.appendChild(document.createTextNode('-- Seleccione Cliente --'));
              option.setAttribute('disabled', true);
              option.setAttribute('selected', true);
              $selectClient.appendChild(option); */
        }

        function getStorage(keyName) {
            const data = localStorage.getItem(keyName);
            if (!data) { // if no value exists associated with the key, return null
                return null;
            }
            const item = JSON.parse(data);
            // If TTL has expired, remove the item from localStorage and return null
            if (Date.now() > item.ttl) {
                localStorage.removeItem(keyName);
                return null;
            }
            // return data if not expired
            return item.value;
        };
        /**
         * @param {string} keyName - A key to identify the value.
         * @param {any} keyValue - A value associated with the key.
         * @param {number} ttl- Time to live in seconds.
         */
        function setStorage(keyName, keyValue, ttl) {
            const data = {
                value: keyValue, // store the value within this object
                ttl: Date.now() + (ttl * 1000), // store the TTL (time to live)
            }
            // store data in LocalStorage
            localStorage.setItem(keyName, JSON.stringify(data));
        }

        const movimientoSelect = document.querySelector('#movimiento_id');
        movimientoSelect.addEventListener('change', handlerChangeMovimiento);
        checkMovimientoId(movimientoSelect.value);

        function handlerChangeMovimiento(evt) {
            checkMovimientoId(evt.target.value);
        }

        function checkMovimientoId(id) {
            const recibo_moviento = {!! $recibo !!};
            //check if it is a recibo
            if (id == recibo_moviento.id) {
                //show container
                toggleReciboContainer('recibo-container');
                toggleReciboContainer('forma_pago_id');
            } else {
                //hide container
                toggleReciboContainer('forma_pago_id', 'none');
            }
        }

        function toggleReciboContainer(id, display = 'flex') {
            const container = document.querySelector('#' + id);
            container.style.display = display;
        }
		checkMovimientoId(movimientoSelect.value);

    }
</script>
