<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        :root {
            --bg-brown-color: #353535;
            --bg-blue-color: #010944;
        }

        table {
            border-spacing: 0;
        }

        .border-color-grey {
            border: #353535 solid 1px;
        }

        .text-end {
            text-align: end;
        }

        .text-center {
            text-align: center;
        }

        .border-color-grey-bottom {
            border-bottom: #353535 solid 1px;
        }

        .bg-blue-color {
            background-color: #010944;
            color: white;
        }

    </style>
</head>

<body>
    <table class="border-color-grey">

        <tbody>
            <tr>
                <td>
                    <img src="data:image/jpg;base64,{{ base64_encode(file_get_contents(storage_path('app/public/images/Logo.jpg'))) }}"
                        height="80" alt="{{ config('app.name', 'Laravel') }}">
                </td>
                <td>
                    <img src="data:image/png;base64,{{ base64_encode(file_get_contents(storage_path('app/public/images/logo-hds.png'))) }}"
                        height="80" alt="{{ config('app.name', 'Laravel') }}">
                </td>
                <td>
                    <img src="data:image/png;base64,{{ base64_encode(file_get_contents(storage_path('app/public/images/logo-hdc.png'))) }}"
                        height="80" alt="{{ config('app.name', 'Laravel') }}">
                </td>
            </tr>
            <tr>

                <td colspan="2" class="text-end">
                    <strong>
                        Fecha:
                    </strong>
                </td>
                <td class="border-color-grey">

                    <strong>

                        {{ $now->format('d/m/Y') }}
                    </strong>
                </td>
            </tr>
            <tr>
                <td class="text-end">Recibimos de</td>
                <td colspan="2" class="border-color-grey">{{ $cliente->full_name }}</td>
            </tr>
            <tr>
                <td class="text-end">La cantidad de pesos:</td>
                <td colspan="2" class="border-color-grey">{{ '$ ' . $recibo->importe }}</td>
            </tr>
            <tr>
                <td class="text-end">Para ser aplicados al pago de:</td>
                <td colspan="2" style="height: 40px" class="border-color-grey">
                    <table border="1" style="width: 100%;">
                        <tr>
                            <td>Comprobante</td>
                            <td>Importe</td>

                        </tr>
                        @foreach ($recibo->imputaciones as $imputacion)
                            <tr>
                                <td>{{ $imputacion->comprobante }}</td>
                                <td>{{ $imputacion->importe }}</td>
                            </tr>

                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td colspan="2">
                    <strong>
                        Recibido por cuenta y orden de Hostería del Cerro S.A.
                    </strong>
                </td>
            </tr>
            <tr>
                <td class="text-end">
                    <strong>
                        VALORES RECIBIDOS
                    </strong>
                </td>
                <td colspan="2" class="border-color-grey">
                    <table border="1" style="width: 100%;">
                        <thead class="border-color-grey">
                            <tr>
                                <td>Metodo de pago</td>
                                <td>MARCA</td>
                                <td>CUOTAS</td>
                                <td>LOTE-CUPON</td>
                                <td>Importe</td>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $recibo->formaPago->nombre }}</td>
                                <td> {{ $recibo->tarjetaCredito ? $recibo->tarjetaCredito->nombre : '-' }}</td>
                                <td>{{ $recibo->cuotas ?: '-' }}</td>
                                <td>{{ ($recibo->lote ? $recibo->lote : '') . ' - ' . ($recibo->cupon ?: '') }}</td>
                                <td>{{ $recibo->importe }}</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="text-center">
                    <p class="border-color-grey-bottom " style="margin-bottom:0;">HOSTERIA DEL CERRO</p>
                    <small>Firma y aclaracion</small>
                </td>
                <td class="text-end">
                    <strong>
                        Total:
                    </strong>
                </td>
                <td class="border-color-grey">
                    {{ '$ ' . $recibo->importe }}
                </td>
            </tr>
            <tr class="bg-blue-color text-center">
                <td class="bg-blue-color">
                    <p>
                        CLUB INN SA
                    </p>
                    <p>
                        CUIT: 30-70962914-6
                    </p>
                    <p>
                        INGRESOS BRUTOS: C.M. 901-220021-9
                    </p>
                </td>
                <td></td>
                <td class="bg-blue-color">
                    <p>
                        Dr. Luis García 695 - Piso 4º A - Tigre
                    </p>
                    <p>
                        Tel. 2078-4513
                    </p>
                </td>
            </tr>
            <tr class="bg-blue-color text-center">
                <td colspan="3">
                    <p>propietarioshosteria@clubinn.net // propietarioshostal@clubinn.net - www.hosteriadelcerro.com.ar
                        // www.hostaldelsol.com.ar</p>
                </td>

            </tr>
        </tbody>
    </table>


</html>
