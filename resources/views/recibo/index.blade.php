@extends('layouts.layout')
@section('title', 'recibos')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row  align-items-center justify-content-between">
        <div class="col-12 col-xl-1 p-0 d-flex align-items-center gap-title">
            <h1 class="title-clubinn"> Recibos</h1>
        </div>
        <div class="col row justify-content-end">
            <div class="col-12 col-xl-5 d-flex align-items-center">
                <div class="col-6">
                    <select class="form-control" name="" id="filterStatus">
                        <option value="" selected disabled>Filtrar por estado</option>
                        <option value="-1" {{ $estadoToFilter == 'all' ? 'selected' : '' }}>Todos</option>

                        @foreach ($estados as $estado)
                            <option value="{{ $estado->id }}" {{ $estadoToFilter == $estado->id ? 'selected' : '' }}>
                                {{ $estado->nombre }}</option>
                        @endforeach
                    </select>
                </div>
				<div class="col-4">
					<input type="text" placeholder="Filtrar por cliente" class="form-control " value="{{ $filterNombre}}" id="filter-cliente">
				</div>
                <div class="col-2">
                    <button class="btn btn-block btn-primary" id="filtrarBtn">
                        filtrar
                    </button>
                </div>
            </div>


            <div class="col-12 col-xl-4 p-1 mr-1 ">
                <form action="{{ route('recibos.export') }}" method="get">
                    <div class="form-group input-group col">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">desde</span>
                        </div>

                        <input type="date" name="desde" id="desde" value="{{ $desde->format('Y-m-d') }}"
                            class="form-control" placeholder="">

                    </div>
                    <div class="form-group input-group col">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">hasta</span>
                        </div>

                        <input type="date" name="hasta" id="hasta" value="{{ $hasta->format('Y-m-d') }}"
                            class="form-control" placeholder="">

                    </div>
                    <button class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Exportar <i class="fas fa-file-excel"
                            style="color: white; width: 2em; height:1em;"></i></button>
                </form>
            </div>
            <div class="col-8 col-xl-2 p-0 d-flex justify-content-end align-items-center">
                <a href="{{ route('recibos.create') }}" type="button"
                    class="btn btn-block btn-clubinn-blue clubinn-blue-color" style=" height: max-content;">Crear recibo</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($recibos->isEmpty())
                <p class="lead">No existen recibos cargado.</p>
            @else

                <!-- Modal -->
                <div class="modal fade" id="staticBackdrop" data-backdrop="true" data-keyboard="false" tabindex="-1"
                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div class="modal-dialog  modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">Imputacion</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered table-clubinn">
                                    <thead>
                                        <th>Seleccionar</th>
                                        <th>fecha vencimiento</th>
                                        <th>Temporada</th>
                                        <th>Importe total</th>
                                        <th>saldo pendiente</th>
                                        <th>Cantidad a imputar</th>
                                    </thead>
                                    <tbody id="tbody">

                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" {{-- data-dismiss="modal" --}}
                                    onclick="cerrarModal()">Aplicar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">Fecha</th>
                        <th scope="col" class="text-nowrap">Identificacion</th>
                        <th scope="col" class="text-nowrap">Cliente</th>
                        <th scope="col" class="text-nowrap">Importe</th>
                        <th scope="col" class="text-nowrap">Estado</th>
                        <th scope="col" class="text-nowrap">
                            Observacion
                        </th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @php
                            $classToStatus = ['Pendiente' => 'bg-warning', 'Aceptado' => 'bg-success', 'Anulado' => 'bg-danger'];

                        @endphp
                        @foreach ($recibos as $recibo)
                            <tr class="{{ $classToStatus[$recibo->estado->nombre] }}">
                                <td class="text-gergal-color text-white">{{ $recibo->fecha_pago->format('d/m/Y') }}</td>
                                <td class="text-gergal-color text-white">{{ $recibo->cliente->identificacion }}</td>
                                <td class="text-gergal-color text-white">{{ $recibo->cliente->full_name }}</td>
                                <td class="text-gergal-color text-white">{{ $recibo->importe . ' $' }}</td>
                                <td class="text-gergal-color text-white">{{ $recibo->estado->nombre }}</td>
                                <td class="text-gergal-color text-white">{{ $recibo->observaciones ?: '-' }}</td>

                                <td>

                                    @if ($recibo->estado->nombre == 'Pendiente')
                                        <form method="post" name="formSubmit" class="form-confirm"
                                            style="display: inline-block;"
                                            action="{{ route('recibos.update', $recibo->id) }}"
                                            id="{{ 'form-confirm-' . $recibo->id }}">
                                            @csrf
                                            @method('PUT')
                                            <input type="hidden" name="estado_id" value="{{ $estadoAceptado->id }}">
                                            <input type="hidden" name="notificacion"
                                                id="{{ 'notificacion-' . $recibo->id }}" value="0" />
											<input type="hidden" name="imputaciones" id="{{ 'imputaciones-' . $recibo->id }}" />
                                            <button type="submit" class="btn btn-outline-primary btn-sm  my-1  btn-confirm"
                                                {{-- data-toggle="modal" data-target="#staticBackdrop" --}}name="btnConfirmar" data-id="{{ $recibo->id }}"
                                                data-importe="{{ $recibo->importe }}">Confirmar</button>
                                        </form>
                                    @endif
                                    @can('recibo-edit')
                                        <a href="{{ route('recibos.edit', $recibo->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan

                                    @can('recibo-edit')
                                        <a href="{{ route('recibos.pdf.export', $recibo->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-file-pdf"></i></a>
                                        @if ($recibo->voucher)
                                            <a href="{{ route('recibos.pdf', $recibo->id) }}" type="button"
                                                class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-file"></i></a>
                                        @endif

                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($recibos, 'appends') ? $recibos->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
    <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>

    <script type="text/javascript">
        window.onload = function() {

            filter = document.querySelector('#filterStatus');
            if (window.location.search) {
                const parameter = window.location.search;
                const url = new URL(document.URL);
                const filterParameter = url.searchParams.get('filter');
                filter.querySelector(`option[value="${filterParameter}"]`).selected = true;
            }

            const btnFiltrar = document.querySelector('#filtrarBtn');
            const desdeDate = document.querySelector('#desde');
            const hastaDate = document.querySelector('#hasta');
            const clienteText = document.querySelector('#filter-cliente');
            btnFiltrar.addEventListener('click', handlerClickFilter)

            function handlerClickFilter(evt) {

                window.location.assign(window.location.origin + window.location.pathname +
                    `?filter=${filter.value}&desde=${desdeDate.value}&hasta=${hastaDate.value}&cliente=${clienteText.value}`);
            }


        }
    </script>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous">
    </script>
    <script type="text/javascript">
        const jquery3_5 = $.noConflict(true);
        const route = '{{ route('url.base.api') }}';

        window.addEventListener('DOMContentLoaded', (evt) => {
            localStorage.setItem('importe', 0);
            localStorage.setItem('recibo_id', 0);
            const formConfirmar = document.querySelectorAll('.btn-confirm');
            for (const confirmar of formConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarForm);
            }

            async function handlerConfirmarForm(evt) {
                jquery3_5('#staticBackdrop').modal('show');

                evt.preventDefault();

                const id = evt.target.dataset.id;
                const importe = parseFloat(evt.target.dataset.importe);
                localStorage.setItem('recibo_id', id);

                localStorage.setItem('importe', importe);
                await fetch(`${route}/recibo/imputacionesPosible/${id}`, {
                        method: 'POST',
                    }).then(response => response.json())
                    .then(json => {
                        const tbody = document.querySelector('#tbody');
                        tbody.innerHTML = '';
                        console.log(json.data);
                        cargarPosibleImputaciones(json.data, tbody, importe);
                    });
            }


            function cargarPosibleImputaciones(posiblesImputaciones, tbody, importe) {
                importe = parseFloat(importe)
                for (const posibleImputacion of posiblesImputaciones) {
                    const tr = createElement('tr');
                    imputacion = montoAImputar(parseFloat(posibleImputacion.saldo), importe);
                    importe = imputacion.importe;
                    const check = createElement('input');
                    check.type = 'checkbox';
                    check.name = 'id[]'
                    check.value = posibleImputacion.id;
                    check.classList.add('form-control');
                    check.checked = imputacion.saldoImputable > 0;

                    tdSelect = createElement('td');
                    tdSelect.appendChild(check);
                    tr.appendChild(tdSelect);

                    tdFechaVencimimiento = createElement('td');
                    fechaVencimiento = createElement('strong');
                    fechaVencimiento.appendChild(document.createTextNode(posibleImputacion.fecha_vencimiento));
                    tdFechaVencimimiento.appendChild(fechaVencimiento);

                    tr.appendChild(tdFechaVencimimiento);

                    tdtemporada = createElement('td');
                    temporada = createElement('strong');
                    temporada.appendChild(document.createTextNode(posibleImputacion.temporada || '-'));
                    tdtemporada.appendChild(temporada);
                    tr.appendChild(tdtemporada);

                    tdSaldoTotal = createElement('td');
                    saldoTotal = createElement('strong');
                    saldoTotal.appendChild(document.createTextNode(posibleImputacion.importe));
                    tdSaldoTotal.appendChild(saldoTotal);
                    tr.appendChild(tdSaldoTotal);

                    tdSaldoPendiente = createElement('td');
                    saldoPendiente = createElement('strong');
                    saldoPendiente.appendChild(document.createTextNode(posibleImputacion.saldo));
                    tdSaldoPendiente.appendChild(saldoPendiente);
                    tr.appendChild(tdSaldoPendiente);

                    tdImputar = createElement('td');
                    const imputarValor = createElement('input');
                    imputarValor.type = "number";
                    imputarValor.max = posibleImputacion.saldo;
                    imputarValor.name = "imputar_valor_" + posibleImputacion.id;
                    imputarValor.classList.add('form-control');

                    imputarValor.value = imputacion.saldoImputable;
                    tdImputar.appendChild(imputarValor);
                    tr.appendChild(tdImputar);

                    tbody.appendChild(tr);

                }
            }

            function montoAImputar(saldoPendiente, saldoRecibo) {
                console.log(saldoPendiente, saldoRecibo, saldoPendiente >= saldoRecibo);

                saldoImputable = 0;
                if (saldoRecibo > 0) {
                    if (saldoPendiente >= saldoRecibo) {
                        saldoImputable = saldoRecibo;
                        saldoRecibo = 0;
                    } else if (saldoPendiente < saldoRecibo) {
                        saldoRecibo -= saldoPendiente;
                        saldoImputable = saldoPendiente;
                    }
                }
                return {
                    saldoImputable,
                    importe: saldoRecibo
                };
            }

            function createElement(type) {
                return document.createElement(type);
            }

            jquery3_5('.modal').on('show.bs.modal', function(e) {
                /*  const selecteds = document.querySelectorAll('[name="id\[\]"]:checked');
        				jquery3_5('modal').modal('show');
        				console.log('hola', jquery3_5().jquery);
        				for (const selected of selecteds) {
        					const input = document.querySelector('imputar_valor_' + selected.id);

        				} */



            });
        })

        function cerrarModal(evt) {

            const selecteds = document.querySelectorAll('[name="id\[\]"]:checked');
            const importeDisponible = parseFloat(localStorage.getItem('importe'));
            const idRecibo = parseFloat(localStorage.getItem('recibo_id'));
            const imputaciones = {};
            let auxSaldosAImputar = 0;
            let close = false;

            if (!selecteds.length) {
                alert('Por favor seleccione(s) un imputacion');
                return;
            } else {
                for (const selected of selecteds) {
                    const input = document.querySelector(`[name=imputar_valor_${selected.value}]`);
                    const aImputar = parseFloat(input.value);
                    if (aImputar == 0) {
                        alert('Tiene una seleccion con cero ');
                        return;
                    } else {
                        imputaciones[`'${selected.value}'`] = aImputar;
                        auxSaldosAImputar += aImputar;
                    }
                }
            }
            if (importeDisponible < auxSaldosAImputar) {
                alert('el saldo seleccionado es mayor al saldo del recibo');
                return;
            }
			const imputacionInput = document.querySelector('#imputaciones-'+idRecibo);
			imputacionInput.value = JSON.stringify(imputaciones) ;
            jquery3_5('#staticBackdrop').modal('hide');
			confimacionReciboSubmit(idRecibo);

        }
		function confimacionReciboSubmit(id){
			const confirmarRecibo = (result) => {
				 if (result.isConfirmed) {
					 const form = document.querySelector('#form-confirm-' + id);

					 const noticacionConfirm = (result) => {
						 if (result.isConfirmed) {
							 const notificacion = document.querySelector('#notificacion-' + id);
							 notificacion.value = 1;
						 }
						 form.submit();
					 }

					 sweetAlert('warning', 'Desea enviar notificacion al cliente?', "",
						 'Si, enviar notificacion!', 'Cancelar.', noticacionConfirm);
				 }

			 }
			 sweetAlert('success', 'Desea confirmar este recibo ?', "",
				 'Si, confirmar!', 'Cancelar.', confirmarRecibo);
		}

		function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
			Swal.fire({
				title: title,
				text: msg,
				icon: icon,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: textConfirmButton,
				cancelButtonText: textCancelButton
			}).then(callbackResult);
		}
    </script>
@endsection
