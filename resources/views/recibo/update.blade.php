@extends('partials.form.form')

@section('form-title', 'Recibo')
@section('form-route', route('recibos.update', $recibo->id))
@section('form-content')
    @method('PUT')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group ">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">$</span>
            </div>
            <input id="importe" placeholder="Importe" type="text" class="form-control @error('importe') is-invalid @enderror"
                name="importe" value="{{ old('importe', $recibo->importe) }}">
            @error('importe')
                <span class="invalid-feedback" style="background-color: red;
                                                                       color: white;
                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de pago</span>
            </div>
            <input type="date" class="form-control  @error('fecha_pago') is-invalid @enderror" id="fecha_pago"
                name="fecha_pago" placeholder="Fecha de pago"
                value="{{ old('fecha_pago', $recibo->fecha_pago ? $recibo->fecha_pago->format('Y-m-d') : null) }}" />
        </div>
        @error('fecha_pago')
            <span class="invalid-feedback" style="background-color: red;
                                                                             color: white;
                                                                             margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    {{--  --}}
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="observaciones" placeholder="Observaciones" type="text"
                class="form-control @error('observaciones') is-invalid @enderror" name="observaciones"
                value="{{ old('observaciones', $recibo->observaciones) }}">
            @error('observaciones')
                <span class="invalid-feedback" style="background-color: red;
                                                                       color: white;
                                                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="text" class="form-control " value="{{ $recibo->formaPago->nombre }}" disabled>
            @error('anio')
                <span class="invalid-feedback" style="background-color: red;
                                               color: white;
                                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5">

            <input type="text" class="form-control " value="{{ $recibo->cliente->full_name }}" disabled>
            @error('')
                <span class="invalid-feedback" style="background-color: red;
                                               color: white;
                                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row" id="pago-container">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('forma_pago_id') is-invalid @enderror" id="forma_pago_id"
                name="forma_pago_id" placeholder="" disabled>
                <option selected disabled>-- Seleccione Forma de pago --</option>
                @foreach ($formasPagos as $FormaPago)
                    <option value="{{ $FormaPago->id }}"
                        {{ old('forma_pago_id', $recibo->forma_pago_id) == $FormaPago->id ? 'selected' : '' }}>
                        {{ $FormaPago->nombre }}</option>
                @endforeach
            </select>
            @error('forma_pago_id')
                <span class="invalid-feedback"
                    style="background-color: red;
                                                                                                                                   color: white;
                                                                                                                                   margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @if ($recibo->cbu_id)
        <div class="form-group row" id="cbu_container" style="display:none">
            <div class="col-md-12 px-5">
                <select class="form-control  @error('cbu_id') is-invalid @enderror" id="cbu_id" name="cbu_id"
                    placeholder="cbu_id">
                    <option selected disabled>-- Seleccione Cbu --</option>

                    @foreach ($cbus as $cbu)
                        <option value="{{ $cbu->id }}"
                            {{ old('cbu_id', $recibo->cbu_id) == $cbu->id ? 'selected' : '' }}>
                            {{ $cbu->banco . ' - ' . $cbu->cbu }}
                        </option>
                    @endforeach
                </select>
                @error('cbu_id')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                           color: white;
                                                                                                                           margin-top: 0;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endif
    @if ($recibo->tarjeta_credito_id)
        <div class="form-group row" id="tc_container">
            <div class="col-md-12 px-5">
                <select class="form-control  @error('tarjeta_credito_id') is-invalid @enderror" id="tarjeta_credito_id"
                    name="tarjeta_credito_id" placeholder="tarjeta_credito_id">
                    <option selected disabled>-- Seleccione tipo de Tarjeta --</option>

                    @foreach ($tcs as $tc)
                        <option value="{{ $tc->id }}"
                            {{ old('tarjeta_credito_id', $recibo->tarjeta_credito_id) == $tc->id ? 'selected' : '' }}>
                            {{ $tc->nombre }}
                        </option>
                    @endforeach
                </select>
                @error('tarjeta_credito_id')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                                   color: white;
                                                                                                                                   margin-top: 0;"
                        role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12 px-5 input-group ">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Ultimos 4 digitos de la tarjeta</span>
                </div>
                <input id="nro_tarjeta" placeholder="N° de tarjeta" type="number" min="1000" max="9999"
                    class="form-control @error('nro_tarjeta') is-invalid @enderror" name="nro_tarjeta"
                    value="{{ old('nro_tarjeta', $recibo->nro_tarjeta) }}">
                @error('nro_tarjeta')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                                                       color: white;
                                                                                                                                                       margin-top: 0;"
                        role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row" id="cuotas_container">
            <div class="col-md-12 px-5 input-group ">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Cuotas</span>
                </div>
                <input id="cuotas" placeholder="Cuotas" type="number"
                    class="form-control @error('cuotas') is-invalid @enderror" name="cuotas"
                    value="{{ old('cuotas', $recibo->cuotas) }}">
                @error('cuotas')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                                                       color: white;
                                                                                                                                                       margin-top: 0;"
                        role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row" id="lote_container">
            <div class="col-md-12 px-5 input-group ">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Lote</span>
                </div>
                <input id="lote" placeholder="lote" type="text" class="form-control @error('lote') is-invalid @enderror"
                    name="lote" value="{{ old('lote', $recibo->lote) }}">
                @error('lote')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                                                       color: white;
                                                                                                                                                       margin-top: 0;"
                        role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row" id="cupon_container">
            <div class="col-md-12 px-5 input-group ">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Cupon</span>
                </div>
                <input id="cupon" placeholder="cupon" type="text" class="form-control @error('cupon') is-invalid @enderror"
                    name="cupon" value="{{ old('cupon', $recibo->cupon) }}">
                @error('cupon')
                    <span class="invalid-feedback"
                        style="background-color: red;
                                                                                                                                                       color: white;
                                                                                                                                                       margin-top: 0;"
                        role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endif
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input type="hidden" name="notificacion" id="{{ 'notificacion-' . $recibo->id }}" value="0" />
            <select class="form-control  @error('estado_id') is-invalid @enderror" id="estado_id"
                name="estado_id" " placeholder=" Estado" data-id="{{ $recibo->id }}">
                <option selected disabled>-- Seleccione estado --</option>
                @foreach ($estados as $estado)
                    <option value="{{ $estado->id }}"
                        {{ old('estado_id', $recibo->estado_id) == $estado->id ? 'selected' : '' }}>
                        {{ $estado->nombre }}</option>
                @endforeach
            </select>
            @error('estado_id')
                <span class="invalid-feedback" style="background-color: red;
                                                           color: white;
                                                           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="voucher">Recibo</span>
            </div>
            <input type="file" class="form-control  @error('voucher') is-invalid @enderror" id="voucher" name="voucher"
                placeholder="voucher" />
        </div>
        @error('voucher')
            <span class="invalid-feedback" style="background-color: red;
                                                                             color: white;
                                                                             margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    @if ($recibo->voucher)
        <div class="form-group row">
            <div class="col-md-12 px-5">
                <a href="{{ route('recibos.pdf', $recibo->id) }}">Descargar Recibo</a>
            </div>
        </div>
    @endif

    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar recibo
            </button>
        </div>
    </div>
    <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>

    <script type="text/javascript">
        const estado = document.querySelector('#estado_id');
        estado.addEventListener('change', handlerChangeEstado);

        function handlerChangeEstado(evt) {
            const form = document.querySelector('form[enctype="multipart/form-data"]');
            const id = evt.target.dataset.id;
            const notificacion = document.querySelector('#notificacion-' + id);
            const noticacionConfirm = (result) => {
                if (result.isConfirmed) {
                    notificacion.value = 1;
                } else if (result.isDismissed) {
                    notificacion.value = 0;

                }

            }
            sweetAlert('warning', 'Desea enviar notificacion al cliente?', "",
                'Si, enviar notificacion!', 'No enviar.', noticacionConfirm);
        }

        function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
            Swal.fire({
                title: title,
                text: msg,
                icon: icon,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: textConfirmButton,
                cancelButtonText: textCancelButton
            }).then(callbackResult);
        }
    </script>

@endsection
