@extends('partials.form.form')

@section('form-title', 'Cargar un uso un usuario')
@section('form-route', route('usos.store'))

@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Año para usar</span>
            </div>
            <select class="form-control  @error('anio') is-invalid @enderror" id="anio" name="anio" placeholder="Año">
                <option selected disabled>-- Seleccione Año --</option>

                @for ($i = ($anio-20); $i < $anio + 10; $i++)
                    <option value="{{ $i }}" {{ old('anio') == $i ? 'selected' : '' }}>{{ $i }}
                    </option>
                @endfor
            </select>
            @error('anio')
                <span class="invalid-feedback" style="background-color: red;
                                   color: white;
                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Observacion</span>
            </div>
            <input class="form-control @error('observacion')  is-invalid @enderror" type="text" name="observacion" id="observacion"
                placeholder="Observacion" value="{{ old('observacion') }}">
            @error('observacion')
                <span class="invalid-feedback" style="background-color: red;
                                             color: white;
                                             margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Semana para uso</span>
            </div>
            <input class="form-control @error('semana')  is-invalid @enderror" type="number" name="semana" id="semana"
                placeholder="Semana" value="{{ old('semana') }}">
            @error('semana')
                <span class="invalid-feedback" style="background-color: red;
                                     color: white;
                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Dias de uso</span>
            </div>
            <input class="form-control @error('dias')  is-invalid @enderror" type="number" name="dias" id="dias"
                placeholder="Nro de dias " value="{{ old('dias') }}">
            @error('dias')
                <span class="invalid-feedback" style="background-color: red;
                                     color: white;
                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    {{-- <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
				<span class="input-group-text" id="basic-addon2">Fecha de uso</span>
			  </div>
            <input type="date" class="form-control  @error('fecha') is-invalid @enderror" id="fecha" name="fecha"
                placeholder="Fecha de inicio" value="{{ $now->format('Y-m-d') }}" />
        </div>
        @error('fecha')
            <span class="invalid-feedback" style="background-color: red;
                     color: white;
                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div> --}}
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de vencimiento</span>
            </div>
            <input type="date" class="form-control  @error('fecha_vencimiento') is-invalid @enderror" id="fecha_vencimiento"
                name="fecha_vencimiento" placeholder="Fecha de vencimiento"
                value="{{old('fecha_vencimiento', $now->addDays(10)->format('Y-m-d') )  }}" />
        </div>
        @error('fecha_vencimiento')
            <span class="invalid-feedback" style="background-color: red;
                                     color: white;
                                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Hotel</span>
            </div>
            <select class="form-control  @error('hotel_id') is-invalid @enderror" id="hotel_id" name="hotel_id"
                placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel_id') == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                                                                   color: white;
                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	{{-- estado --}}
    {{-- <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Estado de uso</span>
            </div>
            <select class="form-control  @error('estado_semana_uso_id') is-invalid @enderror" id="estado_semana_uso_id"
                name="estado_semana_uso_id" placeholder="hotel">
                <option selected disabled>-- Seleccione Estado --</option>
                @foreach ($estados as $estado)
                    <option value="{{ $estado->id }}"
                        {{ old('estado_semana_uso_id') == $estado->id ? 'selected' : '' }}>
                        {{ $estado->nombre }}</option>
                @endforeach
            </select>
            @error('estado_semana_uso_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                   color: white;
                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div> --}}
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Temporada para el uso </span>
            </div>
            <input type="hidden" name="temporada_id" id="temporada_id" value="{{old('temporada_id')}}">
            <input list="temporada_select" class="form-control  @error('temporada_id') is-invalid @enderror"
                id="temporada_input" name="temporada" placeholder="Temporada" autocomplete="off" value="{{old('temporada')}}"/>
            <datalist id="temporada_select" placeholder="temporada" required>
                <option selected disabled>-- Seleccione temporada --</option>

            </datalist> @error('temporada')
                <span class="invalid-feedback" style="background-color: red;
                                                                   color: white;
                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Unidad a usar</span>
            </div>
            <input type="hidden" name="unidad_id" id="unidad_hidden" value="{{old('unidad_id')}}">
            <input class="form-control  @error('unidad_id') is-invalid @enderror" placeholder="Unidad" list="unidad_select"
                id="unidad_input" name="unidad" autocomplete="off" value="{{old('unidad')}}">

            <datalist id="unidad_select" placeholder="unidad" required>

            </datalist>
            @error('unidad_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                           color: white;
                                                                                           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Asignado a</span>
            </div>
            <input type="hidden" name="cliente_id" id="cliente_hidden" value="{{old('cliente_id')}}">
            <input class="form-control  @error('cliente_id') is-invalid @enderror" placeholder="Cliente" list="cliente_id"
                id="cliente_input" name="cliente" autocomplete="off" value="{{old('cliente')}}">

            <datalist id="cliente_id" placeholder="Cliente" required>
                {{-- @foreach ($clientes as $cliente)
                	<option >{{ $cliente->identificacion.' - ' .$cliente->dni.' - '.$cliente->full_name}}</option>

				@endforeach --}}
            </datalist>
            @error('cliente_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                           color: white;
                                                                                           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group col-md-12 px-5">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="fraccionado" value="1"
                {{ old('fraccionado') == 1 ? 'checked' : '' }} name="fraccionado">
            <label class="custom-control-label" for="fraccionado">Fraccionado</label>
			@error('fraccionado')
                <span class="invalid-feedback" style="background-color: red;
                                                                                           color: white;
                                                                                           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Cargar Uso
            </button>
        </div>
    </div>
    <script type="text/javascript">
        const $urlClienteByHotel = '{{ route('cliente.hotel', '') }}';
        const $urlTemporadaByHotel = "{{ route('temporada.hotel', '') }}";
        const $urlUnidadByHotel = "{{ route('unidad.hotel.api', '') }}";

        window.onload = function() {
            $selectHotel = document.querySelector('#hotel_id');
            $selectClient = document.querySelector('#cliente_id');
            $clienteHidden = document.querySelector('#cliente_hidden');
            $clienteInput = document.querySelector('#cliente_input');

            $selectHotel.addEventListener('change', handlerChangeHotel);
            $clienteInput.addEventListener('change', handlerChangeClient);
            $clientesStorage = getStorage('clientes');
            if ($clientesStorage) {
                loadClientes($clientesStorage);
            }

            function handlerChangeHotel(evt) {

                clearElement($selectClient);
                clearElement($temporadaSelect);
                fetch($urlClienteByHotel + '/' + evt.target.value)
                    .then(response => response.json())
                    .then(json => {
                        loadClientes(json.data);
                    });
                fetch($urlTemporadaByHotel + '/' + evt.target.value)
                    .then(response => response.json())
                    .then(json => {
                        console.log('json', json);
                        loadTemporadas(json.data);
                    });

                fetch($urlUnidadByHotel + '/' + evt.target.value)
                    .then(response => response.json())
                    .then(json => {
                        console.log('json', json);
                        loadUnidad(json.data);
                    });

            }
            async function handlerChangeClient(evt) {
                $value = evt.target.value;
                $option = $selectClient.querySelector(`option[value="${$value}"]`);
                $clienteHidden.value = $option.dataset.id;
            }

            function loadClientes(clientes) {
                clearElement($selectClient);
                setStorage('clientes', clientes, 60);
                for (const cliente of clientes) {
                    const option = document.createElement('option');
                    option.dataset.id = cliente.id;
                    option.setAttribute('value', cliente.identificacion + ' - ' + cliente.dni + ' - ' + cliente
                        .nombre + ' ' + cliente.apellido);
                    $selectClient.appendChild(option);
                }
            }

            function clearElement(element) {
                while (element.lastChild) {
                    element.removeChild(element.lastChild);
                }

            }

            function getStorage(keyName) {
                const data = localStorage.getItem(keyName);
                if (!data) { // if no value exists associated with the key, return null
                    return null;
                }
                const item = JSON.parse(data);
                // If TTL has expired, remove the item from localStorage and return null
                if (Date.now() > item.ttl) {
                    localStorage.removeItem(keyName);
                    return null;
                }
                // return data if not expired
                return item.value;
            };
            /**
             * @param {string} keyName - A key to identify the value.
             * @param {any} keyValue - A value associated with the key.
             * @param {number} ttl- Time to live in seconds.
             */
            function setStorage(keyName, keyValue, ttl) {
                const data = {
                    value: keyValue, // store the value within this object
                    ttl: Date.now() + (ttl * 1000), // store the TTL (time to live)
                }
                // store data in LocalStorage
                localStorage.setItem(keyName, JSON.stringify(data));
            }
            $temporadaSelect = document.querySelector('#temporada_select');
            $temporadaHidden = document.querySelector('#temporada_id');
            $temporada_input = document.querySelector('#temporada_input');
            $temporada_input.addEventListener('change', handlerChangeTemporada);
            $temporadasStorage = getStorage('temporadas');
            if ($temporadasStorage) {
                loadTemporadas($temporadasStorage);
            }
            async function handlerChangeTemporada(evt) {
                $value = evt.target.value;
                $option = $temporadaSelect.querySelector(`option[value="${$value}"]`);
                $temporadaHidden.value = $option.dataset.id;
            }

            function loadTemporadas(temporadas) {
                clearElement($temporadaSelect);

                setStorage('temporadas', temporadas, 60);
                for (const temporada of temporadas) {
                    const option = document.createElement('option');
                    option.dataset.id = temporada.id;
                    option.setAttribute('value',
                        temporada.identificacion + ' - ' + temporada.nombre);
                    $temporadaSelect.appendChild(option);
                }
            }
            $unidadSelect = document.querySelector('#unidad_select');
            $unidadHidden = document.querySelector('#unidad_hidden');
            $unidad_input = document.querySelector('#unidad_input');
            $unidad_input.addEventListener('change', handlerChangeUnidad);
            $unidadtorage = getStorage('unidad');
            if ($unidadtorage) {
                loadUnidad($unidadtorage);
            }

            function handlerChangeUnidad(evt) {
                $value = evt.target.value;
                $option = $unidadSelect.querySelector(`option[value="${$value}"]`);
                $unidadHidden.value = $option.dataset.id;
            }

            function loadUnidad(unidades) {
                clearElement($unidadSelect);
                setStorage('unidad', unidades, 60);
				console.log('unidad loaded', unidades);
                for (const unidad of unidades) {
                    const option = document.createElement('option');
                    option.dataset.id = unidad.id;
                    option.setAttribute('value', unidad.nombre);
                    $unidadSelect.appendChild(option);
                }
            }
			const anio = document.querySelector('#anio');
			const fechaVencimiento = document.querySelector('#fecha_vencimiento');
			anio.addEventListener('change', handlerChangeAnio);

			function handlerChangeAnio(evt){



				const newDate = evt.target.value+ fechaVencimiento.value.slice(fechaVencimiento.value.indexOf('-'));
				fechaVencimiento.value = newDate;
				fechaVencimiento.min = newDate;
			}




        }
    </script>

@endsection
