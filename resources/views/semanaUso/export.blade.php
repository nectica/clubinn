<table>
	<thead>
		<tr>

			<th scope="col" class="text-nowrap"> <h1>Unidad</h1> </th>
			<th scope="col" class="text-nowrap"><h1>Cliente</h1> </th>
			<th scope="col" class="text-nowrap"><h1>Vencimiento</h1> </th>
			<th scope="col" class="text-nowrap"><h1>Año</h1> </th>
			<th scope="col" class="text-nowrap"> <h1>Semana</h1> </th>
			<th scope="col" class="text-nowrap"> <h1>Temporada</h1></th>
			<th scope="col" class="text-nowrap"><h1>Hotel</h1> </th>
			<th scope="col" class="text-nowrap"> <h1>Estado</h1></th>
			<th scope="col" class="text-nowrap"><h1>Observacion</h1></th>
		</tr>



	</thead>
	<tbody>
		@foreach ($semanaUsos as $uso)
			<tr>
				<td >
					{{ $uso->unidad->unidadR ? $uso->unidad->unidadR->nombre : $uso->unidad->nombre }}
				</td>
				<td >{{ $uso->cliente->full_name }}</td>
				<td >{{ $uso->fecha_vencimiento->format('d/m/Y') }}</td>
				<td >{{ $uso->anio }}</td>
				<td >{{ $uso->semana }}</td>
				<td >{{ $uso->temporada->nombre }}</td>
				<td >{{ $uso->temporada->hotelR->nombre }}</td>
				<td >{{ $uso->estado->nombre }}</td>
				<td >{{ $uso->observacion ?: '-' }}</td>

			</tr>
		@endforeach
	</tbody>
</table>
