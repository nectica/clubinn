@extends('layouts.layout')
@section('title', 'preciosUnidad por cliente')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col p-0 d-flex align-items-center gap-title">
            <h1 class="title-clubinn">preciosUnidad por cliente</h1>
        </div>
        <div class="col-8 col-xl-2 p-0 d-flex justify-content-end">

        </div>
    </div>

    <form action="{{ route('expensas.store') }}" method="POST">
		@csrf
		<input type="hidden" name="fecha" value="{{ $fecha }}">
        <div class="row">
            <div class="col-12  p-0 table-responsive">
                @if ($preciosUnidad->isEmpty())
                    <p class="lead">No existen preciosUnidad.</p>
                @else
                    <table class="table table-bordered table-clubinn">
                        <thead>
                            <th>Marcar</th>
                            <th>Temporada</th>
                            <th>Unidad</th>
                            <th>Hotel</th>
                            <th>Cliente</th>
							<th>Vencimiento</th>
                            <th>Expensa a</th>
                        </thead>
                        <tbody>
                            <input type="hidden" name="anio" value="{{ $anio }}">
                            <input type="hidden" name="temporada_id" value="{{ $temporada->id }}">
                            @foreach ($preciosUnidad as $precioUnidad)

                                @foreach ($precioUnidad->unidad->clientes()->wherePivot('temporada', $precioUnidad->temporada_id)->withPivot('id')->get()
        as $cliente)
                                    <tr>
                                        <td>
                                            <div class="form-inline ">
                                                <input
                                                    name="{{ 'cliente_' . $cliente->id . '_' . $precioUnidad->unidad_id.'_'.$cliente->pivot->id }}"
                                                    class="form-check-input" type="checkbox" checked>
                                            </div>
                                        </td>
                                        <td>{{ $precioUnidad->temporada->nombre }}</td>
                                        <td>{{ $precioUnidad->unidad->nombre }}</td>
                                        <td>{{ $precioUnidad->unidad->hotelR->nombre }}</td>
                                        <td>{{ $cliente->full_name }}</td>
										<td><input class="form-control" type="date" name="{{ 'fecha_vencimiento_'.$cliente->id . '_' . $precioUnidad->unidad_id.'_'.$cliente->pivot->id }}" value="{{$fecha_vencimiento}}"></td>
                                        <td> <input name="{{ $cliente->id . '_' . $precioUnidad->unidad_id.'_'.$cliente->pivot->id }}"
                                                class="form-control" type="text" value="{{ $precioUnidad->precio }}">
                                        </td>
                                    </tr>

                                @endforeach
                            @endforeach

                        </tbody>
                    </table>


                @endif

            </div>
        </div>
		<div class="row d-flex justify-content-end">
			<div class="col-4 col-xl-2">
				<button type="submit" class="btn btn-primary">
					Cargar expensas
				</button>
			</div>
		</div>
    </form>
@endsection
