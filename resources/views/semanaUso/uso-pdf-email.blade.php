<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
	<h3>Cliente: {{$cliente->full_name}}</h3>
	<h3>Dni: {{$cliente->dni}}</h3>
	<h3>Codigo: {{$cliente->identificacion .'-'.$semanaUso->id.'-'.$semanaUso->estado->id }}</h3>
<table>
	<thead>
		<tr>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Año:</td>
			<td>{{$semanaUso->anio }}</td>
		</tr>
		<tr>
			<td>Temporada:</td>
			<td>{{$semanaUso->temporada->nombre}}</td>
		</tr>
		<tr>
			<td>Hotel: </td>
			<td>{{$semanaUso->hotel->nombre}}</td>
		</tr>
		<tr>
			<td>Unidad:</td>
			<td>{{ $unidadNombre}}</td>
		</tr>
		<tr>
			<td>Estado: </td>
			<td>{{$semanaUso->estado->nombre}}</td>
		</tr>
	</tbody>
</table>
</body>
</html>
