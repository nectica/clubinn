@extends('layouts.layout')
@section('title', 'SemanaUsos')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6   mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col-12 col-xl-2 p-0 d-flex align-items-center gap-title">
            <h1 class="title-clubinn">Usos</h1>
        </div>
        <div class="col-12 col-xl-10 d-flex justify-content-end">
            <form action="" method="get" class="form-inline">
                <div class="form-group">
                    <select class="form-control" name="filter" id="filterStatus">
                        <option value="" selected disabled>Filtrar por estado</option>
                        <option value="-1" {{ $estadoToFilter == -1 ? 'selected' : '' }} >Todos</option>
                        @foreach ($estados as $estado)
                            <option value="{{ $estado->nombre }}"
                                {{ $estadoToFilter == $estado->nombre ? 'selected' : '' }}>
                                {{ $estado->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input placeholder="Filtrar por cliente." name="filterText" class="form-control h-100"
                        type="text" aria-describedby="basic-addon2">
                </div>
                <div class="form-group">
                    <button class="btn btn-block btn-clubinn-blue clubinn-blue-color" type="submit" class>Filtrar</button>
                </div>
            </form>
            <div class="col-12 col-xl-4">
                <a href="{{ route('usos.excel.download') }}" class="btn btn-block btn-clubinn-blue clubinn-blue-color">
                    Exportar <i class="fas fa-file-excel" style="color: white; width: 2em; height:1em;"></i></a>
            </div>
            <div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
                <a href="{{ route('usos.create') }}" type="button"
                    class="btn btn-block btn-clubinn-blue clubinn-blue-color">Crear uso</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($SemanaUsos->isEmpty())
                <p class="lead">No existen Semana de Usos.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('unidad.nombre', 'Unidad', [], ['class' => '
                            clubinn-th-text'])</th>

                        <th scope="col" class="text-nowrap">@sortablelink('cliente.identificacion', 'Identificacion', [],
                            ['class' => ' clubinn-th-text'])</th>

                        <th scope="col" class="text-nowrap">@sortablelink('cliente.full_name', 'Cliente', [], ['class' =>
                            '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('estado.nombre', 'Estado', [], ['class' => '
                            clubinn-th-text'])
                        <th scope="col" class="text-nowrap">@sortablelink('fecha_vencimiento', 'Vencimiento', [],
                            ['class'
                            => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('anio', 'Año', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('dia', 'dia', [], ['class' => '
                            clubinn-th-text'])
                        </th>
                        <th scope="col" class="text-nowrap">@sortablelink('semana', 'Semana', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('temporada.nombre', 'Temporada', [], ['class'
                            => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('observacion', 'Observacion', [], ['class' => '
                            clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('', 'Hotel', [], ['class' => '
                            clubinn-th-text'])
                        </th>
                        </th>

                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($SemanaUsos as $uso)
                            <tr>
                                <td class="text-gergal-color">
                                    {{ $uso->unidad->unidadR ? $uso->unidad->unidadR->nombre : $uso->unidad->nombre }}
                                </td>
                                <td class="text-gergal-color">{{ $uso->cliente->identificacion }}</td>
                                <td class="text-gergal-color">{{ $uso->cliente->full_name }}</td>
                                <td class="text-gergal-color">{{ $uso->estado->nombre }}</td>
                                <td class="text-gergal-color">{{ $uso->fecha_vencimiento->format('d/m/Y') }}</td>
                                <td class="text-gergal-color">{{ $uso->anio }}</td>
                                <td class="text-gergal-color">{{ $uso->dia }}</td>
                                <td class="text-gergal-color">{{ $uso->semana }}</td>
                                <td class="text-gergal-color">{{ $uso->temporada->nombre }}</td>
                                <td class="text-gergal-color">{{ $uso->observacion ?: '-' }}</td>
                                <td class="text-gergal-color">{{ $uso->temporada->hotelR->nombre }}</td>
                                <td style="min-width: 100px;">
                                    <a href="{{ route('usos.edit', $uso->id) }}" type="button"
                                        class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    {{-- corregir error con permisos --}}
                                    @if ($uso->estado->nombre == 'Solicitado' || $uso->estado->nombre == 'Depositado')
                                        <form style="display: inline-block;" id="{{ 'form-confirm-' . $uso->id }}"
                                            action="{{ route('usos.update', $uso->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            @if ($uso->estado->nombre == 'Solicitado')
                                                <input type="hidden" name="estado_semana_uso_id"
                                                    value={{ $estadoConfirmar->id }}>
                                            @endif
                                            @if ($uso->estado->nombre == 'Depositado')
                                                <input type="hidden" name="estado_semana_uso_id"
                                                    value={{ $estadoDepositado->id }}>
                                            @endif
                                            <input type="hidden" name="notificacion"
                                                id="{{ 'notificacion-confirmar-' . $uso->id }}" value="0" />
                                            <button type="submit" class="btn btn-outline-primary btn-sm  my-1 btn-confirmar"
                                                data-id="{{ $uso->id }}">Confirmar</button>
                                        </form>

                                    @endif
                                    @can('uso-edit')
                                    @endcan
                                    @can('uso-delete')
                                        @if ($uso->estado->nombre == 'Solicitado' || $uso->estado->nombre == 'Depositado')

                                            <form style="display: inline-block;" id="{{ 'form-delete-' . $uso->id }}"
                                                action=" {{ route('usos.update', $uso->id) }} " method="POST">
                                                @csrf
                                                @method('PUT')
                                                <input type="hidden" name="estado_semana_uso_id"
                                                    value={{ $estadoRechazado->id }}>
                                                <input type="hidden" name="notificacion"
                                                    id="{{ 'notificacion-rechazar-' . $uso->id }}" value="0" />
                                                <input type="button" value="Rechazar"
                                                    class="btn btn-outline-danger btn-sm btn-rechazar"
                                                    data-id="{{ $uso->id }}" />
                                            </form>
                                        @endif
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($SemanaUsos, 'appends') ? $SemanaUsos->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>

    <script type="text/javascript">

    </script>
@endsection
@section('script')
    <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>
    <script type="text/javascript">
        window.onload = function() {
            filter = document.querySelector('#filterStatus');
            /* filter.addEventListener('change', handlerChangeFilter); */
            if (window.location.search) {
                const parameter = window.location.search
                const value = (parameter.slice(parameter.indexOf('=') + 1)).replace(/%20/g, " ");
                filter.querySelector(`option[value="${value}"]`).selected = true;

            }

            function handlerChangeFilter(evt) {
                window.location.assign(window.location.origin + window.location.pathname + '?filter=' + filter.value)
            }


            const butonnsConfirmar = document.querySelectorAll('.btn-confirmar');
            const butonnsRechazar = document.querySelectorAll('.btn-rechazar');
            for (const confirmar of butonnsConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarButton);
            }
            for (const rechazar of butonnsRechazar) {
                rechazar.addEventListener('click', handlerRechazarButton);
            }

            function handlerConfirmarButton(evt) {
                evt.preventDefault();

                const confirmFunction = (result) => {
                    if (result.isConfirmed) {
                        const form = document.querySelector('#form-confirm-' + evt.target.dataset.id);
                        const id = 'notificacion-confirmar-' + evt.target.dataset.id;
                        askingNotification(id, form);

                    }
                };
                sweetAlert('success', 'Estas seguro que quieres Confirmar?', "Cliente sera notificado!",
                    'Si, confirmar!', 'Cancelar.', confirmFunction);

            }

            function handlerRechazarButton(evt) {
                evt.preventDefault();
                const confirmFunction = (result) => {
                    if (result.isConfirmed) {
                        const form = document.querySelector('#form-delete-' + evt.target.dataset.id);
                        const id = 'notificacion-rechazar-' + evt.target.dataset.id;

                        askingNotification(id, form);

                    }
                };
                sweetAlert('warning', 'Estas seguro que quieres rechazar?', "Cliente sera notificado!", 'Si, rechazar!',
                    'Cancelar.', confirmFunction);

            }

            function askingNotification(id, form) {
                const input = document.querySelector('#' + id);
                const noticacionAsking = (result) => {
                    if (result.isConfirmed) {
                        input.value = 1;
                    } else if (result.isDismissed) {
                        input.value = 0;

                    }
                    form.submit();
                }
                sweetAlert('warning', 'Quiere enviar notificacion al cliente?', "", 'Si, enviar!', 'no enviar.',
                    noticacionAsking);

            }

            function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
                Swal.fire({
                    title: title,
                    text: msg,
                    icon: icon,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: textConfirmButton,
                    cancelButtonText: textCancelButton
                }).then(callbackResult);
            }

        }
    </script>
@endsection
