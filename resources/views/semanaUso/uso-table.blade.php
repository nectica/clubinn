<table>
	<thead>
		<tr>
			<th scope="col" class="text-nowrap clubinn-th-text">Unidad</th>
			<th scope="col" class="text-nowrap clubinn-th-text">Año</th>
			<th scope="col" class="text-nowrap clubinn-th-text">Dias</th>
			<th scope="col" class="text-nowrap clubinn-th-text">Semana</th>
			<th scope="col" class="text-nowrap clubinn-th-text">Temporada</th>
			<th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
		</tr>
	</thead>
	<tbody>
			<tr>

				<td class="text-gergal-color">
					{{ $uso->unidad->unidadR ? $uso->unidad->unidadR->nombre : $uso->unidad->nombre }}</td>
				<td class="text-gergal-color">{{ $uso->anio }}</td>
				<td class="text-gergal-color">{{ $uso->dia }}</td>
				<td class="text-gergal-color">{{ $uso->semana }}</td>
				<td class="text-gergal-color">{{ $uso->temporada->nombre }}</td>
				<td class="text-gergal-color">{{ $uso->temporada->hotelR->nombre }}</td>
			</tr>
	</tbody>
</table>
