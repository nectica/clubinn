@extends('partials.form.form')

@section('form-title', 'Cargar un uso un usuario')
@section('form-route', route('usos.update', $uso->id))

@section('form-content')
    @method('PUT')

    <div class="form-group row">

        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Año para usar</span>
            </div>
            <select class="form-control  @error('anio') is-invalid @enderror" id="anio" name="anio" placeholder="Año">
                <option selected disabled>-- Seleccione Año --</option>

                @for ($i = $anio; $i < $anio + 10; $i++)
                    <option value="{{ $i }}" {{ old('anio', $uso->anio) == $i ? 'selected' : '' }}>
                        {{ $i }}
                    </option>
                @endfor
            </select>
            @error('anio')
                <span class="invalid-feedback" style="background-color: red;
                                                   color: white;
                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Semana para uso</span>
            </div>
            <input class="form-control @error('semana')  is-invalid @enderror" type="number" name="semana" id="semana"
                placeholder="Semana" value="{{ old('semana', $uso->semana) }}">
            @error('semana')
                <span class="invalid-feedback" style="background-color: red;
                                                     color: white;
                                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Observacion</span>
            </div>
            <input class="form-control @error('observacion')  is-invalid @enderror" type="text" name="observacion"
                id="observacion" placeholder="Observacion" value="{{ old('observacion', $uso->observacion) }}">
            @error('observacion')
                <span class="invalid-feedback" style="background-color: red;
                                                     color: white;
                                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Dias de uso</span>
            </div>
            <input class="form-control @error('dias')  is-invalid @enderror" type="number" name="dias" id="dias"
                placeholder="Nro de dias " value="{{ old('dias', $uso->dias) }}">
            @error('dias')
                <span class="invalid-feedback" style="background-color: red;
                                                     color: white;
                                                     margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de uso</span>
            </div>
            <input type="date" class="form-control  @error('fecha_uso') is-invalid @enderror" id="fecha_uso"
                name="fecha_uso" placeholder="Fecha de uso"
                value="{{ old('fecha_uso', $uso->fecha_uso ? $uso->fecha_uso->format('Y-m-d') : null) }}" />
        </div>
        @error('fecha_uso')
            <span class="invalid-feedback" style="background-color: red;
                                     color: white;
                                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Fecha de vencimiento</span>
            </div>
            <input type="date" class="form-control  @error('fecha_vencimiento') is-invalid @enderror" id="fecha_vencimiento"
                name="fecha_vencimiento" placeholder="Fecha de vencimiento"
                value="{{ old('fecha_vencimiento', $uso->fecha_vencimiento ? $uso->fecha_vencimiento->format('Y-m-d') : null) }}" />
        </div>
        @error('fecha_vencimiento')
            <span class="invalid-feedback" style="background-color: red;
                                                     color: white;
                                                     margin-top: 0;" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Asignado a</span>
            </div>
            <input type="hidden" name="cliente_id" id="cliente_hidden" value="{{ old('cliente_id', $uso->cliente_id) }}">
            <input class="form-control  @error('cliente_id') is-invalid @enderror" placeholder="Cliente" name="cliente"
                autocomplete="off" value="{{ $uso->cliente->full_name }}" disabled>

            @error('cliente_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                                           color: white;
                                                                                                           margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Hotel</span>
            </div>
            <select class="form-control  @error('hotel_id') is-invalid @enderror" id="hotel_id" name="hotel_id"
                placeholder="hotel" disabled>
                <option>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}"
                        {{ old('hotel_id', $uso->hotel_id) == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                                                                                   color: white;
                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Temporada</span>
            </div>
            <select class="form-control  @error('temporada_id') is-invalid @enderror" id="temporada_id" name="temporada_id"
                placeholder="temporada_id">
                <option selected disabled>-- Seleccione temporada --</option>
                @foreach ($uso->hotel->temporadas as $temporada)
                    <option value="{{ $temporada->id }}"
                        {{ old('temporada_id', $uso->temporada_id) == $temporada->id ? 'selected' : '' }}>
                        {{ $temporada->nombre }}</option>
                @endforeach
            </select>
            @error('temporada_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                   color: white;
                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Unidad</span>
            </div>
            <select class="form-control  @error('unidad_id') is-invalid @enderror" id="unidad_id" name="unidad_id"
                placeholder="unidad_id">
                <option selected disabled>-- Seleccione Unidad --</option>
                @foreach ($uso->hotel->habitaciones as $habitacion)
                    <option value="{{ $habitacion->id }}"
                        {{ old('unidad_id', $uso->unidad_id) == $habitacion->id ? 'selected' : '' }}>
                        {{ $habitacion->nombre }}</option>
                @endforeach
            </select>
            @error('unidad_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                   color: white;
                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">Estado de uso</span>
            </div>
            <input type="hidden" name="notificacion" id="{{ 'notificacion' }}" value="0" />
            <input type="hidden" name="cambio_status" id="cambio_status" value="0" />

            <select class="form-control  @error('estado_semana_uso_id') is-invalid @enderror" id="estado_semana_uso_id"
                name="estado_semana_uso_id" placeholder="hotel">
                <option selected disabled>-- Seleccione Estado --</option>
                @foreach ($estados as $estado)
                    <option value="{{ $estado->id }}"
                        {{ old('estado_semana_uso_id', $uso->estado_semana_uso_id) == $estado->id ? 'selected' : '' }}>
                        {{ $estado->nombre }}</option>
                @endforeach
            </select>
            @error('estado_semana_uso_id')
                <span class="invalid-feedback" style="background-color: red;
                                                                                   color: white;
                                                                                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group col-md-12 px-5">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="fraccionado" value="1"
                {{ old('fraccionado', $uso->fraccionado) == 1 ? 'checked' : '' }} name="fraccionado">
            <label class="custom-control-label" for="fraccionado">Fraccionado</label>
            @error('fraccionado')
                <span class="invalid-feedback" style="background-color: red;
                                                                                                           color: white;
                                                                                                           margin-top: 0;"
                    role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <input type="hidden" name="redirect" value="usos.index">
            <button type="submit" class="btn btn-success">
                Actualizar Uso
            </button>
        </div>
    </div>
    <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>

    <script type="text/javascript">
        const estado = document.querySelector('#estado_semana_uso_id');
		const changeStatusBoolean = document.querySelector('#cambio_status');
        estado.addEventListener('change', handlerChangeEstado);

		function handlerChangeEstado(evt) {
			changeStatusBoolean.value = 1;
		}

        const form = document.querySelector('form[enctype="multipart/form-data"]');
        form.addEventListener('submit', handlerSubmit);

        function handlerSubmit(evt) {
            evt.preventDefault();
            const id = evt.target.dataset.id;
            const notificacion = document.querySelector('#notificacion');
            const noticacionConfirm = (result) => {
                if (result.isConfirmed) {
                    notificacion.value = 1;
                } else if (result.isDismissed) {
                    notificacion.value = 0;
                }
                evt.target.submit();
            }
			console.log(changeStatusBoolean.value);
			if (changeStatusBoolean.value == 1) {
				sweetAlert('warning', 'Desea enviar notificacion al cliente?', "",
					'Si, enviar notificacion!', 'No enviar.', noticacionConfirm);
			}else {
                evt.target.submit();
			}
        }

        function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
            Swal.fire({
                title: title,
                text: msg,
                icon: icon,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: textConfirmButton,
                cancelButtonText: textCancelButton
            }).then(callbackResult);
        }
    </script>
@endsection
