<div class="col-12  p-0 table-responsive">
    @if ($cliente->recibos->isEmpty())
        <p class="lead">No existen recibos cargado.</p>
    @else
        <table class="table table-bordered table-clubinn" id="recibo-table">
            <thead>
                <tr>
                    <th scope="col" class="text-nowrap clubinn-th-text">Fecha</th>
                    <th scope="col" class="text-nowrap clubinn-th-text">Nombre</th>
                    <th scope="col" class="text-nowrap clubinn-th-text">Importe</th>
                    <th scope="col" class="text-nowrap">Estado</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @php
                    $classToStatus = ['Pendiente' => 'bg-warning', 'Aceptado' => 'bg-success', 'Anulado' => 'bg-danger'];

                @endphp
                @foreach ($cliente->recibos as $recibo)
                    <tr class="{{ $classToStatus[$recibo->estado->nombre] }}">
                        <td class="text-gergal-color text-white">
                            {{ $recibo->fecha_pago->format('d/m/Y') }}</td>
                        <td class="text-gergal-color text-white">
                            {{ $recibo->cliente->full_name }}</td>
                        <td class="text-gergal-color text-white">{{ $recibo->importe . ' $' }}
                        </td>
                        <td class="text-gergal-color text-white">{{ $recibo->estado->nombre }}
                        </td>

                        <td>

                            @if ($recibo->estado->nombre == 'Pendiente')
                                <form method="post" name="formSubmit" class="form-confirm"
                                    style="display: inline-block;"
                                    action="{{ route('recibos.update', $recibo->id) }}"
                                    id="{{ 'form-confirm-' . $recibo->id }}">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="estado_id" value="{{ $estadoAceptado->id }}">
                                    <input type="hidden" name="notificacion" id="{{ 'notificacion-' . $recibo->id }}"
                                        value="0" />
                                    <button type="submit" class="btn btn-outline-primary btn-sm  my-1  btn-confirm"
                                        name="btnConfirmar" data-id="{{ $recibo->id }}">Confirmar</button>
                                </form>
                            @endif
                            @can('recibo-edit')
                                <a href="{{ route('recibos.edit', $recibo->id) }}" type="button"
                                    class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                            @endcan

                            @can('recibo-edit')
                                <a href="{{ route('recibos.pdf.export', $recibo->id) }}" type="button"
                                    class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-file-pdf"></i></a>
                                @if ($recibo->voucher)
                                    <a href="{{ route('recibos.pdf', $recibo->id) }}" type="button"
                                        class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-file"></i></a>
                                @endif

                            @endcan
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    @endif

</div>
