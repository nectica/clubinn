<div class="row pt-1 align-items-center justify-content-between mb-2">
    <div class="col p-0 d-flex align-items-center gap-title">

    </div>
    <div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
        <a href="{{ route('comentario.create', $cliente->id) }}" type="button"
            class=" clubinn-blue-color  btn-clubinn-blue  btn btn-block  "> Agregar Comentarios</a>
    </div>
</div>

<div>
    <div class="col-12  p-0 table-responsive">
        @if ($cliente->comentarios->isEmpty())
            <p class="lead p-5">No existen comentarios registrados para este cliente.</p>
        @else
            <table class="table display table-bordered table-clubinn" id="comentarios-table" style="max-width: 100%">
                <thead>
                    <tr>
                        <th scope="col" class="text-nowrap clubinn-th-text">Usuario</th>
                        <th scope="col" class="text-nowrap clubinn-th-text">Observacion</th>
                        <th scope="col" class="text-nowrap clubinn-th-text">fecha</th>
                        <th scope="col" class="text-nowrap clubinn-th-text"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cliente->comentarios as $comentario)
                        <tr>
                            <td class="text-gergal-color">{{ $comentario->user->email }}</td>
                            <td class="text-gergal-color">{{ $comentario->observacion }}</td>
                            <td class="text-gergal-color">{{ $comentario->created_at->format('d/m/Y') }}</td>
                            <td class="text-gergal-color">
                                <a type="button"
								class="btn btn-outline-primary btn-sm  my-1"
                                    href=" {{ route('comentario.edit', ['cliente' => $cliente->id, 'comentario' => $comentario->id]) }}">
									Editar
                                </a>
								<form style="display: inline-block;"  name="form-delete" action="{{ route('comentario.destroy', $comentario->id) }}"
									method="POST">
									@csrf
									@method('DELETE')
									<button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
											class="fas fa-trash-alt"></i></button>
								</form>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>

        @endif
    </div>
</div>
