<div class="col-12  p-0 table-responsive">
	@if ($cliente->usos->isEmpty())
		<p class="lead">No existen Semana de Usos.</p>
	@else
		<table class="table table-bordered table-clubinn" id="uso-table">
			<thead>
				<tr>
					<th scope="col" class="text-nowrap clubinn-th-text">Unidad</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Estado</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Fecha de vencimiento</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Año</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Dias</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Semana</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Temporada</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Observacion</th>
					<th scope="col" class="text-nowrap clubinn-th-text"></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($cliente->usos as $uso)
					<tr>

						<td class="text-gergal-color">
							{{ $uso->unidad->unidadR ? $uso->unidad->unidadR->nombre : $uso->unidad->nombre }}</td>
						<td class="text-gergal-color">{{ $uso->estado->nombre }}</td>
						<td class="text-gergal-color">{{ $uso->fecha_vencimiento->format('d/m/Y') }}
						</td>
						<td class="text-gergal-color">{{ $uso->anio }}</td>
						<td class="text-gergal-color">{{ $uso->dia }}</td>
						<td class="text-gergal-color">{{ $uso->semana }}</td>
						<td class="text-gergal-color">{{ $uso->temporada->nombre }}</td>
						<td class="text-gergal-color">{{ $uso->temporada->hotelR->nombre }}</td>
						<td class="text-gergal-color">{{ $uso->observacion ?: '-' }}</td>
						<td style="min-width: 100px;">
							<a href="{{ route('usos.edit', $uso->id) }}" type="button"
								class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
							{{-- corregir error con permisos --}}
							@if ($uso->estado->nombre == 'Solicitado' || $uso->estado->nombre == 'Depositado')
								<form style="display: inline-block;"  id="{{ 'form-confirm-'.$uso->id }}"
									action="{{ route('usos.update', $uso->id) }}" method="POST">
									@csrf
									@method('PUT')
									@if ($uso->estado->nombre == 'Solicitado')
										<input type="hidden" name="estado_semana_uso_id"
											value={{ $estadoConfirmar->id }}>
									@endif
									@if ($uso->estado->nombre == 'Depositado')
										<input type="hidden" name="estado_semana_uso_id"
											value={{ $estadoDepositado->id }}>
									@endif
									<input type="hidden" name="notificacion"
									id="{{ 'notificacion-confirmar-' .$uso->id }}" value="0" />
									<button type="submit"
										class="btn btn-outline-primary btn-sm  my-1 btn-confirmar" data-id="{{ $uso->id }}">Confirmar</button>
								</form>

							@endif
							@can('uso-edit')
							@endcan
							@can('uso-delete')
								@if ( $uso->estado->nombre == 'Solicitado' || $uso->estado->nombre == 'Depositado')

									<form style="display: inline-block;" id="{{ 'form-delete-'.$uso->id }}"
										action=" {{ route('usos.update', $uso->id) }} " method="POST">
										@csrf
										@method('PUT')
										<input type="hidden" name="estado_semana_uso_id"
											value={{ $estadoRechazado->id }}>
											<input type="hidden" name="notificacion"
									id="{{ 'notificacion-rechazar-' .$uso->id }}" value="0" />
										<input type="button" value="Rechazar"
											class="btn btn-outline-danger btn-sm btn-rechazar"
											data-id="{{ $uso->id }}" />
									</form>
								@endif
							@endcan

						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	@endif

</div>
@section('script')


@endsection
