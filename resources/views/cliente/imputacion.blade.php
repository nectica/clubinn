@extends('layouts.layout')
@section('title', 'Imputacion')


@section('content')
<div class="row mt-2">
	<div class="col-12 col-xl-11 p-0 d-flex justify-content-end">

		<div class="co-2">
			<a  href="{{ route('cliente.show', $cliente->id)}}" class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Ir al cliente</a>
		</div>
	</div>
</div>
<div class="row h-120-px align-items-center justify-content-between">
	<div class="col p-0 d-flex align-items-center gap-title">
		<h1 class="title-clubinn">
			Debe
		</h1>
	</div>
</div>
    <div class="row mt-2">
        <div class="col-12  p-0 table-responsive">
            @if ($debe->isEmpty())
                <p class="lead">No existen debe disponible.</p>
            @else
                <table class="table table-bordered table-clubinn" id="cuenta-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th scope="col" class="text-nowrap clubinn-th-text">fecha</th>
                            <th scope="col" class="text-nowrap clubinn-th-text">Año</th>
                            <th scope="col" class="text-nowrap">Fecha vencimiento</th>
                            <th scope="col" class="text-nowrap">Importe</th>
                            <th scope="col" class="text-nowrap">Saldo</th>
                            <th scope="col" class="text-nowrap">Tipo de movimiento</th>
                            <th scope="col" class="text-nowrap">Temporada</th>
                        </tr>

                    </thead>
                    <tbody>
                        @foreach ($debe as $cc)
                            <tr>
                                <td><input type="radio" name="debe_id" id="debe_id" value={{ $cc->id }} /></td>
                                <td class="text-gergal-color text-white">{{ $cc->fecha->format('d/m/Y') }}</td>
                                <td class="text-gergal-color text-white">{{ $cc->expensa ? $cc->expensa->anio : '-' }}
                                </td>
                                <td class="text-gergal-color text-white">{{ $cc->fecha_vencimiento->format('d/m/Y') }}
                                </td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->importe. ' $' }}</td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->saldo . ' $' }}</td>
                                <input type="hidden" id="{{ 'debe_saldo_' . $cc->id }}" value="{{ $cc->saldo }}">
                                <td class="text-gergal-color text-white">{{ $cc->movimiento->nombre }}
                                </td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->expensa ? $cc->expensa->temporada->nombre : '-' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif

        </div>
    </div>
	<div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<h1 class="title-clubinn">
				Haber
			</h1>
		</div>
	</div>
    <div class="row mt-2">
        <div class="col-12  p-0 table-responsive">
            @if ($haber->isEmpty())
                <p class="lead">No existen haber disponible.</p>
            @else
                <table class="table table-bordered table-clubinn" id="cuenta-table">
                    <thead>
                        <tr>
                            <th></th>
                            <th scope="col" class="text-nowrap clubinn-th-text">fecha</th>
                            <th scope="col" class="text-nowrap clubinn-th-text">Año</th>
                            <th scope="col" class="text-nowrap">Fecha vencimiento</th>
                            <th scope="col" class="text-nowrap">Importe</th>
                            <th scope="col" class="text-nowrap">Saldo</th>
                            <th scope="col" class="text-nowrap">Tipo de movimiento</th>
                            <th scope="col" class="text-nowrap">Temporada</th>
                        </tr>

                    </thead>
                    <tbody>
                        @foreach ($haber as $cc)
                            <tr>
                                <td><input type="radio" name="haber_id" id="haber_id" value={{ $cc->id }} /></td>
                                <td class="text-gergal-color text-white">{{ $cc->fecha->format('d/m/Y') }}</td>
                                <td class="text-gergal-color text-white">{{ $cc->expensa ? $cc->expensa->anio : '-' }}
                                </td>
                                <td class="text-gergal-color text-white">{{ $cc->fecha_vencimiento->format('d/m/Y') }}
                                </td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->importe . ' $' }}</td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->saldo . ' $' }}
                                    <input type="hidden" id="{{ 'haber_saldo_' . $cc->id }}"
                                        value="{{ $cc->saldo }}">

                                </td>
                                <td class="text-gergal-color text-white">{{ $cc->movimiento->nombre }}
                                </td>
                                <td class="text-gergal-color text-white">
                                    {{ $cc->expensa ? $cc->expensa->temporada->nombre : '-' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif

        </div>
    </div>
    <div class="row">
        <div class="col-12 col-xl-11 p-0 d-flex justify-content-end">

            <div class="co-2">
                <button  type="button" class="btn btn-block btn-clubinn-blue clubinn-blue-color" id="imputar"> Imputar
                    pagos</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.addEventListener('load', function() {
            const URLbase = "{{ route('url.base.api') }}"
            class ImputarPago {
                constructor() {
                    this.debeId, this.haberId, this.urlBase, this.buttonId;
                    this.saldoHaberId = 'haber_saldo_', this.saldoDebeId = 'debe_saldo_';
                }
                getElementById(id, selectorFilter = '') {
                    return document.querySelector('#' + id + selectorFilter)
                }
                getElementCheckedById(id) {
                    return this.getElementById(id, ':checked');
                }

                getInput(id) {
                    return this.getElementById(id) || null;
                }

                fetchImputar() {
                    const debeInputId = this.getElementCheckedById(this.debeId);
                    const haberInputId = this.getElementCheckedById(this.haberId);
                    if (!debeInputId) {
                        alert('Seleccione un Debe');
                        return false;
                    }
                    if (!haberInputId) {
                        alert('Seleccione un Haber');
                        return false;
                    }
					const haberRecordId = haberInputId.value, debeRecordId = debeInputId.value;
					const saldo =this.askSaldo(debeRecordId, haberRecordId);
					if (saldo > 0) {
						const formData = new FormData();
						formData.append('saldo', saldo );
						fetch(`${URLbase}/imputar/cliente/${debeRecordId}/${haberRecordId}`, {
								method: 'POST',
								body:formData,
							}).then(response => response.json())
							.then(this.handlerFetchImputar);

					}
                }

                handlerFetchImputar = (json) => {
					if (json.status = 'ok') {
						location.reload();

					}


                }
                addClickListener() {
                    this.getElementById(this.buttonId).addEventListener('click', this.handlerClick);
                }
                handlerClick = (evt) => {
                    this.fetchImputar();
                }
				askSaldo(debeRecordId, haberRecordId){
					const saldoDebe = this.getElementById(this.saldoHaberId +haberRecordId);
                    const saldoHaber = this.getElementById(this.saldoDebeId + debeRecordId);
					console.log(saldoDebe, saldoHaber);
                    const mayor = saldoDebe.value > saldoHaber.value ? saldoDebe.value  : saldoHaber.value ;
                    let saldoImputar = 0;
                    do {
                        saldoImputar = window.prompt("Ingrese saldo que desea imputar: ", mayor);
                    } while (saldoImputar > mayor)

					return saldoImputar;
				}
            }
            const imputar = new ImputarPago();
            imputar.urlBase = URLbase;
            imputar.debeId = 'debe_id';
            imputar.haberId = 'haber_id';
            imputar.buttonId = 'imputar';
            imputar.addClickListener();

        })
    </script>
@endsection
