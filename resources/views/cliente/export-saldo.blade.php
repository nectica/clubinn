<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
	<table class="table table-bordered table-clubinn">
		<thead>
			<tr>
				<th scope="col" class="text-nowrap">Identificación</th>
				<th scope="col" class="text-nowrap">Nombre</th>
				<th scope="col" class="text-nowrap">apellido</th>
				<th scope="col" class="text-nowrap">Email</th>
				<th scope="col" class="text-nowrap">Hotel</th>
				<th>Saldo</th>
				<th>Saldo vencido</th>
			</tr>

		<tbody>
			@foreach ($clientes as $cliente)
				<tr>
					<td >{{ $cliente->identificacion }} </td>
					<td >{{ $cliente->nombre }}</td>
					<td >{{ $cliente->apellido }}</td>
					<td >{{ $cliente->email }}</td>

					<td >{{ $cliente->hotelR->nombre }}</td>

					<td>{{ $cliente->saldo}}</td>
					<td>{{ $cliente->saldo_vencido}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
