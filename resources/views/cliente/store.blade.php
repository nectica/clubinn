@extends('partials.form.form')

@section('form-title', 'Cliente')
@section('form-route', route('cliente.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Identificacion</span>
			</div>
            <input id="identificacion" placeholder="Identificacion" type="text"
                class="form-control @error('identificacion') is-invalid @enderror" name="identificacion"
                value="{{ old('identificacion') }}">
            @error('identificacion')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Nombre</span>
			</div>
            <input id="nombre" placeholder="Nombre" type="text" class="form-control @error('nombre') is-invalid @enderror"
                name="nombre" value="{{ old('nombre') }}" autofocus>
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Apellido</span>
			</div>
            <input id="apellido" type="text" placeholder="Apellido"
                class="form-control @error('apellido') is-invalid @enderror" name="apellido"
                value="{{ old('apellido') }}">

            @error('apellido')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Dni</span>
			</div>
            <input id="dni" type="text" placeholder="Dni" class="form-control @error('dni') is-invalid @enderror" name="dni"
                value="{{ old('dni') }}">

            @error('dni')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Cuil</span>
			</div>
            <input id="cuil" type="text" placeholder="cuil" class="form-control @error('cuil') is-invalid @enderror" name="cuil"
                value="{{ old('cuil') }}">

            @error('cuil')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >fecha nacimiento</span>
			</div>
            <input id="fecha_nacimiento" type="date" placeholder="fecha nacimiento"
                class="form-control @error('fecha_nacimiento') is-invalid @enderror" name="fecha_nacimiento"
                value="{{ old('fecha_nacimiento') }}">

            @error('fecha_nacimiento')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Telefono</span>
			</div>
            <input id="telefono" type="text" placeholder="Telefono"
                class="form-control @error('telefono') is-invalid @enderror" name="telefono"
                value="{{ old('telefono') }}">

            @error('telefono')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Celular</span>
			</div>
            <input id="celular" type="text" placeholder="Celular"
                class="form-control @error('celular') is-invalid @enderror" name="celular" value="{{ old('celular') }}">
            @error('celular')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Domicilio</span>
			</div>
            <input id="domicilio" type="text" placeholder="Domicilio"
                class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{ old('domicilio') }}">
            @error('domicilio')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Localidad</span>
			</div>
            <input id="localidad" type="text" placeholder="Localidad"
                class="form-control @error('localidad') is-invalid @enderror" name="localidad" value="{{ old('localidad') }}">
            @error('localidad')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Provincia</span>
			</div>
            <input id="provincia" type="text" placeholder="Provincia"
                class="form-control @error('provincia') is-invalid @enderror" name="provincia" value="{{ old('provincia') }}">
            @error('provincia')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Pais</span>
			</div>
            <input id="pais" type="text" placeholder="Pais"
                class="form-control @error('pais') is-invalid @enderror" name="pais" value="{{ old('pais') }}">
            @error('pais')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Código postal</span>
			</div>
            <input id="cod_postal" type="text" placeholder="Código postal"
                class="form-control @error('cod_postal') is-invalid @enderror" name="cod_postal" value="{{ old('cod_postal') }}">
            @error('cod_postal')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Email</span>
			</div>
            <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror"
                name="email" value="{{ old('email') }}">
            @error('email')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Email 2</span>
			</div>
            <input id="email2" type="email" placeholder="Email 2" class="form-control @error('email2') is-invalid @enderror"
                name="email2" value="{{ old('email2') }}">
            @error('email2')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Id rci</span>
			</div>
            <input id="id_rci" type="text" placeholder="Id rci"
                class="form-control @error('id_rci') is-invalid @enderror" name="id_rci"
                value="{{ old('id_rci') }}">
            @error('id_rci')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Id interval</span>
			</div>
            <input id="id_interval" type="text" placeholder="id interval"
                class="form-control @error('id_interval') is-invalid @enderror" name="id_interval"
                value="{{ old('id_interval') }}">
            @error('id_interval')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Telefono laboral</span>
			</div>
            <input id="telefono_laboral" type="text" placeholder="Telefono laboral"
                class="form-control @error('telefono_laboral') is-invalid @enderror" name="telefono_laboral"
                value="{{ old('telefono_laboral') }}">
            @error('telefono_laboral')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Nombre de conyugue</span>
			</div>
            <input id="nombre_conyugue" type="text" placeholder="Nombre de conyugue"
                class="form-control @error('nombre_conyugue') is-invalid @enderror" name="nombre_conyugue"
                value="{{ old('nombre_conyugue') }}">
            @error('nombre_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Apellido de conyugue</span>
			</div>
            <input id="apellido_conyugue" type="text" placeholder="Apellido de conyugue"
                class="form-control @error('apellido_conyugue') is-invalid @enderror" name="apellido_conyugue"
                value="{{ old('apellido_conyugue') }}">
            @error('apellido_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Dni de conyugue</span>
			</div>
            <input id="dni_conyugue" type="text" placeholder="Dni conyugue"
                class="form-control @error('dni_conyugue') is-invalid @enderror" name="dni_conyugue"
                value="{{ old('dni_conyugue') }}">
            @error('dni_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Fecha nacimiento de conyugue</span>
			</div>
            <input id="fecha_naciemiento_conyugue" type="date" placeholder="fecha_naciemiento_conyugue"
                class="form-control @error('fecha_naciemiento_conyugue') is-invalid @enderror"
                name="fecha_naciemiento_conyugue" value="{{ old('fecha_naciemiento_conyugue') }}">

            @error('fecha_naciemiento_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Email de conyugue</span>
			</div>
            <input id="email_conyugue" type="email" placeholder="Email conyuge"
                class="form-control @error('email_conyugue') is-invalid @enderror" name="email_conyugue"
                value="{{ old('email_conyugue') }}">
            @error('email_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Celular de conyugue</span>
			</div>
            <input id="celular_conyugue" type="text" placeholder="Celular de conyugue"
                class="form-control @error('celular_conyugue') is-invalid @enderror" name="celular_conyugue"
                value="{{ old('celular_conyugue') }}">
            @error('celular_conyugue')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
			<div class="input-group-append">
                <span class="input-group-text" >Hotel</span>
			</div>
            <select class="form-control  @error('hotel') is-invalid @enderror" id="hotel" name="hotel" placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel') == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                           color: white;
                           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row" >
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text" >Calendario</span>
            </div>
            <input type="file" class="form-control  @error('calendario') is-invalid @enderror" id="calendario" name="calendario"
                placeholder="calendario" />
            @error('calendario')
                <span class="invalid-feedback" style="background-color: red;
                       color: white;
                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear
            </button>
        </div>
    </div>
@endsection
