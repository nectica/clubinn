<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
    <table class="table table-bordered table-clubinn">
        <thead>
            <tr>
                <th scope="col" class="text-nowrap">fecha_vencimiento</th>
                <th scope="col" class="text-nowrap">Nombre</th>
                <th scope="col" class="text-nowrap">Comprobante</th>
                <th scope="col" class="text-nowrap">N°</th>
                <th>Importe</th>
                <th>Temporada</th>
                <th>Año</th>
            </tr>

        <tbody>
            @foreach ($clientes as $cliente)
                <tr>
                    <td>{{ $cliente->fecha_vencimiento ?: '-' }} </td>
                    <td>{{ $cliente->nombre ?: '-' }}</td>
                    <td>{{ $cliente->comprobante ?: '-' }}</td>

                    <td>{{ $cliente->nro ?: '-' }}</td>

                    <td>{{ $cliente->importe ?: '-' }}</td>
                    <td>{{ $cliente->temporada ?: '-' }}</td>
                    <td>{{ $cliente->anio ?: '-' }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
