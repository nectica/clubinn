<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>

<body>
    <table class="table table-bordered table-clubinn">
        <thead>
            <tr>
                <th scope="col" class="text-nowrap">Identificación</th>
                <th scope="col" class="text-nowrap">Id interval</th>
                <th scope="col" class="text-nowrap">Id Rci</th>
                <th scope="col" class="text-nowrap">Dni</th>
                <th scope="col" class="text-nowrap">Cuil</th>
                <th scope="col" class="text-nowrap">Nombre</th>
                <th scope="col" class="text-nowrap">apellido</th>
                <th scope="col" class="text-nowrap">Email</th>
                <th scope="col" class="text-nowrap">Email2</th>
                <th scope="col" class="text-nowrap">Telefono</th>
                <th scope="col" class="text-nowrap">Telefono laboral</th>
                <th scope="col" class="text-nowrap">Celular</th>
                <th scope="col" class="text-nowrap">Fecha de nacimiento</th>
                <th scope="col" class="text-nowrap">Domicilio</th>
                <th scope="col" class="text-nowrap">Localidad</th>
                <th scope="col" class="text-nowrap">Codigo postal</th>
                <th scope="col" class="text-nowrap">Provincia</th>
                <th scope="col" class="text-nowrap">Pais</th>


				<th scope="col" class="text-nowrap">Nombre de conyugue</th>
				<th scope="col" class="text-nowrap">Apellido de conyugue</th>
				<th scope="col" class="text-nowrap">Dni de conyugue</th>
				<th scope="col" class="text-nowrap">Email de conyugue</th>
				<th scope="col" class="text-nowrap">Fecha de fecha denacimiento de conyugue</th>

                <th scope="col" class="text-nowrap">Hotel</th>
                <th scope="col" class="text-nowrap">Unidad</th>
                <th scope="col" class="text-nowrap">Temporadad</th>

            </tr>

        <tbody>
            @foreach ($clientes as $cliente)
                @foreach ($cliente->unidades as $unidad)

                    <tr>
                        <td>{{ $cliente->identificacion }} </td>
                        <td>{{ $cliente->id_interval }} </td>
                        <td>{{ $cliente->id_rci }} </td>
                        <td>{{ $cliente->dni }} </td>
                        <td>{{ $cliente->cuil }} </td>
                        <td>{{ $cliente->nombre }}</td>
                        <td>{{ $cliente->apellido }}</td>
                        <td>{{ $cliente->email }}</td>
                        <td>{{ $cliente->email2 }}</td>
                        <td>{{ $cliente->telefono }}</td>
                        <td>{{ $cliente->telefono_laboral }}</td>
                        <td>{{ $cliente->celular }}</td>
                        <td>{{ $cliente->fecha_nacimiento }}</td>
                        <td>{{ $cliente->domicilio }}</td>
                        <td>{{ $cliente->localidad }}</td>
                        <td>{{ $cliente->cod_postal }}</td>
                        <td>{{ $cliente->provincia }}</td>
                        <td>{{ $cliente->pais }}</td>


                        <td>{{ $cliente->nombre_conyugue }}</td>
                        <td>{{ $cliente->apellido_conyugue }}</td>
                        <td>{{ $cliente->dni_conyugue }}</td>
                        <td>{{ $cliente->email_conyugue }}</td>
                        <td>{{ $cliente->fecha_naciemiento_conyugue }}</td>

                        <td>{{ $cliente->hotelR->nombre }}</td>
                        <td>{{ $unidad->nombre }}</td>
                        <td>{{$unidad->temporadas->where('id', $unidad->pivot->temporada)->first()->nombre }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>

</html>
