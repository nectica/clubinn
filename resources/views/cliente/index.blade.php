@extends('layouts.layout')
@section('title', 'Cliente')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<i class="fas fa-user-circle fa-3x" style="color: #e8a02f !important; "></i>
			<h1 class="title-clubinn"> Cliente</h1>
		</div>
		<div class="col-12 col-xl-8 p-0 d-flex justify-content-end">
			<form  class="col-5 input-group" action="" method="get">
				<input placeholder="Filtrar por nombre, apellido, hotel o email." name="filter" class="form-control h-100" type="text" value="{{ old('filter', $filter) }}">
				<div class="input-group-append"><button class="btn btn-success " type="submit">Filtrar</button></div>
			</form>
			<div class="col">
				<a href="{{ route('cliente.exportar.contratos',old('filter', $filter) ) }}" type="button"
					class="btn btn-block btn-clubinn-blue clubinn-blue-color" >Exportar Contratos</a>
			</div>
			<div class="col">
				<a href="{{ route('imputar.todo') }}" type="button"
					class="btn btn-block btn-clubinn-blue clubinn-blue-color" id="imputarTodo">Imputar Todo</a>
			</div>
			<div class="col">
				<a href="{{ route('cliente.create') }}" type="button"
					class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Crear Cliente</a>
			</div>
		</div>
    </div>
    <div class="row ">
        <div class="col-12 p-0 table-responsive">
            @if ($clientes->isEmpty())
                <p class="lead">No existen clientes.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('identificacion', 'Identificación', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('nombre', 'Nombre', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('apellido', 'Apellido', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('celular', 'Celular', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('email', 'Email', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('hotel', 'Hotel', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($clientes as $cliente)
                            <tr>
                                <td >{{ $cliente->identificacion }} </td>
                                <td >{{ $cliente->nombre }}</td>
                                <td >{{ $cliente->apellido }}</td>
                                <td >{{ $cliente->celular }}</td>
                                <td >{{ $cliente->email }}</td>
                                <td >{{ $cliente->hotelR->nombre }}</td>

                                <td style="
								min-width: 100px;
							">
                                    @can('cliente-edit')
                                        <a href="{{ route('cliente.edit', $cliente->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('cliente-list')

										<a href="{{ route('cliente.show', $cliente->id)}}" class="btn btn-outline-success btn-sm" disabled><i
												class="far fa-eye"></i></a>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($clientes, 'appends') ? $clientes->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
	<script type="text/javascript">
		window.addEventListener('DOMContentLoaded', function(){
			class PrevenirEnvio {
				constructor() {
					this.elementId, this.msg = '';
				}
				getElementById( id, selector = ''){
					return document.querySelector('#' + id+selector);
				}
				preventDefault= (evt) =>{
					if (!confirm(this.msg)) {
						evt.preventDefault();
					}
				}
				addClickListener(){
					const button = this.getElementById(this.elementId);
					button.addEventListener('click', this.preventDefault)
				}
			}

			const prevenir = new PrevenirEnvio();
			prevenir.elementId = 'imputarTodo'
			prevenir.msg = 'Quieres imputar los pagos de todos los clientes ?';
			prevenir.addClickListener();
		})
	</script>
@endsection
