@extends('partials.form.form')

@section('form-title', 'Actualizar contraseña')
@section('form-route', route('cliente.reset.update', $tokenCliente->cliente_id))
@section('form-content')
    @method('PUT')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <input type="hidden" name="token" value="{{ $tokenCliente->token }}">
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text">Email</span>
            </div>
            <input placeholder="email" class="form-control" value="{{ $tokenCliente->cliente->email }}" disabled>
            @error('email')
                <span class="invalid-feedback" style="background-color: red;
                                                                color: white;
                                                                margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text">Clave</span>
            </div>
            <input type="password" placeholder="clave" class="form-control @error('password') is-invalid @enderror"
                name="password">
            @error('password')
                <span class="invalid-feedback" style="background-color: red;
                                                                color: white;
                                                                margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5 input-group">
            <div class="input-group-append">
                <span class="input-group-text">Confirmar clave</span>
            </div>
            <input type="password" placeholder="confirmacion de la clave"
                class="form-control  @error('password_confirmation') is-invalid @enderror" name="password_confirmation">
            @error('password_confirmation')
                <span class="invalid-feedback" style="background-color: red;
                                                                color: white;
                                                                margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar contraseña
            </button>
        </div>
    </div>
@endsection
