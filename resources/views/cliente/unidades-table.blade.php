
<div class="col-12  p-0 table-responsive">
	@if ($cliente->unidades->isEmpty())
		<p class="lead">No existen unidades.</p>
	@else
		<table class="table table-bordered table-clubinn" id="unidad-table">
			<thead>
				<tr>
					<th scope="col" class="text-nowrap clubinn-th-text">Nombre</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Capacidad</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Capacidad maxima</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Temporada</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Semana</th>
					<th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($cliente->unidades as $unidad)
					<tr>
						<td class="text-gergal-color">{{ $unidad->nombre }}</td>
						<td class="text-gergal-color">{{ $unidad->capacidad }}</td>
						<td class="text-gergal-color">{{ $unidad->capacidad_maxima }}</td>
						<td class="text-gergal-color">
							{{ $temporadas->firstWhere('id', $unidad->pivot->temporada)->nombre }}
						</td>
						<td class="text-gergal-color">{{ $unidad->pivot->unidad ?: 0 }}</td>
						<td class="text-gergal-color">{{ $unidad->hotelR->nombre }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	@endif
</div>
