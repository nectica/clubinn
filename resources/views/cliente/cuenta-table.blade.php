<div class="row">
    <div class="col-12 col-xl-11 p-0 d-flex justify-content-end">

        <div class="co-2 px-2">
            <a href="{{ route('imputar.cliente', $cliente->id) }}" type="button"
                class="btn btn-block btn-clubinn-blue clubinn-blue-color" id="imputarTodo"> Imputacion automatica</a>
        </div>
        <div class="co-2 px-2">
            <a href="{{ route('imputar.cliente.recibos', $cliente->id) }}" type="button"
                class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Imputar pago</a>
        </div>
    </div>
</div>
<div class="col-12  p-0 table-responsive">
    @if (count($cliente->ccStore) == 0)
        <p class="lead">No existen registro de cuenta corriente cargado.</p>
    @else
        <table class="table table-bordered table-clubinn" id="cuenta-table">
            <thead>
                <tr>
                    <th scope="col" class="text-nowrap clubinn-th-text">fecha</th>
                    <th scope="col" class="text-nowrap clubinn-th-text">descripcion</th>
                    <th scope="col" class="text-nowrap">Importe</th>
                    <th>debe</th>
                    <th>Haber</th>
                    <th></th>
                </tr>

            </thead>
            <tbody>
                @foreach ($cliente->ccStore as $cc)
                    <tr>
                        <td class="text-gergal-color text-white">{{ $cc->tipo == 2 ? '-' : $cc->fecha_orden }}</td>
                        <td class="text-gergal-color text-white">{{ $cc->descripcion }}</td>

                        <td class="text-gergal-color text-white">
                            {{ $cc->importe . ' $' }}
                        </td>
                        <td class="text-gergal-color text-white">
                            {{ $cc->debe ?: '-' }}
                        </td>
                        <td class="text-gergal-color text-white">
                            {{ $cc->haber ?: '-' }}

                        </td>
                        <td>
                            @if ($cc->tipo == 1 || $cc->tipo == 3)
                                @php
                                    $idParaEliminar = 0;
                                    if ($cc->tipo == 1) {
                                        $idParaEliminar = $cc->id_debe;
                                    } elseif ($cc->tipo == 3) {
                                        $idParaEliminar = $cc->id_haber;
                                    }
                                @endphp
                                @can('cbu-delete')
                                    <form style="display: inline-block;" name="{{ 'form-delete' }}"
                                        action="{{ route('CuentaCorriente.destroy', $idParaEliminar) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                class="fas fa-trash-alt"></i></button>
                                    </form>
                                @endcan
                            @endif
                            @if ($cc->tipo == 2)
                                <a href="{{ route('desimputar.pago', $cc->id) }}"
                                    class="btn btn-block btn-clubinn-blue clubinn-blue-color desimputar"
                                    id="{{ 'desimputar-' . $cc->id }}">
                                    Desimputar
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    @endif
    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            class PrevenirEnvio {
                constructor() {
                    this.elementId, this.msg = '';
                }
                getElementById(id, selector = '') {
                    return document.querySelector('#' + id + selector);
                }
                preventDefault = (evt) => {
                    if (!confirm(this.msg)) {
                        evt.preventDefault();
                    }
                }
                addClickListener() {
                    const button = this.getElementById(this.elementId);
                    button.addEventListener('click', this.preventDefault)
                }
            }
            const prevenir = new PrevenirEnvio();
            prevenir.elementId = 'imputarTodo'
            prevenir.msg = 'Quieres imputar los pagos de todos los clientes ?';
            prevenir.addClickListener();

            const buttons = document.querySelectorAll('.desimputar');
            for (const button of buttons) {
                const prevenir = new PrevenirEnvio();
                prevenir.elementId = button.id;
                prevenir.msg = 'Quieres desimputar los pagos de todos los clientes ?';
                prevenir.addClickListener();
            }
        })
    </script>
</div>
