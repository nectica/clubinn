@extends('layouts.layout')
@section('title', 'Cliente')
@section('head')
    <link href="{{ asset('JdataTable/datatables.min.css') }}" rel="stylesheet">

@endsection

@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
	@if ($message = Session::get('danger'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col p-0 d-flex align-items-center gap-title">
            <i class="fas fa-user-circle fa-3x" style="color: #e8a02f !important; "></i>
            <h1 class="title-clubinn">
                {{ $cliente->full_name }}
                <small style="display: block">{{ $cliente->email }} <span>{{', saldo: '. $cliente->saldo . '$, saldo vencido: '.$cliente->saldo_vencido.'$' }}</span> </small>
            </h1>
        </div>
		<div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
			<a href="{{ route('cliente.saldo.compuesto', $cliente->id) }}" type="button"
				class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Exportar Saldo <i class="fas fa-file-excel" style="color: white; width: 2em; height:1em;"></i></a>
		</div>
    </div>

    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="unidad-tab" data-toggle="tab" href="#unidad" role="tab"
                        aria-controls="unidad" aria-selected="true">Unidades</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="usos-tab" data-toggle="tab" href="#usos" role="tab"
                        aria-controls="profile" aria-selected="false">Usos</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="recibo-tab" data-toggle="tab" href="#recibos" role="tab"
                        aria-controls="recibo" aria-selected="false">Recibos</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="cuenta-tab" data-toggle="tab" href="#cuenta" role="tab"
                        aria-controls="cuenta" aria-selected="false">Cuenta corriente</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="comentario-tab" data-toggle="tab" href="#comentarios" role="tab"
                        aria-controls="comentario" aria-selected="false">Comentarios</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="unidad" role="tabpanel" aria-labelledby="unidad-tab">
                    @include('cliente.unidades-table')
                </div>
                <div class="tab-pane fade" id="usos" role="tabpanel" aria-labelledby="usos-tab">
                    @include('cliente.usos-table')
                </div>
                <div class="tab-pane fade" id="recibos" role="tabpanel" aria-labelledby="recibo-tab">
                    @include('cliente.recibo-table')
                </div>
                <div class="tab-pane fade" id="cuenta" role="tabpanel" aria-labelledby="cuenta-tab">
                    @include('cliente.cuenta-table')
                </div>
                <div class="tab-pane fade" id="comentarios" role="tabpanel" aria-labelledby="comentario-tab">
                    @include('cliente.comentarios-table')

                </div>
            </div>
        </div>

    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/delete.js') }}"></script>
    <script src="{{ asset('JdataTable/datatables.min.js') }}"></script>
    <script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>
    <script type="text/javascript">
        $('#comentarios-table').DataTable({
            language: {
                url: "{{ asset('JdataTable/languages/spanish.json') }}"
            },
			order: [],
        })
        $('#cuenta-table').DataTable({
            language: {
                url: "{{ asset('JdataTable/languages/spanish.json') }}"
            },
			order: [],
        })
        $('#recibo-table').DataTable({
            language: {
                url: "{{ asset('JdataTable/languages/spanish.json') }}"
            },
			order: [],
        })
        $('#uso-table').DataTable({
            language: {
                url: "{{ asset('JdataTable/languages/spanish.json') }}"
            },
			order: [],
        })
        $('#unidad-table').DataTable({
            language: {
                url: "{{ asset('JdataTable/languages/spanish.json') }}"
            },
			order: [],
        })


        window.onload = function() {
            /* confirmar recibo */

            const formConfirmar = document.querySelectorAll('.btn-confirm');
            for (const confirmar of formConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarForm);
            }

            function handlerConfirmarForm(evt) {
                evt.preventDefault();
                /* if (!confirm('')) {
        					evt.preventDefault();
                        } */
                const id = evt.target.dataset.id;
                const confirmarRecibo = (result) => {
                    if (result.isConfirmed) {
                        const form = document.querySelector('#form-confirm-' + id);

                        const noticacionConfirm = (result) => {
                            if (result.isConfirmed) {
                                const notificacion = document.querySelector('#notificacion-' + id);
                                notificacion.value = 1;
                            }
                            form.submit();
                        }
                        sweetAlert('warning', 'Desea enviar notificacion al cliente?', "",
                            'Si, enviar notificacion!', 'No enviar.', noticacionConfirm);
                    }

                }
                sweetAlert('success', 'Desea confirmar este recibo ?', "",
                    'Si, confirmar!', 'Cancelar.', confirmarRecibo);
            }
            /* end recibos  */
            function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
                Swal.fire({
                    title: title,
                    text: msg,
                    icon: icon,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: textConfirmButton,
                    cancelButtonText: textCancelButton
                }).then(callbackResult);
            }

			/* start semana uso */
			const butonnsConfirmar = document.querySelectorAll('.btn-confirmar');
            const butonnsRechazar = document.querySelectorAll('.btn-rechazar');
            for (const confirmar of butonnsConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarButton);
            }
            for (const rechazar of butonnsRechazar) {
                rechazar.addEventListener('click', handlerRechazarButton);
            }

            function handlerConfirmarButton(evt) {
                evt.preventDefault();

				const confirmFunction = (result) => {
                    if (result.isConfirmed) {
						const form = document.querySelector('#form-confirm-'+evt.target.dataset.id);
						const id = 'notificacion-confirmar-'+evt.target.dataset.id;
						askingNotification( id, form);

                    }
                };
				sweetAlert('success', 'Estas seguro que quieres Confirmar?', "Cliente sera notificado!", 'Si, confirmar!', 'Cancelar.', confirmFunction);

            }

            function handlerRechazarButton(evt) {
                evt.preventDefault();
				const confirmFunction = (result) => {
                    if (result.isConfirmed) {
						const form = document.querySelector('#form-delete-'+evt.target.dataset.id);
						const id = 'notificacion-rechazar-'+evt.target.dataset.id;

						askingNotification( id, form);

                    }
                };
				sweetAlert('warning', 'Estas seguro que quieres rechazar?', "Cliente sera notificado!", 'Si, rechazar!', 'Cancelar.', confirmFunction);

            }

			function askingNotification(id, form){
				const input = document.querySelector('#'+id);
				const noticacionAsking = (result) =>{
					if (result.isConfirmed) {
						input.value = 1;
					}else if(result.isDismissed){
						input.value = 0;

					}
					form.submit();
				}
				sweetAlert('warning', 'Quiere enviar notificacion al cliente?', "", 'Si, enviar!', 'no enviar.', noticacionAsking);

			}
			/* end semana uso */
        }
    </script>
@endsection
@section('script')

<script src="{{ asset('sweetalert2/dist/sweetalert2.all.js') }}"></script>


    <script type="text/javascript">
        window.onload = function() {

            const butonnsConfirmar = document.querySelectorAll('.btn-confirmar');
            const butonnsRechazar = document.querySelectorAll('.btn-rechazar');
            for (const confirmar of butonnsConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarButton);
            }
            for (const rechazar of butonnsRechazar) {
                rechazar.addEventListener('click', handlerRechazarButton);
            }

            function handlerConfirmarButton(evt) {
                evt.preventDefault();

                const confirmFunction = (result) => {
                    if (result.isConfirmed) {
                        document.querySelector('#form-confirm-' + evt.target.dataset.id).submit();

                    }
                };
                sweetAlert('success', 'Estas seguro que quieres Confirmar?', "Cliente sera notificado!",
                    'Si, confirmar!', 'Cancelar.', confirmFunction);

            }

            function handlerRechazarButton(evt) {
                evt.preventDefault();
                const confirmFunction = (result) => {
                    if (result.isConfirmed) {
                        document.querySelector('#form-delete-' + evt.target.dataset.id).submit();

                    }
                };
                sweetAlert('warning', 'Estas seguro que quieres rechazar?', "Cliente sera notificado!", 'Si, rechazar!',
                    'Cancelar.', confirmFunction);

            }


            function sweetAlert(icon, title, msg, textConfirmButton, textCancelButton, callbackResult) {
                Swal.fire({
                    title: title,
                    text: msg,
                    icon: icon,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: textConfirmButton,
                    cancelButtonText: textCancelButton
                }).then(callbackResult);
            }

        }
    </script>
@endsection
