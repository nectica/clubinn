@extends('layouts.layout')
@section('title', 'Unidades')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
	<div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<h1 class="title-clubinn"> Unidad</h1>
		</div>
		<div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
			<a href="{{ route('unidad.create') }}" type="button"
				class="btn btn-block btn-clubinn-blue clubinn-blue-color">Crear Unidad</a>
		</div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($unidades->isEmpty())
                <p class="lead">No existen unidades.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('nombre', 'Nombre', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('capacidad', 'Capacidad', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col"  class="text-nowrap">@sortablelink('capacidad_maxima', 'Capacidad maxima', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('hotel', 'Hotel', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($unidades as $unidad)
                            <tr>
                                <td class="text-gergal-color">{{ $unidad->nombre }}</td>
                                <td class="text-gergal-color">{{ $unidad->capacidad }}</td>
                                <td class="text-gergal-color">{{ $unidad->capacidad_maxima }}</td>
                                <td class="text-gergal-color">{{ $unidad->hotelR->nombre }}</td>

                                <td>
                                    @can('unidad-edit')
                                        <a href="{{ route('unidad.edit', $unidad->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('unidad-delete')
                                        <form style="display: inline-block;" name="form-delete" action="{{ route('unidad.destroy', $unidad->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($unidades, 'appends') ? $unidades->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
@endsection
