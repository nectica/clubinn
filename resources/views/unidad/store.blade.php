@extends('partials.form.form')

@section('form-title', 'Unidad')
@section('form-route', route('unidad.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="nombre" placeholder="Nombre" type="text"
                class="form-control @error('nombre') is-invalid @enderror" name="nombre"
                value="{{ old('nombre') }}">
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="capacidad" placeholder="Capacidad" type="text"
                class="form-control @error('capacidad') is-invalid @enderror" name="capacidad"
                value="{{ old('capacidad') }}">
            @error('capacidad')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
	<div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="capacidad_maxima" placeholder="Capacidad maxima" type="text"
                class="form-control @error('capacidad_maxima') is-invalid @enderror" name="capacidad_maxima"
                value="{{ old('capacidad_maxima') }}">
            @error('capacidad_maxima')
                <span class="invalid-feedback" style="background-color: red;
                               color: white;
                               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('hotel') is-invalid @enderror" id="hotel" name="hotel" placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel') == $hotel->id ? 'selected' : ''}}>{{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
                   color: white;
                   margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear
            </button>
        </div>
    </div>
@endsection
