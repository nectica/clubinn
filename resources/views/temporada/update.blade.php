@extends('partials.form.form')

@section('form-title', 'Temporada ')
@section('form-route', route('temporada.update', $temporada->id))
@section('form-content')
    @method('PUT')
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="identificacion" placeholder="Identificacion" type="text"
                class="form-control @error('identificacion') is-invalid @enderror" name="identificacion"
                value="{{ old('identificacion', $temporada->identificacion) }}">
            @error('identificacion')
                <span class="invalid-feedback" style="background-color: red;
              color: white;
              margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
            <input id="nombre" placeholder="Nombre" type="text" class="form-control @error('nombre') is-invalid @enderror"
                name="nombre" value="{{ old('nombre', $temporada->nombre) }}" autofocus>
            @error('nombre')
                <span class="invalid-feedback" style="background-color: red;
               color: white;
               margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>


    <div class="form-group row">
        <div class="col-md-12 px-5">
            <select class="form-control  @error('hotel') is-invalid @enderror" id="hotel" name="hotel" placeholder="hotel">
                <option selected disabled>-- Seleccione hotel --</option>
                @foreach ($hotels as $hotel)
                    <option value="{{ $hotel->id }}" {{ old('hotel', $temporada->hotel) == $hotel->id ? 'selected' : '' }}>
                        {{ $hotel->nombre }}</option>
                @endforeach
            </select>
            @error('hotel')
                <span class="invalid-feedback" style="background-color: red;
           color: white;
           margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Actualizar Temporada
            </button>
        </div>
    </div>
@endsection
