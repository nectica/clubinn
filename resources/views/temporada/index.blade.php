@extends('layouts.layout')
@section('title', 'Temporada')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
	<div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<h1 class="title-clubinn"> Temporada</h1>
		</div>
		<div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
			<a href="{{ route('temporada.create') }}" type="button"
				class="btn btn-block btn-clubinn-blue clubinn-blue-color">Crear temporada</a>
		</div>
    </div>

    <div class="row">
        <div class="col-12  p-0 table-responsive">
            @if ($temporadas->isEmpty())
                <p class="lead">No existen Temporadas.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col"  class="text-nowrap">@sortablelink('identificacion', 'Identificación', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('nombre', 'Nombre', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('hotel', 'Hotel', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($temporadas as $temporada)
                            <tr>
                                <td class="text-gergal-color">{{ $temporada->identificacion }}</td>
                                <td class="text-gergal-color">{{ $temporada->nombre }}</td>
                                <td class="text-gergal-color">{{ $temporada->hotelR->nombre }}</td>

                                <td>
                                    @can('temporada-edit')
                                        <a href="{{ route('temporada.edit', $temporada->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('temporada-delete')
                                        <form style="display: inline-block;"  name="form-delete" action="{{ route('temporada.destroy', $temporada->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($temporadas, 'appends') ? $temporadas->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
@endsection
