@extends('partials.form.form')

@section('form-title', 'Comentario')
@section('form-route', route('comentario.store'))
@section('form-content')
    <div class="form-group row">
        <div class="col-md-12 px-5">
			<input type="hidden" name="cliente_id" value="{{ $cliente->id}}" >
            <input  type="text"
                class="form-control @error('cliente_id') is-invalid @enderror"
                value="{{ $cliente->full_name }}" disabled>
            @error('cliente_id')
                <span class="invalid-feedback" style="background-color: red;
                                       color: white;
                                       margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-12 px-5">
			<label for="observacion">Comentario sobre el cliente</label>

			<textarea  class="form-control @error('observacion') is-invalid @enderror" id="observacion" name="observacion"></textarea>
            @error('observacion')
                <span class="invalid-feedback" style="background-color: red;
                                        color: white;
                                        margin-top: 0;" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>


    <div class="form-group row mb-5">
        <div class="col-md-8 offset-4">
            <button type="submit" class="btn btn-success">
                Crear
            </button>
        </div>
    </div>
@endsection
