@extends('layouts.layout')
@section('title', 'Cliente')
@section('content')

    <div class="row h-120-px align-items-center justify-content-between">
        <div class="col p-0 d-flex align-items-center gap-title">
            <i class="fas fa-user-circle fa-3x" style="color: #e8a02f !important; "></i>
            <h1 class="title-clubinn">
				{{ $cliente->full_name }}
				<small style="display: block">{{ $cliente->email}}</small>
			</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="unidad-tab" data-toggle="tab" href="#unidad" role="tab"
                        aria-controls="unidad" aria-selected="true">Unidades</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="usos-tab" data-toggle="tab" href="#usos" role="tab" aria-controls="profile"
                        aria-selected="false">Usos</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#recibos" role="tab"
                        aria-controls="contact" aria-selected="false">Recibos</a>
                </li>
				<li class="nav-item" role="presentation">
                    <a class="nav-link" id="cuenta-tab" data-toggle="tab" href="#cuenta" role="tab"
                        aria-controls="cuenta" aria-selected="false">Cuenta corriente</a>
                </li>
				<li class="nav-item" role="presentation">
                    <a class="nav-link" id="comentario-tab" data-toggle="tab" href="#comentarios" role="tab"
                        aria-controls="comentario" aria-selected="false">Comentarios</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="unidad" role="tabpanel" aria-labelledby="unidad-tab">
                    <div class="col-12  p-0 table-responsive">
                        @if ($cliente->unidades->isEmpty())
                            <p class="lead">No existen unidades.</p>
                        @else
                            <table class="table table-bordered table-clubinn">
                                <thead>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Nombre</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Capacidad</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Capacidad maxima</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Temporada</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
                                </thead>
                                <tbody>
                                    @foreach ($cliente->unidades as $unidad)
                                        <tr>
                                            <td class="text-gergal-color">{{ $unidad->nombre }}</td>
                                            <td class="text-gergal-color">{{ $unidad->capacidad }}</td>
                                            <td class="text-gergal-color">{{ $unidad->capacidad_maxima }}</td>
                                            <td class="text-gergal-color">{{ $temporadas->firstWhere('id', $unidad->pivot->temporada)->nombre  }}</td>
                                            <td class="text-gergal-color">{{ $unidad->hotelR->nombre }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @endif
                    </div>
                </div>
                <div class="tab-pane fade" id="usos" role="tabpanel" aria-labelledby="usos-tab">
                    <div class="col-12  p-0 table-responsive">
                        @if ($cliente->usos->isEmpty())
                            <p class="lead">No existen Semana de Usos.</p>
                        @else
                            <table class="table table-bordered table-clubinn">
                                <thead>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Unidad</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Cliente</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Estado</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Fecha de vencimiento</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Año</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Dias</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Semana</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Temporada</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text">Hotel</th>
                                    <th scope="col" class="text-nowrap clubinn-th-text"></th>
                                </thead>
                                <tbody>
                                    @foreach ($cliente->usos as $uso)
                                        <tr>

                                            <td class="text-gergal-color">
                                                {{ $uso->unidad->unidadR->nombre }}</td>
                                            <td class="text-gergal-color">{{ $uso->cliente->full_name }}</td>
                                            <td class="text-gergal-color">{{ $uso->estado->nombre }}</td>
                                            <td class="text-gergal-color">{{ $uso->fecha_vencimiento->format('d/m/Y') }}
                                            </td>
                                            <td class="text-gergal-color">{{ $uso->anio }}</td>
                                            <td class="text-gergal-color">{{ $uso->dia }}</td>
                                            <td class="text-gergal-color">{{ $uso->semana }}</td>
                                            <td class="text-gergal-color">{{ $uso->temporada->nombre }}</td>
                                            <td class="text-gergal-color">{{ $uso->temporada->hotelR->nombre }}</td>
                                            <td style="min-width: 100px;">
                                                @if ($uso->estado->nombre == 'Solicitado')
                                                    <form style="display: inline-block;"
                                                        action="{{ route('usos.update', $uso->id) }}" method="POST">
                                                        @csrf
                                                        @method('PUT')
                                                        <button type="button"
                                                            class="btn btn-outline-primary btn-sm  my-1 btn-confirmar">Confirmar</button>
                                                    </form>
                                                @endif
                                                @can('uso-edit')
                                                @endcan
                                                @can('uso-delete')
                                                    <form style="display: inline-block;" name="form-delete"
                                                        action="{{ route('usos.destroy', $uso->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                                class="fas fa-trash-alt"></i></button>
                                                    </form>
                                                @endcan

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @endif

                    </div>
                </div>
                <div class="tab-pane fade" id="recibos" role="tabpanel" aria-labelledby="contact-tab">
					<div class="row">
						<div class="col-12  p-0 table-responsive">
							@if ($cliente->recibos->isEmpty())
								<p class="lead">No existen recibos cargado.</p>
							@else
								<table class="table table-bordered table-clubinn">
									<thead>
										<th scope="col" class="text-nowrap">@sortablelink('fecha_pago', 'Fecha', [], ['class' => '
											clubinn-th-text'])</th>
										<th scope="col" class="text-nowrap">@sortablelink('cliente.nombre', 'Cliente', [], ['class' => '
											clubinn-th-text'])</th>
										<th scope="col" class="text-nowrap">@sortablelink('importe', 'Importe', [], ['class' => '
											clubinn-th-text'])</th>
										<th scope="col" class="text-nowrap">@sortablelink('estado.nombre', 'Estado', [], ['class' => '
											clubinn-th-text'])</th>
										<th scope="col" class="text-nowrap"></th>
									</thead>
									<tbody>
										@php
											$classToStatus = ['Pendiente' => 'bg-warning', 'Aceptado' => 'bg-success', 'Anulado' => 'bg-danger'];

										@endphp
										@foreach ($cliente->recibos as $recibo)
											<tr class="{{ $classToStatus[$recibo->estado->nombre] }}">
												<td class="text-gergal-color text-white">{{ $recibo->fecha_pago->format('d/m/Y') }}</td>
												<td class="text-gergal-color text-white">{{ $recibo->cliente->full_name }}</td>
												<td class="text-gergal-color text-white">{{ $recibo->importe . ' $' }}</td>
												<td class="text-gergal-color text-white">{{ $recibo->estado->nombre }}</td>

												<td>
													<form name="formSubmit" class="form-confirm" style="display: inline-block;"
														action="{{ route('recibos.update', $recibo->id) }}" method="POST">
														@csrf
														@method('PUT')


														<button type="submit" class="btn btn-outline-primary btn-sm  my-1  btn-confirm"
															name="btnConfirmar">Confirmar</button>
													</form>

													@if ($recibo->estado->nombre == 'Pendiente')

													@endif
													@can('recibo-edit')
														<a href="{{ route('recibos.edit', $recibo->id) }}" type="button"
															class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
													@endcan


												</td>
											</tr>
										@endforeach
									</tbody>
								</table>

							@endif

						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="cuenta" role="tabpanel" aria-labelledby="cuenta-tab">
					<div class="row">
						<div class="col-12  p-0 table-responsive">
							@if ($cliente->full_cuenta_corrientes->isEmpty())
								<p class="lead">No existen registro de cuenta corriente cargado.</p>
							@else
								<table class="table table-bordered table-clubinn">
									<thead>
										<th scope="col" class="text-nowrap clubinn-th-text">fecha</th>
										<th scope="col" class="text-nowrap">Fecha vencimiento</th>
										<th scope="col" class="text-nowrap">Importe</th>
										<th scope="col" class="text-nowrap">Tipo de movimiento</th>

									</thead>
									<tbody>
										@foreach ($cliente->full_cuenta_corrientes as $cc)
											<tr >
												<td class="text-gergal-color text-white">{{ $cc->fecha }}</td>
												<td class="text-gergal-color text-white">{{ $cc->fecha_vencimiento }}</td>
												<td class="text-gergal-color text-white">{{ $cc->importe * $cc->movimiento->signo . ' $' }}</td>
												<td class="text-gergal-color text-white">{{ $cc->movimiento->nombre }}</td>


											</tr>
										@endforeach
									</tbody>
								</table>

							@endif

						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="comentarios" role="tabpanel" aria-labelledby="comentario-tab">
					@include('cliente.comentarios-table')

				</div>
            </div>
        </div>

    </div>

    <script type="text/javascript">
        window.onload = function() {

            const butonnsConfirmar = document.querySelectorAll('.btn-confirmar');
            for (const confirmar of butonnsConfirmar) {
                confirmar.addEventListener('click', handlerConfirmarButton);
            }

            function handlerConfirmarButton(evt) {
                if (!confirm('Desea confirmar esta solicitud de uso ?')) {
                    evt.preventDefault();
                }
                evt.submit();
            }
        }
    </script>
@endsection
