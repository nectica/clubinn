@extends('layouts.layout')
@section('title', 'Cliente')
@section('content')
    @if ($message = Session::get('success'))
        <div class="row justify-content-center">
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        </div>
    @endif
    <div class="row h-120-px align-items-center justify-content-between">
		<div class="col p-0 d-flex align-items-center gap-title">
			<i class="fas fa-user-circle fa-3x" style="color: #e8a02f !important; "></i>
			<h1 class="title-clubinn"> Cliente</h1>
		</div>
		<div class="col-8 col-xl-2 p-0 d-flex justify-content-end">
			<a href="{{ route('cliente.create') }}" type="button"
				class="btn btn-block btn-clubinn-blue clubinn-blue-color"> Crear Cliente</a>
		</div>
    </div>
    <div class="row ">
        <div class="col-12 p-0 table-responsive">
            @if ($clientes->isEmpty())
                <p class="lead">No existen clientes.</p>
            @else
                <table class="table table-bordered table-clubinn">
                    <thead>
                        <th scope="col" class="text-nowrap">@sortablelink('identificacion', 'Identificación', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('nombre', 'Nombre', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('apellido', 'Apellido', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('celular', 'Celular', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('email', 'Email', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap">@sortablelink('hotel', 'Hotel', [],  ['class' => ' clubinn-th-text'])</th>
                        <th scope="col" class="text-nowrap"></th>
                    </thead>
                    <tbody>
                        @foreach ($clientes as $cliente)
                            <tr>
                                <td >{{ $cliente->identificacion }} </td>
                                <td >{{ $cliente->nombre }}</td>
                                <td >{{ $cliente->apellido }}</td>
                                <td >{{ $cliente->celular }}</td>
                                <td >{{ $cliente->email }}</td>
                                <td >{{ $cliente->hotelR->nombre }}</td>

                                <td style="
								min-width: 100px;
							">
                                    @can('cliente-edit')
                                        <a href="{{ route('cliente.edit', $cliente->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('cliente-list')

										<a href="{{ route('cliente.show', $cliente->id)}}" class="btn btn-outline-success btn-sm" disabled><i
												class="far fa-eye"></i></a>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($clientes, 'appends') ? $clientes->appends(\Request::except('page'))->render() : '' }}

        </div>
    </div>
@endsection
