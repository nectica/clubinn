<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrecioTemporadaAniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precio_temporada_anios', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('temporada_id');
			$table->integer('anio');
			$table->unsignedBigInteger('precio');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('precio_temporada_anios');
    }
}
