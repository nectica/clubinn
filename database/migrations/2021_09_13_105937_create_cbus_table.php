<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCbusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cbus', function (Blueprint $table) {
            $table->id();
			$table->string('banco')->nullable();
			$table->string('titular')->nullable();
			$table->string('tipo_cuenta')->nullable();
			$table->string('nro_cuenta')->nullable();
			$table->string('cbu')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cbus');
    }
}
