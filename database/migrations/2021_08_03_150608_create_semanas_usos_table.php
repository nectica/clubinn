<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSemanasUsosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('semanas_usos', function (Blueprint $table) {
			$table->id();
			$table->integer('anio');
			$table->dateTime('fecha_vencimiento');
			$table->dateTime('fecha_uso')->nullable();
			$table->integer('semana')->nullable();
			$table->integer('dias_usado')->default(0);
			$table->boolean('fraccionado')->default(false);
			$table->unsignedBigInteger('cliente_id')->nullable();
			$table->unsignedBigInteger('unidad_id')->nullable();
			$table->unsignedBigInteger('hotel_id')->nullable();
			$table->unsignedBigInteger('temporada_id')->nullable();
			$table->unsignedBigInteger('estado_consulta_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semanas_usos');
    }
}
