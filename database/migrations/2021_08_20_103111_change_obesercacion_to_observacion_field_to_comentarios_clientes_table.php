<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeObesercacionToObservacionFieldToComentariosClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comentarios_clientes', function (Blueprint $table) {
			$table->renameColumn('obeservacion','observacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('observacion_field_to_comentarios_clientes', function (Blueprint $table) {
            //
        });
    }
}
