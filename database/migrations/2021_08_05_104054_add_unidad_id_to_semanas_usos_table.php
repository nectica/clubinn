<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnidadIdToSemanasUsosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('semanas_usos', function (Blueprint $table) {
            $table->renameColumn('unidad_id', 'unidad_por_cliente')->nullable();
        });
		Schema::table('semanas_usos', function (Blueprint $table){
			$table->unsignedBigInteger('unidad_id')->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('semanas_usos', function (Blueprint $table) {
            //
        });
    }
}
