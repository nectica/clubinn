<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebeRelHaberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debe_rel_haber', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_deber')->nullable();
            $table->unsignedBigInteger('id_haber')->nullable();
            $table->unsignedBigInteger('importe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debe_rel_haber');
    }
}
