<?php

namespace Database\Seeders;

use App\Models\EstadoSemanaUso;
use Illuminate\Database\Seeder;

class CreateEstadoSemanaUso extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = ['Pendiente', 'Solicitado', 'Guardado', 'Depositado','Depositado confirmado', 'Usado', 'Confirmado', 'Rechazado'];
		foreach ($estados as $key => $estado) {
			EstadoSemanaUso::updateOrCreate(['nombre' => $estado]);
		}
    }
}
