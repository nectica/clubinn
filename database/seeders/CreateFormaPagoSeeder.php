<?php

namespace Database\Seeders;

use App\Models\FormaPago;
use Illuminate\Database\Seeder;

class CreateFormaPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$formas_pagos = [
			['nombre' => 'Efectivo', 'abreviatura' => 'EFE'],
			['nombre' => 'transferencia', 'abreviatura' => 'TRANSF'],
			['nombre' => 'Tarjeta de credito', 'abreviatura' => 'TDC'],
		];
		foreach ($formas_pagos as $forma_pago){
			FormaPago::firstOrCreate($forma_pago);
		}

    }
}
