<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate([
            'email' => 'admin@admin.com'],
            ['name' => 'Admin admin',
            'password' => bcrypt('123456')]);


		$permissions = Permission::pluck('id','id')->all();
		$roleAdmin = Role::firstOrCreate(['name' => 'Admin']);

        $roleAdmin->syncPermissions($permissions);
        $user->assignRole([$roleAdmin ->id]);
    }
}
