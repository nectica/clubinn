<?php

namespace Database\Seeders;

use App\Models\Cbu;
use Illuminate\Database\Seeder;

class cbuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cbus_array = [
			[
				'banco' => 'Banco Supervielle',
				'titular' => 'Club Inn SA',
				'tipo_cuenta' => 'Cuenta Corriente',
				'nro_cuenta' => '20-54195-001',
				'cbu' => '02701000-10000541950018',
				'cuil' => '30-70962914-6',
			],
			[
				'banco' => 'Banco Provincia Bs.As',
				'titular' => 'Club Inn SA',
				'tipo_cuenta' => 'Cuenta Corriente',
				'nro_cuenta' => '55043-7',
				'cbu' => '01400007-01100005504372',
				'cuil' => '30-70962914-6',
			],
			[
				'banco' => 'Banco Macro',
				'titular' => 'Club Inn SA',
				'tipo_cuenta' => 'Cuenta Corriente',
				'nro_cuenta' => '358909409022831',
				'cbu' => '28505893-30094090228311',
				'cuil' => '30-70962914-6',
			],
		];
		foreach ($cbus_array as $cbu_array){
			$cbu =  Cbu::firstOrCreate($cbu_array);
		}
    }
}
