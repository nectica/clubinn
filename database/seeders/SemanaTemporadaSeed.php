<?php

namespace Database\Seeders;

use App\Models\SemanaTemporada;
use App\Models\Temporada;
use Illuminate\Database\Seeder;

class SemanaTemporadaSeed extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$sai = Temporada::where('identificacion', 'SAI')->first();
		$fli = Temporada::where('identificacion', 'FLI')->first();
		for ($i = 28; $i <= 33; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $sai->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $fli->id]);
		}
		$ali = Temporada::where('identificacion', 'ALI')->first();
		$afm = Temporada::where('identificacion', 'AFM')->first();

		SemanaTemporada::firstOrCreate(['semana' => 27, 'temporada_id' => $ali->id]);
		SemanaTemporada::firstOrCreate(['semana' => 27, 'temporada_id' => $afm->id]);
		/* afm y ali 34-38 */
		for ($i = 34; $i <= 38; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $ali->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $afm->id]);
		}
		$avfHdc = Temporada::where('identificacion', 'AVF')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hdc');
		})->first();
		$avfHds1 = Temporada::where('identificacion', 'AVF')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hds1');
		})->first();
		$avfHds2 = Temporada::where('identificacion', 'AVF')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hds2');
		})->first();
		$vfl = Temporada::where('identificacion', 'VFL')->first();
		/* 51 y 52 ,a avf ,afm */
		for ($i = 51; $i <= 52; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHdc->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHds1->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHds1->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $afm->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $vfl->id]);
		}

		/*afm , avf, vfl 1-9 */
		for ($i = 1; $i <= 9; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $afm->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHdc->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHds1->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $avfHds2->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $vfl->id]);
		}
		$mflHdc = Temporada::where('identificacion', 'MFL')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hdc');
		})->first();
		$mflHds1 = Temporada::where('identificacion', 'MFL')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hds1');
		})->first();
		$mflHds2 = Temporada::where('identificacion', 'MFL')->whereHas('hotelR', function($query){
			return $query->where('nombre', 'hds2');
		})->first();
		$mef = Temporada::where('identificacion', 'MEF')->first();
		for ($i=10; $i <= 26 ; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mflHdc->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mef->id]);
		}
		for ($i=39; $i <= 50; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mflHdc->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mef->id]);
		}
		for ($i=10; $i <= 50 ; $i++) {
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mflHds1->id]);
			SemanaTemporada::firstOrCreate(['semana' => $i, 'temporada_id' => $mflHds2->id]);
		}

	}
}
