<?php

namespace Database\Seeders;

use App\Models\Movimiento;
use Illuminate\Database\Seeder;

class MovimientoCCSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$movimimientos = [
			[
				'nombre' => 'Expensas',
				'abreviatura' => 'EXP',
				'signo' => 1,
			],
			[
				'nombre' => 'Recibo',
				'abreviatura' => 'REC',
				'signo' => -1,
			],
			[
				'nombre' => ' Ajuste de saldo positivo',
				'abreviatura' => 'AS+',
				'signo' => 1,
			],
			[
				'nombre' => ' Ajuste de saldo negativo',
				'abreviatura' => 'AS-',
				'signo' => -1,
			],
			[
				'nombre' => 'Diferencia de Temporada',
				'abreviatura' => 'DIFT',
				'signo' => 1,
			],
			[
				'nombre' => 'Diferencia de Capacidad',
				'abreviatura' => 'DIFC',
				'signo' => 1,
			],
			[
				'nombre' => 'Tasa de Intercambio',
				'abreviatura' => 'TAI',
				'signo' => 1,
			],
			[
				'nombre' => 'Expensa Extraordinaria',
				'abreviatura' => 'Exp.Ex',
				'signo' => 1,
			],
			[
				'nombre' => 'Interes por demora',
				'abreviatura' => 'Int.Mora',
				'signo' => 1,
			],
			[
				'nombre' => 'Interes de financiacion',
				'abreviatura' => 'Int.fin',
				'signo' => 1,
			],
			[
				'nombre' => 'Expensa Extraordinaria Parcial',
				'abreviatura' => 'Exp.Ex.Parc',
				'signo' => 1,
			],
		];
		foreach ($movimimientos as $movimiento){
			$resto = $movimiento;
			unset( $resto['nombre']);
			Movimiento::updateOrCreate(['nombre' => $movimiento['nombre']],$resto );
		}
	}
}
