<?php

namespace Database\Seeders;

use App\Models\TarjetaCredito;
use Illuminate\Database\Seeder;

class TarjetaCreditoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tc_arrays = [
			['nombre' => 'visa'],
			['nombre' => 'amex'],
			['nombre' => 'master'],
			['nombre' => 'cabal'],
			['nombre' => 'naranja'],
		];
		foreach ($tc_arrays as $tc ){
			$tc = TarjetaCredito::firstOrCreate($tc);
		}
    }
}
