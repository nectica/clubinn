<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'cliente-list',
            'cliente-create',
            'cliente-edit',
            'cliente-delete',
            'unidad-list',
            'unidad-create',
            'unidad-edit',
            'unidad-delete',
            'hotel-list',
            'hotel-create',
            'hotel-edit',
            'hotel-delete',
            'temporada-list',
            'temporada-create',
            'temporada-edit',
            'temporada-delete',
			'consulta-list',
            'consulta-create',
            'consulta-edit',
            'consulta-delete',
			'expensa-list',
            'expensa-create',
            'expensa-edit',
            'expensa-delete',
			'recibo-list',
            'recibo-create',
            'recibo-edit',
            'recibo-delete',
			'uso-list',
            'uso-create',
            'uso-edit',
            'uso-delete',
			'parametro-list',
            'parametro-create',
            'parametro-edit',
            'parametro-delete',
			'cbu-list',
            'cbu-create',
            'cbu-edit',
            'cbu-delete',
        ];
        foreach ($permissions as $permission) {
            Permission::firstOrCreate(['name' => $permission]);
        }
    }
}
