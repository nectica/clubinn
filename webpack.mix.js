const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
	.js('resources/js/delete.js', 'public/js')
	.js('resources/js/recibo.js', 'public/js')
	.styles(['resources/css/custom.css'],'public/css/custom.css' )
	.sass('resources/sass/app.scss', 'public/css')
	.copyDirectory('resources/JdataTable', 'public/JdataTable')
	.copyDirectory('resources/Datatable-original', 'public/Datatable-original')
	.copyDirectory('resources/sweetalert2', 'public/sweetalert2')
	.setResourceRoot('../')

	.sourceMaps();
