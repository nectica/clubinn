-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-08-2021 a las 22:33:40
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `clubinn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL,
  `identificacion` varchar(10) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `telefono` varchar(200) DEFAULT NULL,
  `celular` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `nombre_conyugue` varchar(200) DEFAULT NULL,
  `apellido_conyugue` varchar(200) DEFAULT NULL,
  `celular_conyugue` varchar(20) DEFAULT NULL,
  `email_conyugue` varchar(200) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL,
  `hotel` int(11) DEFAULT NULL,
  `dni` varchar(45) DEFAULT NULL,
  `dni_conyugue` varchar(45) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `fecha_naciemiento_conyugue` date DEFAULT NULL,
  `domicilio` varchar(500) DEFAULT NULL,
  `localidad` varchar(200) DEFAULT NULL,
  `provincia` varchar(45) DEFAULT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `cod_postal` varchar(45) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `id_rci` varchar(45) DEFAULT NULL,
  `id_interval` varchar(45) DEFAULT NULL,
  `telefono_laboral` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(1, '6', 'MONICA ALICIA', 'RIAT, MONICA ALICIA - VIOLA , ANTONIO LEONARDO', '4248-2545', '154-045-2687 // 155-773-9825 // 155-664-6574 (Martin)', 'aleovio@gmail.com', 'ANTONIO LEONARDO', 'VIOLA ', NULL, NULL, '', 1, '11498489', '0', '1954-04-09', NULL, 'VIEYTES 661', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', 'B1828HGM', 'aviola@victorcontreras.com.ar // martin.l.viola@gmail.com', '0', '6012968', '4294-0584/6723 // 4307-0749'),
(2, '7', 'JORGE', 'BRIZUELA - MEDINA ', '4733-9181', '15-5119-9299   ', 'jorgebrizuela@gmail.com', 'ANA MARIA', 'MEDINA ', NULL, NULL, '', 1, '12776871', '0', '1957-03-10', NULL, 'PERU 436', 'ACASSUSO', 'BUENOS AIRES', 'ARGENTINA', '1641', NULL, '6353-00260', '11798', '4733-9181'),
(3, '9', 'JUAN JOSE', 'ZAGA - BIELSA ', '02342-423985 INHABILITADO', '155-767-2496   ', 'jjzaga@fibertel.com.ar', 'ALICIA', 'BIELSA ', NULL, NULL, '', 1, '5067971', '0', '1948-06-27', NULL, 'BARRERA 245', 'BRAGADO', 'BUENOS AIRES', 'ARGENTINA', 'B6640EKE', 'juanjosezaga@hotmail.com', '6353-00319', '19168', '02342-421547'),
(4, '12', 'SILVIO', 'DI PASQUA - ', '4480-0107', '154-540-5853', 'gerencia@antonellatours.com.ar', '0', NULL, NULL, NULL, '', 1, '20771697', '0', '1969-05-22', NULL, 'JOSE MARMOL 125 BARRIO 1', 'EZEIZA', 'BUENOS AIRES', 'ARGENTINA', 'B1802BCB', 'reservas@antonellatours.com.ar', '0', '6013279', '4137-6666'),
(5, '18', 'RICARDO JOSE/MIRTA', 'SANCHEZ - MC CORMACK', '4567-4305', '154-474-8388   ', 'mirtamccormack@hotmail.com', 'MIRTA', 'MC CORMACK', NULL, NULL, '', 1, '7961237', '11877678', NULL, NULL, 'MIRANDA 3913', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1407GFG', 'mirtam@patagoniaflooring.com', '0', '6013887', '0'),
(6, '19', 'GUNTER E.', 'ROTHKEHL - 0', '4766-8042/9241', '0', 'emva@emva.arnetbiz.com.ar', 'MARGARITA ', '0', NULL, NULL, '', 1, '5563084', '0', '1929-03-07', NULL, 'MIGUEL CANE 1794', 'VILLA ADELINA', 'BUENOS AIRES', 'ARGENTINA', 'B1607AQX', NULL, '6353-01225', '6013575', '0'),
(7, '23', 'JOSE', 'ACHAVAL - CASADO', '4802-4370', '155-329-5755 (Susana)', 'susanacasado@arnet.com.ar', 'SUSANA', 'CASADO', NULL, NULL, '', 1, '8589924', '0', '1951-02-23', NULL, 'AV.CALLAO 1598  2º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1024AAO', NULL, '6353-00348', '6013950', '(0299)442-2981 Madre'),
(8, '24', 'CARLOS ALBERTO', 'WEIS - ', '4783-9548/3811-1049', '153-811-1049   ', 'carlosaweis@gmail.com', '0', NULL, NULL, NULL, '', 1, '13530696', '0', '1959-07-12', NULL, 'LUIS M. CAMPOS 1402, 6º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426BOZ', 'carlos.weis@ypf.com', '0', '6013573', '45441-2000'),
(9, '26', 'MARTA AMELIA', 'CASCALES - MORENO', '4300-2585/4307-6693', '   ', 'escribaniacascales@uolsinectis.com.ar', '0', 'MORENO', NULL, NULL, '', 1, '4220122', '0', '1941-06-24', NULL, 'PIEDRAS 1032', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1070AAV', 'martacascales@uolsinectis.com.ar // mponisio@uolsinectis.com.ar', '0', '6013788', '0'),
(10, '30', 'CARLOS MARIANO', 'BALERDI - ', ' ', '155-420-0618 (Viviana)', 'vivianago66@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '17839519', '0', '1966-01-21', NULL, 'PARERA 138 6º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1014ABD', NULL, '6353-00169', '6013881', '4371-9578 Consultorio  '),
(11, '31', 'CARLOS MARIANO', 'BAEZ - VOLONTE ', '4643-1712 BAEZ // 4373-5019 BAEZ   ', '0', 'cmb-abogados@hotmail.com', 'FABIAN LUIS', 'VOLONTE ', NULL, NULL, '', 1, '14232487', '16280615', '1960-03-19', NULL, 'Bartolomé Mitre 1617 piso 3º oficina\"303\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1037', NULL, '6353-00110', '6013879', '4637-2360 VOLON'),
(12, '32', 'MARIELA', 'BRUNELLO - CALABRO ', '4542-7680', '0', 'mariedecal@hotmail.com', 'PABLO EDUARDO', 'CALABRO ', NULL, NULL, '', 1, '26132826', '26132826', NULL, '1969-01-30', 'AV. R. BALBIN 2545 7º', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1428', '0', '6353-01437', '6013569', '4394-2250'),
(13, '34', 'DANIEL OSVALDO', 'RAVA - ', '4243-1784 // 4318-6270 ', '156-103-7730', 'daniel.rava@osde.com.ar', '0', NULL, NULL, NULL, '', 1, '11551237', '0', '1955-01-30', NULL, 'RIVADAVIA 166', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', 'B1834DLD', NULL, '6353-01153', '6013888', '4313-1009(530)'),
(14, '43', 'RENATO', 'TONIAZZO - FILARDO ', '4661-3825  DESP 20 HS', '155-307-6622   ', 'dominga@dorsasrl.com', 'DOMINGA (mimi)', 'FILARDO ', NULL, NULL, '', 1, '12706734', '0', '1958-09-22', NULL, 'QUINTANA 542', 'ITUZAINGO', 'BUENOS AIRES', 'ARGENTINA', 'B1714JNH', 'renato_toniazzo@yahoo.com.ar', '6353-01348', '6013877', '4481-5424/4621-0397 /4621-9727'),
(15, '51', 'RICARDO MIGUEL', 'ALI - OLIVO ', '4296-2500 no va mas', '156-612-9265 (Ricardo 1) // 154-998-3232(Ricardo 2)', 'RALI@terniumsiderar.com', 'ROSA', 'OLIVO ', NULL, NULL, '', 1, '10814544', '0', '1953-08-11', NULL, 'Vidt 1966, 5º A', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1425', 'aadan@terniumsiderar.com', '0', '6008250', '4239-4143/4030 ADAN'),
(16, '53', 'ADRIANA', 'BORGHESE - REQUEJO ', '4296-8261 // 4290-3098 PADRES   ', NULL, 'adrianaborghese@yahoo.com.ar', 'DANIEL', 'REQUEJO ', NULL, NULL, '', 1, '20469417', '18511998', '1969-01-06', NULL, 'RETA 1339', 'MONTE GRANDE', 'BUENOS AIRES', 'ARGENTINA', 'B1842DDA', '0', '6353-00245', '20089', '155-612-9935 (Daniel)'),
(17, '54', 'ALBERTO JOSE', 'KOMAR - PIZZINI ', '4501-6022', '153-165-2468', 'alberto_komar@hotmail.com', 'SANDRA', 'PIZZINI ', NULL, NULL, '', 1, '12713152', '0', '1958-11-13', NULL, 'NAVARRO 3710', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419IFQ', 'ajkomar@yahoo.com.ar', '0', '20218', '4504-1068/4502-9396'),
(18, '55', 'OSCAR FERNANDO', 'BERMUDEZ - SANFELIX ', '(0237)487-4827', '153-500-8319 SUSANA   ', 'susysanfelix@yahoo.com.ar', 'SUSANA', 'SANFELIX ', NULL, NULL, '', 1, '18272742', '0', '1942-03-03', NULL, 'ALMAFUERTE 1401 BºPRIVADO \"CAMPOS DE ALVAREZ\"', 'FRANCISCO ALVAREZ', 'BUENOS AIRES', 'ARGENTINA', '1746', NULL, '6353-00207', '20091', '155-182-5569 OSCAR'),
(19, '56', 'JUANA MIRTA', 'STEFANI - 0', '0', '155-134-7620 (MATIAS)', 'matiasli@fibertel.com.ar', '0', '0', NULL, NULL, '', 1, '22432600', '0', NULL, NULL, 'AV. DEL LIBERTADOR 2254 25ºB', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '0', '0', '0', '6014050', '0'),
(20, '59', 'HORACIO RUBEN', 'COLOTTO - ', '(02281)429-616', '(02281)155-72-919 sra.   ', 'hrcolotto@gmail.com', '0', NULL, NULL, NULL, '', 1, '17208276', '0', '1964-12-30', NULL, 'BOLIVAR 764', 'AZUL', 'BUENOS AIRES', 'ARGENTINA', 'B7300CBQ', NULL, '6353-00397', '6014058', '(02281)427-066'),
(21, '60', 'MARIO', 'VON PROSCHEK - GALLO ', '4743-5076', '155-763-6363   ', 'm.vonproschek@agrositio.com', 'ALICIA', 'GALLO ', NULL, NULL, '', 1, '8265632', '5594979', '1947-05-25', NULL, 'ACASSUSO 246  DTO 11', 'SAN ISIDRO', 'BUENOS AIRES', 'ARGENTINA', 'B1642CGF', NULL, '6353-00150', '6014045', '0'),
(22, '61', 'HORACIO', 'MARTINEZ - 0', '4743-8354 MARIA // 4312-8862', '155-476-5715   ', 'martinezh@fibertel.com.ar', '0', '0', NULL, NULL, '', 1, '7642458', '0', NULL, NULL, 'JUNCAL 3158 PISO 9', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', 'martinezh@fibertel.com', '0', '20283', '4743-8354 M.Andrea '),
(23, '76', 'JORGE', 'BAUMAN AUBONE - SHEVERIN', '4342-3695/7758', '155-004-4474 (Jorge) // 156-330-8226 (Catalina)', 'catybaumann@gmail.com', 'MARIANO', 'SHEVERIN', NULL, NULL, '', 1, '7375359', '0', NULL, NULL, 'Ángel D\'elía 1021', 'SAN MIGUEL', 'BUENOS AIRES', 'ARGENTINA', '1663', NULL, '0', '6014220', '4664-7237'),
(24, '78', 'GABRIEL', 'PITZZU - REBAGLIATTI, ', '0', '155-857-8839  ', 'gabrielpitzzu@gmail.com', 'MONICA', 'REBAGLIATTI, ', NULL, NULL, '', 1, '13802118', '13368266', '1959-09-04', NULL, 'DIAZ VELEZ 4624 2º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1405', 'GABRIEL.PITZZU@OSPLAD.ORG.AR', '6353-01114', '6013013', '5246-5170'),
(25, '80', 'CONSTANTE', 'ROMANIUK - OLIVERO ', '4443-1803', '155-424-1810 (Cristian) // 155-579-4794 (Gustavo)', 'char@speedy.com.ar', 'OLGA', 'OLIVERO ', NULL, NULL, '', 1, '4444780', '22326903', '1944-07-04', NULL, 'LIBERTAD 749', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', 'B1706CZM', NULL, '6353-01447', '20226', '4669-5312/4969/5381'),
(26, '86', 'DANIEL OSCAR', 'ELIZONDO - BALBIS FREIRE', '4957-3337/471-7934 // 4786-3612 ', '1163071116 (Daniel)', 'pbf@elizondo.com.ar', 'PURIFICACION', 'BALBIS FREIRE', NULL, NULL, '', 1, '7771289', '0', '1956-07-06', NULL, 'LA PAMPA 2326 5º of 507', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428EAP', 'daniel@elizondo.com.ar', '8784792', '6014149', '1563071117 (Puri)'),
(27, '87', 'ALEJANDRO', 'MORCILLO DE CASTRO - BASSO ', '4541-5058', '155-805-0601', 'bassograce@hotmail.com', 'GRACIELA', 'BASSO ', NULL, NULL, '', 1, '13295513', '12714900', '1957-08-14', NULL, 'GREGORIO JARAMILLO 3320', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1429CSD', 'fivri@argentina.com', '6353-00973', '6014218', '4571-7980 OFICINA *'),
(28, '88', 'MARIO', 'DE ELIA - SALGADO ', '4701-1844', '154-024-5473 (Adriana)', 'alusalgado@gmail.com', 'ADRIANA', 'SALGADO ', NULL, NULL, '', 1, '13890954', '17031111', '1960-10-01', NULL, 'MOLDES 4016', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1429AFX', 'asalgado@ar.ibm.com', '6353-00455', '6014224', '0'),
(29, '91', 'JORGE J.', 'HEFFES - CASTRO, ', '4902-2850 / 43749293 (no estan ahí)', '154-446-3236', 'jorge.heffes@gmail.com', 'LILIANA', 'CASTRO, ', NULL, NULL, '', 1, '11316656', '10760061', '1954-07-13', NULL, 'RIVADAVIA 4658  4º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1424CEO', NULL, '0', '6014310', '4374-0434'),
(30, '93', 'MARCELO', 'VILLALBA - RUIZ ESTEBAN', '5278-1100/1102/1103', '156-800-9250 (Marcelo)', 'marcelo@interlatino.com.ar', 'ROXANA', 'RUIZ ESTEBAN', NULL, NULL, '', 1, '13407718', '0', '1960-10-29', NULL, 'Agustin M Garcia 8852', 'BENAVIDEZ', 'BUENOS AIRES', 'ARGENTINA', '1621', 'info@interlatino.com.ar', '6353-00135', '20366', '5278-1100 153-044-6030 (Corporativo)'),
(31, '104', 'RICARDO ANDRES', 'PAOLINA - GASTON ', '4313-3035', '15-4411-6085   ', 'rapaolina@llyasoc.com', 'LAURA', 'GASTON ', NULL, NULL, '', 1, '14822993', '17812861', '1963-02-10', NULL, 'BILLINGHURST 2522 PISO 8° A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', NULL, '0', '6371158', '0'),
(32, '110', 'ROSANA', 'MARAFIOTI - MOYA', '0', '154-148-2236 (Rosana)', 'rmarafioti@decmara.com', 'GABRIEL', 'MOYA', NULL, NULL, '', 1, '16335241', '16951676', '1963-06-09', NULL, 'GRAL.PACHECO  1700 BARRIO SOLARES DEL TALAR U.F 102  o   ·Arenales 1123 (8º) SRA', 'DON TORCUATO', 'BUENOS AIRES', 'ARGENTINA', 'B1611BDU', '0', '6353-00881', '6014306', '4659-0205(PAPA)'),
(33, '112', 'JOSE ALBERTO', 'AGLIETTO - ORGNERO ', '(03492)426-104', '(03492)156-66-032', 'jose@agliettoingenieria.com.ar', 'SUSANA BEATRIZ', 'ORGNERO ', NULL, NULL, '', 1, '5531831', '0', '1949-08-06', NULL, 'ALEM 295', 'RAFAELA', 'SANTA FE', 'ARGENTINA', 'S2300AHE', 'aglietto@agliettoingenieria.com.ar', '6353-00024', '6013954', '(03492)420-226'),
(34, '114', 'JORGE', 'ALLAMPRESE - GUARDIA ', '(0223)494-0650 // (0223)481-9720 Hija Graciela', '(0223)154-001-865 (Sebastian) // (0223)154-008-317', 'ventas@eltriunfosrl.com.ar', 'GRACIELA', 'GUARDIA ', NULL, NULL, '', 1, '2633974', '10099235', '1927-06-06', NULL, 'ROQUE SAENZ PEÑA 169', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7608BAC', 'sebaguardia@hotmail.com', '6353-01449', '6013572', '(0223)4821629'),
(35, '118', 'GUILLERMO', 'DALLAS - NARDI ', '4209-0576', '155-614-7898', 'guillermo_dallas@hotmail.com', 'SILVIA INES', 'NARDI ', NULL, NULL, '', 1, '17923855', '18352282', '1967-02-18', NULL, 'CATAMARCA 847', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', '1824', 'gdallas@pcdiscount.com.ar', '6353-00443', '21012', '4262-1020/1385'),
(36, '121', 'GUSTAVO ARIEL', 'DIVINSKY - ', '4857-1231/4582-2979', '155-585-5310', 'g_divinsky@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '23299255', '0', NULL, NULL, 'FRIAS 128  3º A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1414', NULL, '0', '6014863', '4983-7079'),
(37, '127', 'SERGIO', 'SLAIFSTEIN - YOHAL, ', '4702-5013', '15-5661-4305   ', 'sslaifstein@gmail.com', 'CLAUDIA', 'YOHAL, ', NULL, NULL, '', 1, '12446866', '13081489', '1958-09-15', NULL, 'VUELTA DE OBLIGADO 2880(6ºB)', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1429AVB', NULL, '6353-01312', '20801', '0'),
(38, '132', 'MERCEDES', 'ESTEBAN - FASANELLA ', '02941-434291', '   ', 'mercedes-esteban@hotmail.com', 'LUIS', 'FASANELLA ', NULL, NULL, '', 1, '11441186', '0', NULL, NULL, 'ENTRE RIOS 179', 'GENERAL ROCA', 'RIO NEGRO', 'ARGENTINA', 'R8332KPC', 'luis@consorfrutargenti.com.ar', '6353-00558', '20335', '0'),
(39, '135', 'SILVIA RAQUEL', 'BECHELLI - ', '4542-9406 (PART Y FAX)', '155-924-8726 (Ella) // 154-045-4260 (El)', 'bechellisilvia@gmail.com', '0', NULL, NULL, NULL, '', 1, '5595399', '0', '1947-03-26', NULL, 'MANUEL UGARTE 3894', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1430BBL', NULL, '0', '26493', '4798-1941/1551'),
(40, '136', 'LAUDELINO', 'SUAREZ - MARDUEL', '4393-3940 // 4322-2926 ( ya no esta mas aca) ', 'no tengo tel actualizado de contacto', 'cris.marduel@gmail.com', 'MARIA', 'MARDUEL', NULL, NULL, '', 1, '5514015', '6298816', '1947-06-15', NULL, 'ANDRES LAMAS 2594', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1416BJD', NULL, '6353-01335', '6014681', '4687-1911 MERCADO   '),
(41, '145', 'HECTOR', 'BADARACCO - ALAIMO ', '4253-3471', '153-113-3471 (Nilda)', 'losbada@hotmail.com.ar', 'NILDA', 'ALAIMO ', NULL, NULL, '', 1, '5141597', '2035841', NULL, NULL, 'PRINGLES 613', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', 'B1878GIM', 'nildabadaracco@hotmail.com', '6353-00109', '6014848', '4224-7920'),
(42, '149', 'CARLOS', 'PELLANDINI - URIBARRI, ', '4245-3057', '155-036-5292 (Carlos) // 155-418-8923 (Susana)', 'carlospellandini@gmail.com', 'SUSANA', 'URIBARRI', NULL, NULL, '', 1, '7760602', '5408127', '1945-03-22', '1946-07-31', 'COLON 69', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', 'B1834HMA', 'mpelland1980@hotmail.com', '6353-01080', '6013537', '4245-3057'),
(43, '161', 'MARIO LUIS', 'PANELLI - ', '4503-6126', '155-525-3888   ', 'arcadiaeducativa@gmail.com', '0', NULL, NULL, NULL, '', 1, '20406664', '0', '1969-03-23', NULL, 'GANDARA 2285 o MONROE 4881', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', 'C1431DAA', NULL, '6353-01060', '6015132', '0'),
(44, '165', 'MARTHA', 'DOPORTO - BALLESTEROS ', '4776-6116', '116-556-2615 (Gonzalo)', 'Gonzalo.Ballesteros@schneider-electric.com', 'JULIO', 'BALLESTEROS ', NULL, NULL, '', 1, '3300426', '0', '1937-09-02', NULL, 'ZAPATA 31  19º \"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1426', 'marthaballesta@yahoo.com.ar', '6353-00528', '6014295', '0'),
(45, '176', 'CLAUDIO FABIAN', 'BALDRICH - BOSISIO ', '155-504-1233 (Mónica)', '155-794-9100 (Claudio)', 'claudiobaldrich@gmail.com', 'MONICA', 'BOSISIO ', NULL, NULL, '', 1, '16672621', '0', '1963-10-30', NULL, 'USPALLATA 2275', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', 'B1643ANU', 'monicab@bosisio.com.ar // melina_baldrich@hotmail.com', '0', '6014679', '4719-5626'),
(46, '184', 'JULIO EDUARDO', 'BOGADO - SABATE', '4702-8673', '115-714-8926 (Mariel)', 'marsabate10@hotmail.com.ar', 'MARIEL', 'SABATE', NULL, NULL, '', 1, '8321916', '0', '1956-07-23', NULL, 'ROQUE PEREZ 3694 1º \"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1430FBX', 'marielsabate@icloud.com', '0', '918585', '4831-3712 REST'),
(47, '185', 'SILVANA', 'BOLZICCO - IVANOVICH', '0223-4795445', '(0223)155-97-1802   ', 'sabolzicco2@hotmail.com', 'ALEJANDRO', 'IVANOVICH', NULL, NULL, '', 1, '13908693', '0', '1960-10-02', NULL, 'SAGASTIZABAL 5445', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7605FQI', 'ivanovichalejandro@gmail.com', '6014166', '6014166', '0'),
(48, '191', 'JORGE ADOLFO', 'BOTTAZZINI - ', '4737-5663', NULL, 'ana-ines@bottazzini.com', '0', NULL, NULL, NULL, '', 1, '4319356', '0', '1940-07-14', NULL, 'SERRANO 1095', 'SAN ISIDRO', 'BUENOS AIRES', 'ARGENTINA', 'C1609GZK', 'ana-ines@bottazzini.com', 'ID RCI', 'ID INTERVAL', '4768-0505   '),
(49, '195', 'FABIO', 'PAPPARINI - ', '4871-3174', '155-183-1275 (Osvaldo)   ', 'turisvip@hotmail.com', '0', NULL, NULL, NULL, '', 1, '17401465', '0', '1965-07-06', NULL, 'De las azaleas 42', 'TIGRE ', 'BUENOS AIRES', 'ARGENTINA', 'C1428CMB', NULL, '6353-01063', '6013880', '4781-9509 MADRE'),
(50, '198', 'SILVIA C.', 'SERI - REY', '4794-0242', '155-517-2509   ', 'silviacristinaseri@yahoo.com.ar', 'CARLOS', 'REY', NULL, NULL, '', 1, '10801191', '11056643', '1953-03-01', NULL, 'QUINTANA 3852', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636AOJ', 'silviaseri@gmail.com', '6353-01298', '13696', '0'),
(51, '203', 'FERNANDO', 'SCIACCALUGA - ORRILLO ', '4648-0005', '154-411-7751', 'ferscia@hotmail.com', 'SANDRA', 'ORRILLO ', NULL, NULL, '', 1, '14557718', 'DIRE ELLA ASUNCION 4986', '1961-08-19', NULL, 'MARIANO ACOSTA 137 PISO 2° A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419HHN', 'rojolegal@hotmail.com', '6353-01445', '19664', '1540791554 ORRILLO'),
(52, '208', 'LUIS ADOLFO', 'FRANCH - ', '4215-0010', '154-415-0347   ', 'fliafranch@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '11667219', '0', '1955-06-16', NULL, 'CLUB DE CAMPO ABRIL(CASA 92)AUTOP.LA PLATA-KM.33-', 'HUDSON', 'BUENOS AIRES', 'ARGENTINA', '1885', NULL, '2604-01489', '19173', '4257-0994'),
(53, '209', 'EDGARDO JOSE', 'FUSARI - BUSTOS ', '4552-0518', '   ', 'doky1@live.com.ar', 'HORTENSIA', 'BUSTOS ', NULL, NULL, '', 1, '4179508', '4453825', '1936-02-04', NULL, 'AV. FOREST 1404 PISO 6', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1427CEY', 'inescrespo11@hotmail.com', '0', '6013539', '0'),
(54, '211', 'M. Victoria', 'CACERES - PERA', '4665-2244', '154-174-7815 (M. Victoria)', '1810mariec@gmail.com', 'M. Eugenia (FALLECIÓ)', 'PERA', NULL, NULL, '', 1, '18122610', '2665567', NULL, NULL, 'Debenedetti 1152', 'LA LUCILA', 'BUENOS AIRES', 'ARGENTINA', '1637', '  caceresvicky@hotmail.com', '6353-00288', '6016210', '0'),
(55, '225', 'EDUARDO DOMINGO', 'CASTRO - ZARELLI ', '4278-1046', '156-370-3462 (EDUARDO)', 'elbazarelli@hotmail.com', 'ELBA NOEMI', 'ZARELLI ', NULL, NULL, '', 1, '4558008', '0', '1946-09-12', NULL, 'LUIS AGOTE 891', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', 'B1878BPQ', 'castro@gruasa.com.ar', '0', '6016259', '4210-4022 (Fábrica)'),
(56, '228', 'FERNANDO R.', 'NAVARRO - BORGNA ', '0223-4893111', '223-5023181', 'vetlazomar@speedy.com.ar', 'MARIA DE LOS ANGELES', 'BORGNA ', NULL, NULL, '', 1, '8705142', '13230150', '1947-06-09', NULL, 'OLAVARRIA 4328', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7602CZF', 'vetlazomar@speedy.com.ar', '6353-01001', '20359', '0223-4813644'),
(57, '231', 'SANDRA', 'MILLAN - REDOLATTI ', '(0223) 4791-065', '(0223)155-24-4079 (Horacio) // (0223)155-24-1085 Sandra', 'redolatti@aphsa.com.ar', 'HORACIO', 'REDOLATTI ', NULL, NULL, '', 1, '12990076', '11350130', NULL, NULL, 'SAGASTIZABAL 5495', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7605FQI', NULL, '0', '20365', '(0223)494-6290 // (0223)494-0072 Ofic. Horacio'),
(58, '233', 'SUSANA', 'FRANCHI - FRANCHI ', '4796-9422', '155-127-8735   ', 'susanaangelacia@gmail.com', 'ENRIQUE JOSE', 'FRANCHI ', NULL, NULL, '', 1, '14951899', '16050120', '1961-10-23', NULL, 'MELO 1605', 'VICENTE LOPEZ', 'BUENOS AIRES', 'ARGENTINA', '1638', NULL, '6353-00384', '6015707', '4314-2472*SR FRANChi'),
(59, '248', 'MONICA SUSANA', 'MOTA - ECHAVARRIA ', '(0264)422-2677', '(0264) 154-15-3796 Sra. // (0264) 156-60-1638 ', 'susana.echavarria@hotmail.com', 'REINALDO', 'ECHAVARRIA ', NULL, NULL, '', 1, '11481377', '0', '1955-02-13', NULL, 'ESTADOS UNIDOS 449 NORTE', 'VILLA AMERICA', 'SAN JUAN', 'ARGENTINA', '5400', 'susana.echavarria@hotmail.com', '6353-00981', '19242', '(0264) 422-3278 Fax'),
(60, '252', 'EDUARDO NORBERTO', 'PIETRA - MUSACCHIO', '4501-8766', '154-445-4544 (Susana) // 154-493-0810 (Eduardo)', 'susana_pietra@yahoo.com.ar', 'SUSANA', 'MUSACCHIO', NULL, NULL, '', 1, '4443912', '4837749', '1944-11-26', NULL, 'MARCOS PAZ 4314', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419BSF', '0', '0', '6012852', '0'),
(61, '255', 'MARIO', 'CARBONETTI - BARRON, ', '(0351)471-7453', '(0351)156-77-1475 Mario', 'margarita@barron.com.ar', 'MARGARITA', 'BARRON, ', NULL, NULL, '', 1, '8009195', '6029591', '1950-02-02', NULL, 'FRAGUEIRO 1363', 'CORDOBA', 'CORDOBA', 'ARGENTINA', 'X5000KSI', 'victoria_recalde@hotmail.com', '0', '22269', '4705-11430'),
(62, '256', 'ROBERTO J.', 'FLORES - Susana', '0297-4063134', '0', 'ingrjflores@yahoo.com.ar', 'Esposa', 'Susana', NULL, NULL, '', 1, '11769486', '0', '1955-11-03', NULL, 'VELEZ SARFIELD 1635', 'COMODORO RIVADAVIA', 'CHUBUT', 'ARGENTINA', '9000', NULL, '0', '24689', '(0297) 447-9992 '),
(63, '261', 'ROBERTO', 'GUIDETTI - MARTINEZ, ', '4249-2930', '156-245-1217 (Roberto)', 'rguidetti@speedy.com.ar', 'LAURA', 'MARTINEZ, ', NULL, NULL, '', 1, '11703781', '11998274', '1955-10-04', NULL, 'PRINGLES 1442', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', 'B1824IJV', NULL, '6353-00721', '6017902', '4287-7079'),
(64, '286', 'CARLOS ALBERTO', 'JOHANSON - JOHANSON ', '4806-9609', '154-040-2259   ', 'carlosjohanson@yahoo.com.ar', 'MARIA TERESA', 'JOHANSON ', NULL, NULL, '', 1, '5004763', '0', '1944-01-30', NULL, 'LARREA 1450  4º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1117ABN', 'carlosjohanson@yahoo.com.ar', '0', '6015703', '0'),
(65, '294', 'GENARO', 'AVERSA - GRONDONA ', '4201-8327', '154-470-0524', 'cfontana@fueguinohotel.com.ar', 'LILIANA', 'GRONDONA ', NULL, NULL, '', 1, '13678713', '0', '1959-12-20', NULL, 'GRAL. PAZ 96 6º \"A\"', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', 'B1870CQB', '0', '6353-00103', '6017089', '5787-0722 Secretaria'),
(66, '297', 'JORGE E.', 'NAUDI - 0', '4432-5489', '154-565-6080 (Jorge)', 'jnaudi@toptechsa.com', 'MARÍA LUISA', '0', NULL, NULL, '', 1, '7684404', '0', '1949-11-17', NULL, 'AV.RIVADAVIA 5485 1  \"C\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1424CEJ', 'jorgenaudi@gmail.com', '0', '6015985', '4551-1271 /         4555-3733/7136'),
(67, '312', 'HECTOR HUGO', 'TORMO - LUPANI ', '(0230) 448-0720', '155-621-9240 Sra.', 'familia_tormo@hotmail.com', 'MABEL', 'LUPANI ', NULL, NULL, '', 1, '10668603', '11421767', '1953-01-09', NULL, 'AV. JUAN B. ALBERDI 1342  4ºB', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1406', NULL, '6353-01349 // 6017072 (II)', '6017072', '112-243-3400 (Hugo)'),
(68, '331', 'MARIA SUSANA DE LAS MERCE', 'VILLEGAS - VILLEGAS ', '(0261)423-0971', '(0261)156-50-6245 (Maria) // (0261) 156-587-059 (Francisco)', 'mariasusanavillegasmarco@yahoo.com.ar', 'FRANCISCO', 'VILLEGAS ', NULL, NULL, '', 1, '4884466', '8153965', '1949-08-17', NULL, 'LEMOS 691 4º 1', 'MENDOZA', 'MENDOZA', 'ARGENTINA', '5500', NULL, '6353-00142', '6016119', '(0261)4444-624'),
(69, '335', 'GLADYS ELIZABETH', 'NUÑEZ - ', '0', '155-981-0922', 'elizabet77nunez@gmail.com', '0', NULL, NULL, NULL, '', 1, '13072777', '0', '1958-03-22', NULL, 'AV. SAN MARTIN 3188(5º 10)', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1416CST', NULL, '0', '25233', '4317-5771'),
(70, '336', 'RODOLFO', 'MARTIN - LORINAZ ', '03735-498430 *', '(03735) 156-25-285   ', 'oficina.va@gmail.com', 'YOLANDA', 'LORINAZ ', NULL, NULL, '', 1, '7924013', '0', '1947-02-03', NULL, 'MARIANO MORENO S/N', 'CORONEL DU GRATY', 'CHACO', 'ARGENTINA', '3541', NULL, '6353-00898', '23740', '0'),
(71, '339', 'HORACIO ALCIDES', 'BLANCO - SERENO ', '02331-499311/327', '0230215642908 (Horacio) // 0230215534814 (Raquel)', 'blancolajulia@gmail.com', 'RAQUEL', 'SERENO ', NULL, NULL, '', 1, '13456876', '17579602', '1959-06-24', NULL, 'ESTRADA S/N', 'FALUCHO', 'LA PAMPA', 'ARGENTINA', 'L6215AAA', 'rhsereno@gmail.com', '6353-00228', '6017281', '02331-499311'),
(72, '345', 'CARLOS FELIX', 'ROCHA - ', '4787-4488LUCIA', '155-229-3425   ', 'carlos@rochanet.com.ar', '0', NULL, NULL, NULL, '', 1, '10901369', '0', '1953-10-26', NULL, 'JURAMENTO 2017 3º \"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428AED', 'administracion@rochanet.com.ar', '4235-02346', '6000862', '4787-4488 LUCIA'),
(73, '348', 'ALBERTO', 'CALIGARI - 0', '0299-4430650', '0299-155-811496   ', 'alberto.caligari@gmail.com', 'CRISTINA ', '0', NULL, NULL, '', 1, '10037959', '12287293', '1952-01-24', NULL, 'MINISTRO GONZALEZ 241 2 \"A\"', 'NEUQUEN', 'NEUQUEN', 'ARGENTINA', 'Q8300HPE', 'alberto.caligari@petrobras.com', '3781-01099 // II: 6017058', '6017058', '0299-4911354/5 IN227'),
(74, '350', 'MIRTA ISABEL', 'RIVIERE - PASCIULLI', '4552-5274', '156-181-3205', 'betinapas3@yahoo.com.ar', 'EDUARDO', 'PASCIULLI', '31234234', 'betinapas3@yahoo.com.ar', '', 1, '4641964', '4440430', '1943-10-10', NULL, 'OLLEROS 2657', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426CRW', 'mirtariviere@yahoo.com.ar // eduardopas3@yahoo.com.ar', '0', '23736', '0'),
(75, '354', 'GUSTAVO ADRIAN', 'GHISLETTI - ', '4702-0474', '156-093-4041   ', 'gustavo.ghisletti@eltete.com.ar', '0', NULL, NULL, NULL, '', 1, '17435993', '16822320', '1965-05-26', NULL, 'ARCOS 3161 4º \"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1429BNN', 'gustavoghisletti@fibertel.com.ar', '0', '6021496', '153-281-4929'),
(76, '359', 'HECTOR VICENTE', 'DE DONATO - SANCHEZ ', '4799-2863', '155-011-2140   ', 'hdedonato@tuteur.com.ar', 'ADRIANA', 'SANCHEZ ', NULL, NULL, '', 1, '8629074', '0', '1951-09-26', NULL, 'MARIANO MORENO 1763', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636DZG', NULL, '6353-00454', '6015708', '0'),
(77, '376', 'GUSTAVO', 'ARRIGO - 0', '4362-0460', '153-447-1140', 'gustavo.arrigo@gmail.com', '0', '0', NULL, NULL, '', 1, '20618895', '0', '1969-04-06', NULL, 'FELIX DE AZARA 365 PISO 6 A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1267', NULL, '0', '0', '0'),
(78, '383', 'MIGUEL ANGEL', 'PUERTA - ISALI ', '4627-6365/4483-3377', '155-517-0401 Mirta // 155-517-0400 Miguel', 'miguelpuerta.ar@gmail.com', 'MIRTA', 'ISALI ', NULL, NULL, '', 1, '10607715', '0', '1953-08-12', NULL, 'MACHADO 1135', 'MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1708EOK', 'map@mapgroup.com.ar', '0', '24899', '4483-3336/3999'),
(79, '392', 'CARLOS ALBERTO', 'BELLIA - 0', '4627-0396/0404', '155-322-7794 ', 'cabellia@yahoo.com', '0', '0', NULL, NULL, '', 1, '7731882', '0', '1942-09-17', NULL, 'YATAY 751', 'MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1708JHM', 'fliabellia@infovia.com.ar', '6353-00193', '6019066', '4925-6666 // 4924-1694 Mar '),
(80, '398', 'OSVALDO NESTOR', 'GONZALEZ - 0', '4783-0745 DE ÉL', '15 4407-2129 // 155-747-2503 (Emilia)  ', 'altaodontologia@fibertel.com.ar', '0', '0', NULL, NULL, '', 1, '4556137', '0', '1951-12-29', NULL, 'LA PAMPA 2132 PB.\"H\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428EAL', 'emigonzalez81@hotmail.com; charrodoc@hotmail.com', '0', '21002', '4784-9946/4788-3705'),
(81, '399', 'RICARDO', 'WOLNY - SALA', '4624-3036', '15-40290923 - SRA.   ', 'pnsalas@gmail.com', 'PATRICIA', 'SALA', NULL, NULL, '', 1, '12092398', '12691489', '1958-05-20', NULL, 'CALLE PASTEUR 3110-', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', 'B1712DFB', 'rew777@gmail.com', '0', '6018386', '4130-8810 trabajo Sr. Banco'),
(82, '413', 'OBDULIO', 'DEL GIORGIO - ZUMARRAGA ', '02292-452599', '(02281) 155-69-371', 'tatocapodg@yahoo.com.ar', 'MARIA MONICA', 'ZUMARRAGA ', NULL, NULL, '', 1, '4988196', '0', NULL, NULL, 'OTAMENDI 252', 'BENITO JUAREZ', 'BUENOS AIRES', 'ARGENTINA', 'B7020DTF', 'delgiorgiomaquinarias@yahoo.com.ar', '6353-00476', '6009673', '02292-453834'),
(83, '417', 'SUSANA', 'GOMEZ IZA - 0', '0', '(02284) 156-02-522 (Cecilia) // (02284) 156-02-372 (Ezequiel)', 'ezequielgi@hotmail.com', 'CAROLA', '0', NULL, NULL, '', 1, '4288203', '0', '1941-11-19', NULL, 'LIBERTAD 844 11', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1012AAR', 'florencia.defeo@yahoo.com.ar', 'ID RCI', 'ID INTERVAL', '0'),
(84, '425', 'EMILIO', 'FIGUEROA BUNGE - ', '(02320)466-3890', '156-769-4850 (Sofía) // 153-183-5330 (María, hija sofía)', 'sofiauranga1@gmail.com', '0', NULL, NULL, NULL, '', 1, '8272879', '0', NULL, NULL, 'MEXICO 350', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', 'B1630BLB', 'efigueroa@nidera.com.ar', '6353-00595', '9003', '(02322)428-497(Of.) // (02322)432-710 Of.'),
(85, '446', 'MARIA LUISA', 'MARTINAT - MARTINAT', '0', '(0351)153-905-558 (M. Luisa) / (0351)152-311-392 (Nicolás hijo)', 'cpstone2003@yahoo.com.ar', 'MIGUEL ', 'MARTINAT', NULL, NULL, '', 1, '6493490', '0', '1950-12-08', NULL, 'MARINERO AGUILA 3735', 'JARDIN ESPINOSA', 'CORDOBA', 'ARGENTINA', '5014', 'cpstone2003@yahoo.com.ar', 'ID RCI', 'ID INTERVAL', '0351-4940508'),
(86, '447', 'GUILLERMO CLAUDIO', 'FORCINITO - FORCINITO ', '(0221) 422-2916', '153-811-2091 (Romina)', 'rforci@hotmail.com', 'ROMINA', 'FORCINITO ', NULL, NULL, '', 1, '11389721', '27620119', '1954-12-13', NULL, 'CALLE 67 Nº 316 entre 1 Y 2', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'b1904awn', 'romina.forcinito@ypf.com', '0', '23754', '0'),
(87, '466', 'JORGE OSCAR', 'GRISOLI - GANDARA ', '(0223)493-0125', '(0223) 154-39-5137 (Jorge) // (2944) 153-64-819 (Edith)', 'jorgegrisoli@fibertel.com.ar', 'EDITH', 'GANDARA ', NULL, NULL, '', 1, '7701932', '5983304', '1949-07-18', NULL, 'AV. PUEYRREDON 1028 3º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1118AAN', 'eganda@arnet.com.ar', '6353-00710', '6019576', '0'),
(88, '473', 'JORGE CARLOS', 'TREBINI - MATRANCA ', '0223-4950863', '223154394608', 'jorgeisabel13@hotmail.com', 'ISABEL ROSA', 'MATRANCA ', NULL, NULL, '', 1, '10103871', '3792221', '1952-02-14', NULL, 'COLON 1712, 5º\"G\"', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7600FXX', 'jorgetrebini@gmail.com', '0', '6019286', '0223-4869039 equivocado'),
(89, '475', 'FABIAN RAUL', 'DE LA IGLESIA - ARLIA', '4687-5039 // 4044-7631', '154-044-7623', 'fabilili1986@hotmail.com', 'LILIANA', 'ARLIA', NULL, NULL, '', 1, '16937316', '16937921', '1964-07-28', NULL, 'SALADILLO 2421 (ENTRE RODO  Y DIRECTORIO)', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1440FFW', '0', '6353-00459', '6018379', '4671-1619'),
(90, '479', 'GLADYS SILVIA', 'QUINTAS - ALTAVISTA ', '0', '(02241)156-70-656', 'gladysquintas@gmail.com', '0', 'ALTAVISTA ', NULL, NULL, '', 1, '13679229', '0', '1960-01-12', NULL, '15 de agosto  GolfChascomus CountryClub C.3 Unid.277', 'ROSAS', 'BUENOS AIRES', 'ARGENTINA', '7130', 'gladysquintas@hotmail.com', 'ID RCI', 'ID INTERVAL', '4343-4409'),
(91, '491', 'JORGE', 'FORCADA RUIZ - ', '4201-1610 MADRE', '1565373545   ', 'guilleforcada@gmail.com', '0', NULL, NULL, NULL, '', 1, '13134004', '0', '1959-04-23', NULL, 'BERUTTI 138', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', 'B1870CVP', 'guilleforcada@gmail.com', '0', '25313', '0'),
(92, '505', 'VICTOR', 'CRISTINA - REBEILLE ', '4795-5369', '15-57632592   ', 'victord_cristina@yahoo.com.ar', 'ALICIA', 'REBEILLE ', NULL, NULL, '', 1, '8247069', '0', '1945-08-03', NULL, 'ITALIA 2828', 'FLORIDA', 'BUENOS AIRES', 'ARGENTINA', 'B1602DPN', NULL, '6353-00435', '6020172', '0'),
(93, '510', 'JOSE MARIA', 'SAENZ VALIENTE - HAMILTON ', '03327-481347/485387', '15-50103625 SRA.   ', 'clotasaenzval@hotmail.com', 'CLAUDIA G.', 'HAMILTON ', NULL, NULL, '', 1, '7627344', '10141958', '1949-02-26', NULL, 'juana manso 205 piso 1 ', 'BENAVIDEZ', 'BUENOS AIRES', 'ARGENTINA', '1621', 'dpais@svya.com.ar ', '0', '6020262', '4021-7700/7748 (Estudio Lilián)'),
(94, '514', 'CARLOS  GABRIEL', 'DI BELLA - NAPOLITANO ', '4523-7025', '153-163-7906', 'carlosgdibella@hotmail.com', 'ADRIANA', 'NAPOLITANO ', NULL, NULL, '', 1, '12965240', '16236144', '1957-04-08', NULL, 'PASAJE DEL TEMPLE 2557', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1427DMA', '0', '6353-00495', '6018852', '0'),
(95, '518', 'MARIA EUGENIA', 'BAVCAR - DELRIVO ', '4231-3612/4298-1778', '154-052-5185   ', 'administracion@forja-atlas.com.ar', 'LUIS', 'DELRIVO ', NULL, NULL, '', 1, '16737729', '0', '1963-11-01', NULL, 'FRIEDRICH 345', 'LAVALLOL', 'BUENOS AIRES', 'ARGENTINA', 'B1836', 'mebavcar@gmail.com', '6353-00188', '6019298', '4298-1778/3650......'),
(96, '525', 'GABRIEL FEDERICO', 'JIMENEZ - FABREGAT ', '4502-1344', '155-228-4500 (Gabriel)  ', 'gfjimenez@arnetbiz.com.ar', 'MARIA CRISTINA', 'FABREGAT ', NULL, NULL, '', 1, '10126823', '11320030', '1951-09-17', NULL, 'LAVALLOL 3356', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1417AFB', 'belligotti@fibertel.com.ar', '0', '26279', '4522-8680/4303-2121'),
(97, '526', 'SILVIO JORGE', 'KALEKIN - ', '4637-5919', '154-429-1758', 'silvio.k@kalekin.com.ar', '0', NULL, NULL, NULL, '', 1, '11773823', '22004241', '1956-02-14', NULL, 'J. B. Albertdi 1475 4º C', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1407', 'gerente@afixsa.com.ar', '0', '6019057', '4303-4303 L a V 9 a 18 hs.'),
(98, '533', 'OSCAR', 'PANA - TRONCA ', '0351-4233985', '(0351)4237577 SRA // (0351)155-117-690 Sr.', 'oscarepana@hotmail.com.ar', 'GRACIELA', 'TRONCA ', NULL, NULL, '', 1, '11301283', '13983027', '1954-09-25', NULL, 'DERQUI 60 Bº NUEVA CORDOBA', 'CORDOBA', 'CORDOBA', 'ARGENTINA', 'X5000GXB', '0', '0', '6019390', '0351-4220348'),
(99, '540', 'ANTONIO EMILIO', 'STACCHIOLA - NAVARRO ', '0388-4253915', '3884777210', 'antoniostacchiola@hotmail.com', 'CARLA MARIA', 'NAVARRO ', NULL, NULL, '', 1, '12006614', '13284692', '1956-06-13', NULL, 'RUTA PCIAL. Nº 1 KM. 5', 'S.SALVADOR DE JUJUY', 'JUJUY', 'ARGENTINA', 'Y4600HPF', 'carlamnavarro@hotmail.com', '6019666 (Interval)', '6019666', '0388-4233080 OF'),
(100, '547', 'RENE ALBERTO', 'BERGARA - ', '(0376)446-7848', '(0376)154-27-3145', 'reneal@arnet.com.ar', '0', NULL, NULL, NULL, '', 1, '6082556', '6673184', '1948-09-09', NULL, 'AV.AGUADO Y AV.CENTENARIO (CH 106)EDIF 6 PISO 2 \"C\"', 'POSADAS', 'MISIONES', 'ARGENTINA', '-3300', 'rene_albertobergara@hotmail.com', '6353-00204', '26805', '0'),
(101, '551', 'DIBUR GABRIEL/ERICA/NADIA', 'DIBUR MAYRA - DIBUR ', '4452-6647 hijo', '153-881-8953', 'mierydibur@gmail.com', 'ALMIRA SIMIC', 'DIBUR ', NULL, NULL, '', 1, '23371260', '6254746 ', '1973-08-09', NULL, 'SUIPACHA 1331 MASCHWITZ PRIVADO LOTE 80', 'MASCHWITZ', 'BUENOS AIRES', 'ARGENTINA', '0', '0', '0', '26306', '4452-6647 (133)'),
(102, '555', 'ANALIA', 'MENCONI - VIDAL ', '4281-9321', '155-003-5656 //  153-247-0178 (Miguel)', 'vidaljorge@securityandrisk.com.ar', 'JORGE', 'VIDAL ', NULL, NULL, '', 1, '14530455', '0', '1960-11-29', NULL, 'AV RAMON SANTAMARINA 650', 'MONTE GRANDE', 'BUENOS AIRES', 'ARGENTINA', 'B1842AFT', 'analiamenconi@hotmail.com', 'ID RCI', 'ID INTERVAL', '4296-7969'),
(103, '559', 'MARIA ALICIA', 'BARBOZA - GALLAND ', '(0221)482-8756 (Fijo Karina Galland)', '(0221)155-085-835 (Gustavo)', 'gcgalland@gmail.com', 'GUSTAVO CARLOS', 'GALLAND ', NULL, NULL, '', 1, '6262101', '7370276', NULL, NULL, 'Calle 18 entre 42 y 43 Nº 512', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1897GGW', 'gcgalland@gmail.com', '0', '6015709', '0221-155768269 CEL KARINA'),
(104, '571', 'EMILSE IRENE', 'CRECHIA - BUSCH ', '4768-4561', '   ', 'gustavobusch@yahoo.com.ar', 'GUSTAVO', 'BUSCH ', NULL, NULL, '', 1, '12542797', '0', '1957-09-04', NULL, 'AMERICA 3789', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', 'B1653HFE', 'telecentro_brown@hotmail.com', '0', '6019307', '4768-4561'),
(105, '576', 'JUAN CARLOS', 'DE MELO - MUSCOLINI ', '4667-3109', '113-561-3073 (Juan Carlos) // 113-561-3073 (Monica)', 'monicamuscolini@yahoo.com.ar', 'MONICA VIVIANA', 'MUSCOLINI ', NULL, NULL, '', 1, '92333693', '17273866', '1961-04-17', NULL, 'SANTA FE 2888 Unid.33 COUNTRY SAN MIGUEL DE GHISO', 'BELLA VISTA', 'BUENOS AIRES', 'ARGENTINA', '1661', 'juniordemelo5@yahoo.com.ar', '6353-00465', '1', '4664-8195/4668-0448 '),
(106, '580', 'HUGO', 'BIANCHI - CAHEN SALABERRY', '4961-4305', '154-412-5027   ', 'bianchihj@gmail.com', 'JACQUELINE ', 'CAHEN SALABERRY', NULL, NULL, '', 1, '4551008', '5310737', '1946-03-12', NULL, 'MARCELO T.DE ALVEAR 2320 PISO 7º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1122AAL', NULL, '6353-00217', '6019293', '(0220)483-4273'),
(107, '588', 'EDUARDO CARLOS', 'FERNANDEZ - RIOS ', '4931-6285 SE MUDO', '4381-2633  mama   ', 'lecuarius40@gmail.com', 'MARIANA', 'RIOS ', NULL, NULL, '', 1, '13264605', '14680151', '1959-07-23', NULL, 'MEXICO 1522  5º \"D\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1097ABF', NULL, '6353-00576', '6020311', '155-924-4149'),
(108, '589', 'CECILIA MARINA', 'DE BARI - ', '0', '   ', 'ceci.debari@gmail.com', '0', NULL, NULL, NULL, '', 1, '24551546', '0', '1975-05-13', NULL, 'México 628, 3º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1430BLA', 'ceci.debari@gmail.com', 'ID RCI', 'ID INTERVAL', '4307-5038 // 4300-2449'),
(109, '593', 'MIRTA HAYDEE/MARIA CELINA', 'FILIGUERA - ', '4696-3676 // (0237) 462-5827 (Celina)', '0', 'negricati@hotmail.com', '0', NULL, NULL, NULL, '', 1, '4617470', '0', '1943-06-20', NULL, 'PARANA 853 E/BALCARCE Y ARENALES', 'MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1708CLG', 'macelinasilva43@gmail.com', '6353-00596', '6019393', '4628-6618 // 4696-3676'),
(110, '594', 'MYRIAM NOEMI', 'ALVA - VILLANUEVA', '0', '155-183-5512 (Myriam)', 'myriamalva@hotmail.com', 'GABRIEL ERNESTO', 'VILLANUEVA', NULL, NULL, '', 1, '11267714', '0', '1954-12-02', NULL, 'SOLIS 1389', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', '1686', 'natylasala@yahoo.com.ar ', '6353-00138', '26715', '0'),
(111, '597', 'ADRIANA RAQUEL', 'DIE - URIBE ', '0297-4451214', '(0297)156-24-6534 (Enrique)', 'laboratorioalfa@speedy.com.ar', 'ENRIQUE RENE', 'URIBE ', NULL, NULL, '', 1, '13564756', '1115551', '1957-12-06', NULL, 'Armada República 34', 'RADA TILLY', 'CHUBUT', 'ARGENTINA', 'U9001AJB', 'uribe_alfa@yahoo.com.ar', '0', '6019579', '0297-4460056 URIBE'),
(112, '619', 'OSCAR ALFREDO', 'SANZ - ', '4786-5623', '154-473-7475   ', 'oski007@hotmail.com', '0', NULL, NULL, NULL, '', 1, '10401557', '14215901', '1953-01-22', NULL, 'CONGRESO 2768 7º \"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428', 'puertoalegria@hotmail.com', '6353-01274', '6019671', '0'),
(113, '630', 'Juan Pablo', 'VEGA - HERRERA', '4827-3950 juan pablo *', '153-807-1889 Sra.   ', 'juanpvega@hotmail.com', 'IRMA', 'HERRERA', NULL, NULL, '', 1, '10733247', '11366293', '1953-06-25', NULL, 'AV SANTA FE 3524 3º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425BGX', 'rosivega@hotmail.com', '2369-00194', '6019714', '4327-1914 NO ES ABON'),
(114, '635', 'ALBERTO CESAR', 'ACHAGA - DALESIO ', '4737-0210', '154-578-1391 (Alberto)', 'albertocesarachaga@gmail.com', 'GRACIELA', 'DALESIO ', NULL, NULL, '', 1, '11768099', '12588943', '1955-09-04', NULL, 'URUGUAY 3241 UNID.FUNCIONAL  N 3. B.LAS VICTORIAS', 'SAN FERNANDO', 'BUENOS AIRES', 'ARGENTINA', '1646', NULL, 'NO TIENE', '6019242', '4788-9898 Int. 133'),
(115, '641', 'CRISTIAN', 'BJERRUM - PILIPIAK ', '0', '153-227-1345   (Cristian)', 'cristianbjerrum@yahoo.com.ar', 'RODOLFO', 'PILIPIAK ', NULL, NULL, '', 1, '17741447', '20202283', '1966-01-21', NULL, 'CAAGUAZU 6222', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1408ETL', NULL, '6353-00225', '6019406', '0'),
(116, '644', 'LIA STELLA MARIS', 'ALVAREZ - 0', '(0223)451-5426', '(0223)155-20-0067 (Daniel) // (0223)155-76-8734 (Lía)', 'liasmalvarez@hotmail.com', '0', '0', NULL, NULL, '', 1, '14319375', '0', NULL, NULL, 'VIAMONTE 3524', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7602DGG', 'danielpennisi@hotmail.com', '0', '6019677', '(0223)451-2051'),
(117, '647', 'INGRID B.', 'CADENASSO - FANTON ', '02941-431204', '(02984)15591573   ', 'fannestor@hotmail.com', 'NESTOR PABLO', 'FANTON ', NULL, NULL, '', 1, '5978195', '8213805', '1951-07-24', NULL, 'RIO NEGRO 1554', 'GENERAL ROCA', 'RIO NEGRO', 'ARGENTINA', 'R8332FXJ', NULL, '6353-00289', '6019747', '02941-436547'),
(118, '656', 'FERNANDO MANUEL R.ALVES', 'GOMES RODRIGUEZ - IANNINI', '(+55) 13 32849464', '(+55) 13 997414163', 'fgomes@litoral.com.br', 'SILVANA MARIA', 'IANNINI', NULL, NULL, '', 1, '7130097', '3795722', '1955-07-23', NULL, 'AV.WASHINGTON LUIZ N 514/92  CEP:11.055-000', 'SANTOS-CEP 11005-000', 'SAO PAULO - BRASIL', 'BRASIL', '0', 'fernando@arbes.com.br; fmragomes@gmail.com', '6353-00680', '6019738', '0'),
(119, '665', 'MIGUEL ALBERTO', 'LOPEZ - DIAZ ', '0', '(03487)155-467-72 Miguel', 'lopezmiguelalberto@hotmail.com', 'SYLVINA', 'DIAZ ', NULL, NULL, '', 1, '8572730', '13776675', '1951-07-27', NULL, 'PJE RIO DE LA PLATA 108', 'ZARATE', 'BUENOS AIRES', 'ARGENTINA', 'B2800NOA', 'lopezmiguelalberto@hotmail.com', '0', '6019575', '(03487)430-174'),
(120, '666', 'CARLOS', 'CAMPODONICO - DOVAL', '4793-6322', '4721-6756/FAX-6532   15-6367-0997', 'campodonicoc@gmail.com', 'GRACIELA', 'DOVAL', NULL, NULL, '', 1, '11864435', '12080616', NULL, '1955-08-19', 'LIBERTAD 1034', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1640GEF', NULL, '0', '6019686', '4721-6532'),
(121, '667', 'RAFAEL HERNANDO', 'VITALE - 0', '4666-5346', '1541848078 / 1533444851 (Rafael)', 'rafavitale@funmarketing.com.ar; rafavitale1@gmail.com', '0', '0', NULL, NULL, '', 1, '22501606', '0', NULL, NULL, 'MOSCONI 2669', 'LOS POLVORINES', 'BUENOS AIRES', 'ARGENTINA', 'B1613FSO', 'sofiaespagnol@gmail.com', '0', '26733', '4660-2844'),
(122, '668', 'EDGARD', 'PRON - ', '4871-0140', '155-339-7047', 'pron0705@gmail.com', '0', NULL, NULL, NULL, '', 1, '12907470', '0', '1957-05-07', NULL, 'DEL YACARE 14 BARRIO CASTORES 1', 'NORDELTA', 'BUENOS AIRES', 'ARGENTINA', 'B1670CAD', '0', '0', '26794', '0'),
(123, '669', 'MARIA JULIA', 'BRACERAS - 0', '02983-433292', '(02983)155-53-145 (María)', 'verobraceras@speedy.com.ar', 'MIGUEL ', '0', NULL, NULL, '', 1, '17233998', '0', '1964-10-13', NULL, 'OLAVARRIA 554', 'TRES ARROYOS', 'BUENOS AIRES', 'ARGENTINA', '7500', 'mjbraceras@gmail.com', '0', '27123', '15-4427-7764 miguel'),
(124, '670', 'JULIO LUIS ALBERTO', 'SACHER - DOMINGUEZ ', '(02284)455-269 (Liliana)', '2284-497776 SRA', 'lilidom@live.com.ar', 'LILIANA', 'DOMINGUEZ ', NULL, NULL, '', 1, '5466781', '13503199', '1936-03-21', NULL, 'CALLE GRAL. PAZ 3255 CASILLA DE CORREO 388', 'OLAVARRIA', 'BUENOS AIRES', 'ARGENTINA', '7400', 'lilidom@live.com.ar', '0', '6019764', '(02284)428-733/24'),
(125, '674', 'JOSE', 'ARRAYAGO - BOUHEBENT', '(0294)446-4088 (Hugo) // 4521-7826 (Jose)', '154-472-7933 (José) // (0294)154-36-0942 (Hugo)', 'hbouhebent@gmail.com', 'PATRICIA', 'BOUHEBENT', NULL, NULL, '', 1, '8403432', '7677520', '1950-12-05', NULL, 'Diucas 153 Barrio Pajaro Azul', 'BARILOCHE', 'Rio Negro', 'ARGENTINA', '8401', 'josearrayago@fibertel.com.ar', 'ID RCI', 'ID INTERVAL', '4793-2902'),
(126, '678', 'EDUARDO', 'PIEVANI - MILLAN ', '(0221)483-4857', '(0221)15-420-0063 (Eduardo)', 'eapievani@yahoo.com.ar', 'ANALIA', 'MILLAN ', NULL, NULL, '', 1, '8346359', '0', '1946-07-04', NULL, 'CALLE 18 N 226', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1896JKF', 'analiadivi@yahoo.com.ar', '943-01117', '26814', '155-049-8585 sra'),
(127, '681', 'EUGENIO CESAR', 'BARGIELA - 0', '2070-0300', '114-470-0692', 'estudiobargielae@gmail.com', 'MONICA', '0', NULL, NULL, '', 1, '11734187', '0', '1955-09-05', NULL, 'REMEDIOS 3360 PISO 3º\"A\"', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1407HJB', '0', '6353-00177', '6020033', '4613-1661'),
(128, '683', 'PABLO RAUL', 'FERRARI - FERNANDINI ', '4658-3186/ 4480-9088sra', '155-938-6866 sra.   ', 'stephaniefernandini@hotmail.com', 'STEPHANIE', 'FERNANDINI ', NULL, NULL, '', 1, '18310352', '92189449', '1966-10-03', NULL, 'MAYOR CASTELLI 520', 'VILLA SARMIENTO - MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1707AYL', 'pablo.ferrari@aa.com // Stephanie.fernandini@aa.com', '0', '6019768', '4480-8016/8215 FAX'),
(129, '685', 'ERNESTO', 'SCHOON - MEZZANO ', '4432-4026/44324014 HAYDEE', '156-200-8582 (Marcela)', 'marcelaschoon@yahoo.com.ar', 'MARCELA C.', 'MEZZANO ', NULL, NULL, '', 1, '10868302', '12572131', '1953-07-18', NULL, 'MIRO 89, 7º\"C\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1406CUA', 'ernestoschoon@yahoo.com.ar', '0', '4445', '15-5008-5111 SR'),
(130, '688', 'FILOMENA', 'D\'AMBROSIO - BIANCHI ', '02316)45-2161', '02316-454053 HNA LUCIA   ', 'dambrosiofilomena6@gmail.com', 'JORGE ADRIAN', 'BIANCHI ', NULL, NULL, '', 1, '13524106', '10179941', '1948-01-05', NULL, 'AV.ROCA 414', 'DAIREAUX', 'BUENOS AIRES', 'ARGENTINA', 'B6555CDR', 'filo@dxred.com.ar', '6353-00444', '6020507', '02314-15400709'),
(131, '690', 'HUGO LUIS', 'LUJAN - YULITA ', '4801-6807', '154-171-8999 HUGO / 11-6359-9806 susana', 'sulujan@fibertel.com.ar', 'SUSANA LAURA', 'YULITA ', NULL, NULL, '', 1, '4318967', '5077188', '1940-08-10', NULL, 'FIGUEROA ALCORTA 3501 5º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425CLB', '0', '0', '6019918', '0'),
(132, '693', 'AMALIA MAGDALENA', 'LUJAN - 0', '4554-4363', '154-061-7767 (Amalia)', 'amalia.chuli.lujan@gmail.com', '0', '0', NULL, NULL, '', 1, '3544143', '0', '1937-01-27', NULL, 'SAN MARTIN 66 6º OFIC. 611', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1004', 'guillermina.montanari@gmail.com', '6353-00854', '6019761', '0'),
(133, '704', 'MARCELA', 'GARCIA RUBINO - ALVAREZ DE MORENO ', '4793-6001/4836-2256/2263', '152-363-5086 (Marcela)', 'marsygarcia@gmail.com', 'PATRICIA', 'ALVAREZ DE MORENO ', NULL, NULL, '', 1, '14255552', '14790477', '1960-08-20', NULL, 'RICARDO GUIERREZ 2725', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1642', '0', '0', '27127', '155308-4228 Horacio'),
(134, '705', 'CLAUDIO OMAR', 'GORRIZ - CEPA ', '03388-422310', '(03388) 154-17-862 (Daniel) //(03388)156-78-579 (Claudio) ', 'cf_arqs@hotmail.com', 'DANIEL ANTONIO', 'CEPA ', NULL, NULL, '', 1, '12567079', '12213802', NULL, '1958-03-13', 'SAN MARTIN 210', 'GENERAL VILLEGAS', 'BUENOS AIRES', 'ARGENTINA', 'B6230DCF', NULL, '6353-00366', '1433145', '0'),
(135, '711', 'TERESITA OFELIA', 'CALDERON - ', '4502-7458/4653-2343', '155-064-2067', 'mielni@fibertel.com.ar', '0', NULL, NULL, NULL, '', 1, '4936033', '0', '1945-02-11', NULL, 'PEDRO MORAN 3342', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419HKH', 'natashamielni@hotmail.com', '0456-01228', '27217', '4657-3231 ING HENRY'),
(136, '712', 'LOURDES', 'DOMBRAUSKAS, LOURDES - VILLAROYA ,  LOURDES', '4295-9860', '155949-8327', 'ivannauskas@gmail.com', ' LOURDES', 'VILLAROYA ', NULL, NULL, '', 1, '5900363', '0', '1946-05-28', NULL, 'SENADOR PALLARES 1545', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', '-1824', 'eduardo_torres@sinectis.com.ar', '0', '6020207', '0'),
(137, '720', 'HECTOR GUILLERMO', 'SPINELLI - ULECIA ', '(0221)425-0125 // (0221)452-5292', '(0221) 156-150-365', 'anaulecia@hotmail.com', 'ANA MARIA', 'ULECIA ', NULL, NULL, '', 1, '8575888', '12662567', '1951-09-12', NULL, 'CALLE 59 Nº 677 Y 1/2 entre calle 8 y 9', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1900BTA', 'hgspinelli@hotmail.com', '0', '27307', '(0221)425-3629 SRA /(0221)427-0737'),
(138, '726', 'FERNANDO FACUNDO', 'GONZALEZ - BUSTOS PAZ', '4871-7353/4553-0040 PEña', '(0383) 154-60-1481 (Daniela)', 'fliagonzalezcat@yahoo.com.ar', 'DANIELA MARIA', 'BUSTOS PAZ', NULL, NULL, '', 1, '20009267', '18439694', '1968-02-02', NULL, 'DE LOS JUNCOS 9 BARRIO LA ISLA LOTE 225', 'NORDELTA', 'BUENOS AIRES', 'ARGENTINA', '1670', NULL, '6353-00689', '6019691', '4553-0040 INT 237'),
(139, '727', 'FERNANDO OSCAR', 'MASSUH - OLCESE ', '(02322)458-140', '153-180-0806', 'fernando.massuh@grupopenaflor.com.ar', 'MARIA VICTORIA', 'OLCESE ', NULL, NULL, '', 1, '16021879', '18226622', '1966-01-20', NULL, 'BARRIOS LA PRADERA HARAS DEL PILAR LOTE 58', 'PILAR', 'CAPITAL FEDERAL', 'ARGENTINA', '1629', 'fernando.massuh@grupopenaflor.com.ar', '6353-00918', '6020179', '4349-1858'),
(140, '733', 'EDUARDO', 'DYCZKOWSKI - ARREGUI ', '0', '156-390-7199 (Mónica)', 'monica.arregui@grupoestisol.com', 'MONICA', 'ARREGUI ', NULL, NULL, '', 1, '13217443', '0', '1961-04-11', NULL, 'JUAN FARRELL 2964', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', 'B1824GWD', 'monicagraciel@hotmail.com', '6353-00541', '6019577', '4911-8111 Int. 213 (Mónica)'),
(141, '741', 'MARTIN', 'MARINO HECTOR - ERCORECA ', '0223-4730013', '0223-4783020........   0223-155277800', 'martin51marino@hotmail.com', 'ANGELICA B.', 'ERCORECA ', NULL, NULL, '', 1, '8588568', '10396857', '1951-10-04', NULL, 'GUIDO 1341', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7600CIA', 'beatrizercoreca@hotmail.com', '0', '27682', '0223-4783020'),
(142, '743', 'ANA MARIA', 'GHIONE - ARAGON ', '(02954)425-783/425-756', '2954-593107', 'drmiguelaragon@gmail.com', 'MIGUEL ANTONIO', 'ARAGON ', NULL, NULL, '', 1, '11711373', '8560924', '1955-04-14', NULL, 'HIPOLITO YRIGOYEN 667', 'SANTA ROSA', 'LA PAMPA', 'ARGENTINA', 'L6300AVM', 'ana_ghione@hotmail.com', '0', '26712', '(02954)432-322 // (02954)453-201 fax Laborat.'),
(143, '750', 'MARIA DE LAS MERCEDES', 'PRADO - LICASTRO PRADO', '4552-4311 ( no corresp a un cliente en serv)', '4702-3065   4312-1221-INT.117 TRABAJO', 'juliana.prado@klixar.com', 'FLORENCIA', 'LICASTRO PRADO', NULL, NULL, '', 1, '4885618', '23229544', NULL, NULL, 'CONDE 1995  7º DTO.16', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428DAC', NULL, '6353-01123', '6019767', '4315-4946'),
(144, '764', 'OSVALDO RAMON', 'CASTAÑEDA - ', '4925-1435', '155-025-6567', 'osvaldo.cataneda@gmail.com', '0', NULL, NULL, NULL, '', 1, '4412475', '0', NULL, NULL, 'AV.del LIBERTADOR 1068-P 10', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1112ABN', 'unyka@hotmail.com', '0', '6019730', '0'),
(145, '766', 'WALTER', 'CICCALE - FRACCARO', '4799-3933', '155-748-7911 o 153-126-4821 (Walter) ', 'walterfabian1964@gmail.com', 'MARIANA ANDREA', 'FRACCARO', NULL, NULL, '', 1, '16844957', '20742113', '1964-02-20', '1969-09-25', 'DEBENEDETTI 3183/3083', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', NULL, NULL, NULL, '153-202-3547 (Mariana)'),
(146, '777', 'LUIS CESAR', 'ALVAREZ - PINNA ', '(02954)42-8157', '(02954)155-51-187 (Pinna) // 155-25-700 (Luis)', 'nutripam1@hotmail.com', 'DANIEL ROQUE', 'PINNA ', NULL, NULL, '', 1, '12194542', '12877949', '1958-03-09', NULL, 'AV.ARTURO ILLIA 623', 'SANTA ROSA', 'LA PAMPA', 'ARGENTINA', 'L6300BCD', 'arqdrp@hotmail.com', '6353-00060', '6027711', '(02954)42-7341');
INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(147, '787', 'ADRIANA', 'SOMOZA - RASENTE', '(02323) 497-058 // (02323) 432-208', '154-986-4930 Sr.', 'betorasente@yahoo.com.ar', 'ADALBERTO', 'RASENTE', NULL, NULL, '', 1, '12068855', '16041865', '1958-02-24', NULL, 'LOS TEJEDORES 129', 'JOSE M JAUREGUI', 'BUENOS AIRES', 'ARGENTINA', 'B6706BRA', 'rorasente@hotmail.com', '6353-01315', '6023679', '(02323)497-768 Of. '),
(148, '811', 'CARLOS', 'ESTRADA - ESTRADA ', '4735-3155', '154-483102', 'carlosestrada@ormiflex.com', 'MARIA ALEJANDRA', 'ESTRADA ', NULL, NULL, '', 1, '11565363', '11635204', '1955-01-07', NULL, 'LOS OLIVOS 890 BºLAS BARRANCAS', 'BOULOGNE', 'BUENOS AIRES', 'ARGENTINA', 'B1609ENR', 'alerestrada1@gmail.com // ormiflex@ormiflex.com', '0', '26422', '4713-1080'),
(149, '820', 'MONICA', 'PERKINS - DEYMONNAZ ', '4804-9736', '154-425-2961   ', 'monicadperkins@yahoo.com.ar', '0', 'DEYMONNAZ ', NULL, NULL, '', 1, '6186715', '0', NULL, NULL, 'AV.ALVEAR 1918 - 1º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1129', NULL, '6353-01096', '1', '4813-0649'),
(150, '826', 'RAUL OMAR', 'PLEE - 0', '4815-0756/4315-1378 E', '156-456-7171', 'plee@fibertel.com.ar', 'MATILDE ', '0', NULL, NULL, '', 1, '10754382', '0', '1953-06-03', NULL, 'Av. Alvear 1531 3º A', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1014', NULL, '6353-01116', '1', '4665-1004'),
(151, '840', 'DIEGO', 'LANUS - CASARES ', '4792-2705/ 4806-0718', '1532792765 SRA   4577-2565', 'adolfoglanus@gmail.com', 'MARIA', 'CASARES ', NULL, NULL, '', 1, '24314725', '10390391', '1975-01-11', NULL, 'RICARDO GUTIERREZ 2423', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1640ADW', 'mariatutia@gmail.com', '6353-00809', '31609', '4747-6071'),
(152, '846', 'ALEJANDRO', 'DODERO - ', '4326-0707 secret', '4326-0707-FAX 115   154-447-1557', 'kdv@fibertel.com.ar; laura@elpicaflor.com.ar  ', '0', NULL, NULL, NULL, '', 1, '10203542', '0', '1951-08-12', NULL, 'AV.ROQUE SAENZ PEÑA 720 8º PISO', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1035AAP', 'Osvaldo@elpicaflor.com.ar; administracion@elpicaflor.com.ar', '6353-00520', '8996', '4806-4806 (De 10 a 18 Hrs) - Kitty'),
(153, '852', 'JUAN MANUEL', 'LOZADA - ', '0', '155-625-4995 (Juan Manuel)', 'jmlozada10@hotmail.com', '0', NULL, NULL, NULL, '', 1, '4552843', '0', '1942-07-20', NULL, 'PARANA 1239 - 6 º B', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425AYA', '0', 'ID RCI', 'ID INTERVAL', '4813-2342 (Estudio)'),
(154, '880', 'PERLA', 'VASS - 0', '4794-8045', '116-366-0526 (Perla)', 'vperla@fibertel.com.ar', '0', '0', NULL, NULL, '', 1, '4533542', '0', NULL, NULL, 'DIAZ VELEZ 2526', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636EEX', NULL, '6353-00283', '6010354', '0'),
(155, '897', 'RODOLFO', 'MICHALEK - AMOR ', '4661-2233', '011-1544266014', 'mana.federovsky@comex.com.ar', 'ALICIA', 'AMOR ', NULL, NULL, '', 1, '7768717', '4933533', NULL, NULL, 'SARMIENTO 2652', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', 'B1712BOZ', 'romialam@hotmail.com', '6353-00941', '6014682', '4325-8980 int.105'),
(156, '904', 'ERNESTO', 'CRESCIA - EX ', '4743-1295', '156-512-4040 (Ernesto) // (03388)154-80-366 (Federico)', 'ecrescia@fibertel.com.ar', 'OYUELA', 'EX ', NULL, NULL, '', 1, '7757762', '0', '1945-04-27', NULL, 'ROQUE SAENZ PEñA 684', 'SAN ISIDRO', 'BUENOS AIRES', 'ARGENTINA', '1642', '0', 'ID RCI', 'ID INTERVAL', '155-117-2717 (Whatsapp Ernesto)'),
(157, '909', 'MARCELA RYAN DE', 'CALVENTE - ', '4477-3957 // (0230)443-0099', '116-864-2803', 'admpoloplus@gmail.com', '0', NULL, NULL, NULL, '', 1, '12460821', '0', '1952-08-17', NULL, 'AV VENANCIO CASTRO RUTA 28 1400', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', 'B1629ACN', NULL, '6353-00294', '9005', '02322-430099/FAX'),
(158, '918', 'RICARDO FRANCISCO', 'LAMASTRA - ', '4794-9148', '156-372-0158', 'lamastra_m@hotmail.com', '0', NULL, NULL, NULL, '', 1, '6430385', '0', NULL, NULL, 'ALBERDI 720', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636FOF', NULL, '0', '6006071', '4370-5584'),
(159, '921', 'Juan Pedro Martin', 'BURS - PEREYRA IRAOLA', '(0223) 451-6167 Pereyra', '(0223) 155-24-4182 // (0223)154-56-2007 (Maria)', 'marinaburs36@gmail.com', 'Rafael', 'PEREYRA IRAOLA', NULL, NULL, '', 1, '16975381', '16496401', NULL, NULL, 'ANCAVILU 910', 'TANDIL', 'BUENOS AIRES', 'ARGENTINA', '7000', 'maym64@yahoo.com.ar', '6353-00274', '6007246', '(0223) 451-8262'),
(160, '922', 'RODOLFO', 'REQUEJO - REQUEJO ', '4374-8619', '154-413-5115 (Rodolfo)', 'estudio@rfcia.com.ar', 'DE TURRI CRISTINA', 'REQUEJO ', NULL, NULL, '', 1, '4465796', '10964061', '1948-09-08', NULL, 'ARCOS 1641 PISO 16', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426BGK', 'sandra-seguros@hotmail.com; danireque@hotmail.com', '6353-01161', '6010544', '4393-4611esmePA'),
(161, '924', 'ALBERTO', 'MANCKE - PADIN (FALLECIO)', '4732-0844', '116-680-0695', 'amancke@dataclient.com.ar', 'MONICA ', 'PADIN (FALLECIO)', NULL, NULL, '', 1, '5454316', '0', NULL, NULL, 'LONARDI 466', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', 'B1643FAJ', 'avmgma@hotmail.com', '3161-00401', '31608', '4361-1212/1354int121'),
(162, '934', 'RICARDO A.', 'PALACIOS - palacios', NULL, '(0341) 153-650-816', 'ripalacios@arnet.com.ar', 'beatriz susana', 'palacios', NULL, NULL, '', 1, '6057768', '0', NULL, NULL, 'BALCARCE 1711 5', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000GGA', 'ripalacios@arnet.com.ar', 'ID RCI', 'ID INTERVAL', '(0341) 525-7627'),
(163, '942', 'SERGIO ANIBAL', 'CZYZYK - CHURBA ', '4382-2648 TEL.Y FAX', '4782-9085/154-069-9335   ', 'sergioczyzyk@gmail.com', 'PAULINA.', 'CHURBA ', NULL, NULL, '', 1, '11824284', '0', '1955-09-12', NULL, 'CRAMER 1879   10º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428CTA', '0', '0', '1', '154-069-9336 SERGIO'),
(164, '945', 'ALICIA', 'SINGER - TORDO ', '4812-5650/4815-8626 SR.', '154-197-4662 (Alicia) / 1540819530', 'info@americanhair.com.ar', 'SERGIO', 'TORDO ', NULL, NULL, '', 1, '0', '0', NULL, NULL, 'TALCAHUANO 1022 PISO 1º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1013AAV', 'mirtallorente@hotmail.com', 'ID RCI', 'ID INTERVAL', '4737-3067'),
(165, '964', 'ROBERTO', 'MORALES - MORO ', '4623-8312', '4854-9441/4855-4589   ', 'rmorales@macroargentina.com.ar', 'MARINA', 'MORO ', NULL, NULL, '', 1, '5223140', '0', '1950-10-12', NULL, 'MACHADO  2745', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', 'B1712BLA', '0', '6353-00967', '6009198', '4854-9441'),
(166, '965', 'GUILLERMO HUGO', 'MARTI - SILVIA ', '4242-1834/15-6961-1218SRA', '4778-2867 FAX   ', 'gmarti@tecnoaccion.com.ar', 'CARROCERA', 'SILVIA ', NULL, NULL, '', 1, '10879245', '11428766', NULL, NULL, 'RODRIGUEZ PEÑA 668', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', 'B1828HIN', 'silviacarrocera@hotmail.com', '6353-00897', '6020767', '4778-2861/4242-2752'),
(167, '969', 'HECTOR', 'LUFRANO TAMASI - BRUNETTI', '4750-3146', '155-226-5362', 'estudiolufrano@hotmail.com', 'SANDRA', 'BRUNETTI', NULL, NULL, '', 1, '11488834', '0', '1955-02-23', NULL, 'AV. SAN MARTIN 3113 2', 'CASEROS', 'BUENOS AIRES', 'ARGENTINA', 'B1678GQH', 'cerrodelmanantial@live.com.ar', '0', '9779', '0'),
(168, '982', 'MARIANO', 'DI YORIO - ', '4571-6886', '15-4448-2370   ', 'm.diyo@hotmail.com', '0', NULL, NULL, NULL, '', 1, '4433980', '0', '1944-07-02', NULL, 'Franco 3350 Unidad 21', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1416', 'info@hugomascheroni.com.ar', 'ID RCI', 'ID INTERVAL', '0'),
(169, '983', 'OSCAR', 'DITZEND - DOBOKA', '4717-4895', '155-564-7807   ', 'oscar@thesa.com.ar', 'ALICIA', 'DOBOKA', NULL, NULL, '', 1, '12286283', '0', NULL, NULL, 'NICOLAS REPETO 4077', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636CTS', '0', 'ID RCI', 'ID INTERVAL', '4762-0786'),
(170, '993', 'DANIEL', 'CARMONA - PEREZ', '4553-3530', '154-448-0756   ', 'danocar@gmail.com', 'SILVIA', 'PEREZ', NULL, NULL, '', 1, '10151439', '6689862', NULL, NULL, 'AV. DE LOS INCAS 3765', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1427DNC', 'tiempodeviajar@coopvgg.com.ar', '6353-00326', '15342', '4553-3530'),
(171, '994', 'ANA MARIA', 'IERVASI - ', '4942-2897', '155-620-6461', 'aitallermexico@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '5866234', '0', '1948-02-16', NULL, 'ADOLFO ALSINA 2514', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1090AAP', NULL, '0', '13987', '4942-2896'),
(172, '1000', 'ADELMA LUCIA', 'HERRERA - ', '4222-5446', '4243-5555 INT 1109 // 155-062-3678', 'alherrera@intramed.net', '0', NULL, NULL, NULL, '', 1, '13922374', '0', '1959-08-18', NULL, 'GRAL PAZ 8 PISO 16 \"A\"', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', 'B1870CQB', NULL, '6353-00741', '27409', '4201-4355'),
(173, '1101', 'MIRTA ALICIA', 'TRAJTEMBERG - 0', '4865-6004', '15-4537-9599', 'mirtatravers@hotmail.com', '0', '0', NULL, NULL, '', 1, '5868303', '0', NULL, NULL, 'PANAMA 9234 PISO 8 A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '0', NULL, '0', '0', '4865-5771'),
(174, '1103', 'ANALIA JUDITH', 'BAUM - 0', '3972-4746', '113-130-2303', 'analiabaum@gmail.com', '0', '0', NULL, NULL, '', 1, '25477570', '0', NULL, NULL, 'Pasaje Inca 3849', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '0', 'analia.baum@hospitalitaliano.org.ar', '0', '0', '4959-0200 Int. 5783'),
(175, '1104', 'ESTEBAN JORGE', 'PALOMA - SCHIAVI', '0', '   ', 'estebanpaloma@hotmail.com', 'MARIA LINDA', 'SCHIAVI', NULL, NULL, '', 1, '13090571', '11358842', NULL, NULL, 'H FERNANDEZ 31 PISO 1 A ', 'CORDOBA', 'CORDOBA', 'ARGENTINA', '5000', NULL, '2777-00136', '0', '(0351)460-7199 (Consultorio)'),
(176, '1105', 'CLAUDIO GUSTAVO', 'PEREZ - POLETTI DE PEREZ', '5354-8326', '154-439-7322 (Claudio)', 'claudiogperez@gmail.com', 'VERONICA JULIA', 'POLETTI DE PEREZ', NULL, NULL, '', 1, '16844828', '93585588', '1963-08-29', '1965-01-16', 'Lima 2716 ', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '0', '0', '4793-7835 (Dejar Msj)'),
(177, '1517', 'GUNTER', 'SCHULZ - ACEVEDO ', '(02243)452-571', '(02241)156-96-839 sra   ', 'schulz.marion@gmail.com', 'MARIA TEODORA', 'ACEVEDO ', NULL, NULL, '', 1, '5372918', '4587511', '1936-05-23', NULL, 'DORREGO 1345 CC 151', 'GENERAL BELGRANO', 'BUENOS AIRES', 'ARGENTINA', '7223', NULL, '6353-01288', '6020567', '(02241)156-83-053 sr'),
(178, '1543', 'RICARDO', 'MAC MULLEN - MUZIO ', '(02241) 431-276', '1126920532', 'ricardomacmu@yahoo.com.ar', 'LYDIA', 'MUZIO ', NULL, NULL, '', 1, '14014430', '16421272', '1960-09-24', NULL, 'LAMADRID 77', 'CHASCOMUS', 'BUENOS AIRES', 'ARGENTINA', 'B7130DVA', 'lydia_muzio@yahoo.com.ar', '0', '6020765', '0'),
(179, '1577', 'LILIANA', 'CARAMUTI - VILLEMUR ', '(0299)443-9179', '(299) 154-288-959 Héctor', 'hvillemur@gmail.com', 'HECTOR', 'VILLEMUR ', NULL, NULL, '', 1, '16745177', '13574115', '1963-09-04', NULL, 'MENDOZA 1000', 'NEUQUEN', 'NEUQUEN', 'ARGENTINA', '8300', 'lilianacaramuti@gmail.com', '6353-00313', '20336', '0'),
(180, '1580', 'JOSE DE LA CRUZ', 'NARVAEZ - MALOOF PINTO', '00575-3597255 TEL/FAX', '00575-3595995 TEL OK   ', 'yazloof05@hotmail.com', 'YAZMIN', 'MALOOF PINTO', NULL, NULL, '', 1, '92873893', '92873866', '1960-01-28', NULL, 'CALLE 95-NRO 42 F 264', 'BARRANQUILLA', 'COLOMBIA-', 'COLOMBIA', '08001000', 'samirnarvaez@hotmail.com', '0', '6021063', '00575-3735319 fax'),
(181, '1581', 'GLADYS', 'NEWTON - ', '(02983) 432-416', '(02983) 155-74-427 (Gladys)', 'cmdoumecq@gmail.com', '0', NULL, NULL, NULL, '', 1, '3624851', '0', '1938-01-01', NULL, 'CORDOBA 265', 'TRES ARROYOS', 'BUENOS AIRES', 'ARGENTINA', 'B7500GXE', 'marianadoumecq@speedy.com.ar', '6353-01007', '6022600', '0'),
(182, '1589', 'DANIEL', 'GRINSTEIN - RAPOPORT ', '4797-6019', '15-4170-4297 SR.   4310-4447 SR  *', 'dgrinst@fibertel.com.ar', 'RUTH NOEMI', 'RAPOPORT ', NULL, NULL, '', 1, '10203012', '10380117', '1951-12-06', NULL, 'LAPRIDA 1046', 'VICENTE LOPEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1638CSF', 'dgrinstein@pan-energy.com', '2775-03853', '6021497', '310-4450/'),
(183, '1596', 'HUGO TOMAS', 'MANSILLA - FALETTO ', '4327-4837', '152-235-5224 (Hugo)', 'faletto@fibertel.com.ar', 'SILVIA BEATRIZ', 'FALETTO ', NULL, NULL, '', 1, '10515116', '11630474', '1952-09-06', NULL, 'MAIPU 216 3º \"D\"   o     AMARO GIURA 1181 3º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1084ABF', NULL, '6353-00878', '7359408', '0'),
(184, '1614', 'JORGE RAMON', 'HERNANDEZ - CACCIA ', '02477-428298', '02477-432513 - FAX   ', 'jorherus@hotmail.com', 'JORGELINA JUANA', 'CACCIA ', NULL, NULL, '', 1, '4390550', '2635725', '1942-03-31', NULL, 'CARLOS PELLEGRINI 3212', 'PERGAMINO', 'BUENOS AIRES', 'ARGENTINA', 'B2700MNR', 'Juliana.Hernandez@aa.com // juliher@gmail.com', '0', '9742', '0'),
(185, '1615', 'SERGIO HECTOR', 'CONDE - 0', '4782-2500', '15-6218-7135   ', 'sergiohectorconde@gmail.com', '0', '0', NULL, NULL, '', 1, '14204111', '0', '1960-11-07', NULL, 'ZAPIOLA 2355 PISO 1º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1428', 'shc@arnet.com.ar', '6353-00399', '6021632', '4717-8620'),
(186, '1616', 'ESTEBAN TADEO', 'GAUBECA - MOLINA ', '4664-0485', '0', 'gaubeca.molina@gmail.com', 'CARMEN ADELA', 'MOLINA ', NULL, NULL, '', 1, '4357095', '5574007', '1940-09-28', NULL, 'ROQUE SAENZ PEÑA 1970', 'SAN MIGUEL', 'BUENOS AIRES', 'ARGENTINA', 'B1663FKR', 'gaubeca.esteban@gmail.com', '0', '6021636', '4382-5351'),
(187, '1619', 'JUAN CARLOS ANTONIO', 'ACUÑA - GUTIERREZ ', '(0223)495-6833', '(0223)156-827-500   ', 'sujucacrc@hotmail.com', 'SUSANA', 'GUTIERREZ ', NULL, NULL, '', 1, '6517416', '0', '1941-02-28', NULL, 'ESPAÑA 1971', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7600CYM', 'acuprieto@gmail.com', '6353-00021', '6010633', '(0223)491-2601'),
(188, '1623', 'EDGARDO RUBEN', 'SCHMIT - HOLZMANN ', '0291-4538536 PADRE', '0291-4500160 CASA   0291-154291814    *', 'edgardo.schmit@petrobras.com', 'NORMA RAQUEL', 'HOLZMANN ', NULL, NULL, '', 1, '13744692', '13723899', '1961-01-17', NULL, 'PEDRO PICO 445', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000JZI', 'er_s60@hotmail.com', '6353-00008', '6019243', '0291-4519339mje'),
(189, '1625', 'JAIME ENRIQUE', 'ALVAREZ - BRANDONI ', '(02966)429-195 ', '(02966) 155-75-613', 'jeaspanish@hotmail.com', 'NOEMI G', 'BRANDONI ', NULL, NULL, '', 1, '8559093', '6405513', '1951-07-25', NULL, 'Orkeke 33', 'RIO GALLEGOS', 'SANTA CRUZ', 'ARGENTINA', 'Z9400IIH', 'icambridge@infovia.com.ar', '6353-00055', '29366', '0'),
(190, '1634', 'SUSANA VERONICA', 'TARDIVO - ROJO ', '(0230)464-5182', '155-061-2883', 'svtardivo@gmail.com', 'JULIO CESAR', 'ROJO ', NULL, NULL, '', 1, '6280835', '5719908', '1950-05-06', NULL, 'BRIO. LA MADRUGADA U FUNC. 46', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', '1631', '0', '0', '6022347', '0'),
(191, '1643', 'JORGE RAUL', 'CIRIBE - CENA ', '0341-4409846', '(0341)155-009-467 (Jorge)  ', 'josefinacena@hotmail.com', 'JOSEFINA ANA', 'CENA ', NULL, NULL, '', 1, '8599262', '13502475', '1951-05-27', NULL, 'BVARD.OROÑO 1236', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000DSZ', 'jorgeciribe@gmail.com', '6353-00391', '6023006', '4406184-TRAB.SRA'),
(192, '1646', 'SERGIO IGNACIO', 'NINO - DOMINGUEZ ', '0', '11-68500008', 'nachonino@gmail.com', 'ANGELICA', 'DOMINGUEZ ', NULL, NULL, '', 1, '21671461', '0', '1970-06-13', NULL, 'PELLEGRINI 582', 'GENERAL RODRIGUEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1748JBA', 'nachonino@gmail.com', '0', '6015171', '(0237) 485-3336 (Of. Ignacio'),
(193, '1652', 'FRANCISCO', 'MONTERO - SARMORIA ', '0', '(0280)154-419-791 (Ella)', 'marielsarmoria@hotmail.com', 'MARIA ELENA', 'SARMORIA ', NULL, NULL, '', 1, '8586309', '11576490', '1951-06-20', '1955-04-07', 'Complejo Bon Le, Edificio 4, 2º \'B\'', 'PUERTO MADRYN', 'CHUBUT', 'ARGENTINA', '9120', NULL, '4235-04495', '6022832', '0'),
(194, '1675', 'IRMA LILIAN', 'JARA TRACCHIA - JARA', '4861-8855', '154-069-4672', 'jaratracchia@gmail.com', 'DALMIRO', 'JARA', NULL, NULL, '', 1, '14789483', '0', '1961-11-26', NULL, 'GALLO 606; TORRE 2; Piso 27; Dpto. 2', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1174', 'jaratracchia@gmail.com', '0', '6025117', '4371-7832'),
(195, '1680', 'MARIA DE LOS ANGELES', 'DI MONACO - SANCHEZ ', '(0341) 481-9852', '(0341)155-04-063 Sr. // (0341)155-84-2897 (María)', 'mariadelosangelesdimonaco@hotmail.com', 'OSCAR EDGARDO', 'SANCHEZ ', NULL, NULL, '', 1, '14328015', '0', '1958-03-29', NULL, 'AYACUCHO 1768', 'ROSARIO', 'SANTA FE', 'ARGENTINA', '2000', '0', '400000282', '6025710', '0341-424-8702'),
(196, '1684', 'MARCELA CATERINA', 'GOROSITO - DI MONACO', '0341-4635501', '(0341)155-901-886', 'marcelagorosito@gmail.com', 'RENE', 'DI MONACO', NULL, NULL, '', 1, '13448801', '0', '1957-11-09', NULL, 'CORRIENTES 5680', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2011KGV', NULL, '0', '33243', '0'),
(197, '1688', 'RAUL ALFREDO', 'AZCONA - ALBERTO ', '0', '(03537)155-93-176 (Raúl) // (0351)155-94-9245 SR.', 'azcona.raul@gmail.com', 'CATALINA LUCIA', 'ALBERTO ', NULL, NULL, '', 1, '12293522', '0', '1958-05-05', NULL, 'INTENDENTE VILLARROEL 921', 'BELL VILLE', 'CORDOBA', 'ARGENTINA', '2550', 'catalina.alberto@gmail.com', '0', '6025047', '0'),
(198, '1690', 'ALFREDO OSCAR', 'CARLOMAGNO - CARLOMAGNO ', '(0341)485-2888 ', '(0341)155-864-500   ', 'olimpicosviajes@gmail.com', 'MA.DE LOS MILAGROS', 'CARLOMAGNO ', NULL, NULL, '', 1, '6056717', '31116055', '1944-05-25', NULL, 'PARAGUAY 1769', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000FZA', NULL, '6353-00323', '6025115', '0'),
(199, '1693', 'MIGUEL ANGEL', 'PIRANI - ', '(0341)455-2919 ', '0341-4540307 HIJO   ', 'miguelpirani@hotmail.com', '0', NULL, NULL, NULL, '', 1, '12282281', '0', '1956-01-15', NULL, 'ITURRASPE 4255', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2005NRI', NULL, '2775-05508', '6025176', '(0341)420-3028/3040'),
(200, '1705', 'ELSA BEATRIZ', 'POLERI - DI NENNO (FALLECIÓ)', '0299-4427078', '15-6107-0303   ', 'martin.dinenno@bpn.com.ar', 'RUBEN SEBASTIAN', 'DI NENNO (FALLECIÓ)', NULL, NULL, '', 1, '5379945', '5379945', '1942-08-14', NULL, 'JOSE MARIA GUTIERREZ 474', 'NEUQUEN', 'NEUQUEN', 'ARGENTINA', 'Q8300OMF', 'elsipoleri@hotmail.com', '0', '6025116', '0'),
(201, '1706', 'JORGE OMAR', 'SEIGUERMAN - VAL ', '(03487)422-921', '155-179-7653 El   ', 'elenavalmaestro@gmail.com', 'ELENA BEATRIZ', 'VAL ', NULL, NULL, '', 1, '7764606', '5475271', '1945-10-23', NULL, 'FELIX PAGOLA 556/564', 'ZARATE', 'BUENOS AIRES', 'ARGENTINA', 'B2800DCL', 'jojarey@hotmail.com', '4235-05041', '6025118', '155-180-8046 Ella'),
(202, '1709', 'SILVIA INES', 'DE ANGELIS DE BAZAN - BAZAN ', '4773-5071', '15-3760-9552 SRA.   ', 'ezebazan@yahoo.com.ar', '-SERGIO BAZAN', 'BAZAN ', NULL, NULL, '', 1, '6551180', '0', '1951-01-15', NULL, 'SOLD DE LA INDEP. 913 3º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426BTK', NULL, '6353-00451', '6025347', '15-5696-8956 SR'),
(203, '1715', 'SONIA CARMEN', 'NEGRI - TURON ', '0220-4825671', '15-51824796   ', 'estudioturon@hotmail.com', 'JORGE R.', 'TURON ', NULL, NULL, '', 1, '11730252', '0', '1954-12-03', NULL, 'FINOCHIETTO 768 ESQ. CENTENARIO', 'SAN ANTONIO DE PADUA', 'BUENOS AIRES', 'ARGENTINA', 'B1718CSP', 'hobi79@hotmail.com', '6353-01005', '6025349', '0220-4827211'),
(204, '1718', 'MARCELA ALEJANDRA', 'NOVARO - CAÑIBANO ', '4832-5297/4703-2859', '155-029-2961 (Marcela) // 154-471-5656 (Jorge) ', 'jorgiscani@live.com.ar', 'JORGE ERNESTO', 'CAÑIBANO ', NULL, NULL, '', 1, '14618371', '8403582', '1962-02-14', NULL, 'JULIAN ALVAREZ 2546 4º 18', '0', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', 'maralenovaro@gmail.com', '0', '1580027', '154-471-5656 el'),
(205, '1721', 'MARIA VIRGINIA/SARA VICEN', 'MARCOLONGO - SALVATIERRA ', '0221-154347347 Sara', '(02983)156-15-387 Jose   ', 'marcolongojose@hotmail.com', 'SARA (FALLECIO)', 'SALVATIERRA ', NULL, NULL, '', 1, '26250540', '4928940', NULL, NULL, 'BRANDSEN 354', 'TRES ARROYOS', 'BUENOS AIRES', 'ARGENTINA', 'B7500GLH', 'mvir08@hotmail.com; marcolongo_sara@yahoo.com.ar', '6030443 (Interval)', '6030443', '0'),
(206, '1742', 'LILIANA', 'TELLO - SCHIAVONE', '0261-4390368', '0261-4317600   ', 'lilyschiavone@yahoo.com.ar', 'JOSÉ ', 'SCHIAVONE', NULL, NULL, '', 1, '11809308', '10564999', NULL, NULL, 'BOGOTA 7622 B.HUERTO DEL SOL -LA PUNTILLA-', 'LUJAN DE CUYO', 'MENDOZA', 'ARGENTINA', '5505', NULL, '6353-01285', '6025668', '0261-155256800......'),
(207, '1745', 'GUILLERMO', 'RIMOLDI - ', '(03489)433-712', '156-107-4284 / 11(15) 61074284', 'guillermo_rimoldi@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '13473442', '0', '1959-12-11', NULL, 'LUIS COSTA 1652', 'CAMPANA', 'BUENOS AIRES', 'ARGENTINA', 'B2804AGV', 'grimoldi@exiros.com', '0', '6028887', '0'),
(208, '1749', 'VICENTE', 'FERNANDEZ - TORRES ', '0', '(0294) 154-69-0968 (Margarita)', 'janosovo@gmail.com', 'MARGARITA DELIA', 'TORRES ', NULL, NULL, '', 1, '7747695', '6187909 ', '1944-07-14', NULL, 'RUIZ MORENO 530 ', 'BARILOCHE', 'RIO NEGRO', 'ARGENTINA', 'R8400HVL', 'marga.torreshalavacs@yahoo.com.ar', '0', '6025882', '(0294) 442-7163 (dp de las 17 hs)'),
(209, '1759', 'MARCOS', 'OTEIZA - ALONSO ', '0', '114-189-4121 (Victoria) // 115-163-4221 (Marcos)', 'valonso6385@gmail.com', 'VICTORIA', 'ALONSO ', NULL, NULL, '', 1, '12892581', '14222665', '1959-01-10', NULL, 'Av. Santa Maria 6385', 'TIGRE', 'BUENOS AIRES', 'ARGENTINA', '0', 'moteiza59@gmail.com', '0', '6028240', '(02396)477-600'),
(210, '1774', 'JULIO ENRIQUE', 'MUTUBERRIA - LANDI ', '(2494)406-504', '(0249)154-535-403 (Julio)', 'cletoju@hotmail.com', 'MARTA SUSANA', 'LANDI ', NULL, NULL, '', 1, '12970598', '0', NULL, NULL, 'Linstow 1138', 'TANDIL', 'BUENOS AIRES', 'ARGENTINA', 'B7000AQY', '0', '0', '6026075', '(0249)442-3533'),
(211, '1776', 'NORMA SUSANA', 'MALVESTITI - CONTI ', '0341-4246798', '0', 'norsumal@yahoo.com.ar', 'JORGE', 'CONTI ', NULL, NULL, '', 1, '11100851', '0', '1954-08-27', NULL, '3 DE FEBRERO 47 PISO 9', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000BKA', 'jorgitoconti@hotmail.com', '0', '6026072', '0341-4828090'),
(212, '1777', 'OSCAR', 'COPPA - HACKERLICH ', '4772-3150/4775-4563 OFSRA', '1553176696 ó 1521858377 ó 4799-2387', 'ericahackerlich@yahoo.com.ar', 'ERICA', 'HACKERLICH ', NULL, NULL, '', 1, '4597745', '10901199', '1943-11-01', NULL, 'SALTA 4085', 'LA LUCILA-OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1637BXQ', 'vcoppa@gmail.com', '0', '6026404', '4766-4541 COPPA *'),
(213, '1781', 'GUSTAVO GABRIEL', 'PEREZ - DEMARCHI ', '02965-458201', '0280-154274943', 'crickas@crickas.com.ar', 'GLADYS BEATRIZ', 'DEMARCHI ', NULL, NULL, '', 1, '14202950', '0', '1963-01-10', NULL, 'LOVE PARRY 396', 'PUERTO MADRYN', 'CHUBUT', 'ARGENTINA', '9120', NULL, '0', '6026403', '42134'),
(214, '1789', 'DOMINGO LORENZO', 'GRANEROS - REALE ', '5197-5248', '156-134-2622 sr   ', 'domingo.graneros@gmail.com', 'ADRIANA', 'REALE ', NULL, NULL, '', 1, '11425770', '12091101', '1954-08-20', NULL, 'RAVIGNANI 2049  6º B', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', 'reale.adriana@gmail.com', '0', '6026634', '0'),
(215, '1790', 'SENAYDA', 'DUTRA - BARILARO ', '03484-430-916', '(03546) 154-64-127 // (03546) 154-15-791 (Roberto) ', 'senydutra@hotmail.com', 'ROBERTO', 'BARILARO ', NULL, NULL, '', 1, '5292296', '8256931', '1946-06-04', NULL, 'Av. Las Magnolias 518, Villa Gral. Belgrano', 'Cordoba', 'Cordoba', 'ARGENTINA', '5194', '0', '0', '6026629', '03546-463679'),
(216, '1795', 'ENRIQUE ALBERTO', 'GAMERRO - DIAZ ', '(03461)461-553', '152-166-8807 (Enrique)', 'eagamerro@hotmail.com', 'MARISA', 'DIAZ ', NULL, NULL, '', 1, '11151986', '0', '1955-02-08', NULL, 'AV.CENTRAL 1580', 'SAN NICOLAS', 'BUENOS AIRES', 'ARGENTINA', 'B2900MAN', 'pilufunes@hotmail.com', '2775-00159', '34635', '03461422229 / 0336 4461553'),
(217, '1800', 'FEDERICO', 'HELMAN - RESCIA ', '0341-4481065', '0341-155994365   ', 'fedehelman@gmail.com', 'MARTA E.', 'RESCIA ', NULL, NULL, '', 1, '10404590', '0', '1956-07-01', NULL, 'BENEGAS 7926', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000JSI', NULL, '6353-00736', '6027814', '(0341)4216593'),
(218, '1805', 'LAURA', 'ROSSI - CARBONATTI ', '4696-7175 (Rossi) // 4623-5525 (Carbonatti)', '155-721-4144 // (02284) 154-71-833 (Rossi)', 'dioclaurossi@gmail.com', 'CLAUDIO RAUL', 'CARBONATTI ', NULL, NULL, '', 1, '20965172', '13235531', NULL, NULL, 'Balcarce 1921', 'MORON', 'BUENOS AIRES', 'ARGENTINA', '1708', 'lauravrossi@gmail.com', 'ID RCI', 'ID INTERVAL', '(02284) 493-002'),
(219, '1818', 'CLAUDIA ELENA', 'QUINTELA - DI PROSPERO', '56-2-242-6635/4865-3676CF', '   ', 'idiprosp@yahoo.com', 'IGNACIO MARTIN', 'DI PROSPERO', NULL, NULL, '', 1, '12960280', '14569393', '1958-09-20', NULL, 'AV. BICENTENARIO 4035 DEPTO 2022', 'VITACURA', 'SANTIAGO DE CHILE', 'CHILE', '0', 'claudiaquintela58@yahoo.com', '0', '6028528', '0'),
(220, '1834', 'HECTOR RAFAEL', 'BAGÜES - ALVEZ ', '0353-4523719', '0', 'hrbagues@hotmail.com', 'HILDA NORMA', 'ALVEZ ', NULL, NULL, '', 1, '13015302', '13015165', '1957-02-28', NULL, 'MANUELA OCAMPO 1235', 'VILLA MARIA', 'CORDOBA', 'ARGENTINA', 'X5900AZC', NULL, '6353-00112', '6022730', '0'),
(221, '1838', 'MARIO ALBERTO', 'ACUÑA - VEGA ', '(0351)4216-304', '(0351) 156-174-779', 'acunaar@yahoo.com', 'GLORIA', 'VEGA ', NULL, NULL, '', 1, '12746443', '17916112', NULL, NULL, 'AV.GRAL PAZ 81 5ºPISO OF.9', 'CORDOBA', 'CORDOBA', 'ARGENTINA', 'X5000JLA', NULL, '6353-00020', '6029351', '(0351)4211-373   '),
(222, '1840', 'RODOLFO AUGUSTO', 'PREZ - BUDIP ', '4864-9965', '15-69353436 SR.   ', 'prezrodolfo@hotmail.com', 'MARTA INES', 'BUDIP ', NULL, NULL, '', 1, '7593041', '6025915', '1947-06-13', NULL, 'AV.CORDOBA 3398 9 \"D\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1187AAV', 'prezrodolfo@hotmail.com', '6353-01124', '6029350', '15-59985518 SRA.'),
(223, '1849', 'JUAN CARLOS', 'SANTANA SANCHEZ - 0', '0381-4351827', '(0381) 156-31-9434 (Juan C.)', 'jcsantanasanchez@yahoo.com.ar', '0', '0', NULL, NULL, '', 1, '12148789', '0', '1958-01-10', NULL, 'BRASIL 775', 'YERBA BUENA', 'TUCUMAN', 'ARGENTINA', 'T4107DIO', '0', '6353-01269', '7322555', '03863-461220 // 0381-4330378 conmix   '),
(224, '1851', 'LAURA', 'MAGRINI - AMEGHINO ', '155-787-0129 (Laura)', '4515-0728 FAX.-   15-5958-7079 SR.', 'juan.ameghino@dialum.com', 'JUAN JOSE', 'AMEGHINO ', NULL, NULL, '', 1, '10965888', '0', '1954-07-05', NULL, 'OLGA COSSETTINI 1350 - 3ºD', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1107', 'lauraameghino@yahoo.com.ar', '6353-00865', '6033567', '0237-4873770'),
(225, '1852', 'MARIA AURORA', 'AMEGHINO - TOLLOCZKO ', '02322-470521', '154-409-2530 (Andrzej) / 154-973-7993 (M. Aurora)', 'atolloczko@yahoo.com', 'ANDRZEJ', 'TOLLOCZKO ', NULL, NULL, '', 1, '5898027', '0', '1948-06-07', NULL, 'pilcahue 21 mapuche country club', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', '1669', 'dodiameghino@hotmail.com', '6353-00063', '6004271', '(03487)429-904'),
(226, '1869', 'CLAUDIO/GRACIELA', 'GARBER - MOSKOVICS ', '4962-1338', '4863-0500   ', 'gramosko@hotmail.com', 'GRACIELA', 'MOSKOVICS ', NULL, NULL, '', 1, '14866671', '18132085', NULL, NULL, 'AV.PUEYRREDON 1234 PISO 12', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1118AAP', 'claudio.garber@gmail.com', '6353-00631', '6030371', '0'),
(227, '1870', 'SONIA CRISTINA', 'RIED - AROCENA ', '02265-432036', '0223-155-203775 cel sonia // 02265-432798 fax   ', 'solvideodigital@hotmail.com', 'AMILCAR  ALIDIO', 'AROCENA ', NULL, NULL, '', 1, '14197792', '13348493', '1960-12-25', NULL, 'ALVEAR 378', 'CORONEL VIDAL', 'BUENOS AIRES', 'ARGENTINA', 'B7174AXH', NULL, '6353-01172', '6030134', '02265-432798 SRA'),
(228, '1878', 'ANA GRACIELA', 'VULCANO - ', '4801-3156', '1164729556 Ricardo', 'vulcanoana90@hotmail.com', '0', NULL, NULL, NULL, '', 1, '6208806', '0', '1950-05-17', NULL, 'AV.LAS HERAS 3115 13  \"49\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425ASI', NULL, '0', '6030992', '0'),
(229, '1904', 'SILVIA C.', 'VALLARINO - ', '4813-9270 / 4811-6683', '4347-3837 TRABAJO   ', 'silviavallarino@hotmail.com', '0', NULL, NULL, NULL, '', 1, '5937533', '0', '1948-11-26', NULL, 'AV. QUINTANA 71 9º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1014ACA', NULL, '3996-05969', '6030890', '4372-8321 FAX'),
(230, '1911', '0', 'CEREAL \"C\" S.A. - POZZI', '0341-471195/96/405022', '0341-4496557   (oficina)', 'info@cerealc.com.ar', 'Sergio', 'POZZI', NULL, NULL, '', 1, '11750370', '0', '1955-03-28', NULL, 'PARAGUAY 727 PISO 7', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000CTU', 'silvia@boeroycia.com.ar', '6353-00604', '6037057', '0341-4405022 (éste)'),
(231, '1913', 'PEDRO', 'PRPIC KRESIMIR - ', '0297-497-4061/(0297)497-4285', '(02974)155-76-726', 'farmasancayetanolh@hotmail.com', 'ANTONIA  (ESPOSA)', NULL, NULL, NULL, '', 1, '18557149', '0', '1946-04-12', NULL, 'PERITO MORENO 742', 'LAS HERAS', 'SANTA CRUZ', 'ARGENTINA', 'Z9017CSU', '0', '0', '37970', '0'),
(232, '1915', 'HERMINIA', 'RISO - RISO ', '4262-5371', '155-578-7806 (Valeria)', 'compras@s3-stranges.com.ar', 'EUGENIO', 'RISO ', NULL, NULL, '', 1, '13577446', '13030737', '1957-05-21', NULL, 'SAN VLADIMIRO 3269', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', 'B1824PCC', 'lpicochet@llyasoc.com.ar; herminiariso@hotmail.com', '0', '6031197', '4249-6907 // 4262-2403 (Ofic)'),
(233, '1987', 'RUBEN', 'CASTILLO GIRAUDO - ', '0', '(0370) 154-41-3376', 'rcgiraudo@hotmail.com', '0', NULL, NULL, NULL, '', 1, '12168306', '0', '1956-05-17', NULL, 'BRANDSEN 636', 'FORMOSA', 'FORMOSA', 'ARGENTINA', 'P3600CXN', NULL, '6353-00357', '1297187', '0'),
(234, '2008', 'MARIA ESTELLA', 'ZABALOY - LONGONI ', '(0291)486-0328', '(0291)154-180-969 (Estela)', 'zabaloym@gmail.com', 'JORGE', 'LONGONI ', NULL, NULL, '', 1, '12740505', '0', '1958-09-27', NULL, 'MARTIN MALHARRO 360', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000GRR', NULL, '6353-00165', '1', '4814-4929 AL 32 SR.'),
(235, '2012', 'GLADYS RAFAELA', 'FARIAS - PERALTA MARTINEZ', '(02320) 413-981', '116-372-9166', 'glafar59@hotmail.com', 'RICARDO PABLO', 'PERALTA MARTINEZ', NULL, NULL, '', 1, '12639815', '13277353', '1959-02-08', NULL, 'SANTOS VEGA 615', 'GRAND BOURG', 'BUENOS AIRES', 'ARGENTINA', 'B1615KVM', NULL, '0', '1504929', '0'),
(236, '2033', 'JORGE/MONICA', 'DELUCCHI (fallecio) - PISGLIAR ', '(0221) 471-0012', '(0221) 155-48-7430 Monica', 'monapisgliar@gmail.com', 'MONICA ALICIA', 'PISGLIAR ', NULL, NULL, '', 1, '12735631', '0', '1958-11-19', NULL, 'CALLE 18 Nº 3096 GONNET', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1897BLT', '0', '6353-00483', '1601715', '0'),
(237, '2036', 'GUSTAVO', 'SANCHEZ - ARRUIZ ', '0223-4931633/4946600 FAX', '223-6020386 gust / (0223) 155-04-0007 (Agustín)', 'estebansa89@hotmail.com', 'VIVIAN', 'ARRUIZ ', NULL, NULL, '', 1, '14318524', '14630169', '1961-04-10', NULL, 'TUCUMAN 3929', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7602CMW', 'agustin_sa@hotmail.com', '6353-01257', '1597026', '0223-4919990 CONSULT'),
(238, '2051', 'DORA LILIANA', 'HERNANDEZ - ', '03487-423900', '03487-423514 MAMA   ', 'dlhernandez@fibertel.com.ar', '0', NULL, NULL, NULL, '', 1, '16092114', '0', '1962-07-09', NULL, 'INDEPENDENCIA 1079', 'ZARATE', 'BUENOS AIRES', 'ARGENTINA', 'B2800HKB', NULL, '3781-01236', '1568511', '0'),
(239, '2054', 'JUAN CARLOS', 'RODRIGUEZ - BAUZADA ', '0223-4826551/4944578', '2235565230', 'jrodriguezrb@speedy.com.ar', 'SUSANA E.', 'BAUZADA ', NULL, NULL, '', 1, '12516497', '12729559', '1958-12-01', NULL, 'MATEOTTI 1979 PLANTA ALTA', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7605ATB', NULL, '3616-1817', '1540007', '0223-4944578'),
(240, '2060', 'HUGO', 'CARRARA - 0', '0341-4248512', '0341-4113002 - LOCUT   ', 'hdcarrara@hotmail.com', '0', '0', NULL, NULL, '', 1, '8112088', '0', '1950-06-09', NULL, 'PASAJE SANTA CRUZ 318 \"A\"', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000BGB', NULL, '6353-00336', '1', '0341-4499197'),
(241, '2070', 'MARCELO', 'CARDACCI - ONEGA ', '02257-429395', '(02257) 155-38-713', 'miriamonega@gmail.com', 'MIRIAM', 'ONEGA ', NULL, NULL, '', 1, '18290669', '17506923', '1967-09-11', NULL, 'LIBRES DEL SUR 135', 'MAR DE AJO', 'BUENOS AIRES', 'ARGENTINA', '7109', NULL, '0', '1437044', '0'),
(242, '2076', 'WALTER', 'CANIGLIA - LAVERGNE', '4683-0535', '1559948284', 'walter.caniglia@dayton.com.ar', '0', 'LAVERGNE', NULL, NULL, '', 1, '14873420', '0', '1962-03-25', NULL, 'SAN PEDRO 4852 PB 4', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1407HNZ', NULL, '6353-00303', '1591808', '4411-9646 mvl'),
(243, '2078', 'NELSA LUISA', 'FORESTELLO - ', '4826-1518', '11-68182541', 'wbaudonnet@hotmail.com', '0', NULL, NULL, NULL, '', 1, '0', '0', '1935-04-11', NULL, 'LAPRIDA 1385 15º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425EKG', NULL, '0', '1550811', '4822-8080 (walter hijo)'),
(244, '2087', 'HECTOR ORLANDO', 'ROMANIELLO - BAINOTTI ', '(0291)451-6747 (Claudia)', '(0291)154-27-2856 (Claudia)', 'hcroma@fibertel.com.ar', 'CLAUDIA', 'BAINOTTI ', NULL, NULL, '', 1, '14935407', '16462978', '1962-08-01', NULL, 'BERUTTI 378', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000BHX', NULL, '6353-00272', '1591800', '(0291)453-1553 (Graciela)'),
(245, '2088', 'GUSTAVO H.', 'KRALJ - SANCHEZ ', '(02252)525-646', '(02252)154-17-960  (Gustavo)', 'gustavokralj@gmail.com', 'CLAUDIA', 'SANCHEZ ', NULL, NULL, '', 1, '16767194', '0', '1964-01-13', NULL, 'CALLE 21 ESQ.24', 'S. CLEMENTE DEL TUYU', 'BUENOS AIRES', 'ARGENTINA', '7105', NULL, '6353-00789', '1601716', '0'),
(246, '2095', 'ORLANDO', 'ARGAÑARAZ - CASINGHINO ', '4901-6366', '0', 'mcchary@yahoo.com.ar', 'MIRTA', 'CASINGHINO ', NULL, NULL, '', 1, '7591136', '0', '1947-01-01', NULL, 'BEAUCHEFF 368 DEPTO. 7', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1424BDH', NULL, '6353-01453', '1579743', '0'),
(247, '2110', 'SILVIO', 'DE MIGUEL - ANDRADE ', '0221-4276089', '0221-4217535   ', 'nanas1996@hotmail.com', 'ANA MARIA', 'ANDRADE ', NULL, NULL, '', 1, '25415088', '12631119', '1976-07-05', NULL, 'CALLE 71 Nº 428 DEPTO. 3', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', NULL, '0', '1720419', '0221-4826130'),
(248, '2123', 'CESAR RAUL', 'ALEMAN CHAVEZ - HUENCHO OLIVARES', '5294-8814', '153-140-8810 (César) ', 'cesaralemanchavez@yahoo.com', 'JESSICA', 'HUENCHO OLIVARES', NULL, NULL, '', 1, '18754384', '92857638', NULL, NULL, 'SARGENTO GOMEZ 1526', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', 'B1686END', '0', '6353-00035', '1', '4669-3390/4214-1414'),
(249, '2124', 'Vanesa Carola', 'CAPELLINI - 0', '4582-3026 Vanesa', '1157206507 (Vanesa)', 'vanesacapellini@hotmail.com', '0', '0', NULL, NULL, '', 1, '25705059', '0', '1976-12-25', NULL, 'SAN BLAS 3142', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1416BCS', 'vanesacapellini@yahoo.com.ar; vanesacapellini@gmail.com', '6353-01169', '1670260', '(0220) 482-4309'),
(250, '3002', 'ALEJANDRO', 'JENIK - ', 'FAX 4762-1462', '155-181-4324   ', 'ale_jenik@hotmail.com', '0', NULL, NULL, NULL, '', 1, '17762453', '0', '1965-09-26', NULL, 'BELGRANO 2325', 'MUNRO', 'BUENOS AIRES', 'ARGENTINA', 'B1605CGA', 'ale_jenik@hotmail.com', 'ID RCI', 'ID INTERVAL', '15-51752864'),
(251, '5002', 'PAOLA SANDRA', 'DI GIORGIO - VALIÑO ', '4772-8992', '005982-7090368 URUGUAY   ', 'paodigiorgio@gmail.com', 'NORMA', 'VALIÑO ', NULL, NULL, '', 1, '18181975', '0', '1965-11-27', NULL, 'MIGUELETES 992', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426BUL', NULL, '6353-00497', '6025873', '0059899-919-753.CEL.'),
(252, '5003', 'HIGINIO', 'DUARTE - ', '4628-0238', '   ', 'elbydu@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '5446538', '0', '1931-10-19', NULL, 'CHIVILCOY 1481', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', 'B1712IYK', 'www.elbydu.com.ar', '0', '6025878', '0'),
(253, '5004', 'JUAN CARLOS', 'FALCONI - ', '4743-3488', '153-561-5506', 'jotafalconi@hotmail.com', '0', NULL, NULL, NULL, '', 1, '8269372', '0', '1947-12-27', NULL, 'JUAN CRUZ VARELA 933', 'SAN ISIDRO', 'BUENOS AIRES', 'ARGENTINA', 'B1642GUE', 'margaritafalconi@gmail.com.ar', '6353-00565', '6025877', '4736-7433/7021'),
(254, '5011', 'JOSE ALBERTO', 'FLORES - BRUZZONI ', '4961-2703 ', '   ', 'jflores_44@hotmail.com', 'MARTHA', 'BRUZZONI ', NULL, NULL, '', 1, '7939486', '0', '1944-05-15', NULL, 'BOULOGNE SUR MER 960 3º\"C\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1213AAP', 'jflores_44@hotmail.com', '6353-00601', 'ID INTERVAL', '35302783'),
(255, '5018', 'PEDRO', 'BRAMATI, PEDRO - ROSARIGO , ZULEMA', '0280 4436508', '0280 154352968 Beatriz', 'brosanigo@yahoo.com.ar', 'ZULEMA', 'ROSARIGO ', NULL, NULL, '', 1, '8279642', '0', '1951-01-09', NULL, 'CHACABUCO 906', 'TRELEW', 'CHUBUT', 'ARGENTINA', '9100', 'pedrobramati@speedy.com.ar', '6353-00256', '6029356', '0280 154664162 Pedro'),
(256, '5026', 'MARCELO', 'PAGLIUCA - BENETTI ', '4753-3974/4724-2103', '15-5661-2673', 'marcelopagliuca@hotmail.com', 'ADRIANA', 'BENETTI ', NULL, NULL, '', 1, '13062273', '0', '1959-04-18', NULL, 'CALLE 41 EX MARTINEZ 2162', 'V.MAIPU-SAN MARTIN', 'BUENOS AIRES', 'ARGENTINA', '1650', NULL, '0', '37468', '4755-0594 FAX.'),
(257, '5033', 'JORGE', 'DOUGLAS PRICE - MURIAS ', '0298-4452461', '298415611423', 'jorgedouglas952@gmail.com', 'VERONICA', 'MURIAS ', NULL, NULL, '', 1, '10424261', '0', '1952-01-31', NULL, 'LEANDRO ALEM 291', 'ALLEN', 'RIO NEGRO', 'ARGENTINA', 'R8328DHE', '0', '0', '6030986', '02941-451000'),
(258, '5049', 'JOSE ADOLFO', 'MATZKIN - ', '02922-466118/462718', '(0291)155-70-3688   ', 'matzkinsemillas@solnetisp.com', '0', NULL, NULL, NULL, '', 1, '8364437', '0', NULL, NULL, 'COLON 940', 'CORONEL PRINGLES', 'BUENOS AIRES', 'ARGENTINA', 'B7530AIP', 'matzkinsemillas@solnetisp.com.ar', 'ID RCI', 'ID INTERVAL', '02922-463527/4326fax'),
(259, '5053', 'CARLOS ANDRES', 'ROBLES MEDINA - PEREZ ', '4541-9502', '156-204-1792 (Carlos)', 'carlosarmedina@hotmail.com', 'VIVIANA ALICIA', 'PEREZ ', NULL, NULL, '', 1, '11835011', '11959157', NULL, NULL, 'RUIZ HUIDOBRO 3737, 16º\"G\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1430', 'andresrmedina@yahoo.com.ar', '5905-00105', '6002655', '4666-4895 int.6900/6800'),
(260, '6016', 'ELIZABETH', 'SACO - CALCULLI ', '4713-2739', '154-438-5986 (Claudio)   ', 'claudio.calculli60@gmail.com', 'CLAUDIO', 'CALCULLI ', NULL, NULL, '', 1, '14614984', '14026587', '1961-03-25', NULL, 'PASAJE TOKIO 2049', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1407DTA', 'elizabeth.saco@gmail.com', '0', '1', '4488-0237'),
(261, '6051', 'JUAN JOSE', 'SIRI - GHIANO', '(03401) 420-886 ', '  ', 'ingsiri@ingsiri.com.ar', 'LIANA DEL PILAR', 'GHIANO', NULL, NULL, '', 1, '11520273', '13936268', '1956-02-07', NULL, 'RUTA 13 Y BIANCHI Nº 2535', 'EL TREBOL', 'SANTA FE', 'ARGENTINA', 'S2535DSJ', NULL, '6353-01310', '6033905', '(03401) 423-077 // (03401)421-260 (Laboral) '),
(262, '6060', 'MONICA MARTA', 'LILLO TRONCOSO - GHISOLFO ', '4823-5098/4821-3167', '155-713-4355 SR   154-472-7738', 'rghisolfo06@yahoo.com.ar', 'RUBEN', 'GHISOLFO ', NULL, NULL, '', 1, '18730558', '10756019', '1957-01-23', NULL, 'ANCHORENA 1352  3º\"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425ELF', NULL, '6353-00826', '41979', '15-5400-7738'),
(263, '6061', 'TARCISIO ANTONIO', 'SANTORO - 0', '55-11-4581 6492', '55 11 99743491   ', 'tarcisio.santoro@gmail.com', '0', '0', NULL, NULL, '', 1, '5887666', '0', '1956-06-07', NULL, 'RUA PROFESSOR EMILIO MAZZOLA Nº 403', 'JARDIM SAMAMBAIA', 'CEP13211-690 JUNDIAI', 'BRASIL', '0', NULL, '6353-01271', '6037129', '55-11-99005527'),
(264, '6076', 'CELSO', 'MERA NUÑEZ - GARRO ', '0237-4627072', '155-014-8437 (Celso)', 'celso553@hotmail.com', 'ADRIANA', 'GARRO ', NULL, NULL, '', 1, '93520635', '0', '1960-12-05', NULL, 'AZCUENAGA 2578', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', 'B1744IPP', 'garro_6@hotmail.com', '6353-00935', '6035406', '0237-4627072 FAX'),
(265, '6078', 'FLAVIO HUGO', 'PEVERELLI - ', '4502-0719/4502-2075', '154-992-4435 // 156-398-7161 (Mariela)', 'hugo.peverelli@selectarcarg.com.ar', '0', NULL, NULL, NULL, '', 1, '10383008', '0', '1952-06-20', NULL, 'NUEVA YORK 3851', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419HDK', 'soldacentro@selectarcarg.com.ar', '0', '1', '4754-0353/0349'),
(266, '6081', 'NORMA MABEL', 'ALONSO DE CERDA - CERDA ', '4794-9057', '113-572-7149', 'cerdagustavo@gmail.com', 'GUSTAVO', 'CERDA ', NULL, NULL, '', 1, '14519629', '0', '1961-05-08', NULL, 'AVELLANEDA 3473', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636ATA', '0', '6353-00050', '6035075', '4799-7354'),
(267, '6087', 'MONICA LETICIA', 'GIUSTI - ', '(0223)479-6143 ', '2234216541', 'monizoo.mg@gmail.com', '0', NULL, NULL, NULL, '', 1, '10606807', '0', '1952-07-31', NULL, 'PUJIA 3465', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7605LEK', 'minizoo@copetel.com.ar', '6353-00673', '1', '0'),
(268, '6090', 'FRANCO', 'ARMELLIN - ', '4781-4774', '155-327-5663   ', 'farmellin@fibertel.com.ar', '0', NULL, NULL, NULL, '', 1, '12045460', '0', '1958-03-31', NULL, '3 de FEBRERO   1230 4ºA', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1426BJL', NULL, '6353-00087', '1', '0'),
(269, '6101', 'FLAVIA', 'AMUSATEGUI - ALLIGNANI ', '(0299)443-9119', '(0299)154-050-144 (Allignani)', 'famusategui@hotmail.com', 'EDGARDO', 'ALLIGNANI ', NULL, NULL, '', 1, '17641048', '0', '1965-06-09', NULL, 'ANTU RUCA 2406 ESQ. ALVAREZ', 'NEUQUEN', 'NEUQUEN', 'ARGENTINA', '8300', 'gilbertoallignani@gmail.com', '6353-00066', '1492489', '(0299)443-5348'),
(270, '6120', 'JOSE GUSTAVO', 'GUARIDO - MURRAY ', '(02302)426-796', '421952(COLEGIO EQUIV   ', 'gusguari@hotmail.com', 'SANDRA', 'MURRAY ', NULL, NULL, '', 1, '14554465', '14554472', '1961-11-18', NULL, 'CALLE 18 Nº 303', 'GENERAL PICO', 'LA PAMPA', 'ARGENTINA', 'L6360DQG', 'sandramurray08@gmail.com', '0', '1495248', '(02302)433-973'),
(271, '6123', 'SORAYA SANDRA', 'MUALEM - MONTERO ', '0341-4402759', '(0341) 153-27-8118   ', 'monteronoelia@hotmail.com', 'NOELIA E.', 'MONTERO ', NULL, NULL, '', 1, '13145072', '32166132', '1959-04-04', NULL, 'LAPRIDA 836  4º\"A\"', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000CFH', NULL, '6353-00963', '1461557', '0'),
(272, '6132', 'RICARDO JOSE', 'PUCHETA BONAPARTE - WEBER ', '0351-4224124', '0351-155177711', 'bpucheta@hotmail.com', 'BARBARA', 'WEBER ', NULL, NULL, '', 1, '7971613', '0', '1943-04-15', NULL, 'MONTEVIDEO 634', 'CORDOBA', 'CORDOBA', 'ARGENTINA', 'X5000AXH', 'ricardopucheta@estpuchetabonaparte.arnetbiz.com.ar', '0', '1544557', '0543-20086'),
(273, '6133', 'NESTOR HORACIO', 'GURIANI - ', '4567-1031 MAMA', '0', 'nestorguriani@hotmail.com', '0', NULL, NULL, NULL, '', 1, '13132589', '0', '1959-07-29', NULL, 'LOPE DE VEGA 1620 2º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1407BOP', 'nestorguriani@hotmail.com', 'ID RCI', 'ID INTERVAL', '4521-0134   '),
(274, '6137', 'MARCELA MARIA DEL CARMEN', 'MANCINI - FLAMINIO ', '4571-7673', '152-862-9935 (Marcela)', 'marmancini64@gmail.com', 'LEONARDO', 'FLAMINIO ', NULL, NULL, '', 1, '16938286', '16763970', NULL, NULL, 'GUTENBERG 2919', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419IDG', 'marmancini64@gmail.comm', '6353-00871', '1536766', '4342-2822 (ESTUDIO)'),
(275, '6148', 'GUSTAVO', 'ANSCHUTZ - PANDOLFI ', '4771-2116', '155-002-1288 (Gustavo) / 153-066-2500 (Susana)', 'gjea@2vias.com.ar', 'SUSANA BEATRIZ', 'PANDOLFI ', NULL, NULL, '', 1, '8275745', '11022622', '1950-02-09', NULL, 'RAVIGNANI 2314', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425FYF', 'kleopandolfi@yahoo.com.ar', '0', '1474815', '15-4165-8592/ EL'),
(276, '6173', 'OSMAR ABEL', 'GOMES - ARCHET ', '0034922-619-098', '115-308-9880', 'hector.gomes@icloud.com', 'M. VICTORIA', 'ARCHET ', NULL, NULL, '', 1, '16870378', '0', '1963-10-24', NULL, 'Calle de los Sueños 66, piso 3, dpto.6', 'Sta. Cruz de Tenerif', 'STA CRUZ DE TENERIFE', 'ESPAÑA', '38108', 'administracion@centrodelmarmol.com.ar', '0', '33958', '4661-0857'),
(277, '6176', 'NORMA MABEL', 'NIEVAS, NORMA MABEL - BASINI , RODOLFO DANIEL', '/FAX4665-7555', '4346-7300 INT.7215..   ', 'nievasn@hotmail.com', 'RODOLFO DANIEL', 'BASINI ', NULL, NULL, '', 1, '10850683', '10248622', NULL, NULL, 'PEDRO DE MENDOZA 669', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', 'B1686NFE', 'Rbasini@argentina.com', '0', '1533403', '0'),
(278, '6186', 'CARINA MARCELA', 'RUSSO - STELLA ', '4464-3629', '4654-6398(MAMA) 154-471-0755', 'arielstella73@hotmail.com', 'ARIEL ENRIQUE', 'STELLA ', NULL, NULL, '', 1, '18537502', '0', '1967-10-10', NULL, 'CATRIEL 587', 'V.SARMIENTO-HAEDO', 'BUENOS AIRES', 'ARGENTINA', '-1706', NULL, '6353-01236', '1504926', '4658-3020'),
(279, '6190', 'DANIEL EMILIO', 'ROJO - ', '(0223) 451-6014 // (0223) 492-0911', '(0223) 154-21-7707   ', 'danielemiliorojo@gmail.com', '0', NULL, NULL, NULL, '', 1, '12200421', '0', '1958-07-15', NULL, 'ALVARADO 1048', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7602HFV', 'derojo@afip.gob.ar', '6353-01207', '1512144', '(0223) 489-6718 Fax'),
(280, '6194', 'ALFONSO', 'MENDEZ - MENDEZ ', '4803-8608', '155-011-7284   ', 'mediadorprejudicial@gmail.com', 'INES', 'MENDEZ ', NULL, NULL, '', 1, '14222780', '17482650', '1961-08-03', NULL, 'CABELLO 3961 8º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '0', 'mediadorprejudicial@gmail.com', '0', '1', '4311-8311'),
(281, '6196', 'LEOPOLDO', 'LARIGUET - CONCHEZ ', '02302-15602439', '02302-15602440/481663 hijo IVAN    ', 'nacholariguet@gmail.com', 'ANA MARIA', 'CONCHEZ ', NULL, NULL, '', 1, '4382236', '6631336', '1941-08-30', NULL, 'PARANA 1243, 7º \"B\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1018ADE', 'ilariguet@yahoo.com.ar', '6353-00812', '1', '02302-482231 FAX'),
(282, '6202', 'ROSA ISABEL', 'DE MARCO - 0', '4802-9400', '153-838-4848', 'demarcoisabel@gmail.com', '0', '0', NULL, NULL, '', 1, '11958853', '0', '1958-03-16', NULL, 'CASTEX 3575 PISO 3° A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1018AAO', 'contacto@isabeldemarco.com.ar', '0', '1643895', '4-780-0613'),
(283, '6208', 'LUIS', 'REBOLLO - CLAVERIE ', '(02335)472-089 // 4783-6910 Mamá', ' (02302)156-00-796 // (02302) 154-61-176 (Bibiana)', 'bibiclaverie@hotmail.com', 'BIBIANA', 'CLAVERIE ', NULL, NULL, '', 1, '14499561', '14405905', '1961-12-31', NULL, 'CORRIENTES 654', 'ING.LUIGGI', 'LA PAMPA', 'ARGENTINA', 'L6205ALL', 'bibiclaverie@hotmail.com', '6353-00117', '1544556', 'FAX 471548/472164'),
(284, '6210', 'HUGO', 'DE PALMA - BEAUCAMP ', '02281-433569', '0', 'cenefa@speedy.com.ar', 'MARIA CRISTINA', 'BEAUCAMP ', NULL, NULL, '', 1, '10400433', '6519151', '1952-08-08', NULL, 'CANEVA 630', 'AZUL', 'BUENOS AIRES', 'ARGENTINA', 'B7300FJN', 'hugonefroazul@hotmail.com', '0', '6006705', '(02281)433-204 / (02281) 426-941'),
(285, '6211', 'BEATRIZ  LAURA', 'MARTINEZ - TABLADO ', '4798-3955 (Trabajo Beatriz)', '1540557702', 'ceb_lab@hotmail.com', 'ALEJANDRO', 'TABLADO ', NULL, NULL, '', 1, '12543519', '0', '1956-10-25', NULL, 'POSADAS 145', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', 'B1643FTC', NULL, '0', '1526818', '4798-9015 (FAX).....'),
(286, '6212', 'MIGUEL ALEJANDRO', 'VASSALLO - FRIAS ', '(0351)484-5243 PARTICULAR', '(0351)156-172-368', 'vassallosrl@gmail.com', 'NORA', 'FRIAS ', NULL, NULL, '', 1, '16565842', '16565842', '1964-02-26', NULL, 'PUESTO DEL MARQUES 5636       Bº QUEBRADA DE LAS ROSAS', 'CORDOBA', 'CORDOBA', 'ARGENTINA', '5003', NULL, '6353-00119', '1536768', '(0351)461-6757'),
(287, '7028', 'ALICIA GRACIELA', 'DIAZ - COHEN ', '4687-4516 // 5778-0094 (Alicia)', '154-492-1616 (Raúl) // 11-66775261 (Alicia)', 'diazalicia@yahoo.com', 'RAUL NORBERTO', 'COHEN ', NULL, NULL, '', 1, '17635449', '11734237', '1966-01-14', NULL, 'AV. EMILIO CASTRO  6598', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1408IGP', '0', '6353-00503', '1', '4331-4489 A.D.O'),
(288, '7029', 'MARIA JOSEFA', 'CAIA - PASQUADIBISCEGLIE ', '4361-2781', '155-691-1523', 'anabellapasqua@gmail.com', 'OSCAR', 'PASQUADIBISCEGLIE ', NULL, NULL, '', 1, '10373875', '0', '1952-01-16', NULL, 'ISABEL LA CATOLICA 136 - 3º B', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1292AAE', '0', '6353-00290', '6025724', '0'),
(289, '7047', 'CLAUDIA JULIA', 'ALVAREZ - PEREZ BREA', '4629-2475', '154-475-5303 (Guillermo) // 154-446-2369/4488', 'gpbrea@speedy.com.ar', 'GUILLERMO', 'PEREZ BREA', NULL, NULL, '', 1, '13022925', '0', '1957-06-24', NULL, 'AVELLANEDA 1278  1º\"2\"', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', NULL, '6353-00057', '1054433', '4628-7656'),
(290, '7048', 'ANDREA', 'PELLIZER - MASTACHE ', '4621-7111', '154-412-5528 SR.MASTACHE   ', 'agustinmastache@gmail.com', ' ALBERTO', 'MASTACHE ', NULL, NULL, '', 1, '14078217', '0', '1960-03-29', NULL, 'AV.PTE PERON 10298 UNIDAD 184', 'ITUZAINGO', 'BUENOS AIRES', 'ARGENTINA', '1714', 'albmastache@gmail.com', '0', '40642', '155-661-0836'),
(291, '7062', 'RAFAEL', 'MADERO - BROWN ', '4719-5401', '15-4998-4865', 'rafaelmadero1@gmail.com', 'PATRICIA', 'BROWN ', NULL, NULL, '', 1, '12543444', '13753801', '1958-07-16', NULL, 'USPALLATA 2553', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', 'B1643AOA', NULL, '0', '6037338', '4747-7656'),
(292, '7063', 'VERONICA ALEJANDRA', 'SOMOZA - HERRERIA ', '4331-8555', '1544960647   ', 'fernando.herreria@aylingseguros.com.ar', 'FERNANDO', 'HERRERIA ', NULL, NULL, '', 1, '17006857', '16916711', '1964-10-31', NULL, 'TACUARI 32 PISO 10', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1071', '0', '6353-01316', '1454193', '4343-1157/4342-0893'),
(293, '7084', 'ALMA CECILIA', 'GUERRERO - BOCAR ', '4252-5177', '156-626-0866 (Alma)', 'almascribbles@hotmail.com', 'CARLOS', 'BOCAR ', NULL, NULL, '', 1, '14583228', '0', '1961-07-11', NULL, 'COMODORO RIVADAVIA 557', 'BERNAL OESTE', 'BUENOS AIRES', 'ARGENTINA', '1876', NULL, '6353-00717', '6038419', '0'),
(294, '7092', 'SILVANA', 'CIVIERO - OTAMENDI ', '0', '02284-15539486 OTAMENDI', 'eotamendi@hotmail.es', 'HECTOR', 'OTAMENDI ', NULL, NULL, '', 1, '13026848', '17429237', '1957-04-28', NULL, 'CERRITO 2395', 'OLAVARRIA', 'BUENOS AIRES', 'ARGENTINA', 'B7400CBA', 'silvanaciviero@hotmail.com', '6353-00393', '1', '02284-444333 of'),
(295, '10004', 'PATRICIA BEATRIZ', 'CRIVELLI - FERRARO ', '4744-7910 Casa', '155262-9319 Fernando   ', 'fernando_ferraro@hotmail.com', 'FERNANDO CARLOS', 'FERRARO ', NULL, NULL, '', 1, '13011126', '12543921', '1958-04-02', NULL, 'ALTE. BROWN  858', 'SAN FERNANDO', 'BUENOS AIRES', 'ARGENTINA', 'B1646HLD', NULL, '6353-00003', '6025340', '155261-5357 Patricia');
INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(296, '10005', 'VIVIANA', 'STECHINA - FERNANDEZ ', '03482-480817', '03482-15508720 Viviana', 'viviferni@hotmail.com', 'DANIEL', 'FERNANDEZ ', NULL, NULL, '', 1, '20717009', '17559263', '1968-12-28', NULL, 'CALLE 11 Nº1181', 'AVELLANEDA', 'SANTA FE', 'ARGENTINA', 'S3561BDL', NULL, '0', '1670226', '0'),
(297, '10028', 'RICARDO', 'PETROVICH - PETROVICH ', '0221-4645390', '(0221) 155-996-529 (Juan)', 'ricardopetrovich@hotmail.com', 'JUAN CARLOS', 'PETROVICH ', NULL, NULL, '', 1, '17480006', '21616346', NULL, NULL, 'Calle 25 Nº 2528 Entre 505 y 508', 'Hernández, La Plata', 'BUENOS AIRES', 'ARGENTINA', '1903', 'juan_petrovich@yahoo.com.ar', '6353-01099', '6031773', '0221-4296808 JUAN'),
(298, '10035', 'LILIANA MARGARITA', 'ROMPANI - MAINERI ', '4744-1353 ', '   ', 'lilianamaineri@hotmail.com', 'Juan Carlos', 'MAINERI ', NULL, NULL, '', 1, '16057118', '0', '1962-06-02', NULL, 'LUIS PASTEUR 483', 'VICTORIA', 'BUENOS AIRES', 'ARGENTINA', '1644', NULL, '6353-01216', '1657697', '4744-7286 (mama)'),
(299, '10042', 'MARCELO MARCOS FABIAN', 'HENCEK - ', '(02966)426-010', '(02966)431-983/970 - (02966)156-21-619', 'emanuelcalzados@hotmail.com', '0', NULL, NULL, NULL, '', 1, '16029939', '0', '1962-07-16', NULL, 'MAIPU 265', 'RIO GALLEGOS', 'SANTA CRUZ', 'ARGENTINA', 'Z9400BFE', '(02966)428-930 / 9.30 a 12 ', '6353-00737', '1681029', '(02966)432-997'),
(300, '10044', 'MARIA DEL ROSARIO EMILIA', 'ALONSO - ALBONICO ', '4553 2739', '155-101-4431 (Hugo)', 'hcalbonico@fibertel.com.ar', 'HUGO', 'ALBONICO ', NULL, NULL, '', 1, '6282951', '0', '1950-01-20', NULL, 'CIUDAD DE LA PAZ 550 7º\"A\"', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', 'C1426AGJ', '0', 'ID RCI', 'ID INTERVAL', '4553-2739'),
(301, '10047', 'CARLOS ADRIAN', 'SANCHEZ - MELCHIORI ', '0220-4857219', '155-037-9033', 'flaviamelchiori@yahoo.com.ar', 'FLAVIA', 'MELCHIORI ', NULL, NULL, '', 1, '23328407', '0', '1973-07-19', NULL, 'AV.ARGENTINA 876', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1722', 'neoacuario1964@yahoo.com.ar', '0', '1725674', '4546-9268......'),
(302, '10050', 'CARLOS URIEL', 'MORENO - DI IORIO', '4663-5661', '15-5657-4448   ', 'luzmariadiiorio@gmail.com', 'LUZ MARIA', 'DI IORIO', NULL, NULL, '', 1, '25226117', '6027484', NULL, NULL, 'UNIVERSIDAD DE BS.AS 2522', 'VILLA DE MAYO', 'BUENOS AIRES', 'ARGENTINA', '1410', 'joosmore@yahoo.com.ar', '0', '6023152', '156-740-5679 Luz'),
(303, '10057', 'GRACIELA', 'RODRIGUEZ - GRIS ', '03476-422958 trabajo ella', '341155026088', 'tayrona52@hotmail.com', 'ANGEL', 'GRIS ', NULL, NULL, '', 1, '6421443', '0', '1951-06-27', NULL, 'MITRE 248 8º', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000CDO', 'tayrona52@hotmail.com', '6353-01199', '1654840', '(0341)447-0561'),
(304, '10058', 'DANIELA BEATRIZ', 'BERNAUD - CEBALLOS ', '0', '156-941-0576 (Daniela) // 152-501-9000 (Alejandro)', 'atoceballos@gmail.com', 'ALEJANDRO', 'CEBALLOS ', NULL, NULL, '', 1, '20464327', '0', '1968-08-27', NULL, 'Bulnes 1787 8ºA', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', 'danibernaud@gmail.com', '6353-00209', '1626483', '4826-1732'),
(305, '10061', 'LUIS MARIA', 'RAZQUIN - CASTILLA ', '02357-493023', '02396-15621211   ', 'maritacastilla@hotmail.com', 'MARIA DE LOS ANGELES', 'CASTILLA ', NULL, NULL, '', 1, '17296629', '17454950', '1965-05-30', NULL, 'SALTA S/N', 'CURARU-PDO.C.TEJEDOR', 'BUENOS AIRES', 'ARGENTINA', 'B6451XAD', 'maritacastilla@hotmail.com', '6353-01154', '1619459', '0'),
(306, '10073', 'RODOLFO ADALBERTO', 'OLLE - ANOLLES ', '4696-7585/4658-2205', '   ', 'gilgao@hotmail.com', 'ELENA MANUELA', 'ANOLLES ', NULL, NULL, '', 1, '17198091', '6362231', NULL, NULL, 'HUMAHUACA 2249', 'MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1708HSU', 'pau_lilla84@hotmail.com', '6353-01026', '1837601', '0'),
(307, '10074', 'SILVINA M. DE LOS ANGELES', 'MARQUES - DE VINCENZO', '4242-1303 (Sil) ', '155-706-1790 (Antonio) // 156-376-7513 (Silvina) (Aparentemente es Nº equivocado)', 'antoniodevincenzo@yahoo.com.ar', 'ANTONIO', 'DE VINCENZO', NULL, NULL, '', 1, '20060812', '0', '1968-03-06', NULL, 'FRENCH 655 e/ San Martín y Alvear CP: 1828 (Silvina)', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', 'B1828HKO', 'silvimarques@hotmail.com // coprogresar@yahoo.com.ar', '6353-00892', '1640919', '0'),
(308, '10079', 'SUSANA', 'RIVERO - RODRIGUEZ ', '(0299)496-3405', '(0299)154-56-9437   ', 'albertango53@hotmail.com', 'ALBERTO', 'RODRIGUEZ ', NULL, NULL, '', 1, '16309640', '10891031', '1963-10-18', NULL, 'BARRIO SUYAI ACC.4 PB \"C\"', 'PLAZA HUINCUL', 'NEUQUEN', 'ARGENTINA', 'Q8318GGR', 'amadaesposa63@hotmail.com', '0', '1831699', '(0299)154-66-2408'),
(309, '10080', 'ROSA ESTER', 'WALTHER - ', '(0230)448-6452', '1557492385 (Rosa)', 'rosaesterwalther@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '12900041', '0', NULL, NULL, 'SAN MARTIN 1466', 'PRESIDENTE DERQUI', 'BUENOS AIRES', 'ARGENTINA', 'B1635AHO', '0', '0', '1785146', '0'),
(310, '10081', 'NOEMI NORMA', 'BRACAMONTE - WASMUTH ', '4460-1888', '15-6117-1888   ', 'neoacuario1964@yahoo.com.ar', 'JUAN CARLOS', 'WASMUTH ', NULL, NULL, '', 1, '12717982', '4145586', '1964-01-24', NULL, 'PEDRO CHUTRO 499', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '6353-00254', '1707563', '0'),
(311, '10092', 'MIRTA RAQUEL', 'BENZ - MARTINEZ ', '(02284)440-717', '(02284)156-72-725 (Mirta) // (02284)155-36-606', 'mirtarbenz@hotmail.com', 'EMILIO GERMAN', 'MARTINEZ ', NULL, NULL, '', 1, '14906153', '14501234', '1962-05-27', NULL, 'SAENZ PEÑA 3624', 'OLAVARRIA', 'BUENOS AIRES', 'ARGENTINA', 'B7400LUN', NULL, '6353-00201', '1768536', '02284-442796'),
(312, '10093', 'ALBA MARIANA', 'SUAREZ - BARCELO ', '4393-5900 ', '155-810-4910 (Alba) // 156-494-1252 (Javier)', 'albasuarez@live.com.ar', 'JAVIER ALBERTO', 'BARCELO ', NULL, NULL, '', 1, '18220759', '17743684', '1967-05-13', NULL, 'MONTEVIDEO 1590 - 11º \'D\'', 'TIGRE', 'BUENOS AIRES', 'ARGENTINA', 'B1011CNR', NULL, '6353-01336', '0', '4 372-4886'),
(313, '10115', 'CARLOS ALBERTO', 'NATALE - MONACO ', '4504-4993', '153-042-0067 (Carlos)', 'cnatale@bancoindustrial.com.ar', 'ELIZABETH', 'MONACO ', NULL, NULL, '', 1, '17141985', '18288589', '1965-10-22', NULL, 'AV. GRAL MOSCONI 3916 2º\"A\"', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', 'C1419ERR', '0', '0', '1770635', '0'),
(314, '10131', 'MIRTA SUSANA', 'CICCARELLA - BAY ', '0', '(02326) 154-04-827 (Walter)', 'wbay@granelsur.com.ar', 'WALTER', 'BAY ', NULL, NULL, '', 1, '14395273', '11923356', '1961-11-03', NULL, 'CONSORCIO 1 CASA 6', 'SAN ANTONIO DE ARECO', 'BUENOS AIRES', 'ARGENTINA', '2760', 'mirtaciccarella@hotmail.com // alan.bay.ab@gmail.com', '0', '1780910', '0'),
(315, '10134', 'FABIAN HORACIO', 'MAFFEO - MAFFEO ', '02358-442277/443865 ESTE', '06875-2533   ', 'farmaciamaffeo@ceviamonte.com.ar', 'SERGIO ALBERTO', 'MAFFEO ', NULL, NULL, '', 1, '17227220', '18550093', '1964-08-25', NULL, 'ITALIA 627', 'LOS TOLDOS', 'BUENOS AIRES', 'ARGENTINA', 'B6015AKO', NULL, '6353-00862', '1837602', '02358-443865'),
(316, '10140', 'NELIDA INES', 'PUENTE N - ', '4295-0697', '15-6562 6378   ', 'inespuente56@hotmail.com', '0', NULL, NULL, NULL, '', 1, '11917643', '0', '1936-01-19', NULL, 'LOMAS VALENTINAS 645', 'EZEIZA', 'BUENOS AIRES', 'ARGENTINA', 'B1804CVE', 'inespuente56@hotmail.com', '6353-01131', '2006449', '0'),
(317, '10147', 'GUSTAVO ADOLFO', 'DI MATTEO - RADICI ', '4645-0226', '154-472-1595 (Gustavo)', 'dimatteogustavo@yahoo.com.ar', 'LILIANA', 'RADICI ', NULL, NULL, '', 1, '14857278', '13927181', '1962-01-04', NULL, 'GRITO DE ALCORTA 1505', 'MORON', 'BUENOS AIRES', 'ARGENTINA', 'B1708GWO', NULL, '3781-26744', '1747559', '4629-3732/3931 (FAX)'),
(318, '10156', 'JORGE', 'CASTELLO - CASTIGLIONI ', '4798-1391', '154-479-9040 Sra.   1549735557', 'profecastello@yahoo.com.ar', 'OLGA', 'CASTIGLIONI ', NULL, NULL, '', 1, '8558671', '11179853', '1951-06-29', NULL, 'SAAVEDRA 2231', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1640CAI', NULL, '6353-00354', '1755230', '4798-1391'),
(319, '10171', 'GUILLERMO CESAR', 'PINTO - ', '4663-1565', '1544095189', 'pintofabris@hotmail.com', '0', NULL, NULL, NULL, '', 1, '4597762', '0', '1943-12-20', NULL, 'AMENABAR 3445 (ENTRE TRES ARROYOS Y SUCRE)', 'VILLA DE MAYO', 'BUENOS AIRES', 'ARGENTINA', 'B1614GOF', 'pintogc43@speedy.com.ar', '6353-01110', '1822712', '4663-1565'),
(320, '10172', 'MARIA ELENA', 'MOLINA - TURON ', '02281-424163/23074', '02281-423367   ', 'turonmolina@gmail.com', 'GUILLERMO RAUL', 'TURON ', NULL, NULL, '', 1, '11822543', '11170406', '1955-11-26', NULL, 'RIVADAVIA 688', 'AZUL', 'BUENOS AIRES', 'ARGENTINA', 'B7300GHN', NULL, '6353-00954', '1', '02281-15403714 SR'),
(321, '10176', 'ALFREDO HORACIO', 'GUARINO - CONCHESO ', '4687-1234', '   154-993-0220 // 156-376-1126 (Alfredo)', 'familiaguarino@hotmail.com', 'NORMA ALICIA', 'CONCHESO ', NULL, NULL, '', 1, '5091545', '0', '1949-02-27', NULL, 'LISANDO DE LA TORRE 2406', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', 'C1440ECZ', 'guarinomc@hotmail.com', '6353 - 01457', '1773979', '4687-5439/4632-2501'),
(322, '10177', 'NESTOR OSCAR', 'AUGE - DEHAUT ', '0223-4790388/4745082', '0223-15-520-2226/0708   ', 'noauge@speedy.com.ar', 'MARIANA', 'DEHAUT ', NULL, NULL, '', 1, '13616883', '17018371', '1959-10-23', NULL, 'Calle Torre de Vera y Aragón 2285', 'MAR DEL PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B7604GKA', 'asesoria@speedy.com.ar', '6353-00101', '1780909', '0'),
(323, '10179', 'GISELA', 'ALESI - GARAU ', '4813-3273', '154-022-7191   ', 'gisela.alesi@gmail.com', 'JORGE', 'GARAU ', NULL, NULL, '', 1, '20891993', '0', '1969-05-09', NULL, 'CERRITO 1514  9º\" A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1010ABF', 'gisela.alesi@fibertel.com.ar', '0', '1796392', '0'),
(324, '10181', 'VERONICA GRACIELA', 'SAMMARTINO - BRACERAS ', '02983-432127/429100int 75', '02983-15568952 ella.', 'verosammartino@hotmail.com', 'ALEJANDRO LADISLAO', 'BRACERAS ', NULL, NULL, '', 1, '17454158', '13735953', '1965-09-29', NULL, 'calle 1810 nª 964', 'TRES ARROYOS', 'BUENOS AIRES', 'ARGENTINA', 'B7500EDS', 'alejandrobraceras@speedy.com.ar', '0', '1790216', '0'),
(325, '10182', 'VIVIANA ALICIA', 'BARRIOS - PEREZ ', '(0221)453-5767', '   ', 'barriosvivi@gmail.com', 'GERARDO HECTOR', 'PEREZ ', NULL, NULL, '', 1, '12707937', '14464307', '1961-07-21', NULL, 'CALLE 8 N°1631  entre     65 y 66', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1904CMI', 'gperezc@repsolypf.com', '6353-00183', '6025589', '0'),
(326, '10187', 'EDUARDO', 'BERTA - MONDELO ', '4656-1804', '   ', 'nmondelo@gador.com.ar', 'NELIDA', 'MONDELO ', NULL, NULL, '', 1, '13217768', '11862832', '1957-04-11', NULL, 'SARGENTO CABRAL 952', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', 'B1704AFT', '0', '6353-00212', '1611169', '0'),
(327, '10196', 'DANIEL RICARDO', 'AGRAIN - ', '(0261)432-6170', '(0261)156-592-184   ', 'danielagrain@soluciones-constructivas.com', '0', NULL, NULL, NULL, '', 1, '8157173', '0', '1947-05-25', NULL, 'SAN JUAN 175', 'MENDOZA', 'MENDOZA', 'ARGENTINA', '5500', 'f.campeotto@soluciones-constructivas.com', '6353-00026', '1796389', '0'),
(328, '10199', 'JUAN CARLOS', 'GORDICZ - MARZETTI ', '4795-1307', '15-4082-4708', 'am.marzetti@gmail.com', 'ANDREA', 'MARZETTI ', NULL, NULL, '', 1, '11266760', '14768341', '1955-02-06', NULL, 'CNEL ANGEL MONASTERIO 369', 'VICENTE LOPEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1638AOG', 'jgordicz@orbiseguros.com.ar', '0', '1832125', '154-406-8574 (Andrea)'),
(329, '10203', 'EVELIA DANILA', 'BOSCHETTO - ARCE ', '02262-436173/423244', '02262-428864 FAX   02262-421956-TRABAJO SR.', 'locuacuario@hotmail.com', 'RODOLFO JUAN DOMINGO', 'ARCE ', NULL, NULL, '', 1, '20042530', '17394447', '1968-03-11', NULL, 'CALLE 34 Nº 5344', 'NECOCHEA', 'BUENOS AIRES', 'ARGENTINA', 'B7630IKG', 'danilaboschetto@hotmail.com', '0', '1846626', '02262-421956'),
(330, '10209', 'ITALO CEFERIN', 'ROMAGNOLI - BUJAN ', '0', '(0221) 155-94-1150 (Ítalo)', 'italo.c.romagnoli@gmail.com', 'MARIA SUSANA', 'BUJAN ', NULL, NULL, '', 1, '11157629', '10390083', '1954-02-06', NULL, 'CALLE 120 N  23, entre diag. 74 y calle 33', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1902BXA', NULL, '3543-00695', '1', '0'),
(331, '10211', 'ALFREDO', 'AMADO - LACROIX ', '4921-6784', '155-818-2411 (Cansell Monserrat (Nuera))', 'patolacroix@gmail.com', 'PATRICIA NOEMI', 'LACROIX ', NULL, NULL, '', 1, '11399843', '11294836', '1955-02-17', NULL, 'LANZA 2237 PB 3', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1437CPE', 'mcansell@hotmail.com', '6353-00062', '1855123', '0'),
(332, '10219', 'MARIO/ALEJANDRO', 'DE ELIA - MORCILLO DE CASTRO', '4701-1844/4541-5058', '154-024-5473 (Adriana) // 155-750-0846 (Graciela)', 'alusalgado@gmail.com', 'ALEJANDRO', 'MORCILLO DE CASTRO', NULL, NULL, '', 1, '13890954', '12714900', NULL, NULL, 'MOLDES 4016', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1429AFX', 'bassograce@hotmail.com\'', '0', '6014224', '0'),
(333, '10227', 'MARCELO GREGORIO', 'SANTA CRUZ - MARTIN GRANDI', '(0341)440-0404', '(0341) 156-90-8678 (Patricia)', 'patriciamarting@hotmail.com', 'PATRICIA', 'MARTIN GRANDI', NULL, NULL, '', 1, '10410914', '13958906', '1952-02-22', NULL, 'TUCUMAN 1083', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000AMC', 'santacruzmarcelo@fibertel.com.ar', '6353-01268', '1', '(0341)448-7697/45'),
(334, '10238', 'RICARDO', 'FRITZLER - FERNANDEZ ', '02241-430198', '02241-15671673   ', 'ricardofritzler@gmail.com', 'MARY E.', 'FERNANDEZ ', NULL, NULL, '', 1, '14266904', '14870154', '1961-03-15', NULL, 'MENDOZA 322', 'CHASCOMUS', 'BUENOS AIRES', 'ARGENTINA', 'B7130EAH', NULL, '6353-00616', '1820151', '0'),
(335, '10240', 'OSVALDO JORGE', 'IMHOFF - REGGIARDO ', '03404-421764/154-10-666', '(03404)156-39-625', 'lagunafm@scarlos.com.ar', 'ALEJANDRA -NO FIRMO', 'REGGIARDO ', NULL, NULL, '', 1, '14743024', '0', '1961-11-02', NULL, 'ANSELMO GAMINARA 410 Y RIVADAVIA', 'SAN CARLOS CENTRO', 'SANTA FE', 'ARGENTINA', 'S3013BDJ', 'reggiardo_ale@yahoo.com.ar', '0', '1885884', '03404-420578    *'),
(336, '10250', 'RICARDO JORGE', 'OKS - BECERRA ', '4294-2858 casa Sra. (ex)', '156-358-7938 SRA.   ', 'maugebecerra@yahoo.com.ar', 'MARIA EUGENIA', 'BECERRA ', NULL, NULL, '', 1, '11450335', '0', '1955-02-07', NULL, 'PARANA 275  4º \"8\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1017AAE', 'rjoks@escribaniaoks.com.ar', '0', '1', '4374-1753 Escrib'),
(337, '10253', 'OSCAR', 'CARTA - 0', '0341-4360809', '(0341)156-92-6700', 'oscar.carta@paladini.com', '0', '0', NULL, NULL, '', 1, '12788687', '0', '1958-10-12', NULL, 'AV.GENOVA 822 4º \"A\"', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2005DNY', NULL, '6353-00343', '1', '(0341)492-4139 FAX // (0341) 492-1801 Int. 137 // '),
(338, '10263', 'LETICIA G.', 'LARRONDO - VILLANUEVA', '02257-461445', '225715660090', 'leticia_larrondo@hotmail.com', 'JUAN JOSE', 'VILLANUEVA', NULL, NULL, '', 1, '14318099', '10590075', '1952-11-21', NULL, 'AV. TUCUMAN 1708', 'SAN BERNARDO', 'BUENOS AIRES', 'ARGENTINA', 'B7111CIF', NULL, '6353-00139', '1901864', '02257-15-660090 sra.'),
(339, '10270', 'PEDRO OMAR (FALLECIÓ)', 'TOLOSA - TOLOSA ', '(0362) 4432-293', '(0362) 154-56-5756 (Pedro)', 'tolosapedro@arnet.com.ar', 'DANIEL RUBEN', 'TOLOSA ', NULL, NULL, '', 1, '4404868', '28602197', '1963-03-12', NULL, 'AV. LAVALLE 575', 'RESISTENCIA', 'CHACO', 'ARGENTINA', 'H3500AQF', 'danytolosa@yahoo.com.ar', '6353-01346', '1842248', '0'),
(340, '10274', 'HUGO', 'SIGLIANI - ASIS MARTINEZ', '(0221) 457-2586 // 501-3381', '(0221) 154-94-6331   ', 'hc10654@yahoo.com.ar', 'MONICA', 'ASIS MARTINEZ', NULL, NULL, '', 1, '10230654', '10705864', '1952-05-02', NULL, 'calle 26 Nº 1239', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1900ETM', 'siye21@gmail.com', '0', '2090638', '0'),
(341, '10302', 'HUGO RICARDO', 'MANDRINO - BOGHOSSIAN ', '4663-0524', '15-52618405   ', 'hrmandrino1953@hotmail.com', 'SILVANA', 'BOGHOSSIAN ', NULL, NULL, '', 1, '10764384', '11636779', '1953-11-20', NULL, 'PERITO MORENO 961', 'LOS POLVORINES', 'BUENOS AIRES', 'ARGENTINA', 'B1613FYS', 'huri2011man@gmail.com', 'ID RCI', 'ID INTERVAL', '15-3180-6694 / 11-3580-8000 sr'),
(342, '10309', 'CARLOS ALBERTO', 'PERALTA - PERALTA ', '4292-2290', '156-054-1203 sr   ', 'ailenlineainfantil@hotmail.com', 'JOSE FRANCISCO', 'PERALTA ', NULL, NULL, '', 1, '12131703', '10960087', '1958-05-04', NULL, 'POSADAS 530 ESQ. SIXTO FERNANDEZ', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', 'B1832DQL', NULL, '6353-01084', '1886800', '0'),
(343, '10314', 'NESTOR  EDUARDO', 'BARANZINI - COLUCCIO ', '4299-4093', '15-50447654 SRA   15-40300392 SR', 'nestor.baranzini@marby.com.ar', 'BEATRIZ INES', 'COLUCCIO ', NULL, NULL, '', 1, '11888116', '13087314', '1955-12-01', NULL, 'AV. ESPORA 3360', 'BURZACO', 'BUENOS AIRES', 'ARGENTINA', 'B1852GAO', 'nestorbaranzini@hotmail.com', '6353-00172', '1863434', '4708-4033'),
(344, '10315', 'GRACIELA ALEJANDRA', 'MOLLE - PALERMO ', '4631-7623 ( no corresp)', '154-997-1316 SR.   ', 'graciela.palermo@hotmail.com', 'TOMAS', 'PALERMO ', NULL, NULL, '', 1, '14369543', '0', '1960-11-14', NULL, 'NEUQUEN 1630', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1406FOD', 'dani.palermo@hotmail.com', '6353-00957', '2106321', '4633-1492 fax'),
(345, '10329', 'DOMINGA ELISA', 'CASTILLO - CIAPPARELLI ', '02322-485768', '(02317) 154-18-581 (Rodolfo)', 'RCiappareli@bpba.com.ar', 'RODOLFO HORACIO', 'CIAPPARELLI ', NULL, NULL, '', 1, '11757215', '12735481', '1955-05-28', NULL, 'RIVADAVIA 770', 'PRESIDENTE DERQUI', 'BUENOS AIRES', 'ARGENTINA', 'B1635ADN', 'martinaviajes@yahoo.com.ar', '0', '2069205', '4664-7265/7080 fax'),
(346, '10332', 'CARLOS JOSE', 'FERNANDEZ - LABORDE ', '4290-3882/4341-8567', '154-413-0214', 'cjfernandez45@gmail.com', 'MARIANA INES', 'LABORDE ', NULL, NULL, '', 1, '7764955', '5179547', '1945-09-11', NULL, 'CARDEZA 1864', 'LUIS GUILLON', 'BUENOS AIRES', 'ARGENTINA', 'C1838AFR', '0', '0', '1912605', '0'),
(347, '10336', 'ROLANDO JORGE', 'FONDATI - NAUMIEC ', '4744-4936', '155-388-2737 (Silvana)', 'terraceramicos@hotmail.com', 'SILVANA MARIA', 'NAUMIEC ', NULL, NULL, '', 1, '20435970', '20435763', '1969-02-10', NULL, 'AV. HIPOLITO YRIGOYEN 2562 (x Ruta 202)', 'SAN FERNANDO', 'BUENOS AIRES', 'ARGENTINA', '1646', NULL, '6353-00605', '1', '0'),
(348, '10337', 'GUSTAVO RAMON', 'PEREZ - LAPPONI ', '(0291)453-1553', '(0291)154-05-2094 (Graciela) // (0291)156-43-3520', 'perez_gustavo56@yahoo.com.ar', 'GRACIELA JUANA', 'LAPPONI ', NULL, NULL, '', 1, '12278871', '13461276', '1956-10-26', NULL, 'ESPAÑA 577', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000JFK', NULL, '0', '2111227', '(0291)453-1553'),
(349, '10338', 'JORGE RICARDO', 'ALMEIRA - DELGADO ', '4701-6058', '156-692-9343 (Jorge) // 154-939-5184 (Tomasa)', 'tomy-iara2003@hotmail.com', 'CRISTINA', 'DELGADO ', NULL, NULL, '', 1, '20510174', '0', '1968-10-15', NULL, 'AV.CONGRESO 2277  1º\"C\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1428BVG', 'jorgea100@hotmail.com', '0', '2020128', '0'),
(350, '10342', 'NORA NOEMI', 'AVILA - PEZZUTTI ', '0291-4520677 *', '(0291)154-06-6111 (Jorge)', 'estudiopezzutti@bvconline.com.ar', 'JORGE RAUL', 'PEZZUTTI ', NULL, NULL, '', 1, '10411765', '7787666', '1952-02-16', NULL, 'ALSINA 95 PISO 14 OFIC. 1/3', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000IHA', NULL, '6353-00105', '2073253', '(0291)452-9140 // (0291)453-7304 '),
(351, '10345', 'NORBERTO', 'SALOMONE - MONTERO ', '0291-4821476 CASA', '(0291)154-05-7111 (Norberto) / (0291)156-49-2795 (Martìn)', 'salomonenorberto@fibertel.com', 'SUSANA', 'MONTERO ', NULL, NULL, '', 1, '5507144', '4776784', '1945-08-16', NULL, 'CASTELAR 1342', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000BSJ', 'martinsalomone@fibertel.com.ar', '0', '2073251', '(0291)456-0029 '),
(352, '10354', 'PATRICIA VIVIANA', 'ALVAREZ - BEA ', '4662-1936', '155-053-0713 (Patricia)', 'patriciabea60@hotmail.com', 'RICARDO', 'BEA ', NULL, NULL, '', 1, '10192954', '0', '1960-04-03', NULL, 'CARBAJAL 533 ', 'HURLINGHAM', 'CAPITAL FEDERAL', 'ARGENTINA', '1686', 'ricardobea58@hotmail.com', '6353-00054', '1869919', '4751-2666'),
(353, '10361', 'DAMIAN ANDRES', 'MARTIN - VIGLIANCO ', '0', '(0291) 154-468-183 Mamá // (0291)-15 418 7351', 'dmartin147@hotmail.com', 'CECILIA', 'VIGLIANCO ', NULL, NULL, '', 1, '28062963', '0', '1980-03-18', NULL, 'ESTADOS UNIDOS 264', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', 'B8000GPK', 'jmartin147@hotmail.com', '0', '2052161', '(0291) 481-5596 JC'),
(354, '10366', 'SUSANA EMILIA', 'COLONNA - COLONNA ', '4612-9891', '3534-9049', 'susic2008@hotmail.com', 'ADRIANA MATILDE', 'COLONNA ', NULL, NULL, '', 1, '16386819', '18667742', '1960-10-12', NULL, 'FCO.BILBAO 2632', '0', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1406FGB', 'amcolonna_6@yahoo.com.ar', '0', '2126773', '154-162-1732 (Susana)'),
(355, '10368', 'AMERICO', 'JURASICS - ROMAN ', '0299-4482639', '299154739282 (Americo) // 299155465382 (Lidia)', 'marialidia.romano@gmail.com', 'MARIA LIDIA', 'ROMAN ', NULL, NULL, '', 1, '7777763', '4557264', '1946-07-04', NULL, 'Bº MUDON MANZ. 126 DUPLEX 525 (CANELA Y ASMAR)', 'NEUQUEN', 'NEUQUEN', 'ARGENTINA', '-8300', '0', '6353-00780', '1', '0'),
(356, '10375', 'PATRICIA ANA', 'LUQUE - MELLANO ', '0341-4825501', '0341-155555752 patricia.', 'patrianlu@hotmail.com', 'SERGIO RAUL', 'MELLANO ', NULL, NULL, '', 1, '16652649', '14455091', '1964-05-05', NULL, 'MAIPU 3012', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2001RWD', 'patrianlu@hotmail.com', 'ID RCI', 'ID INTERVAL', '0'),
(357, '10400', 'ALEJANDRO', 'TOSI - 0', '4776-1861/4775-1604', '154-440-2591', 'alejandro@tosi.cc', 'SILVINA', '0', NULL, NULL, '', 1, '12274702', '0', '1956-10-15', NULL, 'AV DEL LIBERTADOR 3672', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425ABW', '0', '6353-01355', '1894702', '4239-2800  o  15-4414-0836 SILVINA'),
(358, '10424', 'Sofía', 'PIZARRO (Falleció) - PIZARRO (Falleció) - 0', '0358-4633080/4647120', '3585608808', 'sofiacpizarro@hotmail.com', 'EDUARDO JUAN', 'PIZARRO (Falleció) - 0', NULL, NULL, '', 1, '28579803', '6647946', NULL, '1941-07-21', 'DINKELDEIN 2570', 'RIO CUARTO', 'CORDOBA', 'ARGENTINA', 'X5806CUZ', 'ejp.rcua@gmail.com', '6353-01115', '1', '0358-4643860'),
(359, '10425', 'LEANDRO JOSE', 'SIMON - PERALBA GARCIA', '4871-0649 // 4308-1644/5', '155-886-3839 (Soledad)', 'fliasimon@gmail.com', 'M.SOLEDAD', 'PERALBA GARCIA', NULL, NULL, '', 1, '18442982', '22110186', '1967-10-11', NULL, 'DEL PUENTE 35 BºLA ISLA NORDELTA    o    LA RIOJA 1553 CAP.FE   ', 'TIGRE', 'BUENOS AIRES', 'ARGENTINA', '1670', 'simonleandrojose@gmail.com', '0', '1', '4308-1644'),
(360, '10436', 'JUAN CARLOS GUILLERMO', 'KREBS - VEGA', '0', '(0264) 422-1038 // (0264) 154-16-2861', 'jcgkrebs@hotmail.com', 'STELLA M.', 'VEGA', NULL, NULL, '', 1, '7639566', '6197439', '1949-03-06', NULL, 'GRAL. SOLER 1287 SUR', 'SAN JUAN', 'SAN JUAN', 'ARGENTINA', '5400', NULL, '6353-00791', '6013154', '0264-4275117'),
(361, '10438', 'MARIELA ALEJANDRA', 'MYBURG - ', '0297-4452488 PART', '(0297) 154-710-945 (Mariela)', 'mmyburg20@gmail.com', '0', NULL, NULL, NULL, '', 1, '21355280', '0', '1970-02-20', NULL, 'AONI KENK 931', 'RADA TILLY', 'CHUBUT', 'ARGENTINA', '9001', 'mamyburg@hotmail.com', '6353-00993', '2134812', '0'),
(362, '10460', 'HORACIO', 'CIBEIRA - ', '0', '152-228-1347  (Horacio)', 'horaciocibeira@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '2582629', '0', '1929-01-29', NULL, 'CAMPANA 3810', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419AGH', '0', '6353-00387', '2031322', '0'),
(363, '10480', 'MARCELO', 'LANARI - DURAZZO ', '(0221) 453-9547', '(0221)155-011-385   ', 'lanarimm@yahoo.com', 'MARIA ELENA', 'DURAZZO ', NULL, NULL, '', 1, '11359482', '12063270', '1954-12-10', NULL, 'CALLE 63 Nº 374', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', 'B1904AJP', NULL, '6353-00804', '1', '0'),
(364, '10489', 'MIGUEL MARIO', 'MASUH - VAZQUEZ ', '0', '(0261)153-038-200', 'mmmasuh@gmail.com', 'MARIA LORENA', 'VAZQUEZ ', NULL, NULL, '', 1, '16925279', '22939720', '1965-07-27', NULL, 'ACC SUR 3829 L.7 RUTA 40 KM 22,5 HARAS DE PERDRIEL', 'LUJAN DE CUYO', 'MENDOZA', 'ARGENTINA', 'M5507ADA', 'lorevazq@yahoo.com.ar', '0', '1', '(0223) 451-0559 Silvia // (0261)413-1300 Int.1617 // 1326   '),
(365, '10499', 'JORGE LUIS', 'CESTI - ', '4821-5582', '15-4490-3697   ', 'jorgecesti@gmail.com', '0', NULL, NULL, NULL, '', 1, '17918646', '0', '1967-04-20', NULL, 'LAPRIDA 1335 2º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1425', '0', '6353-00372', '0', '5222-8969/67 (FAX)'),
(366, '10503', 'ANTONIO OSVALDO', 'PECAR - BATALLA ', '02262-430032', '(02262)155-13-239  ', 'osvaldopecar@hotmail.com', 'ROSANA MARIA', 'BATALLA ', NULL, NULL, '', 1, '16815356', '16815356', '1964-07-13', NULL, 'AV. SAN MARTIN 682 2º \"5\" EDIF. DORREGO', 'NECOCHEA', 'BUENOS AIRES', 'ARGENTINA', 'B7630FXD', NULL, '6353-01078', '2122503', '(02262)450-403 fax   '),
(367, '10511', 'MIGUEL ANGEL', 'TRABOSCIA - ABRIL ', '0230-4494344           15-5654-7739', '1540426096 / 1531646356 (Abril Rita)', 'MTraboscia@uocra.org', 'RITA ADRIANA', 'ABRIL ', NULL, NULL, '', 1, '13664876', '17613072', '1960-01-30', NULL, 'BOGOTA 1643 (BºPELLEGRINI II VILLA ROSA)', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', '-1631', 'miguelt@uocra.org', 'ID RCI', 'ID INTERVAL', '4384-7171 SOLO MJE'),
(368, '10517', 'MARCELO JAVIER', 'PEDROZO - LOPEZ RIOS', '4488-9443', '156-105-8711', 'marcelojpedrozo15@gmail.com', 'SILVINA CARLA', 'LOPEZ RIOS', NULL, NULL, '', 1, '16549055', '17955372', '1963-05-07', NULL, 'HIPOLITO YRIGOYEN 456', 'CIUDADELA', 'BUENOS AIRES', 'ARGENTINA', '1702', NULL, '6353-01079', '2159375', '5274-6367/6498 trab'),
(369, '10520', 'NOEMI HAYDEE', 'ELIAS - LONTRATO ', '4656-4120', '155-697-1687   ', 'hectorlontrato@yahoo.com.ar', 'HECTOR OSCAR', 'LONTRATO ', NULL, NULL, '', 1, '12728033', '13030406', '1956-11-12', NULL, 'PASCO 440 DEPTO. 4   o   DIAG.NORTE 530(3º)', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', 'B1704GIJ', '0', '0', '2126519', '155-054-1834'),
(370, '10531', 'CLAUDIA ROSA', 'PACQUOLA - ORTEGA ', '4799-7989', '4687-0937 FAX   156-377-3573 sra', 'claudiapacquola@hotmail.com', 'DANIEL', 'ORTEGA ', NULL, NULL, '', 1, '14817492', '0', '1962-01-04', NULL, 'MARCONI 822', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', NULL, '6353-01045', '1', '4671-4271-TRABA'),
(371, '10551', 'DANIEL', 'BOGAO - ERBACCI ', '(0220)482-5607', '154-937-8554 (Daniel)  ', 'sandra_fabianaerbacci@hotmail.com', 'SANDRA FABIANA', 'ERBACCI ', NULL, NULL, '', 1, '16270762', '0', '1964-06-05', NULL, '25 DE MAYO 1480 EX 555', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', 'B1722LZT', 'rominacornaglia@live.com.ar; daniel_bogao@hotmail.com', '0', '1', '155-567-6863 (Sandra)'),
(372, '10553', 'LUIS ALBERTO', 'LAFALCE - MANGIERI ', '4204-9792', '156-245-3815 (Luis)', 'lafalcesa@luislafalcesa.com.ar', 'ISABEL', 'MANGIERI ', NULL, NULL, '', 1, '8309854', '6202536', '1950-01-01', NULL, 'SUPISICHE 284  SUSPICHE 172', 'SARANDI', 'BUENOS AIRES', 'ARGENTINA', 'B1872BTF', 'luis_alberto@luislafalcesa.com.ar', '6353-00797', '1', '4204-5539  '),
(373, '10565', 'GUILLERMO', 'GALANTE - OLABARRIETA ', '0381-4228165', '(0381) 155-21-7778 (Guillermo)', 'ggalante@segurosrivadavia.com', 'ROSA MARIA', 'OLABARRIETA ', NULL, NULL, '', 1, '8472032', '13524204', '1951-03-05', NULL, 'CATAMARCA 440 6º \"B\"', 'S.MIGUEL DE TUCUMAN', 'TUCUMAN', 'ARGENTINA', 'T4000ITJ', 'rosa_olabarrieta@hotmail.com', '0', '2190404', '(0381)431-1006 Int. 755'),
(374, '10577', 'CAMILO', 'BAGLIETTO - ', '02920-422757', '1526160004', 'anamarialamas@yahoo.es', '0', NULL, NULL, NULL, '', 1, '4260331', '0', '1937-11-10', NULL, 'GARRONE 867', 'VIEDMA', 'RIO NEGRO', 'ARGENTINA', 'R8500AEQ', 'etelcastaneda@hotmail.com', '6353-00111', '2226873', '02920-428906'),
(375, '10586', 'ALBERTO OSCAR', 'GENOVESE - ', '0', '(03491)155-04-488', 'albertogenovese@soon.com.ar', '0', NULL, NULL, NULL, '', 1, '8281257', '0', '1950-09-21', NULL, 'AV. RIVADAVIA 1180', 'TOSTADO', 'SANTA FE', 'ARGENTINA', 'S3060AYO', NULL, '0', '0', '(03491)470-501 // (03491)470-001 Int. 4'),
(376, '10588', 'MARIA EUGENIA', 'TROMBINI - OLGUIN - OLGUIN', '(02229) 492-828', '114-530-7981 (Flavio)', 'eugeniat55@hotmail.com', 'FLAVIO GUSTAVO', 'OLGUIN', NULL, NULL, '', 1, '22913687', '20123018', '1972-09-14', '1969-01-19', '618 Y 538 S/N C. A. EL PATO', 'BERAZATEGUI', 'BUENOS AIRES', 'ARGENTINA', '0', 'golguin@knauf.com.ar', '4235-04853', '2197994', '1141880646 (M. Eugenia)'),
(377, '10594', 'MIGUEL ANGEL', 'DERRIGO - ORTIZ ', '(0220)485-9223', '153-238-1642 (Miguel) ', 'patricialortiz@yahoo.com.ar', 'PATRICIA LAURA', 'ORTIZ ', NULL, NULL, '', 1, '13523172', '16125058', '1959-09-21', NULL, 'MITRE 590', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', 'B1722DKL', '0', '6353-00487', '0', '156-430-3434 (Patricia)'),
(378, '10597', 'SILVINA MARIEL', 'DIVAN, SILVINA MARIEL - LLAURO , LETICIA OCTANIA', NULL, 'a02954-15594416   ', 'jahudiaz26@hotmail.com', 'LETICIA OCTANIA', 'LLAURO ', NULL, NULL, '', 1, '23780105', '23857493', '1974-04-18', NULL, 'MARCOS MOLAS 55', 'SANTA ROSA', 'LA PAMPA', 'ARGENTINA', 'L6300EUA', 'javier.diaz@bancodelapampa.com.ar', '6353-00517', '0', NULL),
(379, '10598', 'JUAN CARLOS', 'DI GESU - 0', '(0353) 488-3711', '(0353) 154-123-461', 'jcdigesu@yahoo.com.ar', '0', '0', NULL, NULL, '', 1, '11506594', '0', '1965-06-12', NULL, 'HIPOLITO YRIGOYEN 652', 'UCACHA', 'CORDOBA', 'ARGENTINA', '2677', NULL, '6353-00496', '0', '0'),
(380, '10606', 'NESTOR LUIS', 'GONZALEZ - ', '(02229)441-365', '153-760-6818 (Néstor)   ', 'nestor.gonza@gmail.com', '0', NULL, NULL, NULL, '', 1, '16490249', '0', '1963-05-30', NULL, 'CALLE 134 A Nº 5289 entre 52B Y 53', 'BERAZATEGUI', 'BUENOS AIRES', 'ARGENTINA', '1885', 'yinesgonzalez@gmail.com', '6353-00692', '0', '156-743-2992 (Inés)'),
(381, '10627', 'HORACIO', 'JALIL - DENARO', '4918-6047 FAX', '155-020-4081 (Gladys) // 155-020-4082 (Alejandro)', 'productos_sorianosrl@hotmail.com', 'DELIA IRENE', 'DENARO', NULL, NULL, '', 1, '4431278', '5943086', '1944-04-09', NULL, 'TABARE 3131', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1437', NULL, '6353-01451', '0', '4918-2688 FABR.'),
(382, '10629', 'MARC', 'CHANTRIER - ', '33-0-147167036', '33-1-47167036   ', 'marc.chantrier@wanadoo.fr', '0', NULL, NULL, NULL, '', 1, '920299022502', '0', '1951-02-04', NULL, '4 RUE MOUILLON 92 RUEIL MALMAISON', '0', 'FRANCIA', 'FRANCIA', '92500', NULL, '6353-00449', '2265317', '33-607 494376'),
(383, '10640', 'VICTOR EMILIO', 'RODRIGUEZ - CESAR ', '4792-0030', '15-4446-8913   ', 'verrodriguez@yahoo.com', 'PATRICIA ANDREA', 'CESAR ', NULL, NULL, '', 1, '17286395', '16481081', '1964-12-02', NULL, 'ENTRE RIOS 1124', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1640GBH', NULL, '0', '7532826', '0'),
(384, '10648', 'MONICA VIVIANA', 'NUÑEZ - RUIZ ', '0341-4821299/4470866', '0341-4210125 SR   ', 'juanjoseruiz_57@hotmail.com', 'JUAN JOSE', 'RUIZ ', NULL, NULL, '', 1, '14440858', '13449857', '1961-07-26', NULL, 'BALCARCE 1711 6º', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000GGA', NULL, '6353-01020', '0', '0341-156908706 ella'),
(385, '10699', 'MARIELA', 'MILEO DE - BLANCO MARCELO (PRIMO BOGADO)', '0', '154-403-4429 (Marcelo) // 154-405-3343 (Mariela) // 154-405-3345 (Ramiro)', 'marceloblancostfsa@gmail.com', '0', 'BLANCO MARCELO (PRIMO BOGADO)', NULL, NULL, '', 1, '22293855', '14012958', '1971-07-19', NULL, 'GABOTO 1190', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', 'B1686', 'mblanco@artdc.com.ar', '6353-00943', '0', '4394-5115'),
(386, '10704', 'JUAN FRANCISCO', 'OROPESA - GALIZIA ', '4252-9288', '1150614454', 'galiziateresa@yahoo.com.ar', 'TERESA', 'GALIZIA ', NULL, NULL, '', 1, '10513589', '0', NULL, NULL, 'TACUARI 198', 'BERNAL', 'BUENOS AIRES', 'ARGENTINA', '1876', 'galiziateresa@laboratoriogalizia.com.ar', '6353-01454', '0', '4251-9476 / 4252-0916 Sra.'),
(387, '10705', 'LUIS PABLO', 'RUTTIMANN - KANE ', '4758-8512', '154-416-2207', 'gruporuttimann@fibertel.com.ar', 'CECILIA DEBORA', 'KANE ', NULL, NULL, '', 1, '16348102', '17829307', '1962-11-06', NULL, 'MITTELHOLZER 2274', 'CIUDAD JARDIN-EL PAL', 'BUENOS AIRES', 'ARGENTINA', 'B1684AJD', 'colorado611@hotmail.com', '0', '0', '4758-3005/9812   '),
(388, '10709', 'JOSE MANUEL', 'GOMEZ ROBLES - MERLO ', '4801-2979', '155-609-4936 (José)   ', 'gomrob@hotmail.com', 'DORA SILVIA', 'MERLO ', NULL, NULL, '', 1, '4555076', '5761037', '1946-07-12', NULL, 'JUAN FCO. SEGUI 3672 PISO 28', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425ACD', 'dorimer@gmail.com', '0', '0', '155-609-4949 (Dora)'),
(389, '10900', 'REP: GONZALEZ A', 'AGB S.A. - BORRILE', '4807-8524/8528 (Vanina Secr.)', '4802-0207   ', 'recepcion@agbsa.com.ar', 'AUGUSTO', 'BORRILE', NULL, NULL, '', 1, 'CUIT 33-69724569-9', '0', NULL, NULL, 'PACHECO DE MELO 1825 5º Of. 5', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '1126', 'aborlle@agbsa.com.ar', '4121-00561', '1', '4313-7526'),
(390, '12000', 'GREGORIO A.', 'NOCEDA - ', '4657-8212', '154-945-2830 (Gregorio) // 115-690-7499 (Laura)', 'gnoceda@intramed.net', '0', NULL, NULL, NULL, '', 1, '13492831', '0', '1960-10-17', NULL, 'CHACABUCO 85 DTO C', 'CIUDADELA', 'BUENOS AIRES', 'ARGENTINA', 'B1702FJB', NULL, '0', '0', '4488-1181'),
(391, '12004', 'ANGELA JULIA', 'PETRUZZELLA - FERNANDEZ ', '0', '1567600433', 'anyu_1964@hotmail.com', 'CLAUDIO ROBERTO', 'FERNANDEZ ', NULL, NULL, '', 1, '17172539', '17172539', '1964-04-27', NULL, 'GENERAL MADARIAGA 2075', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', '1824', 'drclaudiofernandez@fibertel.com.ar', '0', '0', '0'),
(392, '12008', '0', 'SIGONLINE S.A.  - SPINELLI', '4813-0429(FAX)/4815-1388', '0', 'ospinelli@sigonline.com', 'OMAR', 'SPINELLI', NULL, NULL, '', 1, 'CUIT 30707208836', '8550469', NULL, NULL, 'CERRITO 1540 4º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1010ABF', 'ospinelli@sigonline.com', 'ID RCI', 'ID INTERVAL', '154-477-6697'),
(393, '12010', 'ALEJANDRO', 'BAGDADI - ', '4786-5100/5122', '153-092-3505   ', 'alebagdadi@consultoresmyb.com.ar', '0', NULL, NULL, NULL, '', 1, '12548911', '0', NULL, NULL, 'VIRREY DEL PINO 2458  8º\"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1426', NULL, '4313-00039', '6003259', '0'),
(394, '12013', 'CLAUDIA BEATRIZ', 'SAAVEDRA - MENENDEZ ', '4222-2133', '1544795374 (Gustavo)', 'menendez.gustavo@hotmail.com', 'CARLOS GUSTAVO', 'MENENDEZ ', NULL, NULL, '', 1, '14070502', '14264855', NULL, NULL, 'AV. MITRE 816 PISO 13º \"A\"', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', 'menendez.gustavo@hotmail.com', '6353-1381', '0', '4222-6700 FAX'),
(395, '12015', 'MIGUEL ANGEL', 'CANNIZZO - CELONA ', '4243-7588 casa/4244-1165', '15-65037662   *   ', 'contamc@speedy.com.ar', 'ANA', 'CELONA ', NULL, NULL, '', 1, '10833057', '11492296', '1953-07-04', NULL, 'HIPOLITO IRIGOYEN 8959 3ºB', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', 'anitacan54@hotmail.com', '0', '0', '0'),
(396, '12018', 'IRMA HAYDEE', 'DONNARI - MAREY ', '4941-7167 / 3526-2520 (Preferente)', '155-701-9003 (Irma)', 'rmarey@yahoo.com', 'ROBERTO', 'MAREY ', NULL, NULL, '', 1, '3975506', '4313000', NULL, NULL, 'PICHINCHA 1136', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1219ACN', 'irma.donnari@gmail.com', '6353-01401', '1447950', '0'),
(397, '12024', 'ANGEL', 'CIRIESI - DIP ', '0237-4058720 Hilda', '44503212- DIP MARTA   ', 'a.ciriesi@hotmail.com', 'HILDA BEATRIZ', 'DIP ', NULL, NULL, '', 1, '8319734', '10255023', '1950-07-14', NULL, 'C.M.JOLY 1568', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', '1744', 'ventas@savenca.com', 'ID RCI', 'ID INTERVAL', '1557279466'),
(398, '12028', 'CARLOS FRANCISCO', 'BRECELJ - SALUM ', '4231-4642', '155-818-4977   ', 'mercedessalum@yahoo.com.ar', 'MARIA MERCEDES', 'SALUM ', NULL, NULL, '', 1, '8424845', '0', NULL, NULL, '25 DE MAYO 414', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', '1834', NULL, '6353-01384', '0', '0'),
(399, '12029', 'BLANCA', 'MUÑOZ - MORAN ', '0', '154-160-9421   ', 'blanca_200512@hotmail.com', 'JORGE', 'MORAN ', NULL, NULL, '', 1, '11623929', '0', NULL, NULL, 'SOLDADO TOLEDO 5777- LA PERLITA', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', '1744', NULL, '6353-01390', '0', '0'),
(400, '12032', 'CARLOS', 'VON DER BECKE - ', '4666-7796 PATRICIA (SRA)', '15-4414-4003   ', 'patoprille@hotmail.com', '0', NULL, NULL, NULL, '', 1, '11230414', '0', '1954-05-06', NULL, 'RECONQUISTA 412', 'BELLA VISTA', 'BUENOS AIRES', 'ARGENTINA', 'B1661CJJ', 'carlos@durbeck.com.ar', '4013-00300', '0', '0'),
(401, '12034', 'PABLO JAVIER', 'VIDAL - ', '4797-0523', '(0294)154-77-9801', 'vidalpab@hotmail.com', '0', NULL, NULL, NULL, '', 1, '24312080', '0', NULL, NULL, 'Av. Libertador 886, 6º \"B\"', 'VICENTE LOPEZ', 'BUENOS AIRES', 'ARGENTINA', '1638', NULL, '6353-01392', '0', '0'),
(402, '12035', 'ROBERTO M', 'ALTAMIRANO - BIANCHI ', '(0237)463-5059', '155-805-4425 (Marcela)', 'dramarcbianchi@yahoo.com.ar', 'MARCELA', 'BIANCHI ', NULL, NULL, '', 1, '18377378', '12169051', '1967-09-17', NULL, 'PASO 2215 ESQ. CENTENARIO', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', 'B1744IOI', NULL, '6353-01404', '0', '0'),
(403, '12039', 'ALBERTO MARIO', 'ALBARIÑAS - ROMANELLO ', '4791-0890', '154-142-0068 (Angela) // 156-570-9960 (Alberto)', 'alberto.albarinas@icbc.com.ar', 'ANGELA', 'ROMANELLO ', NULL, NULL, '', 1, '11565095', '0', '1954-12-07', NULL, 'FRANCISCO BEIRO 2446', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', 'B1636CFJ', 'angelaromanello@yahoo.com.ar', '4013-00009', '0', '4311-1315(int.15)sra'),
(404, '12043', 'ANGELINA', 'ALLAMPRESE - ', '4571-7795', '154-055-8707 (Angelina)', 'angelinaallamprese@gmail.com', '0', NULL, NULL, NULL, '', 1, '6142721', '0', NULL, NULL, 'LADINES 3006', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1419EYN', 'ara@argentina.com', '4235-03300', '0', '0'),
(405, '12044', 'NESTOR', 'LOGUZZO - ARIAS ', '4823-0072', '1540618237 (Cristina) / 1530717790 (Néstor) ', 'nestor.flp@gmail.com', 'MARIA CRISTINA', 'ARIAS ', NULL, NULL, '', 1, '10161791', '10187732', '1951-10-22', NULL, 'CHARCAS 3026, 6ºA', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1425BMJ', '0', '0', '6031401', '0'),
(406, '12045', 'ANGEL ALBERTO', 'MAGLIOCCO - ', '6089-1550', '155-025-4888', 'aamagliocco@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '8209028', '0', '1950-01-02', NULL, 'BERTRES 450', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1424AKF', '0', '4013-00187', '0', '0'),
(407, '12048', 'INES LILIANA', 'ROMERO - ', '02392-424401', '(02392)154-57-933', 'ilrposta@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '11149079', '0', '1954-04-20', NULL, 'QUINTANA 245', 'TRENQUE LAUQUEN', 'BUENOS AIRES', 'ARGENTINA', '6400', NULL, '0', '0', '0'),
(408, '12049', 'MABEL ALICIA', 'FORTUNATO - ZARATE ', '4249-8490', '15-62911042   ', 'mabelfortunato@hotmail.com', 'MIGUEL', 'ZARATE ', NULL, NULL, '', 1, '12127153', '0', '1956-09-26', NULL, 'PTE.ILLIA (EX EMILIO MITRE) 2571', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', 'B1824JXA', NULL, '4013-00308', '5748578', '0'),
(409, '12053', 'CARLOS FRANCISCO', 'FALCON - FAYA ', '0343-4242544', '   ', 'lumaf04@hotmail.com', 'LUISA MARIA', 'FAYA ', NULL, NULL, '', 1, '5933307', '4514313', NULL, NULL, 'MANUELA PEDRAZA 466', 'PARANA', 'ENTRE RIOS', 'ARGENTINA', '3100', 'lumaf04@hotmail.com', 'ID RCI', 'ID INTERVAL', '0'),
(410, '12061', 'OSCAR RICARDO', 'GREGORIS - LAMBERTI ', '(03385)495-239', '(03385)155-22-378   ', 'oscargregoris@internet-24.com.ar', 'SILVIA BEATRIZ', 'LAMBERTI ', NULL, NULL, '', 1, '12252594', '16509592', NULL, NULL, 'CORDOBA 175', 'SERRANO', 'CORDOBA', 'ARGENTINA', 'X6125AGB', NULL, '0', '0', '(03385)496-027 '),
(411, '12062', 'LORENA', 'COOMONTE - COOMONTE ', '(0220)483-7472', '155-011-6592', 'huguitoygra@yahoo.com.ar', 'HUGO MIGUEL', 'COOMONTE ', NULL, NULL, '', 1, '28695920', '8402645', '1981-01-30', NULL, 'CARLOS PELLEGRINI 282', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1722', '0', '0', '0', '0'),
(412, '12072', 'HUGO', 'CONSTANTI - MILITANO', '0341-4515306', '0341-153142785 ex sra   ', 'hac.51@hotmail.com', 'KARINA CARLA', 'MILITANO', NULL, NULL, '', 1, '10188051', '20704887', NULL, NULL, 'AV. COSTANERA 2555', 'GRANADERO BAIGORRIA', 'SANTA FE', 'ARGENTINA', '2152', NULL, '3760-00734', '0', '0341-15-6402-621/ SR'),
(413, '12078', 'ALEJANDRO CESAR', 'MALARA - 0', '0', '15-5307-9444 papa de ale- 156-567-5809 Alejandro', 'alejandromalara@hotmail.com', '0', '0', NULL, NULL, '', 1, '16325809', '0', NULL, NULL, 'AMENEDO 568', 'ADROGUE', 'BUENOS AIRES', 'ARGENTINA', '1832', 'escribaniagarello@ciudad.com.ar', '0', '0', '4911-0730 14 hs'),
(414, '12080', 'PABLO MIGUEL', 'SEDMAK - ', '4115-8971/72', '155-424-3856', 'pmsedmak@gmail.com', '0', NULL, NULL, NULL, '', 1, '16944766', '0', '1965-06-08', NULL, 'LAVALLE 1647 1º \"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1048', 'pmsedmak@gmail.com', '6353-01421', '0', '0'),
(415, '12081', 'JULIETA', 'SAYANES - ARCE ', '15-4416-6604   *', '153-264-2526 (Gustavo)', 'gustavo2718@outlook.es', 'GUSTAVO DANIEL', 'ARCE ', NULL, NULL, '', 1, '25294237', '21654391', '1975-10-27', NULL, 'BENITO VILLANUEVA 1033', 'ING. MASCHWITZ', 'BUENOS AIRES', 'ARGENTINA', '1623', NULL, '6353-01422', '0', '0'),
(416, '12082', 'EDUARDO', 'LUPI - PARADA ', '4222-9439', '155-249-6797', 'info@hidraulicalupi.com.ar', 'PATRICIA', 'PARADA ', NULL, NULL, '', 1, '13739921', '13856620', '1960-07-26', NULL, 'GRAL PAZ 62 7º \"A\"', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', '0', '0', '0', '4726-6464 fax OFIC   '),
(417, '12088', 'MARIA INES', 'BARCUS - NARDELLI ', '5218-7797', '155-101-6265   ', 'juan@nmyb.com.ar', 'JUAN CARLOS', 'NARDELLI ', NULL, NULL, '', 1, '16496833', '0', '1963-01-28', NULL, 'Av. Córdoba 838 - 14º', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1055AAQ', 'ines@nmyb.com.ar', '6353-01428', '0', '0'),
(418, '12089', 'HORACIO BERNARDO', 'PANIGAZZI - ', '4783-9960', '155-321-3000   ', 'berpa@live.com.ar', '0', NULL, NULL, NULL, '', 1, '4430363', '0', '1944-05-09', NULL, 'PEDRO IGNACIO RIVERA 2470 6º A', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1428', 'berpa@live.com.ar', '0', '5824070', '4896-2638/FAX'),
(419, '12090', 'JAVIER', 'GARCIA IGARZA - GOMEZ OBLIGADO', '4814-2561', '154-526-9511 (Carolina)', 'jgarciaigarza@k2partners.com.ar', 'CAROLINA', 'GOMEZ OBLIGADO', NULL, NULL, '', 1, '24314557', '25216595', '1974-12-31', NULL, 'CALLAO 1678  6ºPISO', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1024AAP', 'info@k2partners.com.ar', '0', '0', '4805-7555 // 4805-8555 Fax'),
(420, '12094', 'REP: NOBREGA ALEJANDRA M', 'SURICATA S.A. - ', '4393-9050/4322-7911', '4322-2302   ', 'rvillavieja@fibertel.com.ar', '0', NULL, NULL, NULL, '', 1, '11785767', '0', NULL, NULL, 'FLORIDA 622 PISO 1º OFIC.1', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1005', 'escribanianobrega@fibertel.com.ar', '0', '0', '43264143'),
(421, '12097', 'MARIA LILIANA ACOSTA DE', 'ARCHIMBAL - ARCHIMBAL ', '11-1552190800 (enviar aca rosita secretaria)', '155-568-1301 (M. Liliana)', 'larchimb@trabajo.gob.ar', 'FERNANDO', 'ARCHIMBAL ', NULL, NULL, '', 1, '11077437', '8447870', NULL, NULL, 'R.SAENZ PEÑA 938 5º PISO', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1035AAR', 'rmedici@archimbal.com; archimbal@yahoo.com.ar', '6353-00084', '0', '4310-6217'),
(422, '12098', 'STELLA MARIS', 'AMESTOY DE FROMENT - ', '4372-2594 trabajo sra.', '154-424-2311 (Carlos) // 155-041-8733 (Stella)', 'cfroment@bicaabogados.com', '0', NULL, NULL, NULL, '', 1, '11313747', '0', NULL, NULL, 'RODRIGUEZ PEÑA 2043', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1021ABQ', 'stellafroment@hotmail.com', '0943-01833', '0', '0'),
(423, '12100', 'CRISTINA', 'HARDT - ', '02944-441681', '(0294)154-63-1726 (Cristina)', 'tindhardt@gmail.com', '0', NULL, NULL, NULL, '', 1, '11581031', '0', NULL, NULL, 'LOS BUHOS 6105', 'BARILOCHE', 'RIO NEGRO', 'ARGENTINA', '8400', 'salivaraul@hotmail.com', '6353-01431 // II 8092459', '0', '0'),
(424, '12108', 'OSCAR ARTURO', 'QUIHILLALT, OSCAR ARTURO - DIMENNA , LUCIANA', '4798-8722/1558189564-ELLA', '15-5759-1792 EL.   ', 'oqui70@hotmail.com', 'LUCIANA', 'DIMENNA ', NULL, NULL, '', 1, '21363052', '21953666', '1970-12-07', NULL, 'THOMAS EDISON 622', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', 'B1640HQN', 'oqui70@hotmail.com', 'ID RCI', 'ID INTERVAL', '4341-7145'),
(425, '12116', 'SILVANA MARINA', 'PARRA - VATTUONE ', '4658-2327 (escribanía Hernan)', '0', 'hernanvattuone@hotmail.com', 'HERNAN GUSTAVO', 'VATTUONE ', NULL, NULL, '', 1, '17794861', '14116567', NULL, NULL, 'ROSALES 130 1º\"A\"', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', '1704', 'hernanvattuone@hotmail.com', '4235-02353', 'ID INTERVAL', '153-051-1960 (Hernán)'),
(426, '12118', 'MARIA DEL PILAR', 'OLIVERA - ', '4742-2625', '154-445-6756   ', 'piolivera@hotmail.com', '0', NULL, NULL, NULL, '', 1, '11618657', '0', '1954-12-28', NULL, 'LOS AROMOS 1237', 'SAN ISIDRO', 'BUENOS AIRES', 'ARGENTINA', '0', NULL, '6353-01438', '0', '0'),
(427, '12119', 'ANDREA VIVIANA', 'MANGANARO - SPINETTO ', '4799-0973', '154-474-1957   ', 'spinetto_aa@hotmail.com', 'ALEJANDRO MARIO', 'SPINETTO ', NULL, NULL, '', 1, '16304266', '12889792', NULL, NULL, 'RAMON CASTRO 1845/1851', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', NULL, '6353-1439', '0', '0'),
(428, '12121', 'CARLOS ALBERTO', 'MARTINEZ - CORREA ', '4459-7129', '116-977-4943 Sra. // 115-226-4661 (Carlos)', 'tinita.c.h.60@gmail.com', 'CRISTINA HAYDEE', 'CORREA ', NULL, NULL, '', 1, '12107061', '0', '1958-03-14', NULL, 'GLUCK 3454', 'SANTOS TESEI - HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', '1688', 'carlos@estudiocmartinez.com.ar', '6353-01441', '0', '4662-6733/8924 estud'),
(429, '12124', 'CARLOS DANIEL', 'AVELLANEDA - CARBO ', '(0351)478-6191/452-7085', '(0351)155-50-7382 (Cecilia)', 'mariace_3@yahoo.com.ar', 'CECILIA', 'CARBO ', NULL, NULL, '', 1, '12595100', '0', '1957-01-24', NULL, 'MARANAHO 1511 - Bº SAN NICOLAS', 'CORDOBA', 'CORDOBA', 'ARGENTINA', '5012', 'danielavellaneda@hotmail.com', '0', '0', '0'),
(430, '12127', 'SANDRA ISABEL', 'SARANCONE - ', '0341-4400729', '0341-156 194429   ', 'sati@steel.com.ar', '0', NULL, NULL, NULL, '', 1, '12520751', '0', '1958-06-21', NULL, 'Jujuy 1325, piso 18, \"B\"', 'ROSARIO', 'SANTA FE', 'ARGENTINA', 'S2000DRN', 'sarancones@gmail.com', '0', '7348144', '0'),
(431, '12130', 'NATALIA', 'BAYON - DE GREGORIO', '02262-522550', '(02262) 155-58-096 (Natalia)', 'bayonnatalia@hotmail.com', 'JOSE LUIS', 'DE GREGORIO', NULL, NULL, '', 1, '25062910', '0', NULL, NULL, 'AV. 10 Nº 4754', 'NECOCHEA', 'BUENOS AIRES', 'ARGENTINA', '7630', '0', '0', '5628390', '0'),
(432, '12132', 'VICTOR HUGO', 'VIÑUELA - 0', '4384-0432 / 6079-4059', '   ', 'estudio.vhv@gmail.com', '0', '0', NULL, NULL, '', 1, '18195993', '0', '1967-08-08', NULL, 'AV.PTE ROQUE S. PEñA 1119 PISO 8 OFIC. 814', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1035AAG', NULL, '0', '5755279', '0'),
(433, '12143', 'MONICA LILIANA', 'BADANO - PINI', '4795-3202  ', '154-058-8296 (Mónica) // 154-095-8673 (Luis)', 'mbadano@live.com.ar', 'LUIS', 'PINI', NULL, NULL, '', 1, '12890478', '0', '1957-05-16', NULL, 'ITALIA 1625', 'FLORIDA', 'BUENOS AIRES', 'ARGENTINA', '1602', '0', '0', '5876635', '0'),
(434, '12149', 'LUCIO JOSE', 'DEMARCHI - ', '0', '1568777717 Gabriela', 'ldemarchi@empco.com.ar', '0', NULL, NULL, NULL, '', 1, '18148470', '0', '1966-01-30', NULL, 'SARMIENTO 1452 8º \"A\"', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', 'C1042ABB', NULL, '0', '5949493', '5217-6810 (estudio)'),
(435, '12150', 'MARCOS A', 'JAIME MENA - SCAGLIARINI ', '(02374)630-541', '153-864-8289 (Matías llamar a el) // 155-994-4812 (Marcos)  ', 'matias.mena@losplatanos.com', 'MARINA ROXANA', 'SCAGLIARINI ', NULL, NULL, '', 1, '8293806', '0', NULL, NULL, 'GURRUCHAGA 515', 'BOULOGNE', 'BUENOS AIRES', 'ARGENTINA', '1609', 'marcosjaimemena@gmail.com', '0', '5951713', '0'),
(436, '12151', 'Mónica Beatriz', 'ERRAMOUNDEGUY - 0', '4290-7738', '1164571560', 'patriciaerramon@hotmail.com', '0', '0', NULL, NULL, '', 1, '29413881', '0', '1982-02-17', NULL, 'SENILLOSA 1639', 'C.A.B.A.', 'BUENOS AIRES', 'ARGENTINA', 'C1424BPG', NULL, '0', '5987314', '0'),
(437, '12152', 'GUSTAVO FRANCISCO', 'MUÑOZ - 0', '0', '155-460-4008', 'info@palecosa.com.ar', '0', '0', NULL, NULL, '', 1, '12991315', '0', '1959-03-11', NULL, 'JUANA MANSO 670 - 5º \'B3\'', 'CAPITAL FEDERAL', 'BUENOS AIRES', 'ARGENTINA', '0', 'gfmunoz@gmail.com', '0', '1346189', '0'),
(438, '12161', 'LUIS ENRIQUE', 'VARESE - FERRARI', '4798-6947', '114-538-1058 (Luis) - 114-915-1416 (Griselda)', 'luis@estudiovarese.com.ar', 'GRISELDA ALEJANDRA', 'FERRARI', NULL, NULL, '', 1, '13782595', '16056922', NULL, NULL, 'AV. SANTA FE 1878, PISO 1º\'\'A\'', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '0', NULL, '0', '6219567', '4793-2001'),
(439, '12165', 'GUSTAVO EDUARDO', 'NISENBOM - GARCIA ', '4659-0015/17 o 4621-1469', '155-329 7587 (Gustavo)', 'administracion@construman.com.ar', 'FERNANDA ALICIA', 'GARCIA ', NULL, NULL, '', 1, '16380461', '17936705', '1963-05-02', NULL, 'DE LA VIDALITA 1154 - PQUE. LELOIR', 'ITUZAINGO', 'BUENOS AIRES', 'ARGENTINA', '1714', 'administracion@construman.com.ar', '0', '0', '0'),
(440, '12166', 'VICTOR OSCAR', 'RODRIGUEZ - ', '4249-5250', '155-871-7084   ', 'vorodri@yahoo.com', '0', NULL, NULL, NULL, '', 1, '13761241', '0', '1959-06-19', NULL, 'PICHINCHA 2070', 'LANUS', 'BUENOS AIRES', 'ARGENTINA', '1824', '0', '0', '6292363', '0'),
(441, '12168', 'RAMIRO ALBERTO', 'REQUEJADO CARRERE - 0', '(02901)443-883 ', '(02901)154-86-116 ', 'ramiror@tantesara.com', '0', '0', NULL, NULL, '', 1, '23805357', '0', NULL, NULL, 'DE LA LUNA 1834', 'USHUAIA', 'TIERRA DEL FUEGO', 'ARGENTINA', '9410', 'almasa@tantesara.com // rdearriba@tantesara.com', '1472140', '0', '0'),
(442, '12170', 'VALERIA', 'RUSSO - RUSSO ', '0221-4177235 / 4577097', '0', 'russoadolfo@yahoo.com.ar', 'ANTONELA', 'RUSSO ', NULL, NULL, '', 1, '18772924', '28483315', NULL, NULL, 'CALLE 18 Nº 1431', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', 'arielstella73@hotmail.com', '0', '6017061', '0221-482-0165'),
(443, '12174', 'PATRICIA ELVIRA', 'HERNANDEZ - VASQUEZ ', '03487-427113', '(03487)15-64-4439   ', 'phprop@hotmail.com', 'RICARDO', 'VASQUEZ ', NULL, NULL, '', 1, '17136436', '14559385', NULL, NULL, '25 DE MAYO Nº 654', 'ZARATE', 'BUENOS AIRES', 'ARGENTINA', '2800', NULL, '0', '0', '0');
INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(444, '12176', 'MARIA SILVINA', 'VILLAREAL - COZZARIN ', '4942-8324/4346-7476', '153-148-9779   ', 'villarrealm@pseguros.com.ar', 'HORACIO', 'COZZARIN ', NULL, NULL, '', 1, '21647669', '0', '1970-08-23', NULL, 'MEXICO 2571', 'CAPITAL FEDERAL', 'CAPITAL FEDERAL', 'ARGENTINA', '1223', 'villarrealm@pseguros.com.ar', '0', '6423414', '4346-7476 Ella prov.seguros'),
(445, '12179', 'MIRTA', 'TAPIA - ', '(0291)454-8490', '(0291) 154-059-650 (Silvana)', 'smilozzi@yahoo.com', '0', NULL, NULL, NULL, '', 1, '6530012', '0', NULL, NULL, 'VIEYTES 407', 'BAHIA BLANCA', 'BUENOS AIRES', 'ARGENTINA', '8000', 'smilozzi@yahoo.com', '0', '6475625', '0'),
(446, '12182', 'MARIANO', 'SAPA - ', '4450-4900', '15-6860-9479   ', 'marianosapa@yahoo.com.ar', '0', NULL, NULL, NULL, '', 1, '26056988', '0', NULL, NULL, 'SANABRIA 4360', 'V.TESEI - HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', '0', NULL, '0', '0', '0'),
(447, '12192', 'FERNANDA', 'CLEMENT - CLEMENT', '(03327)483-472', '156-578-3621 (Fernanda)', 'ferclement@hotmail.com', 'ROBERTO', 'CLEMENT', NULL, NULL, '', 1, '14412157', '0', NULL, NULL, 'AV. GRAL PERON 7245', 'BENAVIDEZ', 'BUENOS AIRES', 'ARGENTINA', '1621', 'delfina_clement@hotmail.com // rober.clement@gmail.com', '0', '0', '0'),
(448, '12193', 'ADOLFO GUSTAVO (H)', 'LANUS - CASARES ', '0', '4577-2565/4792-2705   ', 'adolfoglanus@gmail.com', 'MARIA', 'CASARES ', NULL, NULL, '', 1, '27255312', '0', '1979-06-26', NULL, 'RICARDO GUTIERREZ 2423', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', NULL, '0', '0', '0'),
(449, '12194', 'GUSTAVO ALBERTO', 'SOFFICI - ', '0', '154-493-7036', 'gsoffici@gmail.com', '0', NULL, NULL, NULL, '', 1, '21836145', '0', NULL, NULL, 'RONDEAU 163', 'LAVALLOL', 'BUENOS AIRES', 'ARGENTINA', '1836', NULL, '0', '0', '0'),
(450, '20000', 'MONICA', 'GUZMAN, MONICA - HAGER , GERARDO', '4764-1555', '   ', 'gerardohager@nialtec.com.ar', 'GERARDO', 'HAGER ', NULL, NULL, '', 1, '17772166', '0', '1965-12-18', NULL, 'ROSARIO 4346', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', 'B1653AEF', NULL, '6353-01374', '0', '4484-4544 FAX. HAGER'),
(451, '10099', 'HUGO VICENTE', 'RUESGA - BAIONI', '02923-498754/498024', '02923-15640531   ', 'ruesga@ruesga.com.ar', 'ROSSANA VELIA', 'BAIONI', NULL, NULL, '', 1, '14501501', '0', NULL, NULL, 'SANTAMARINA 99', 'PUAN', 'BUENOS AIRES', 'Argentina', 'B8180BDA', NULL, '6353-01229', '1693441', '0'),
(452, '10435', 'DANIEL ERNESTO', 'BUSTOS - CUBO ', '(0264)423-5094', '(0264) 154-43-2121 (Andrea)', 'andrebustos@gmail.com', 'GRACIELA INES', 'CUBO ', NULL, NULL, '', 1, '8327076', '11482685', '1950-02-20', '1954-05-31', 'AV. LDOR. SAN MARTIN 3463 OESTE RIVADAVIA', 'SAN JUAN', 'SAN JUAN', 'ARGENTINA', '5400', NULL, '0', '2126772', '0'),
(453, '7', 'CLAUDIA NOEMI', 'AGUIRRE, CLAUDIA NOEMI', '4768-4051', '155-5953693', 'licclauaguirre@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '21470667', NULL, '1970-06-04', NULL, 'JEAN JEAURES 2717', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', NULL, '4235-00586', '0', '4453-1324 // FAX 5480-5101'),
(454, '974', 'NESTOR', 'ALARCON - ARATA', '(03329)424-814', '(03329)155-11-711 (Nestor)', 'pirocuatro@gmail.com', 'MABEL', 'ARATA', NULL, NULL, '', 2, '4634431', '6200895', '1944-05-01', '1950-04-30', 'BOTTARO 852', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', 'pirodos@hotmail.com', '3443-2443', '0', '(03329)155-27-288 (Mabel)'),
(455, '9', 'DANIEL HUGO', 'ALBANO - ', '4943-6810', '154-439-0333', 'toxytox@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '12927744', '0', NULL, NULL, 'AVDA BRASIL 2817', 'CABA', 'CABA', 'ARGENTINA', '1260', '0', 'NO TIENE', '0', NULL),
(456, '811', 'MONICA', 'ALEJOS - CARDARELLI', '4751-2065', '156-564-7842', 'alejosmonica@hotmail.com', 'ANTONIO', 'CARDARELLI', NULL, NULL, '', 2, '12849853', '11172988', '1958-10-23', '1954-08-31', 'BELGRANO 5918', 'PALOMAR', 'BUENOS AIRES', 'ARGENTINA', '1684', '0', 'NO TIENE', '0', NULL),
(457, '16', 'OSVALDO FABIAN', 'ALMEIDA - GRECO', '4566-0079', '154-421-6530', 'sgtosanders@gmail.com', 'NÉLIDA CRISTINA', 'GRECO', NULL, NULL, '', 2, '11666782', '0', '1960-12-13', NULL, 'BERMUDEZ 1669 ', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', '7461-00898', '0', '4988-1385'),
(458, '27', 'CELIA VIVIANA', 'ANTON - ', '(0379)442-8779', '0379-154686357', 'vivianaanton193@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '12181483', '0', NULL, NULL, 'STA. FÉ. 935 PISO 4 DEPTO. B', 'CORRIENTES', 'CORRIENTES', 'ARGENTINA', '3400', '0', '4235-00589', '0', NULL),
(459, '31', 'CARLOS MARCELO', 'ARANDA - ', '4247-3900', '154-042-0581', 'marceloaranda44@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '13263352', '0', NULL, NULL, 'ONCATIVO 2143', 'LANUS ESTE', 'BUENOS AIRES', 'ARGENTINA', '1824', '0', 'NO TIENE', '0', NULL),
(460, '36', 'VERÓNICA ROSA', 'ARTUSO - CESANELLI', '4791-2768', '153-092-2901 (Verónica) // 153-092-2900 (Jorge) ', 'deanastasie@yahoo.com.ar', 'JORGE', 'CESANELLI', NULL, NULL, '', 2, '11773580', '10424487', '1955-02-11', '1952-05-17', 'ROQUE SAENZ PEÑA 2884', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', 'NO TIENE', '0', '4794-3138 (negocio)'),
(461, '46', 'CARLOS LUIS ', 'BARCALA - GIURITZA', '(0237)462-6550 ', '156-309-9588 (Marta)', 'mmbarcala@yahoo.com.ar', 'MARTA', 'GIURITZA', NULL, NULL, '', 2, '4929787', '0', NULL, NULL, 'VICENTE LOPEZ Y PLANES 525', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', '1744', '0', '4235-00594', '0', NULL),
(462, '980', 'MIGUEL ANGEL', 'BARROSO - ', '(0299) 442-1839', '116-559-3162', 'elisadanielajury@gmail.com', 'ELISA', NULL, NULL, NULL, '', 2, '16408188', '0', '1963-07-27', NULL, 'LA RIOJA 983 ', 'NEUQUEN', 'NEUQUÉN', 'ARGENTINA', '8300', '0', 'NO TIENE', '0', '0'),
(463, '53', 'JORGE ALBERTO', 'BELTRAMI - MAGNO', '(0221)469-2031', '(0221)156-042-031', 'cam1959@yahoo.com.ar', 'CECILIA ALEJANDRA', 'MAGNO', NULL, NULL, '', 2, '11773832', '0', NULL, NULL, 'HORACIO CESTINO 713', 'ENSENADA', 'BUENOS AIRES', 'ARGENTINA', '1925', 'chechub3@hotmail.com', '4235-00596', '0', '(0221)424-4775 // FAX (0221)424-4775'),
(464, '54', 'MARIO', 'BELZUNCE - ', '4798-2181', '156-829-7149', 'mebelzunce@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 2, '6819708', '0', NULL, NULL, 'AVDA SANTA FE 1435 PISO 11 \"B\"', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', ' madbel@hotmail.com; Mariana.Belzunce@allianz.com.ar; suantonio09@hotmail.com', '7461-00255', '0', NULL),
(465, '58', 'HORACIO DANIEL', 'BETOLDI - FUSARO', '4612-0174', '153-148-7891', 'hbetoldi@yahoo.com.ar', 'SILVIA CECILIA', 'FUSARO', NULL, NULL, '', 2, '13404288', '16453737', '1959-06-10', '1963-08-12', 'AVDA. VARELA 586 ', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '4235-00597', '0', NULL),
(466, '981', 'ENRIQUE JOSE', 'BOFFA - NOAT', '(03329)426-833', '(03329)153-04-402 (Susana)', 'nicoboffa@gmail.com', 'SUSANA INES', 'NOAT', NULL, NULL, '', 2, '4690242', '5886501', NULL, '1949-01-18', 'LAS HERAS 730', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', '0', '4235-02505', '0', '0'),
(467, '977', 'JULIO OMAR', 'BORDIÑUK - GENTILE', '4672-2065', '156-496-0438', 'marite.gentile@gmail.com', 'MARIA TERESA', 'GENTILE', NULL, NULL, '', 2, '0', '17737415', '1964-07-11', NULL, 'JOSÉ BONIFACIO 4170 EDIFICIO 12 \"B\" PISO 2º DTO \"19\"', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', 'NO TIENE', '0', '4323-4731'),
(468, '860', 'GUSTAVO EDUARDO', 'BRAGHIRIOLI - ', '4571-1569 ', '155-013-4236', 'gusvavi@gmail.com', NULL, NULL, NULL, NULL, '', 2, '14619424', '0', '1961-12-18', NULL, 'PASAJE EL FOJON 4431', 'CABA', 'CABA', 'ARGENTINA', '1419', '0', 'NO TIENE', '0', NULL),
(469, '78', 'HILDA IRMA', 'BROUSSAIN, HILDA IRMA - MONTEAGUDO, CARLOS', '4903-6526', '154-492-3542', 'hibroussain@hotmail.com', 'CARLOS', 'MONTEAGUDO', NULL, NULL, '', 2, '12825018', '8521185', '1957-01-28', '1950-12-09', 'JOSÉ MARÍA MORENO 483  8º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', 'NO TIENE', '0', NULL),
(470, '87', 'MARIA', 'CALO - PRESTA', '4431-1978 (amiga)', '0', '0', 'FELIX PEDRO', 'PRESTA', NULL, NULL, '', 2, '13404288', '0', '1941-07-16', '1938-07-28', 'J. BAUTISTA ALBERDI 1.274  4ª  \" C \"', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '4235-00524', '0', '4642-9977 (tarde)'),
(471, '91', 'EDUARDO OMAR', 'CAMPI - GARCIA', '(34963)444-142', '156-666-0300 (Juan) // 34600577946', 'jmajluf@contextoempresarial.com.ar', 'ANA MARIA', 'GARCIA', NULL, NULL, '', 2, '12727335', '0', '1956-12-11', NULL, 'C/ PIANISTA AMPARO ITURBI 23 PUERTA 1', 'VALENCIA', 'ESPAÑA', 'ARGENTINA', '46007', 'axel.ono@hotmail.es', 'NO TIENE', '0', '4105-9100/9141/ Juan Majluf // FAX 4105-9120'),
(472, '95', 'MARISA ROSANA', 'CANGIANO - ', '4203-8840', '156-423-0508 // 155-424-2184 (Jorge)', 'gerencia@m4editorial.com.ar', NULL, NULL, NULL, NULL, '', 2, '17662952', '0', '1966-02-14', NULL, 'JUAN B. ALBERDI 125', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', '0', '4235-04296', '0', '4357-2316'),
(473, '96', 'NESTOR HORACIO', 'CAPILLAS - RIOS', '2062-2861', '154-072-0754 (Maite)', 'maitecapillas@hotmail.com', 'MAITE', 'RIOS', NULL, NULL, '', 2, '4859206', '12787821', NULL, NULL, 'CONDARCO 551', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', '1834', '0', '4235-01847', '0', '153-832-1217 (Leonel)'),
(474, '101', 'RAUL MARCELO', 'CARBALLEIRA - GONZALEZ', '4777-8146', '154-418-1076 (Mónica)', 'magonz@intramed.net', 'MÓNICA ALICIA', 'GONZALEZ', NULL, NULL, '', 2, '11387111', '0', NULL, NULL, 'AREVALO 2706  6PISO DTO\"A\"', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', '155-039-7723'),
(475, '201', 'EDUARDO ALBERTO', 'CARBONE - JAJCEVIC', '4701-6631', '156-289-4751', 'eduardo.carbone@fibertel.com.ar', 'MARIANA', 'JAJCEVIC', NULL, NULL, '', 2, '17260434', '0', NULL, NULL, 'MOLDES 3433 8 PISO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1429', '0', '4235-01901', '0', '5236-6062'),
(476, '976', 'MONICA', 'CARNEVALE, MONICA', '4259-4516', '155-1839976', 'monibea2015@gmail.com', NULL, NULL, NULL, NULL, '', 2, '14341062', NULL, NULL, NULL, 'CRERRITO 1040', 'BERNAL', 'BUENOS AIRES', 'ARGENTINA', '0', '0', '3443-02491', '0', NULL),
(477, '105', 'RICARDO', 'CARRANZA - RICCIO', '4551-1116', '0', 'ricardocarranza19@yahoo.com.ar', 'ANGELA SUSANA', 'RICCIO', NULL, NULL, '', 2, '4264586', '4944119', NULL, NULL, 'CESPEDES 3301', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', 'Trabajo hijo: 5480-5000'),
(478, '987', 'MARIA EVA', 'CARRIZO - 0', '0', '155-925-2335 (No corresp. A abonado en serv)', '0', '0', '0', NULL, NULL, '', 2, '6496244', '0', NULL, NULL, 'ISABEL LA CATOLICA 1081 - 25º \'Q\'', 'CABA', 'CABA', 'ARGENTINA', '1268', '0', 'NO TIENE', '0', '152-329-6003'),
(479, '107', 'JOSE', 'CARRIZO DE ALCE - ALCE', '4642-1049 // 153-695-8245 (María)', '153-660-8568 // 11-2703-6744  (José) // 153-216-9148 (Fernando)', 'fernandobaamondeproducciones@gmail.com', 'MARIA', 'ALCE', NULL, NULL, '', 2, '0', '0', NULL, NULL, 'BENITO JUAREZ 4337', 'CABA', 'CABA', 'ARGENTINA', '1419', 'maria_m_alce@hotmail.com ; martin121799@gmail.com', '4235-02356', '0', 'José: 4505-2545'),
(480, '111', 'ALEJANDRO', 'CASTELO - CALVO', '4799-1837', '155-342-1922 (Marcela)', 'alecastelo@yahoo.com.ar', 'MARCELA', 'CALVO', NULL, NULL, '', 2, '17202425', '17023309', '1964-09-15', '1965-01-09', 'JOSÉ MARÍA PAZ 3426 ', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '4235-02089', '0', '155-623-1535 '),
(481, '118', 'RUBEN EDUARDO', 'CASUCCIO - ', '2196-1160', '156-012-8335', 'recasuccio@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '14857065', '0', NULL, NULL, 'SARGENTO CABRAL 1915', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', NULL),
(482, '119', 'TERESA', 'CAZON - ', '(0387)431-4942', '(0387)155-937-640', 'grbinda@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '4452436', '0', NULL, NULL, 'PASAJE YAPEYÚ 1861', 'SALTA', 'SALTA', 'ARGENTINA', '4400', '0', '4235-02191', '0', '0'),
(483, '122', 'Eduardo', 'CHAU - ', '4708-9536', '155-0120638', 'edujchau@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '31071250', NULL, NULL, NULL, NULL, NULL, NULL, 'ARGENTINA', '0', 'eypul14@gmail.com', 'NO TIENE', '0', NULL),
(484, '130', 'FLORINDA', 'CIMINO - ', '(0348)446-8195', '114-176-5217 // 156-461-0335', 'flory_cimino@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '17305800', '0', '1965-09-03', NULL, '9 DE JULIO 1426 ', 'SAN FERNANDO', 'BUENOS AIRES', 'ARGENTINA', '1646', '0', '4235-04186', '0', '0'),
(485, '136', 'BLANCA IRIS', 'COBOS - ', '(0220)476-2122', '154-409-3350', 'blanky111@hotmail.com.ar', NULL, NULL, NULL, NULL, '', 2, '12312889', '0', '1958-03-17', NULL, 'CORONEL FELIX BRAVO 1152', 'GRAL. LAS HERAS', 'BUENOS AIRES', 'ARGENTINA', '1741', '0', '4235-03732', '0', 'FAX (0220)476-2158'),
(486, '144', 'JOSE', 'COLUCCI - CANEPA', '4901-9503', '0', 'maru_1707@hotmail.com', 'ALICIA BEATRIZ', 'CANEPA', NULL, NULL, '', 2, '14223704', '2974107', '1931-07-22', '1932-03-16', 'AVDA. LA PLATA 67 7º PISO  DTO 1', 'CABA', 'CABA', 'ARGENTINA', '1184', '0', 'NO TIENE', '0', NULL),
(487, '154', 'ROBERTO GERMAN', 'CORTON - ', '4656-4676', '155-025-6006', 'rgcorton@ciudad.com.ar', NULL, NULL, NULL, NULL, '', 2, '4264586', '0', NULL, NULL, 'PIZZURNO 61', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', '1704', 'roberto.corton@osplad.org.ar', '4235-00528', '0', '5032-9422'),
(488, '162', 'MABEL ALICIA', 'CREGO - ELISEI', '4300-1169', '1554-813036', 'mcrego@crexelups.com', 'HUGO', 'ELISEI', NULL, NULL, '', 2, '11768775', '10460769', '1955-03-21', '1952-09-10', 'ISABEL LA CATOLICA 61 DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1268', 'mabelcrego@fibertel.com.ar', 'NO TIENE', '0', '4300-5575 // FAX 4307-8243'),
(489, '164', 'MILA FLORENTINA', 'CROCCI - CEDIO', '4982-0738', '153-2715474', 'milaflorentina@yahoo.com.ar', 'DANIEL CARLOS', 'CEDIO', NULL, NULL, '', 2, '4479411', '4141744', '1942-12-15', NULL, 'AVDA. DIAZ VELEZ 4537  3º PISO  DTO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', 'NO TIENE', '0', NULL),
(490, '168', 'SIRO', 'CUZZONI - ', '4854-3178', '154-479-9176', 'oficinavirtualsc@gmail.com', NULL, NULL, NULL, NULL, '', 2, '4281070', '0', '1938-07-21', NULL, 'AVDA. JUAN B. JUSTO 2819 2º PISO DTO \"D\"', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '4235-04388', '0', NULL),
(491, '170', 'FERNANDO JOSE', 'DA PONTE - ACUÑA', '(0221)474-1435', '0', 'ana.acunia@gmail.com', 'ANA', 'ACUÑA', NULL, NULL, '', 2, '10230649', '0', NULL, NULL, '417 Y 135', 'VILLA ELISA', 'BUENOS AIRES', 'ARGENTINA', '0', 'ferjodapo@gmail.com', '4235-02811', '0', NULL),
(492, '850', 'NORMA RITA', 'D\'ABBISOGNO - ', '4699-2938', '156-596-3509', 'andrea.santillan5@gmail.com', NULL, NULL, NULL, NULL, '', 2, '3577215', '0', '1936-11-14', NULL, 'CAVIA 496- LOMAS DEL MIRADOR', 'LOMAS DEL MIRADOR', 'BUENOS AIRES', 'ARGENTINA', '1752', '0', 'NO TIENE', '0', NULL),
(493, '173', 'NORA NOEMI', 'DABOVE - ', '(0237)484-1578', '155-618-3152', 'dabovenora@gmail.com', NULL, NULL, NULL, NULL, '', 2, '4257283', '0', NULL, NULL, 'PUEYRREDON 368', 'GENERAL RODRIGUEZ', 'BUENOS AIRES', 'ARGENTINA', '1748', '0', 'NO TIENE', '0', '(0237)485-0058//484-0124 // FAX (0237)485-0058 (Yerno Sr. Minet)'),
(494, '179', 'ADALBERTO CLAUDIO', 'DAVICO - MELARAGNO', '4628-4975', '153-478-1695', 'davicoclaudio@hotmail.com', 'DORA ESTELA', 'MELARAGNO', NULL, NULL, '', 2, '12021332', '12022134', '1958-01-16', '1958-05-24', 'MACHADO 1616', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', 'NO TIENE', '0', '4483-4218/4489-2531 '),
(495, '182', 'ALDO ROBERTO', 'DAVIDOVSKY - ', '4855-1140', '155-720-6331', 'Aldo_Davidovsky@bat.com', NULL, NULL, NULL, NULL, '', 2, '13924511', '0', NULL, NULL, 'LOYOLA 430 2º PISO DTO: \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1414', 'abrahammotl@hotmail.com', '0', '0', '4724-8444 int: 2253'),
(496, '189', 'LILIANA', 'DE SOUSA MARTINS - ', '4392-0308', '155-016-7132', 'lilidesousa@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 2, '14937589', '0', '1961-09-27', NULL, 'LAMADRID 901', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', 'lilianadesousa@speedy.com.ar', '0', '0', NULL),
(497, '200', 'MIGUEL ANTONIO', 'DIAB - BRESCIA', '0341-4304182', '(0341) 153-168-900', 'myadiab@hotmail.com', 'AIDA', 'BRESCIA', NULL, NULL, '', 2, '11124754', '0', NULL, NULL, 'RICHIERE 1284 ', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', '0', '0', 'FAX (0341)435-9718'),
(498, '597', 'DIANA ANA', 'DOKMETZIAN - RECCIO', '4771-9876', 'Diana: 1541-996173', 'dianadok@gmail.com', 'FIDEL CARLOS', 'RECCIO', NULL, NULL, '', 2, '5673813', '0', '1947-08-05', '1953-12-25', 'CASA: JULIÁN ALVAREZ 1019 NEGOCIO: JULIÁN ALVAREZ 1017', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '4235-03739', '0', '4771-0237/'),
(499, '972', 'ERNESTO JUAN', 'ECHEVERRIA - ', '4760-2722', '156-042-2934', 'c_sieche@live.com.ar', NULL, NULL, NULL, NULL, '', 2, '8253224', '0', '1946-04-15', NULL, 'SAN LORENZO 1987- VILLA BALLESTER CP 1653', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', 'NO TIENE', '0', '4768-5121 - FAX 4847-3556'),
(500, '224', 'ALICIA ENRIQUETA', 'ETCHART - FLORES', '4631-6897', '0', 'aliciaenriquetaetchart@gmail.com', 'JORGE', 'FLORES', NULL, NULL, '', 2, '6361824', '7596903', '1950-04-29', '1947-05-06', 'PASAJE RAWSON JCAJAL 1220', 'CABA', 'CABA', 'ARGENTINA', '1406', 'daniruock@hotmail.com', '0', '0', '4613-6848'),
(501, '792', 'ALDO', 'FABIETTI - YOVINE', '4750-2321', '154-947-0308', 'seguridad_higiene_industrial@yahoo.com.ar', 'GLADYS', 'YOVINE', NULL, NULL, '', 2, '13431696', '13492097', NULL, NULL, NULL, NULL, NULL, 'ARGENTINA', '0', '0', 'NO TIENE', '0', '4759-9813 (trabajo esposo)'),
(502, '857', 'SUSANA BEATRIZ', 'FAVERIO - VILLALOBOS', '6380-6072', '155-954-3111', 'susanafaverio@yahoo.com.ar', 'JORGE A.', 'VILLALOBOS', NULL, NULL, '', 2, '12079929', '12983346', '1956-03-20', '1957-10-09', 'MAZA 694 ', 'CABA', 'CABA', 'ARGENTINA', '1220', '0', '0', '0', NULL),
(503, '766', 'Rosa', 'ALVAREZ - FERNANDEZ', '4792-9731 // 4793-1902 (Rosa)', '1555-148390  // 155-730-2868 (Rosa)', 'rosatrini@hotmail.com', 'María Ester', 'FERNANDEZ', NULL, NULL, '', 2, '6032479', '0', '1953-06-13', '1952-06-14', 'LASISLAO MARTINEZ 276 P.B. \"C\"', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '0', '0', '4792-0107'),
(504, '237', 'GERONIMO RAMON', 'FERNANDEZ - ', '4483-2679', '155-489-0456', 'docerobles@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '11862908', '0', NULL, NULL, 'RIO PIEDRAS 255, 2º C', 'MORÓN', 'BUENOS AIRES', 'ARGENTINA', '1708', '0', 'NO TIENE', '0', NULL),
(505, '238', 'JORGE LUIS', 'FERNANDEZ - ', '4244-8151', '154-969-4170', 'lena3vi@gmail.com', NULL, NULL, NULL, NULL, '', 2, '6082330', '0', NULL, NULL, 'JUNCAL 1044 - ', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', '1834', 'Jorge.FernandezBaldi@bancogalicia.com.ar', '4235-03391', '0', NULL),
(506, '240', 'MANUEL', 'FERNÁNDEZ LOPEZ - CARUSSO', '4373-5069', '155-402-7478 // 155-840-7640 (Gonzalo)', 'lilianaecaruso7@gmail.com', 'LILIANA EDITH', 'CARUSSO', NULL, NULL, '', 2, '12021332', '14157199', '1960-03-15', '1960-03-24', 'MORENO 1817 3º \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1094', 'fliaferlop@yahoo.com.ar', 'NO TIENE', '0', '4774-4772'),
(507, '725', 'Graciela', 'FERNANDEZ - MISURACA', '4503-7112', '153-165-6027 (Graciela)', 'gracielafergarat@gmail.com', 'Ariel', 'MISURACA', NULL, NULL, '', 2, '13741129', '14151681', NULL, NULL, '0', '0', 'BUENOS AIRES', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '154-539-7187 (Ariel)'),
(508, '244', 'MARIA CRISTINA', 'FERRANDINI - MARTINEZ', '4582-7222', '155-026-0544', 'cristina.ferrandini@gmail.com', 'ADRIAN ENRIQUE', 'MARTINEZ', NULL, NULL, '', 2, '6647886', '4979862', NULL, NULL, 'BOLIVIA 2580', 'CABA', 'CABA', 'ARGENTINA', '1417', 'envasadorasalvacio.rosario@hotmail.com (Jorge)', '4235-00620', '0', NULL),
(509, '274', 'CRISTINA CELIA', 'FERRARI DE MIR - MIR', '(0221)479-3203', '(0221)154-084-287', 'cristinacmir@gmail.com', 'HECTOR JAIME', 'MIR', NULL, NULL, '', 2, '11431446', '11123892', '1954-12-27', '1954-05-02', 'CALLE 139 Nº 669 ENTRE 45 Y 46', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', '0', '4235-00580', '0', NULL),
(510, '248', 'REINALDO CARLOS', 'FERRAUDI - ', '4450-2535/0427', '155-126-4902', 'rferraudi@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '14565128', '0', NULL, NULL, 'INTENDENTE CARRERE 775 -HAEDO-  1706', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', NULL),
(511, '412', 'MONICA KARINA (FALLECIÓ)', 'FERRERO - Andrea y Gladys hermanas ellas se van a ocupar', '5197-7102 (Andrea)', '11-49499297 (Andrea)', 'andyferrero@hotmail.com', NULL, 'Andrea y Gladys hermanas ellas se van a ocupar', NULL, NULL, '', 2, '22362162', '0', NULL, NULL, 'AV. A.T. DE ALVEAR 430  1ER  PISO DTO 1', 'DON TORCUATO', 'BUENOS AIRES', 'ARGENTINA', '1406', '0', 'NO TIENE', '0', '0'),
(512, '252', 'HUGO', 'FILIPOVIC - POMPA', '42068869', 'Marisa:155-803-2277 // Hugo: 153-630-8328', 'hugo_filipovic@yahoo.com.ar', 'MARISA', 'POMPA', NULL, NULL, '', 2, '14701393', '0', NULL, NULL, 'Pasaje Sarrachaga 6019 1º3', 'WILDE', 'BUENOS AIRES', 'ARGENTINA', 'B1875ACE', '0', '0', '0', '4381-4002 INT 42'),
(513, '263', 'ELIDA CESILIA', 'FRASSON - OTERO', '4571-7678', '154-030-0276 (José)', 'esteticadalauzier@yahoo.com.ar', 'JOSE CARLOS', 'OTERO', NULL, NULL, '', 2, '3566734', '4401333', '1937-07-01', '1943-01-09', 'MARISCAL FRANCISCO SOLANO LÓPEZ 2759', 'CABA', 'CABA', 'ARGENTINA', '1419', '0', '4235-00534', '0', '4574-4188'),
(514, '272', 'MÓNICA', 'GABAY - ', '4792-8044', '154-550-5963', 'monagabay@yahoo.com', NULL, NULL, NULL, NULL, '', 2, '21482725', '0', NULL, NULL, 'FRAY LUIS BELTRAN 850', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '0', '0', '5235-0761'),
(515, '275', 'ROSA', 'GARBELLANO - HIDALGO', '4521-2247', '155-175-0027', 'rosag5916@gmail.com', 'JORGE LUIS', 'HIDALGO', NULL, NULL, '', 2, '3757717', '0', '1938-06-25', '1933-12-10', 'COMBATIENTES DE MALVINAS 3970 4TO PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1431', 'cidepcursos@gmail.com // fabian12@fibertel.com.ar // laurapenapsp@gmail.com', '0', '0', 'Hijo Fabián: 4555-7303 - FAX 4897-4653/54'),
(516, '296', 'MARTHA BEATRIZ', 'GATTI - PUGNALI', '4242-2679', '116-027-2838', 'gattimar12@yahoo.com.ar', 'ARMANDO LUIS', 'PUGNALI', NULL, NULL, '', 2, '13935235', '0', NULL, NULL, 'CABRERA 1053', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', NULL, 'NO TIENE', NULL, '0'),
(517, '303', 'SILVANA ROSA', 'GENTILE - ', '4584-5780', '156-970-3441', 'silvanag55@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '12013725', '0', NULL, NULL, 'FRAGATA SARMIENTO 950 ', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', '0', '0', '4585-4007 - FAX 4585-6593'),
(518, '307', 'DANIEL OSCAR', 'GILBERTO - COSTA', '4650-3151', '156-473-5014', 'dogilberto@intramed.net', 'LILIANA ROSA', 'COSTA', NULL, NULL, '', 2, '11597935', '0', NULL, NULL, 'DIRECTORIO 591-', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '0', '0', NULL),
(519, '309', 'JORGE ALBERTO', 'GIORDANO - RELLA', '4300-3015', '154-474-8036', 'jgiordano@jagi.com.ar', 'VIVIANA ZULEMA CONCEPCION', 'RELLA', NULL, NULL, '', 2, '14195159', '17636056', '1960-09-15', '1966-01-06', 'AVDA. MARTÍN GARCIA 560  8º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1268', '0', '0', '0', '5218-4506/07'),
(520, '314', 'MARCELO OSCAR', 'GIULIANO - ', '4201-7067', '155-054-7106 M // 155-228-5258 (Horacio)', 'estebangiuli@hotmail.com', 'LIDIA', NULL, NULL, NULL, '', 2, '22364475', '0', NULL, NULL, 'GRAL. LAVALLE 343/345', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', 'mgiuliano@live.com.ar', '4235-04779', '0', 'Esteban: 156-1737897'),
(521, '317', 'RICARDO LUIS', 'GOLTZMAN - CHESZES', '0', '154-475-5580', 'ricardo.goltzman@gmail.com', 'ADELA MARTA', 'CHESZES', NULL, NULL, '', 2, '10130604', '13403623', NULL, NULL, 'GOMEZ DE FONSECA 577', 'CABA', 'CABA', 'ARGENTINA', '1407', 'PabloLujan@mundial.com.ar', '4235-02848', '0', '4303-3142'),
(522, '319', 'CARLOS DANIEL', 'GONCEBATT - ALAYA', '4257-2698', '153-140-8209 (Cristina)', 'cris-alaya@hotmail.com', 'CRISTINA REGINA', 'ALAYA', NULL, NULL, '', 2, '11926524', '12267092', '1955-11-16', '1956-02-08', 'ANTARTIDA ARGENTINA 176 ', 'EZPELETA', 'BUENOS AIRES', 'ARGENTINA', '1882', '0', '0', '0', NULL),
(523, '325', 'MARIA DE LUJAN', 'GONZALEZ ORONO - MUR', '(02945)48-0410', '156-355-2820', 'orosimariamaria@live.com.ar', 'HUGO ALBERTO', 'MUR', NULL, NULL, '', 2, '5290041', '4394095', '1946-05-06', '1942-03-11', 'Juan Manuel de Rosas 342', 'TREVELIN', 'CHUBUT', 'ARGENTINA', '9203', '0', '3169-5999', '0', 'M. Lujan: 153-553-0523'),
(524, '880', 'VIVIANA GRACIELA', 'GRANDI - LOVERAS', '(0341)461-4466', '(0341)153-60-0266 (Jorge) // (0341)155-86-3375 (Viviana)', 'grandiviviana@hotmail.com', 'JORGE', 'LOVERAS', NULL, NULL, '', 2, '12736331', '0', NULL, NULL, 'OLEGARIO V. ANDRADE 771- ', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', 'NO TIENE', '0', '0'),
(525, '328', 'JOSE ANTONIO', 'GRILLO - ', '4650-9313', '155-325-1060', 'grilloja@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '93454346', '0', NULL, NULL, 'Buchardo  1807', 'VILLA LUZURIAGA', 'BUENOS AIRES', 'ARGENTINA', '1754', '0', 'NO TIENE', '0', NULL),
(526, '330', 'EDUARDO DANIEL', 'GRUDEN - ', '4659-0912 (Estudio Gruden)', '154-188-7901 (Daniel)', 'estudiodeg@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '10927686', '0', NULL, NULL, 'DR. PEDRO CHUTRO 654', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', '4650-6135'),
(527, '335', 'GONZALO, DESIDERIO', 'GUMA, GONZALO, DESIDERIO', '4286-2984', '156-537-3517', 'guma.32@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '24569152', NULL, NULL, NULL, 'EJERCITO DE LOS ANDES 640', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', '0', '4235-02670', '0', NULL),
(528, '985', 'DANIEL', 'HERNANDEZ - MAS BENGOECHEA', '155-709-1015 (Daniel)', '156-350-5808 (Cecilia)', 'daniel_hernandez@fibertel.com.ar', 'CECILIA', 'MAS BENGOECHEA', NULL, NULL, '', 2, '22750222', '26258035', '1972-04-15', '1977-09-18', 'JOSÉ HERNANDEZ 2045 - 5º \'A\'', 'CABA', 'CABA', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '0'),
(529, '341', 'CARLOS DANIEL', 'HERRERA - ', '4460-1285', '155-327-6973', 'chyasociados@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '12702377', '0', '1956-12-15', NULL, 'Perito Moreno 1567', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', '1704', '0', '0', '0', '2077-8278'),
(530, '344', 'ESTEBAN', 'HETENYI - DE LA HOZ', '4772-5078', '0', 'estebanhetenyi@yahoo.com.ar', 'ALICIA BEATRIZ', 'DE LA HOZ', NULL, NULL, '', 2, '14768504', '5255523', '1946-11-03', '1945-11-05', 'MAURE 2265', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', '4394-0102 - FAX 4772-5078'),
(531, '982', 'LAURA BEATRIZ', 'HONEKER - ', '4922-7829', '154-065-4894 (Laura)', 'lhoneker@gmail.com', NULL, NULL, NULL, NULL, '', 2, '16144676', '0', NULL, NULL, 'PASAJE REPUBLICA 535', 'CABA', 'CABA', 'ARGENTINA', '1424', 'santoja64@gmail.com', 'NO TIENE', '0', '156-118-6409 (Marcelo)'),
(532, '345', 'JOSE LUIS', 'HORNIK - ', '4862-3742', '156-094-7735 (José) ', 'hornikjoseluis@gmail.com', NULL, NULL, NULL, NULL, '', 2, '11885326', '0', NULL, NULL, 'CORRIENTES 4280 PISO 9 DEP. A  ', 'CABA', 'CABA', 'ARGENTINA', '1195', 'hornik.vivian@gmail.com', '0', '0', '011-2146-3633 (negocio fliar)'),
(533, '990', 'ROBERTO', 'HÜNICKEN - 0', '(0351) 422-8043', '(0351) 155-95-9427 ', 'robhunicken@yahoo.com.ar', '0', '0', NULL, NULL, '', 2, '13708207', '0', NULL, NULL, '0', '0', '0', 'ARGENTINA', '0', 'robhunicken@yahoo.com.ar', '2777-00129', '0', '0'),
(534, '347', 'FAUSTINO', 'IKEI - TAMANAHA', '0', '116-798-6784 (Sabrina)', 's_ikei@hotmail.com', 'ALICIA ELENA', 'TAMANAHA', NULL, NULL, '', 2, '0', '10260636', '1952-02-15', NULL, 'AVDA ESTADO DE ISRAEL 4283 2º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1425', '0', 'NO TIENE', '0', '5282-0471 (Ofic. Sabrina)'),
(535, '256', 'RENE MATILDE', 'INDA - ', '4981-1408', '113-202-3028 (Martín)', 'fortimartin@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '2646676', '0', NULL, NULL, 'EDUARDO ACEVEDO   269 1ª  \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', 'NO TIENE', '0', 'Martin Forti: 155-1420364 (EQUIV)'),
(536, '984', 'HERMELINDA ROSANA', 'ACOSTA - 0', '4953-1550', '155-017-0183', 'racosta@trabajo.gob.ar', '0', '0', NULL, NULL, '', 2, '17839569', '0', NULL, NULL, 'Av. Rivadavia 2069 13ºB', 'CABA', 'CABA', 'ARGENTINA', '1033', '0', '0', '0', '0'),
(537, '355', 'OLGA', 'JACOBSON - ', '(0341)455-6456', '(0341)155-77-4379', 'olgajacobson@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '13488677', '0', '1957-09-28', NULL, 'URIARTE 841', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', '*4235-00675', '0', NULL),
(538, '358', 'JOSE', 'JANICEK - KOLLAR', '4256-3127', '0', 'josejanicek@yahoo.com.ar', 'ELENA', 'KOLLAR', NULL, NULL, '', 2, '18772192', '18772194', '1934-07-19', '1936-02-26', 'CALLE 9 Nº  4950', 'BERAZATEGUI', 'BUENOS AIRES', 'ARGENTINA', '1884', '0', '4235-00576', '0', NULL),
(539, '364', 'ELIAS', 'KAPLAN - MOSCOVITZ', '4524-1696', '155-177-3295 // 156-802-7774 (Darío)', 'dkaplan@ort.edu.ar', 'ELSA', 'MOSCOVITZ', NULL, NULL, '', 2, '8242388', '5696071', '1945-07-16', '1947-06-26', 'Olazabal 5039', 'CABA', 'CABA', 'ARGENTINA', '1431', '0', '0', '0', '155-160-8982 - FAX 4524-1696'),
(540, '365', 'SUSANA', 'KAPLAN - PEREDO', '4743-9779', '154-029-7873', 'susanakp44@gmail.com', 'SALVADOR ISMAEL', 'PEREDO', NULL, NULL, '', 2, '8309814', '8256758', '1944-03-26', '1946-09-20', 'GENERAL MOSCONI 156', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', '1643', '0', '4235-00544', '0', '4732-3525'),
(541, '369', 'CARLOS', 'KLUG - BOESE', '4790-5230', '155307-6781', 'cklug@nakase.com.ar', 'RENATA', 'BOESE', NULL, NULL, '', 2, '5312440', 'LC 5593526', '1950-05-03', '1947-07-03', 'DIAZ VELEZ  2929', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '4235-00040', '0', '4768-4242   INT: 120 - FAX 4768-4242  INT: 111'),
(542, '894', 'GABRIEL JORGE', 'LAZART - LISTE', '4738-9429/4849-0503', '1551556829', 'casabalzar@ciudad.com.ar', 'VIRGINIA', 'LISTE', NULL, NULL, '', 2, '13018346', '16398848', '1959-02-25', '1963-01-16', 'PACIFICO RODRIGUEZ 5185     ', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', '0', '0', NULL),
(543, '388', 'MIRTA SILVIA', 'LIBMAN - ', '4623-5869 // 5430-2062', '156-011-1273', 'arquitectalibman@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '12588322', '0', NULL, NULL, 'CAPDEVILLA 1236 ', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', '0', '0', '4627-2828'),
(544, '394', 'JUAN CARLOS', 'LOMBARDO - ', '4758-1367', '153-208-4939', 'lidiaderico@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '11488719', '0', '1965-09-12', NULL, 'MITRE 6074', 'CASEROS', 'BUENOS AIRES', 'ARGENTINA', '1678', 'lombardoveronica1@gmail.com', 'NO TIENE', '0', 'FAX 4758-7310'),
(545, '399', 'LUIS ALBERTO', 'LOPEZ - RODRIGUEZ', '4487-7529 (Luis)', '155-871-4710 (Elena)', 'elos52@hotmail.com', 'ELENA', 'RODRIGUEZ', NULL, NULL, '', 2, '3845195', '10632521', '1944-03-26', '1952-08-31', 'EDIFICIO 4 ENTRADA 2 3ºPISO DTO 7 BARRIO VILLA ROSSI', 'CIUDAD EVITA', 'BUENOS AIRES', 'ARGENTINA', '1778', '0', '0', '0', '4635-6336 (Elena)'),
(546, '899', 'OBDULIO HECTOR', 'LORUSSO - ', '4432-7643', '155-766-7643', 'obduliolorusso@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '4408056', '0', '1943-01-18', NULL, 'ACHAVAL 631 ', 'CABA', 'CABA', 'ARGENTINA', '1406', 'mari_l83@hotmail.com // paula_l85@hotmail.com', '0', '0', 'Garay. 4306-7940 - FAX Mariana: 156-6003993'),
(547, '903', 'GUILLERMO ERNESTO (FALLECIÓ)', 'MAGNANI - TRESPALACIOS', '02241-475-289', '1559-367407', 'aletrespa@hotmail.com', 'ALEJANDRA ISABEL', 'TRESPALACIOS', NULL, NULL, '', 2, '13228979', '13228880', '1960-03-27', '1959-08-09', 'BARRIO ARQUITECTURA- CASA 31-   ', 'RANCHOS', 'BUENOS AIRES', 'ARGENTINA', '1987', 'guillermo.magnani@mpsa.com', 'NO TIENE', '0', '0'),
(548, '989', 'LILIANA', 'MARASEO - 0', '4257-2849', '156-767-4661', 'liliana_sorsaburu@hotmail.com', '0', '0', NULL, NULL, '', 2, '17321782', '0', NULL, NULL, '0', '0', '0', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '0'),
(549, '420', 'JORGE GUILLERMO', 'MARCEL - ', '4696-2244', '155-420-5806 (Guillermo)', 'vamarcel2005@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '10130604', '0', NULL, NULL, 'CARABOBO 3072', 'SAN JUSTO', 'BUENOS AIRES', 'ARGENTINA', '1754', '0', '6353-00882', '0', 'Viviana: 4461-0503'),
(550, '422', 'ALBERTO OSCAR', 'MARCIO - COCCARO', '4251-2037', '155-389-4190', 'cristian.marcio@gmail.com', 'SILVIA BIBIANA', 'COCCARO', NULL, NULL, '', 2, '4644403', '5631945', NULL, NULL, 'JUJUY 1322 ', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', '1878', '0', NULL, '0', ' - FAX 4257-6458'),
(551, '435', 'MARIA CRISTINA', 'MARTINEZ - MARTINEZ', '4624-9084', '153-273-8151  (Jorge)', 'martinez_mariac@yahoo.com.ar', 'JORGE ALEJANDRO', 'MARTINEZ', NULL, NULL, '', 2, '16755140', '14868692', '1963-05-14', '1962-04-10', 'Segunda Rivadavia 21207', 'MORÓN', 'BUENOS AIRES', 'ARGENTINA', '1708', 'arquiesmar@yahoo.com.ar', '0', '0', 'Hijo: 156-207-2847 Jorge - FAX Cristina: 155-413-0209'),
(552, '437', 'MARIO VIDAL', 'MARTINO, MARIO VIDAL - CRUCIANELLI, ALCIRA ENRIQUETA', '4798-2281 // 4793-3657', NULL, 'mario.v.martino@hotmail.com', 'ALCIRA ENRIQUETA', 'CRUCIANELLI', NULL, NULL, '', 2, '5568289', NULL, NULL, NULL, 'DIAGONAL SALTA 1189', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', 'guillermo.javier.ricci@hotmail.com', NULL, NULL, '4545-2500 - FAX 4542-8081'),
(553, '443', 'CARLOS RUBEN', 'MASKIN - ', '4201-0046', '156-745-0919', 'carlosrmaskin@speedy.com.ar', NULL, NULL, NULL, NULL, '', 2, '5290041', '0', NULL, NULL, 'BERUTI 236', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', '0', '0', '0', NULL),
(554, '908', 'CARLO MARINO', 'MASTROIANI - ', '4451-4984', '155-182-7707', 'escobillonesmastro@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '92421266', '0', '1959-08-12', NULL, 'MUÑOZ 752 ', 'SAN MIGUEL', 'BUENOS AIRES', 'ARGENTINA', '1663', '0', '4235-05034', '0', NULL),
(555, '909', 'DANIEL ERNESTO', 'MATSUYAMA - ', '0', '156-228-4361', 'danielmatsuyama@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '12514141', '0', NULL, NULL, 'BOGOTA 3725 DEPTO 4', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', '0', '0', '03489-492454 - FAX 03489-492462'),
(556, '910', 'JORGE LUIS', 'MELGAR - RODRIGUEZ ROCHA', '5063-8636', '155-063-8635', 'melgarjl@hotmail.com', 'PAOLA', 'RODRIGUEZ ROCHA', NULL, NULL, '', 2, '23087558', '92755233', '1972-12-09', '1975-03-30', 'PASAJE FACUNDO 415', 'CABA', 'CABA', 'ARGENTINA', '1408', '0', '0', '0', '0'),
(557, '456', 'EDUARDO', 'MERELLO - CABRERA', '4734-0306', '155-120-1377', 'normaecabrera@hotmail.com', 'NORMA', 'CABRERA', NULL, NULL, '', 2, '8242956', '6196340', '1944-10-25', '1949-09-13', 'Asamblea 3944', 'SANTOS LUGARES', 'BUENOS AIRES', 'ARGENTINA', '1676', '0', '4235-00646', '0', NULL),
(558, '912', 'GUSTAVO GERARDO', 'MESSORE - COMPAÑY', '4568-8145', '155-848-9695', 'gustavomessore@yahoo.com.ar', 'ALICIA HAYDEE', 'COMPAÑY', NULL, NULL, '', 2, '12453922', '13827874', '1958-10-04', '1960-01-08', 'ARREGUI 3655 CP 1417', 'CABA', 'CABA', 'ARGENTINA', '1417', '0', '0', '0', '0'),
(559, '470', 'OMAR ANTONIO', 'MONTES - SINCCIT', '4650-2715', '156-525-6604', 'omarmontes1@hotmail.com', 'EMILIA MERCEDES', 'SINCCIT', NULL, NULL, '', 2, '7826429', '4750221', NULL, NULL, 'ESMERALDA 1030 ', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00548', '0', NULL),
(560, '676', 'ANA MARIA', 'MONTI - ', '4552-8076', '156-194-1437', 'montiam@gmail.com', NULL, NULL, NULL, NULL, '', 2, '6401392', '0', '1950-05-18', NULL, 'HOLMBERG 1168', 'CABA', 'CABA', 'ARGENTINA', '1428', '0', '0', '0', '4787-1423 - FAX 4552-8076'),
(561, '472', 'GERARDO ANGEL', 'MONTIEL - HORNYAK', '(03327) 416-107', '154-047-6624 (Gerardo)', 'gmontiel@cotelnet.com.ar', 'ALBA GABRIELA', 'HORNYAK', NULL, NULL, '', 2, '14447375', '0', '1960-12-20', '1962-05-29', 'AV.DE LOS CONSTITUYENTES 4099 BARRIO ALTOS DE PACHECO', 'BENAVIDEZ (TIGRE)', 'BUENOS AIRES', 'ARGENTINA', '1621', 'gmontiel@trf.com.ar', 'NO TIENE', '0', '4736-5182/4736-5597'),
(562, '476', 'PABLO JOSE', 'MORENO - ABASTO', '0237-487-2930', '154-949-8882 (Pablo)', 'pablomoreno871@speedy.com.ar', 'PATRICIA MABEL', 'ABASTO', NULL, NULL, '', 2, '17466826', '17701413', NULL, NULL, 'CORONEL PEDRO TOSCANO  874- VILLA SARMIENTO', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00549', '0', NULL),
(563, '63', 'SUSANA HAYDEE', 'MORENO - BITENSKY', '4633-0067', '156-569-4588 (Susana)', 'NO TIENE', 'DANIEL HUGO (FALLECIO)', 'BITENSKY', NULL, NULL, '', 2, '0', '4371373', NULL, NULL, 'CURAPALIGUE 150 5º C', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '0', '0', '0'),
(564, '484', 'PEDRO JOSE', 'NICCOLI - ELISSETCHE', '4217-0308', '156-330-4517 (José)', 'jose.niccoli@yahoo.com.ar', 'SANDRA BEATRIZ', 'ELISSETCHE', NULL, NULL, '', 2, '16533283', '0', '1963-07-19', '1967-01-04', 'LA BLANQUEADA 3801', 'SARANDI', 'BUENOS AIRES', 'ARGENTINA', '1872', 'sandra.elissetche@yahoo.com.ar', '4235-03710', '0', '4208-7871 - FAX Trabajo Sandra: 4227-9384'),
(565, '919', 'JAIME ENRIQUE', 'NOGUERA, JAIME ENRIQUE - IRIGOITI, GRACIELA', '0221-473-0621', '154-282372', 'jenvillaelisa2000@yahoo.com.ar', 'GRACIELA', 'IRIGOITI', NULL, NULL, '', 2, '12466935', '13909563', '1958-06-02', '1959-02-24', 'CALLE 422 BIS Nº CH35 ENTRE 2 Y 3', 'VILLA ELISA', 'BUENOS AIRES', 'ARGENTINA', '1894', '0', 'NO TIENE', '0', NULL),
(566, '920', 'OLGA ESTHER', 'NOVELINO - BARRERA', '02241-481059', '02241-15541394', 'elpalenqueranchos@gmail.com', 'RICARDO', 'BARRERA', NULL, NULL, '', 2, '14961907', '14961997', '1962-07-31', '1963-04-05', 'RIVADAVIA 4270-   ', 'RANCHOS', 'BUENOS AIRES', 'ARGENTINA', '1987', '0', '4235-02755', '0', '02241-482-356'),
(567, '487', 'JUAN CARLOS', 'OCCHIUTO - ', '0220-48-33755', '154-426-0477', 'gertomar@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '13906465', '0', NULL, NULL, 'GUEMES 3560 ', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1722', '0', '0', '0', NULL),
(568, '492', 'FELIX JOSE', 'OJEDA - BOSCO', '4296-0721', '1569576003', 'felix_ojeda2003@yahoo.com.ar', 'MARTA ALICIA', 'BOSCO', NULL, NULL, '', 2, '0', '11169845', '1950-01-09', '1954-06-27', 'JORGE MILES 792', 'MONTE GRANDE', 'BUENOS AIRES', 'ARGENTINA', '1842', '0', '4235-00550', '0', '4346-6860 - FAX 4346-6869'),
(569, '493', 'IRMA GLORIA', 'OLEINK - 0', '4774-8593', '153-353-2760', 'igoleink@hotmail.com', '0', '0', NULL, NULL, '', 2, '3548402', '0', '1936-11-04', NULL, 'OLLEROS 1811 3º B', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', '4235-00551', '0', '0'),
(570, '497', 'RODOLFO MIGUEL', 'OREIRO - ', '4613-6606', '154-073-6198', 'oflodorleugim@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '8290456', '0', NULL, NULL, 'CULPINA 91 6ºB', 'CABA', 'CABA', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '4343-5882'),
(571, '927', 'ELBA MARIA', 'PAGANO - GARAY', '4787-2965', '156-950-4243 (Elba)', 'epagano2013@gmail.com', 'JUAN JOSE', 'GARAY', NULL, NULL, '', 2, '11421448', '5222057', '1955-04-08', '1947-11-17', 'USARES 2280 1º PISO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1428', 'jotajotage@yahoo.com', '0', '0', '154049-5645 (Juan Jose) '),
(572, '513', 'STELLA MARIS', 'PAGLIERI - CAPRIOLI', '0232-4425043', '(02324) 156-49-705', 'stellamaris-pc@hotmail.com', 'RODOLFO CARLOS', 'CAPRIOLI', NULL, NULL, '', 2, '5941454', 'LE 4928587', '1948-12-30', '1946-04-06', 'CALLE 27 Nª 1002', 'MERCEDES', 'BUENOS AIRES', 'ARGENTINA', '6600', '0', 'NO TIENE', '0', ' - FAX 02324-15649705'),
(573, '516', 'SUSANA', 'PANDOLFI - ', '4687-2857', '156-195-8315', 'susipandolfi@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '11154019', '0', '1953-10-28', NULL, 'GUAMINI 1718', 'CABA', 'CABA', 'ARGENTINA', '1440', '0', '0', '0', NULL),
(574, '525', 'WALTER JAVIER', 'PAREDES - ', '03461-43-6798', '03461-15642151', 'paredes4c@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '17733174', '0', NULL, NULL, 'RONDÓ 135', 'SAN NICOLAS', 'BUENOS AIRES', 'ARGENTINA', '2900', '0', '4235-03450', '0', NULL),
(575, '486', 'MABEL CESAREA', 'PASQUINI - ', '(03465)497-022', '0', 'cacho887@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '3676669', '0', '1938-08-24', NULL, 'GARAY 498', 'BOMBAL', 'SANTA FÉ', 'ARGENTINA', '2179', '0', '4235-00006', '0', '03465-497666 - FAX Sobrina miriam: 0341-498-7551'),
(576, '530', 'MARCELO DANIEL', 'PAVON - DAVILE', '2076-2167', '154-046-4280', 'marcelopavon_7@hotmail.com', 'SANDRA', 'DAVILE', NULL, NULL, '', 2, '16626244', '18394227', '1963-08-14', '1967-03-11', 'NECOCHEA 3507', 'LOMAS DEL MIRADOR', 'BUENOS AIRES', 'ARGENTINA', '1752', '0', '0', '0', '4682-9114'),
(577, '536', 'CARLOS ALBERTO', 'PEREIRAS - NUNES DA FONSECA', '03488-638-032', '154-147-8104 (Nancy)', 'familiapereiras@hotmail.com', 'NANCY', 'NUNES DA FONSECA', NULL, NULL, '', 2, '4433292', '0', NULL, NULL, 'CAMPO GRANDE CLUB DE CAMPO LOTE 32 ', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', '1629', 'caperei12@gmail.com', '4235-00286', '0', NULL),
(578, '548', 'LUIS ALBERTO', 'PEREZ - FERNANDEZ', '4635-3356', '0', 'perez.luis.alberto@hotmail.com', 'CARMEN', 'FERNANDEZ', NULL, NULL, '', 2, '13302192', '0', NULL, NULL, 'PIERES  1173 DEP. B', 'CABA', 'CABA', 'ARGENTINA', '1440', '0', '0', '0', '4635-3078'),
(579, '550', 'ALICIA BEATRIZ', 'PICATTO - ', '(03496)423-312', '(03496) 154-64-935 (Alicia)', 'romigonzalez_83@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '14760947', '0', NULL, NULL, 'ALMAFUERTE 625', 'ESPERANZA', 'SANTA FÉ', 'ARGENTINA', '3080', '0', '*4235-00655', '0', '(03496)420-507 (Alicia)'),
(580, '563', 'JOSÉ MARÍA', 'PINTOS - CARRICONDO', '4257-0175', '155-018-2654', 'info.pintos@gmail.com', 'GLADYS', 'CARRICONDO', NULL, NULL, '', 2, '10932003', '0', '1953-03-28', '1959-05-12', 'LIBERTAD 344', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', '1878', '0', '4235-03499', '0', '4343-3134'),
(581, '916', 'CLAUDIA', 'PIRRI - ', '4665-4331', '115-669-0524', 'claudia_pirri@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '16379038', '0', '1963-04-20', NULL, 'LOS PATOS 2255 -    ', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', '1686', '0', '0', '0', '155-669-0524 - FAX 4822-4705'),
(582, '570', 'SILVIA NOEMI', 'PREGAL - BOUJON', '4295-9529', '153-638-2406 (Marcelo) // 155-925-6633 (Silvia)', 'silviapregal@gmail.com', 'MARCELO NESTOR', 'BOUJON', NULL, NULL, '', 2, '16321109', '14482132', '1963-03-06', '1961-03-10', 'RUTA 52 Nº 3350', 'CANNING', 'BUENOS AIRES', 'ARGENTINA', '1804', 'mboujon@datamarkets.com.ar', '4235-00553', '0', NULL),
(583, '978', 'MARIA CRISTINA', 'PUENTE - ', '4724-4924', '154-973-1948', 'puentemc812@gmail.com', NULL, NULL, NULL, NULL, '', 2, '10108126', '0', '1951-12-08', NULL, 'VENEZUELA 3170 PISO 1º DTO \"B\"', 'FLORIDA', 'BUENOS AIRES', 'ARGENTINA', '1602', '0', '4235-01971', '0', '4724-4925'),
(584, '578', 'DIEGO FERNANDO', 'QUEIMALIÑOS, DIEGO FERNANDO - BATTINI, OLGA ROSANA', '4763-5194', '155-228-5263', 'diegofer68@live.com.ar', 'OLGA ROSANA', 'BATTINI', NULL, NULL, '', 2, '20251715', '21538335', NULL, NULL, 'LOS CEIBOS 1264', 'VILLA ADELINA', 'BUENOS AIRES', 'ARGENTINA', '1607', '0', NULL, '0', '154-940-8354 Rosana'),
(585, '579', 'DESIDERIO ANTONIO', 'QUIROGA - TOSTICARELLI', '(0358) 472-7152', '(0266) 154-65-3095 (Eduardo) / 02664 15653095 (eduardo)', 'desiderio777@hotmail.com', 'MARTA RAQUEL', 'TOSTICARELLI', NULL, NULL, '', 2, '0', '2390196', '1927-07-07', '1935-10-01', 'Ayacucho Nro. 866 ', 'CAPITAL', 'SAN LUIS ', 'ARGENTINA', '5700', 'escribaniaquiroga@gmail.com', '4235-00082', '0', '(0266) 443-5897 (Escribanía)'),
(586, '454', 'Hipólito Enrique', 'RAMIREZ - ', '0237-487-3138', '156-094-4240', 'henriqueramirez@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '14193667', '0', NULL, NULL, 'LEGUIZAMÓN 1359 ', 'FRANCISCO ALVAREZ', 'BUENOS AIRES', 'ARGENTINA', '1746', '0', 'NO TIENE', '0', '0237-487-2292'),
(587, '934', 'JORGE OMAR', 'RAPONI - CUCCO', '(03465)470-418', '(0341)156-16-0804', 'raponijo@gmail.com', 'VIVIANA TERESA', 'CUCCO', NULL, NULL, '', 2, '10944856', '10944891', '1953-11-02', '1954-06-22', 'A. ANGELOZZI 766', 'ALCORTA', 'SANTA FÉ', 'ARGENTINA', '2117', '0', '*4235-01436', '0', 'Banco: 03462-480188'),
(588, '609', 'CARLOS ALBERTO', 'RODRIGUEZ - ', '(02241)42-3041', '(02241)156-72-898', 'cacho54rodrigue@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '10854329', '0', NULL, NULL, 'JACARANDÁ 21 ENTRE HIPÓLITO IRIGOYEN Y CASALINS', 'CHASCOMUS', 'BUENOS AIRES', 'ARGENTINA', '7130', '0', 'NO TIENE', '0', NULL),
(589, '938', 'ANIBAL D', 'RODRIGUEZ - MOSCOLONI', '(02271)420-455', '02226-1560-5654', 'anibal_rodriguez460@yahoo.com.ar', 'MARIA INES', 'MOSCOLONI', NULL, NULL, '', 2, '11961836', '12735052', '1956-01-18', '1956-12-02', 'AVDA SAN MARTIN 460', 'SAN MIGUEL DEL MONTE', 'BUENOS AIRES', 'ARGENTINA', '7220', '0', 'NO TIENE', '0', '02226-15442540'),
(590, '633', 'DANIEL ALBERTO', 'RUETTER - ', '(0264)423-5255', '264155047090', 'danielruetter91@gmail.com', 'GLADYS', NULL, NULL, NULL, '', 2, '10 370191', '0', NULL, NULL, 'HERMOGENES RUIZ SUR 1230', 'SAN JUAN', 'SAN JUAN', 'ARGENTINA', '5400', '0', 'NO TIENE', '0', NULL),
(591, '605', 'María Pía', 'RUFFOLO - RIVOTTA', '4245-0251', '155-311-9846 // 154-472-0148 (Preferente)', 'mariapiaruffolo@hotmail.com', 'ABEL RAUL (FALLECIÓ)', 'RIVOTTA', NULL, NULL, '', 2, '93869757', '0', NULL, NULL, 'RAMÓN FALCÓN 1450', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', '0', 'NO TIENE', '0', NULL),
(592, '649', 'ANA MARIA TENAGLIA', 'SANTILLAN - TENAGLIA', '4764-1803', '153-244-4889 (Juan Carlos) // 153-244-5311 (Ana) // 153-416-7002 (Lucía)', 'lucia_tenaglia@yahoo.com.ar', 'JUAN CARLOS', 'TENAGLIA', NULL, NULL, '', 2, '10823474', '8626893', NULL, NULL, 'BOULEVARD BALLESTER 6063', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', 'NO TIENE', '0', ' - FAX 4781-1746 (hermano)'),
(593, '653', 'EDUARDO', 'SARTO - RODRIGUEZ DURAZZO', '4703-3783', '1540-743040', 'eduardosarto@yahoo.com.ar', 'VIVIANA', 'RODRIGUEZ DURAZZO', NULL, NULL, '', 2, '17635321', '18122554', '1966-03-06', '1966-10-07', 'AVDA CABILDO 4392 P.B. 6', 'CABA', 'CABA', 'ARGENTINA', '1429', '0', '4235-01940', '0', 'Nextel: 159*2829'),
(594, '656', 'PABLO', 'SCHAER - 0', '0', '(0388)154-73-0588 (Pablo)', 'pablo.schaer@aromasdelaquebrada.com.ar', '0', '0', NULL, NULL, '', 2, '26359162', '0', '1977-11-19', NULL, 'BELGRANO 417', 'TILCARA', 'JUJUY', 'ARGENTINA', 'Y4624AFI', '0', '4235-04052', '0', NULL),
(595, '944', 'ORLANDO HECTOR', 'SCHIAFFINO - GONZALEZ', '4902-5866', '1556629033', 'mrgonzalezds@hotmail.com', 'MARIA ROSA', 'GONZALEZ', NULL, NULL, '', 2, '8104751', '12093102', '1949-12-26', '1955-04-16', 'RIVADAVIA 4687  3ER PISO DTO \"B\" ', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', '4235-02873', '0', '4127-7202'),
(596, '665', 'CARLOS', 'SCOTTI - RODRIGUEZ', '4658-0799/4209-9596', '154-472-2233', 'scotticarlos@yahoo.com.ar', 'BEATRIZ AMELIA', 'RODRIGUEZ', NULL, NULL, '', 2, '4607125', '4738889', '1944-07-21', '1943-12-18', 'PARAGUAY 762 ', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00557', '0', NULL),
(597, '139', 'JOSE ANTONIO', 'SEGUNDO COLLADOS - BARBORINI', '4237-7883', '155-651-6961', 'segundo.jose@hotmail.com', 'LILIANA INES', 'BARBORINI', NULL, NULL, '', 2, '93397652', '12760408', '1945-01-02', '1958-11-29', 'URQUIZA 341 ', 'FLORENCIO VARELA', 'BUENOS AIRES', 'ARGENTINA', '1888', 'claudia_pirri@hotmail.com // moro_antonella@hotmail.com', 'NO TIENE', '0', '4355-1620'),
(598, '945', 'JAVIER FRANCISCO', 'SENIN - MARTINEZ SIERRA', '0', '153-790-0559 (María)', 'soki52@hotmail.com', 'María del Socorro', 'MARTINEZ SIERRA', NULL, NULL, '', 2, '12821653', '0', '1958-09-19', NULL, 'AVDA.DEL LIBERTADOR 2491   PISO 19 DEPTO \"D\"', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '*4235-01989', '0', '0'),
(599, '680', 'MIRTHA', 'SIGNORELLO - PAGANELLI', '4682-9363', '156-753-4424', 'mirtamsignorello@gmail.com', 'ANGEL ALBERTO', 'PAGANELLI', NULL, NULL, '', 2, '0', '13222718', '1956-10-20', '1957-05-05', 'SEVERO G. GRANDE DE ZEQUEIRA 5875', 'CABA', 'CABA', 'ARGENTINA', '1440', 'rmbarrera164@hotmail.com', '*4235-00665', '0', '4864-6883'),
(600, '947', 'LILIA SILVIA', 'SILVERA - DIAZ', '4741-3658', '152-322-2751', 'liliasilvera@gmail.com', 'SERGIO NORBERTO', 'DIAZ', NULL, NULL, '', 2, '13610708', '12441630', '1959-12-30', '1958-06-23', 'TRIUNVIRATO 1612-    ', 'DON TORCUATO', 'BUENOS AIRES', 'ARGENTINA', '1611', 'martinplast@hotmail.com', 'NO TIENE', '0', ' - FAX Sergio: 154-9143381'),
(601, '686', 'JUAN DOMINGO RAMON', 'SOSA - ', '(0220)494-2039', '156-304-6123', 'jrintegral@hotmail.com', 'ELISA', NULL, NULL, NULL, '', 2, '7829702', '0', NULL, NULL, 'ALBERTO ECHAGÜE 2335', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1716', '0', '4235-00666', '0', '(0220)494-2065'),
(602, '690', 'CARLOS ENRIQUE', 'SPECTOR - RESCH', '4632-4946', '155-793-3058  Sofía', 'cspector@cponline.org.ar', 'SOFIA', 'RESCH', NULL, NULL, '', 2, '13566912', '0', '1959-09-24', '1960-08-11', 'MORELLOS 794  PISO 7º  DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1406', 'soresch@hotmail.com', 'NO TIENE', '0', '5300-4007'),
(603, '695', 'HILDA ELIZABETH', 'STEINBAUM - ', '4855-8505', '154-928-0106 (Gustavo Smeke)', 'elmotivo23@hotmail.com', NULL, NULL, NULL, NULL, '', 2, '4461311', '0', NULL, NULL, 'SCALABRINI ORTIZ 258 9º \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '*4235-00753', '0', '155-939-1960 (Hilda)'),
(604, '699', 'RAUL ANTONIO', 'SUAREZ, RAUL ANTONIO - ALBA, MONICA', '4661-0877', '15656561882', 'malba@estudiocontablealba.com.ar', 'MONICA', 'ALBA', NULL, NULL, '', 2, '0', '12228491', NULL, NULL, 'ALVAREZ JONTE 2095', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', NULL, '0', '5777-4523 - FAX 4623-5764'),
(605, '706', 'GRACIELA', 'SULE - MARTINEZ', '0', '1558-145975', 'graciela_sule@hotmail.com', 'JUAN CARLOS', 'MARTINEZ', NULL, NULL, '', 2, '5075501', '7667485', '1956-09-10', '1949-10-29', 'SAENZ PEÑA 1157', 'TIGRE', 'BUENOS AIRES', 'ARGENTINA', '1648', '0', '*4235-00560', '0', '4749-6915 - FAX 4731-0022'),
(606, '713', 'DANIEL PEDRO', 'TARDIVO - CALLES', '0', '11 - 5061-2883', 'danielpedrotardivo@gmail.com', 'MARINA', 'CALLES', NULL, NULL, '', 2, '13881743', '22115370', '1959-10-30', '1971-04-13', 'CORRIENTES 2032  3 PISO 2A2', 'CABA', 'CABA', 'ARGENTINA', '1045', '0', 'NO TIENE', '0', NULL);
INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(607, '714', 'JORGE ALBERTO', 'TAYLOR, JORGE ALBERTO - OCHOA, SILVIA MARIEL', '(0221)469-3684', '0221-15428-4625', 'jorge_a_taylor@hotmail.com', 'SILVIA MARIEL', 'OCHOA', NULL, NULL, '', 2, '17291520', NULL, NULL, NULL, 'SAN MARTIN 138 ', 'ENSENADA', 'BUENOS AIRES', 'ARGENTINA', '1925', NULL, 'NO TIENE', NULL, NULL),
(608, '715', 'BEATRIZ', 'TEDESCO - GIOVANELLI', '(02323) 492-380', '(02323) 153-63-225 (Beatriz)', 'viveromundodeplantas@gmail.com', 'ARMANDO', 'GIOVANELLI', NULL, NULL, '', 2, '0', '4750652', NULL, NULL, 'MATEO S. CASO 809', 'CAPILLA DEL SEÑOR', 'BUENOS AIRES', 'ARGENTINA', '2914', 'kukytedesco50@gmail.com', '0', '0', NULL),
(609, '730', 'IRENE BEATRIZ', 'COUFFIGNAL - TORRES', '(03329)424-920', '0', 'gediamante@yahoo.com.ar', 'ORFILIO', 'TORRES', NULL, NULL, '', 2, '2388250', '2588250', '1936-09-29', '1929-06-14', '3 DE FEBRERO 180', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', 'gdiamante59@gmail.com', '4235-02789', '0', NULL),
(610, '954', 'NELIDA HAYDEE', 'TUMANE - STELLATO', '4302-0404', '155-804-9683 / 155-6591736 (NELIDA)', 'nellytumane@hotmail.com', 'MIGUEL ANGEL', 'STELLATO', NULL, NULL, '', 2, '6201430', '5530661', '1949-09-17', '1948-12-20', 'CALIFORNIA 1518 PB DEPTO A- CAP FED  CP 1289', 'CABA', 'CABA', 'ARGENTINA', '1289', '0', 'NO TIENE', '0', ' - FAX 4205-5144'),
(611, '752', 'GUILLERMO', 'VALIMBRI - VALIMBRI', '0221-4711519', '0221-155919450', 'guillermovalimbri@hotmail.com', 'GUILLERMO LUIS', 'VALIMBRI', NULL, NULL, '', 2, '0', '0', NULL, NULL, 'CALLE 26 Nº 3894 ENTRE 493 Y 494', 'GONET', 'BUENOS AIRES', 'ARGENTINA', '1897', '0', '0', '0', NULL),
(612, '757', 'MARTHA', 'VALSAMAKIS - ', '4641-4431 // 4641-4943', '156-271-3374 // 156-270-4431 (Silvina ella se ocupa)', 'marthavals10@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '3617576', '0', NULL, NULL, 'MONTIEL 316 1º PISO DTO 4', 'CABA', 'CABA', 'ARGENTINA', '1408', 'silvinaven@yahoo.com', '4235-04147', '0', '4661-2123 '),
(613, '106', 'VERÓNICA JORGELINA', 'VAZQUEZ - 0', '4793-7352', '153-642-4591', 'Veronica.J.Vazquez@walmart.com', '0', '0', NULL, NULL, '', 2, '22655035', '0', NULL, NULL, 'EDUARDO COSTA 1850 1ºA', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '4235-03785', '0', '0'),
(614, '764', 'HECTOR OSCAR', 'VAZQUEZ - RIOS', '4236-5743', '154-939-4696 (Gricelda)', 'griceldarios@gmail.com', 'GRICELDA BEATRIZ', 'RIOS', NULL, NULL, '', 2, '0', '10816152', '1951-01-15', '1953-02-24', 'ALTAMIRA 1580', 'RAFAEL CALZADA', 'BUENOS AIRES', 'ARGENTINA', '1847', '0', '4235-00565', '0', '4291-0355/4291-0975 - FAX 4291-0975'),
(615, '776', 'FERNANDO', 'VIDAL - PAZZELLI', '(03462)450-837 ', '154-477-3771 // (03462)154-11-601 ', 'alepazper60@gmail.com', 'ALEJANDRA', 'PAZZELLI', NULL, NULL, '', 2, '12078031', '0', '1958-02-09', NULL, 'CALLE 53 1193', 'VILLA CAÑAS', 'SANTA FÉ', 'ARGENTINA', '0', '0', '4235-04903', '0', '0'),
(616, '779', 'ESTEBAN RAMON', 'VILLAMAYOR - CACERES', '4922-8730', '154-0308347', 'villamayoresteban@gmail.com', 'ESTELA MARIA', 'CACERES', NULL, NULL, '', 2, '6613958', '6287123', '1949-04-25', '1950-03-09', 'RIGLOS 569 2º PISO', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', 'NO TIENE', '0', '4348-3500 int: 2067'),
(617, '780', 'EDMUNDO', 'VILLAMIL - ANGIO', '2060-2767', '156-052-9703 (Daina) // 116-749-1641 (Elsa)', 'elsaangio@gmail.com', 'ELSA NORMA', 'ANGIO', NULL, NULL, '', 2, '0', '2733029', '1933-07-01', '1936-03-30', 'BAIGORRIA 5571 DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1408', '0', 'NO TIENE', '0', '4978-1480'),
(618, '782', 'ANIBAL GUSTAVO', 'VISUS - GARCIA', '(0221) 482-7955', '156-786-2505', 'fliavisus@gmail.com', 'ELSA SOLEDAD', 'GARCIA', NULL, NULL, '', 2, '5065216', '4503995', NULL, NULL, 'CALLE 39 Nº 715 ENTRE CALLE 9 Y 10', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', '0', 'NO TIENE', '0', '4275-1280  INT. 110'),
(619, '962', 'RICARDO ENRIQUE', 'WILSON - ', '4244-6361', '154-195-4052', 'ricardoewilson@gmail.com', NULL, NULL, NULL, NULL, '', 2, '10740528', '0', NULL, NULL, 'BOEDO 916', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', '0', '4235-01672', '0', ' - FAX 4202-0298'),
(620, '964', 'RICARDO CRUZ', 'ZABALZA - ', '4375-4286', '153-236-3653 (Alexandra) / 153-236-3654 (Ricardo)', 'alexandrabzabalza@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 2, '4417259', '0', '1943-06-06', NULL, 'PARANA 690 PISO 4 DTO \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1017', 'jczabalza@gmail.com', 'NO TIENE', '0', '4374-5454/4371-3189 // 153-236-3650 (Juan Cruz)'),
(621, '965', 'JUAN ANTONIO AD', 'ZAFFARONI - BONOMO', '3965-4742', '156-902-6826', 'jzaffaroni@gmail.com', 'SANDRA', 'BONOMO', NULL, NULL, '', 2, '18231302', '0', '1967-08-23', NULL, 'REMEDIOS DE ESCALADA DE SAN MARTIN 4948 ', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', 'NO TIENE', '0', 'Sandra: 155-645-0176'),
(622, '798', 'JORGE RICARDO', 'ZANIRATTO - GARCIA', '4242-2605', '1560-538940 (no puede recibir llam)', 'stellammgarcia@hotmail.com', 'STELLA MARIS', 'GARCIA', NULL, NULL, '', 2, '14851133', '10814276', NULL, NULL, 'ACEVEDO 1335', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', '0', '4235-00571', '0', '4248-4115'),
(623, '808', 'EDUARDO', 'ZIMERMAN - ', '4854-3613', '154-0558473', 'emzimerman@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 2, '12949920', '0', NULL, NULL, 'LUIS MARÍA DRAGO 349', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', 'NO TIENE', '0', NULL),
(624, '526', 'PATRICIA LOURDES', 'PARES - PARES', '03488-440986', '155-565-4225 (Patricia)', 'patolourdes@hotmail.com', 'MIGUEL ALFREDO', 'PARES', NULL, NULL, '', 2, '17201265', '0', '1964-07-29', NULL, 'ACHEGA 3174', 'CABA', 'BUENOS AIRES', 'ARGENTINA', '0', '0', '4235-04942', '0', NULL),
(625, '906', 'DOMINGO', 'MAONE - ', '0341-421-7036', '(0341)153-427-677 (Ezequiel)', 'cannonrosario@yahoo.com.ar', 'EZEQUIEL MATIAS', NULL, NULL, NULL, '', 2, '4859539', '0', NULL, NULL, 'MENDOZA 120 2º PISO', 'ROSARIO', 'SANTA FÉ', '0', '2000', '0', '0', '0', NULL),
(626, '973', 'CARLOS', 'QUINTEROS - PEREZ', '4451-3282', '155-591-3438', 'cquinteros79@hotmail.com', 'SUSANA NOEMI', 'PEREZ', NULL, NULL, '', 2, '0', '5477455', '1947-02-14', '1947-01-30', 'JUAN JOSE PASO 1234 ', 'MUÑIZ', 'BUENOS AIRES', 'ARGENTINA', '1663', '0', 'NO TIENE', '0', '4469-8800 int: 229'),
(627, '7', 'CLAUDIA NOEMI', 'AGUIRRE, CLAUDIA NOEMI', '4768-4051', '155-5953693', 'licclauaguirre@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '21470667', NULL, '1970-06-04', NULL, 'JEAN JEAURES 2717', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', NULL, '4235-00586', '0', '4453-1324 // FAX 5480-5101'),
(628, '974', 'NESTOR', 'ALARCON - ARATA', '(03329)424-814', '(03329)155-11-711 (Nestor)', 'pirocuatro@gmail.com', 'MABEL', 'ARATA', NULL, NULL, '', 3, '4634431', '6200895', '1944-05-01', '1950-04-30', 'BOTTARO 852', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', 'pirodos@hotmail.com', '3443-2443', '0', '(03329)155-27-288 (Mabel)'),
(629, '9', 'DANIEL HUGO', 'ALBANO - ', '4943-6810', '154-439-0333', 'toxytox@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '12927744', '0', NULL, NULL, 'AVDA BRASIL 2817', 'CABA', 'CABA', 'ARGENTINA', '1260', '0', 'NO TIENE', '0', NULL),
(630, '811', 'MONICA', 'ALEJOS - CARDARELLI', '4751-2065', '156-564-7842', 'alejosmonica@hotmail.com', 'ANTONIO', 'CARDARELLI', NULL, NULL, '', 3, '12849853', '11172988', '1958-10-23', '1954-08-31', 'BELGRANO 5918', 'PALOMAR', 'BUENOS AIRES', 'ARGENTINA', '1684', '0', 'NO TIENE', '0', NULL),
(631, '16', 'OSVALDO FABIAN', 'ALMEIDA - GRECO', '4566-0079', '154-421-6530', 'sgtosanders@gmail.com', 'NÉLIDA CRISTINA', 'GRECO', NULL, NULL, '', 3, '11666782', '0', '1960-12-13', NULL, 'BERMUDEZ 1669 ', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', '7461-00898', '0', '4988-1385'),
(632, '27', 'CELIA VIVIANA', 'ANTON - ', '(0379)442-8779', '0379-154686357', 'vivianaanton193@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '12181483', '0', NULL, NULL, 'STA. FÉ. 935 PISO 4 DEPTO. B', 'CORRIENTES', 'CORRIENTES', 'ARGENTINA', '3400', '0', '4235-00589', '0', NULL),
(633, '31', 'CARLOS MARCELO', 'ARANDA - ', '4247-3900', '154-042-0581', 'marceloaranda44@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '13263352', '0', NULL, NULL, 'ONCATIVO 2143', 'LANUS ESTE', 'BUENOS AIRES', 'ARGENTINA', '1824', '0', 'NO TIENE', '0', NULL),
(634, '36', 'VERÓNICA ROSA', 'ARTUSO - CESANELLI', '4791-2768', '153-092-2901 (Verónica) // 153-092-2900 (Jorge) ', 'deanastasie@yahoo.com.ar', 'JORGE', 'CESANELLI', NULL, NULL, '', 3, '11773580', '10424487', '1955-02-11', '1952-05-17', 'ROQUE SAENZ PEÑA 2884', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', 'NO TIENE', '0', '4794-3138 (negocio)'),
(635, '46', 'CARLOS LUIS ', 'BARCALA - GIURITZA', '(0237)462-6550 ', '156-309-9588 (Marta)', 'mmbarcala@yahoo.com.ar', 'MARTA', 'GIURITZA', NULL, NULL, '', 3, '4929787', '0', NULL, NULL, 'VICENTE LOPEZ Y PLANES 525', 'MORENO', 'BUENOS AIRES', 'ARGENTINA', '1744', '0', '4235-00594', '0', NULL),
(636, '980', 'MIGUEL ANGEL', 'BARROSO - ', '(0299) 442-1839', '116-559-3162', 'elisadanielajury@gmail.com', 'ELISA', NULL, NULL, NULL, '', 3, '16408188', '0', '1963-07-27', NULL, 'LA RIOJA 983 ', 'NEUQUEN', 'NEUQUÉN', 'ARGENTINA', '8300', '0', 'NO TIENE', '0', '0'),
(637, '53', 'JORGE ALBERTO', 'BELTRAMI - MAGNO', '(0221)469-2031', '(0221)156-042-031', 'cam1959@yahoo.com.ar', 'CECILIA ALEJANDRA', 'MAGNO', NULL, NULL, '', 3, '11773832', '0', NULL, NULL, 'HORACIO CESTINO 713', 'ENSENADA', 'BUENOS AIRES', 'ARGENTINA', '1925', 'chechub3@hotmail.com', '4235-00596', '0', '(0221)424-4775 // FAX (0221)424-4775'),
(638, '54', 'MARIO', 'BELZUNCE - ', '4798-2181', '156-829-7149', 'mebelzunce@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 3, '6819708', '0', NULL, NULL, 'AVDA SANTA FE 1435 PISO 11 \"B\"', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', ' madbel@hotmail.com; Mariana.Belzunce@allianz.com.ar; suantonio09@hotmail.com', '7461-00255', '0', NULL),
(639, '58', 'HORACIO DANIEL', 'BETOLDI - FUSARO', '4612-0174', '153-148-7891', 'hbetoldi@yahoo.com.ar', 'SILVIA CECILIA', 'FUSARO', NULL, NULL, '', 3, '13404288', '16453737', '1959-06-10', '1963-08-12', 'AVDA. VARELA 586 ', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '4235-00597', '0', NULL),
(640, '981', 'ENRIQUE JOSE', 'BOFFA - NOAT', '(03329)426-833', '(03329)153-04-402 (Susana)', 'nicoboffa@gmail.com', 'SUSANA INES', 'NOAT', NULL, NULL, '', 3, '4690242', '5886501', NULL, '1949-01-18', 'LAS HERAS 730', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', '0', '4235-02505', '0', '0'),
(641, '977', 'JULIO OMAR', 'BORDIÑUK - GENTILE', '4672-2065', '156-496-0438', 'marite.gentile@gmail.com', 'MARIA TERESA', 'GENTILE', NULL, NULL, '', 3, '0', '17737415', '1964-07-11', NULL, 'JOSÉ BONIFACIO 4170 EDIFICIO 12 \"B\" PISO 2º DTO \"19\"', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', 'NO TIENE', '0', '4323-4731'),
(642, '860', 'GUSTAVO EDUARDO', 'BRAGHIRIOLI - ', '4571-1569 ', '155-013-4236', 'gusvavi@gmail.com', NULL, NULL, NULL, NULL, '', 3, '14619424', '0', '1961-12-18', NULL, 'PASAJE EL FOJON 4431', 'CABA', 'CABA', 'ARGENTINA', '1419', '0', 'NO TIENE', '0', NULL),
(643, '78', 'HILDA IRMA', 'BROUSSAIN, HILDA IRMA - MONTEAGUDO, CARLOS', '4903-6526', '154-492-3542', 'hibroussain@hotmail.com', 'CARLOS', 'MONTEAGUDO', NULL, NULL, '', 3, '12825018', '8521185', '1957-01-28', '1950-12-09', 'JOSÉ MARÍA MORENO 483  8º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', 'NO TIENE', '0', NULL),
(644, '87', 'MARIA', 'CALO - PRESTA', '4431-1978 (amiga)', '0', '0', 'FELIX PEDRO', 'PRESTA', NULL, NULL, '', 3, '13404288', '0', '1941-07-16', '1938-07-28', 'J. BAUTISTA ALBERDI 1.274  4ª  \" C \"', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '4235-00524', '0', '4642-9977 (tarde)'),
(645, '91', 'EDUARDO OMAR', 'CAMPI - GARCIA', '(34963)444-142', '156-666-0300 (Juan) // 34600577946', 'jmajluf@contextoempresarial.com.ar', 'ANA MARIA', 'GARCIA', NULL, NULL, '', 3, '12727335', '0', '1956-12-11', NULL, 'C/ PIANISTA AMPARO ITURBI 23 PUERTA 1', 'VALENCIA', 'ESPAÑA', 'ARGENTINA', '46007', 'axel.ono@hotmail.es', 'NO TIENE', '0', '4105-9100/9141/ Juan Majluf // FAX 4105-9120'),
(646, '95', 'MARISA ROSANA', 'CANGIANO - ', '4203-8840', '156-423-0508 // 155-424-2184 (Jorge)', 'gerencia@m4editorial.com.ar', NULL, NULL, NULL, NULL, '', 3, '17662952', '0', '1966-02-14', NULL, 'JUAN B. ALBERDI 125', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', '0', '4235-04296', '0', '4357-2316'),
(647, '96', 'NESTOR HORACIO', 'CAPILLAS - RIOS', '2062-2861', '154-072-0754 (Maite)', 'maitecapillas@hotmail.com', 'MAITE', 'RIOS', NULL, NULL, '', 3, '4859206', '12787821', NULL, NULL, 'CONDARCO 551', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', '1834', '0', '4235-01847', '0', '153-832-1217 (Leonel)'),
(648, '101', 'RAUL MARCELO', 'CARBALLEIRA - GONZALEZ', '4777-8146', '154-418-1076 (Mónica)', 'magonz@intramed.net', 'MÓNICA ALICIA', 'GONZALEZ', NULL, NULL, '', 3, '11387111', '0', NULL, NULL, 'AREVALO 2706  6PISO DTO\"A\"', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', '155-039-7723'),
(649, '201', 'EDUARDO ALBERTO', 'CARBONE - JAJCEVIC', '4701-6631', '156-289-4751', 'eduardo.carbone@fibertel.com.ar', 'MARIANA', 'JAJCEVIC', NULL, NULL, '', 3, '17260434', '0', NULL, NULL, 'MOLDES 3433 8 PISO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1429', '0', '4235-01901', '0', '5236-6062'),
(650, '976', 'MONICA', 'CARNEVALE, MONICA', '4259-4516', '155-1839976', 'monibea2015@gmail.com', NULL, NULL, NULL, NULL, '', 3, '14341062', NULL, NULL, NULL, 'CRERRITO 1040', 'BERNAL', 'BUENOS AIRES', 'ARGENTINA', '0', '0', '3443-02491', '0', NULL),
(651, '105', 'RICARDO', 'CARRANZA - RICCIO', '4551-1116', '0', 'ricardocarranza19@yahoo.com.ar', 'ANGELA SUSANA', 'RICCIO', NULL, NULL, '', 3, '4264586', '4944119', NULL, NULL, 'CESPEDES 3301', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', 'Trabajo hijo: 5480-5000'),
(652, '987', 'MARIA EVA', 'CARRIZO - 0', '0', '155-925-2335 (No corresp. A abonado en serv)', '0', '0', '0', NULL, NULL, '', 3, '6496244', '0', NULL, NULL, 'ISABEL LA CATOLICA 1081 - 25º \'Q\'', 'CABA', 'CABA', 'ARGENTINA', '1268', '0', 'NO TIENE', '0', '152-329-6003'),
(653, '107', 'JOSE', 'CARRIZO DE ALCE - ALCE', '4642-1049 // 153-695-8245 (María)', '153-660-8568 // 11-2703-6744  (José) // 153-216-9148 (Fernando)', 'fernandobaamondeproducciones@gmail.com', 'MARIA', 'ALCE', NULL, NULL, '', 3, '0', '0', NULL, NULL, 'BENITO JUAREZ 4337', 'CABA', 'CABA', 'ARGENTINA', '1419', 'maria_m_alce@hotmail.com ; martin121799@gmail.com', '4235-02356', '0', 'José: 4505-2545'),
(654, '111', 'ALEJANDRO', 'CASTELO - CALVO', '4799-1837', '155-342-1922 (Marcela)', 'alecastelo@yahoo.com.ar', 'MARCELA', 'CALVO', NULL, NULL, '', 3, '17202425', '17023309', '1964-09-15', '1965-01-09', 'JOSÉ MARÍA PAZ 3426 ', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '4235-02089', '0', '155-623-1535 '),
(655, '118', 'RUBEN EDUARDO', 'CASUCCIO - ', '2196-1160', '156-012-8335', 'recasuccio@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '14857065', '0', NULL, NULL, 'SARGENTO CABRAL 1915', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', NULL),
(656, '119', 'TERESA', 'CAZON - ', '(0387)431-4942', '(0387)155-937-640', 'grbinda@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '4452436', '0', NULL, NULL, 'PASAJE YAPEYÚ 1861', 'SALTA', 'SALTA', 'ARGENTINA', '4400', '0', '4235-02191', '0', '0'),
(657, '122', 'Eduardo', 'CHAU - ', '4708-9536', '155-0120638', 'edujchau@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '31071250', NULL, NULL, NULL, NULL, NULL, NULL, 'ARGENTINA', '0', 'eypul14@gmail.com', 'NO TIENE', '0', NULL),
(658, '130', 'FLORINDA', 'CIMINO - ', '(0348)446-8195', '114-176-5217 // 156-461-0335', 'flory_cimino@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '17305800', '0', '1965-09-03', NULL, '9 DE JULIO 1426 ', 'SAN FERNANDO', 'BUENOS AIRES', 'ARGENTINA', '1646', '0', '4235-04186', '0', '0'),
(659, '136', 'BLANCA IRIS', 'COBOS - ', '(0220)476-2122', '154-409-3350', 'blanky111@hotmail.com.ar', NULL, NULL, NULL, NULL, '', 3, '12312889', '0', '1958-03-17', NULL, 'CORONEL FELIX BRAVO 1152', 'GRAL. LAS HERAS', 'BUENOS AIRES', 'ARGENTINA', '1741', '0', '4235-03732', '0', 'FAX (0220)476-2158'),
(660, '144', 'JOSE', 'COLUCCI - CANEPA', '4901-9503', '0', 'maru_1707@hotmail.com', 'ALICIA BEATRIZ', 'CANEPA', NULL, NULL, '', 3, '14223704', '2974107', '1931-07-22', '1932-03-16', 'AVDA. LA PLATA 67 7º PISO  DTO 1', 'CABA', 'CABA', 'ARGENTINA', '1184', '0', 'NO TIENE', '0', NULL),
(661, '154', 'ROBERTO GERMAN', 'CORTON - ', '4656-4676', '155-025-6006', 'rgcorton@ciudad.com.ar', NULL, NULL, NULL, NULL, '', 3, '4264586', '0', NULL, NULL, 'PIZZURNO 61', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', '1704', 'roberto.corton@osplad.org.ar', '4235-00528', '0', '5032-9422'),
(662, '162', 'MABEL ALICIA', 'CREGO - ELISEI', '4300-1169', '1554-813036', 'mcrego@crexelups.com', 'HUGO', 'ELISEI', NULL, NULL, '', 3, '11768775', '10460769', '1955-03-21', '1952-09-10', 'ISABEL LA CATOLICA 61 DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1268', 'mabelcrego@fibertel.com.ar', 'NO TIENE', '0', '4300-5575 // FAX 4307-8243'),
(663, '164', 'MILA FLORENTINA', 'CROCCI - CEDIO', '4982-0738', '153-2715474', 'milaflorentina@yahoo.com.ar', 'DANIEL CARLOS', 'CEDIO', NULL, NULL, '', 3, '4479411', '4141744', '1942-12-15', NULL, 'AVDA. DIAZ VELEZ 4537  3º PISO  DTO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', 'NO TIENE', '0', NULL),
(664, '168', 'SIRO', 'CUZZONI - ', '4854-3178', '154-479-9176', 'oficinavirtualsc@gmail.com', NULL, NULL, NULL, NULL, '', 3, '4281070', '0', '1938-07-21', NULL, 'AVDA. JUAN B. JUSTO 2819 2º PISO DTO \"D\"', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '4235-04388', '0', NULL),
(665, '170', 'FERNANDO JOSE', 'DA PONTE - ACUÑA', '(0221)474-1435', '0', 'ana.acunia@gmail.com', 'ANA', 'ACUÑA', NULL, NULL, '', 3, '10230649', '0', NULL, NULL, '417 Y 135', 'VILLA ELISA', 'BUENOS AIRES', 'ARGENTINA', '0', 'ferjodapo@gmail.com', '4235-02811', '0', NULL),
(666, '850', 'NORMA RITA', 'D\'ABBISOGNO - ', '4699-2938', '156-596-3509', 'andrea.santillan5@gmail.com', NULL, NULL, NULL, NULL, '', 3, '3577215', '0', '1936-11-14', NULL, 'CAVIA 496- LOMAS DEL MIRADOR', 'LOMAS DEL MIRADOR', 'BUENOS AIRES', 'ARGENTINA', '1752', '0', 'NO TIENE', '0', NULL),
(667, '173', 'NORA NOEMI', 'DABOVE - ', '(0237)484-1578', '155-618-3152', 'dabovenora@gmail.com', NULL, NULL, NULL, NULL, '', 3, '4257283', '0', NULL, NULL, 'PUEYRREDON 368', 'GENERAL RODRIGUEZ', 'BUENOS AIRES', 'ARGENTINA', '1748', '0', 'NO TIENE', '0', '(0237)485-0058//484-0124 // FAX (0237)485-0058 (Yerno Sr. Minet)'),
(668, '179', 'ADALBERTO CLAUDIO', 'DAVICO - MELARAGNO', '4628-4975', '153-478-1695', 'davicoclaudio@hotmail.com', 'DORA ESTELA', 'MELARAGNO', NULL, NULL, '', 3, '12021332', '12022134', '1958-01-16', '1958-05-24', 'MACHADO 1616', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', 'NO TIENE', '0', '4483-4218/4489-2531 '),
(669, '182', 'ALDO ROBERTO', 'DAVIDOVSKY - ', '4855-1140', '155-720-6331', 'Aldo_Davidovsky@bat.com', NULL, NULL, NULL, NULL, '', 3, '13924511', '0', NULL, NULL, 'LOYOLA 430 2º PISO DTO: \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1414', 'abrahammotl@hotmail.com', '0', '0', '4724-8444 int: 2253'),
(670, '189', 'LILIANA', 'DE SOUSA MARTINS - ', '4392-0308', '155-016-7132', 'lilidesousa@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 3, '14937589', '0', '1961-09-27', NULL, 'LAMADRID 901', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', 'lilianadesousa@speedy.com.ar', '0', '0', NULL),
(671, '200', 'MIGUEL ANTONIO', 'DIAB - BRESCIA', '0341-4304182', '(0341) 153-168-900', 'myadiab@hotmail.com', 'AIDA', 'BRESCIA', NULL, NULL, '', 3, '11124754', '0', NULL, NULL, 'RICHIERE 1284 ', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', '0', '0', 'FAX (0341)435-9718'),
(672, '597', 'DIANA ANA', 'DOKMETZIAN - RECCIO', '4771-9876', 'Diana: 1541-996173', 'dianadok@gmail.com', 'FIDEL CARLOS', 'RECCIO', NULL, NULL, '', 3, '5673813', '0', '1947-08-05', '1953-12-25', 'CASA: JULIÁN ALVAREZ 1019 NEGOCIO: JULIÁN ALVAREZ 1017', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '4235-03739', '0', '4771-0237/'),
(673, '972', 'ERNESTO JUAN', 'ECHEVERRIA - ', '4760-2722', '156-042-2934', 'c_sieche@live.com.ar', NULL, NULL, NULL, NULL, '', 3, '8253224', '0', '1946-04-15', NULL, 'SAN LORENZO 1987- VILLA BALLESTER CP 1653', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', 'NO TIENE', '0', '4768-5121 - FAX 4847-3556'),
(674, '224', 'ALICIA ENRIQUETA', 'ETCHART - FLORES', '4631-6897', '0', 'aliciaenriquetaetchart@gmail.com', 'JORGE', 'FLORES', NULL, NULL, '', 3, '6361824', '7596903', '1950-04-29', '1947-05-06', 'PASAJE RAWSON JCAJAL 1220', 'CABA', 'CABA', 'ARGENTINA', '1406', 'daniruock@hotmail.com', '0', '0', '4613-6848'),
(675, '792', 'ALDO', 'FABIETTI - YOVINE', '4750-2321', '154-947-0308', 'seguridad_higiene_industrial@yahoo.com.ar', 'GLADYS', 'YOVINE', NULL, NULL, '', 3, '13431696', '13492097', NULL, NULL, NULL, NULL, NULL, 'ARGENTINA', '0', '0', 'NO TIENE', '0', '4759-9813 (trabajo esposo)'),
(676, '857', 'SUSANA BEATRIZ', 'FAVERIO - VILLALOBOS', '6380-6072', '155-954-3111', 'susanafaverio@yahoo.com.ar', 'JORGE A.', 'VILLALOBOS', NULL, NULL, '', 3, '12079929', '12983346', '1956-03-20', '1957-10-09', 'MAZA 694 ', 'CABA', 'CABA', 'ARGENTINA', '1220', '0', '0', '0', NULL),
(677, '766', 'Rosa', 'ALVAREZ - FERNANDEZ', '4792-9731 // 4793-1902 (Rosa)', '1555-148390  // 155-730-2868 (Rosa)', 'rosatrini@hotmail.com', 'María Ester', 'FERNANDEZ', NULL, NULL, '', 3, '6032479', '0', '1953-06-13', '1952-06-14', 'LASISLAO MARTINEZ 276 P.B. \"C\"', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '0', '0', '4792-0107'),
(678, '237', 'GERONIMO RAMON', 'FERNANDEZ - ', '4483-2679', '155-489-0456', 'docerobles@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '11862908', '0', NULL, NULL, 'RIO PIEDRAS 255, 2º C', 'MORÓN', 'BUENOS AIRES', 'ARGENTINA', '1708', '0', 'NO TIENE', '0', NULL),
(679, '238', 'JORGE LUIS', 'FERNANDEZ - ', '4244-8151', '154-969-4170', 'lena3vi@gmail.com', NULL, NULL, NULL, NULL, '', 3, '6082330', '0', NULL, NULL, 'JUNCAL 1044 - ', 'TEMPERLEY', 'BUENOS AIRES', 'ARGENTINA', '1834', 'Jorge.FernandezBaldi@bancogalicia.com.ar', '4235-03391', '0', NULL),
(680, '240', 'MANUEL', 'FERNÁNDEZ LOPEZ - CARUSSO', '4373-5069', '155-402-7478 // 155-840-7640 (Gonzalo)', 'lilianaecaruso7@gmail.com', 'LILIANA EDITH', 'CARUSSO', NULL, NULL, '', 3, '12021332', '14157199', '1960-03-15', '1960-03-24', 'MORENO 1817 3º \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1094', 'fliaferlop@yahoo.com.ar', 'NO TIENE', '0', '4774-4772'),
(681, '725', 'Graciela', 'FERNANDEZ - MISURACA', '4503-7112', '153-165-6027 (Graciela)', 'gracielafergarat@gmail.com', 'Ariel', 'MISURACA', NULL, NULL, '', 3, '13741129', '14151681', NULL, NULL, '0', '0', 'BUENOS AIRES', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '154-539-7187 (Ariel)'),
(682, '244', 'MARIA CRISTINA', 'FERRANDINI - MARTINEZ', '4582-7222', '155-026-0544', 'cristina.ferrandini@gmail.com', 'ADRIAN ENRIQUE', 'MARTINEZ', NULL, NULL, '', 3, '6647886', '4979862', NULL, NULL, 'BOLIVIA 2580', 'CABA', 'CABA', 'ARGENTINA', '1417', 'envasadorasalvacio.rosario@hotmail.com (Jorge)', '4235-00620', '0', NULL),
(683, '274', 'CRISTINA CELIA', 'FERRARI DE MIR - MIR', '(0221)479-3203', '(0221)154-084-287', 'cristinacmir@gmail.com', 'HECTOR JAIME', 'MIR', NULL, NULL, '', 3, '11431446', '11123892', '1954-12-27', '1954-05-02', 'CALLE 139 Nº 669 ENTRE 45 Y 46', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', '0', '4235-00580', '0', NULL),
(684, '248', 'REINALDO CARLOS', 'FERRAUDI - ', '4450-2535/0427', '155-126-4902', 'rferraudi@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '14565128', '0', NULL, NULL, 'INTENDENTE CARRERE 775 -HAEDO-  1706', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', NULL),
(685, '412', 'MONICA KARINA (FALLECIÓ)', 'FERRERO - Andrea y Gladys hermanas ellas se van a ocupar', '5197-7102 (Andrea)', '11-49499297 (Andrea)', 'andyferrero@hotmail.com', NULL, 'Andrea y Gladys hermanas ellas se van a ocupar', NULL, NULL, '', 3, '22362162', '0', NULL, NULL, 'AV. A.T. DE ALVEAR 430  1ER  PISO DTO 1', 'DON TORCUATO', 'BUENOS AIRES', 'ARGENTINA', '1406', '0', 'NO TIENE', '0', '0'),
(686, '252', 'HUGO', 'FILIPOVIC - POMPA', '42068869', 'Marisa:155-803-2277 // Hugo: 153-630-8328', 'hugo_filipovic@yahoo.com.ar', 'MARISA', 'POMPA', NULL, NULL, '', 3, '14701393', '0', NULL, NULL, 'Pasaje Sarrachaga 6019 1º3', 'WILDE', 'BUENOS AIRES', 'ARGENTINA', 'B1875ACE', '0', '0', '0', '4381-4002 INT 42'),
(687, '263', 'ELIDA CESILIA', 'FRASSON - OTERO', '4571-7678', '154-030-0276 (José)', 'esteticadalauzier@yahoo.com.ar', 'JOSE CARLOS', 'OTERO', NULL, NULL, '', 3, '3566734', '4401333', '1937-07-01', '1943-01-09', 'MARISCAL FRANCISCO SOLANO LÓPEZ 2759', 'CABA', 'CABA', 'ARGENTINA', '1419', '0', '4235-00534', '0', '4574-4188'),
(688, '272', 'MÓNICA', 'GABAY - ', '4792-8044', '154-550-5963', 'monagabay@yahoo.com', NULL, NULL, NULL, NULL, '', 3, '21482725', '0', NULL, NULL, 'FRAY LUIS BELTRAN 850', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '0', '0', '5235-0761'),
(689, '275', 'ROSA', 'GARBELLANO - HIDALGO', '4521-2247', '155-175-0027', 'rosag5916@gmail.com', 'JORGE LUIS', 'HIDALGO', NULL, NULL, '', 3, '3757717', '0', '1938-06-25', '1933-12-10', 'COMBATIENTES DE MALVINAS 3970 4TO PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1431', 'cidepcursos@gmail.com // fabian12@fibertel.com.ar // laurapenapsp@gmail.com', '0', '0', 'Hijo Fabián: 4555-7303 - FAX 4897-4653/54'),
(690, '296', 'MARTHA BEATRIZ', 'GATTI - PUGNALI', '4242-2679', '116-027-2838', 'gattimar12@yahoo.com.ar', 'ARMANDO LUIS', 'PUGNALI', NULL, NULL, '', 3, '13935235', '0', NULL, NULL, 'CABRERA 1053', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', NULL, 'NO TIENE', NULL, '0'),
(691, '303', 'SILVANA ROSA', 'GENTILE - ', '4584-5780', '156-970-3441', 'silvanag55@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '12013725', '0', NULL, NULL, 'FRAGATA SARMIENTO 950 ', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', '0', '0', '4585-4007 - FAX 4585-6593'),
(692, '307', 'DANIEL OSCAR', 'GILBERTO - COSTA', '4650-3151', '156-473-5014', 'dogilberto@intramed.net', 'LILIANA ROSA', 'COSTA', NULL, NULL, '', 3, '11597935', '0', NULL, NULL, 'DIRECTORIO 591-', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '0', '0', NULL),
(693, '309', 'JORGE ALBERTO', 'GIORDANO - RELLA', '4300-3015', '154-474-8036', 'jgiordano@jagi.com.ar', 'VIVIANA ZULEMA CONCEPCION', 'RELLA', NULL, NULL, '', 3, '14195159', '17636056', '1960-09-15', '1966-01-06', 'AVDA. MARTÍN GARCIA 560  8º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1268', '0', '0', '0', '5218-4506/07'),
(694, '314', 'MARCELO OSCAR', 'GIULIANO - ', '4201-7067', '155-054-7106 M // 155-228-5258 (Horacio)', 'estebangiuli@hotmail.com', 'LIDIA', NULL, NULL, NULL, '', 3, '22364475', '0', NULL, NULL, 'GRAL. LAVALLE 343/345', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', 'mgiuliano@live.com.ar', '4235-04779', '0', 'Esteban: 156-1737897'),
(695, '317', 'RICARDO LUIS', 'GOLTZMAN - CHESZES', '0', '154-475-5580', 'ricardo.goltzman@gmail.com', 'ADELA MARTA', 'CHESZES', NULL, NULL, '', 3, '10130604', '13403623', NULL, NULL, 'GOMEZ DE FONSECA 577', 'CABA', 'CABA', 'ARGENTINA', '1407', 'PabloLujan@mundial.com.ar', '4235-02848', '0', '4303-3142'),
(696, '319', 'CARLOS DANIEL', 'GONCEBATT - ALAYA', '4257-2698', '153-140-8209 (Cristina)', 'cris-alaya@hotmail.com', 'CRISTINA REGINA', 'ALAYA', NULL, NULL, '', 3, '11926524', '12267092', '1955-11-16', '1956-02-08', 'ANTARTIDA ARGENTINA 176 ', 'EZPELETA', 'BUENOS AIRES', 'ARGENTINA', '1882', '0', '0', '0', NULL),
(697, '325', 'MARIA DE LUJAN', 'GONZALEZ ORONO - MUR', '(02945)48-0410', '156-355-2820', 'orosimariamaria@live.com.ar', 'HUGO ALBERTO', 'MUR', NULL, NULL, '', 3, '5290041', '4394095', '1946-05-06', '1942-03-11', 'Juan Manuel de Rosas 342', 'TREVELIN', 'CHUBUT', 'ARGENTINA', '9203', '0', '3169-5999', '0', 'M. Lujan: 153-553-0523'),
(698, '880', 'VIVIANA GRACIELA', 'GRANDI - LOVERAS', '(0341)461-4466', '(0341)153-60-0266 (Jorge) // (0341)155-86-3375 (Viviana)', 'grandiviviana@hotmail.com', 'JORGE', 'LOVERAS', NULL, NULL, '', 3, '12736331', '0', NULL, NULL, 'OLEGARIO V. ANDRADE 771- ', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', 'NO TIENE', '0', '0'),
(699, '328', 'JOSE ANTONIO', 'GRILLO - ', '4650-9313', '155-325-1060', 'grilloja@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '93454346', '0', NULL, NULL, 'Buchardo  1807', 'VILLA LUZURIAGA', 'BUENOS AIRES', 'ARGENTINA', '1754', '0', 'NO TIENE', '0', NULL),
(700, '330', 'EDUARDO DANIEL', 'GRUDEN - ', '4659-0912 (Estudio Gruden)', '154-188-7901 (Daniel)', 'estudiodeg@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '10927686', '0', NULL, NULL, 'DR. PEDRO CHUTRO 654', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', 'NO TIENE', '0', '4650-6135'),
(701, '335', 'GONZALO, DESIDERIO', 'GUMA, GONZALO, DESIDERIO', '4286-2984', '156-537-3517', 'guma.32@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '24569152', NULL, NULL, NULL, 'EJERCITO DE LOS ANDES 640', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', '0', '4235-02670', '0', NULL),
(702, '985', 'DANIEL', 'HERNANDEZ - MAS BENGOECHEA', '155-709-1015 (Daniel)', '156-350-5808 (Cecilia)', 'daniel_hernandez@fibertel.com.ar', 'CECILIA', 'MAS BENGOECHEA', NULL, NULL, '', 3, '22750222', '26258035', '1972-04-15', '1977-09-18', 'JOSÉ HERNANDEZ 2045 - 5º \'A\'', 'CABA', 'CABA', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '0'),
(703, '341', 'CARLOS DANIEL', 'HERRERA - ', '4460-1285', '155-327-6973', 'chyasociados@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '12702377', '0', '1956-12-15', NULL, 'Perito Moreno 1567', 'RAMOS MEJIA', 'BUENOS AIRES', 'ARGENTINA', '1704', '0', '0', '0', '2077-8278'),
(704, '344', 'ESTEBAN', 'HETENYI - DE LA HOZ', '4772-5078', '0', 'estebanhetenyi@yahoo.com.ar', 'ALICIA BEATRIZ', 'DE LA HOZ', NULL, NULL, '', 3, '14768504', '5255523', '1946-11-03', '1945-11-05', 'MAURE 2265', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', 'NO TIENE', '0', '4394-0102 - FAX 4772-5078'),
(705, '982', 'LAURA BEATRIZ', 'HONEKER - ', '4922-7829', '154-065-4894 (Laura)', 'lhoneker@gmail.com', NULL, NULL, NULL, NULL, '', 3, '16144676', '0', NULL, NULL, 'PASAJE REPUBLICA 535', 'CABA', 'CABA', 'ARGENTINA', '1424', 'santoja64@gmail.com', 'NO TIENE', '0', '156-118-6409 (Marcelo)'),
(706, '345', 'JOSE LUIS', 'HORNIK - ', '4862-3742', '156-094-7735 (José) ', 'hornikjoseluis@gmail.com', NULL, NULL, NULL, NULL, '', 3, '11885326', '0', NULL, NULL, 'CORRIENTES 4280 PISO 9 DEP. A  ', 'CABA', 'CABA', 'ARGENTINA', '1195', 'hornik.vivian@gmail.com', '0', '0', '011-2146-3633 (negocio fliar)'),
(707, '990', 'ROBERTO', 'HÜNICKEN - 0', '(0351) 422-8043', '(0351) 155-95-9427 ', 'robhunicken@yahoo.com.ar', '0', '0', NULL, NULL, '', 3, '13708207', '0', NULL, NULL, '0', '0', '0', 'ARGENTINA', '0', 'robhunicken@yahoo.com.ar', '2777-00129', '0', '0'),
(708, '347', 'FAUSTINO', 'IKEI - TAMANAHA', '0', '116-798-6784 (Sabrina)', 's_ikei@hotmail.com', 'ALICIA ELENA', 'TAMANAHA', NULL, NULL, '', 3, '0', '10260636', '1952-02-15', NULL, 'AVDA ESTADO DE ISRAEL 4283 2º PISO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1425', '0', 'NO TIENE', '0', '5282-0471 (Ofic. Sabrina)'),
(709, '256', 'RENE MATILDE', 'INDA - ', '4981-1408', '113-202-3028 (Martín)', 'fortimartin@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '2646676', '0', NULL, NULL, 'EDUARDO ACEVEDO   269 1ª  \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1405', '0', 'NO TIENE', '0', 'Martin Forti: 155-1420364 (EQUIV)'),
(710, '984', 'HERMELINDA ROSANA', 'ACOSTA - 0', '4953-1550', '155-017-0183', 'racosta@trabajo.gob.ar', '0', '0', NULL, NULL, '', 3, '17839569', '0', NULL, NULL, 'Av. Rivadavia 2069 13ºB', 'CABA', 'CABA', 'ARGENTINA', '1033', '0', '0', '0', '0'),
(711, '355', 'OLGA', 'JACOBSON - ', '(0341)455-6456', '(0341)155-77-4379', 'olgajacobson@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '13488677', '0', '1957-09-28', NULL, 'URIARTE 841', 'ROSARIO', 'SANTA FÉ', 'ARGENTINA', '2000', '0', '*4235-00675', '0', NULL),
(712, '358', 'JOSE', 'JANICEK - KOLLAR', '4256-3127', '0', 'josejanicek@yahoo.com.ar', 'ELENA', 'KOLLAR', NULL, NULL, '', 3, '18772192', '18772194', '1934-07-19', '1936-02-26', 'CALLE 9 Nº  4950', 'BERAZATEGUI', 'BUENOS AIRES', 'ARGENTINA', '1884', '0', '4235-00576', '0', NULL),
(713, '364', 'ELIAS', 'KAPLAN - MOSCOVITZ', '4524-1696', '155-177-3295 // 156-802-7774 (Darío)', 'dkaplan@ort.edu.ar', 'ELSA', 'MOSCOVITZ', NULL, NULL, '', 3, '8242388', '5696071', '1945-07-16', '1947-06-26', 'Olazabal 5039', 'CABA', 'CABA', 'ARGENTINA', '1431', '0', '0', '0', '155-160-8982 - FAX 4524-1696'),
(714, '365', 'SUSANA', 'KAPLAN - PEREDO', '4743-9779', '154-029-7873', 'susanakp44@gmail.com', 'SALVADOR ISMAEL', 'PEREDO', NULL, NULL, '', 3, '8309814', '8256758', '1944-03-26', '1946-09-20', 'GENERAL MOSCONI 156', 'BECCAR', 'BUENOS AIRES', 'ARGENTINA', '1643', '0', '4235-00544', '0', '4732-3525'),
(715, '369', 'CARLOS', 'KLUG - BOESE', '4790-5230', '155307-6781', 'cklug@nakase.com.ar', 'RENATA', 'BOESE', NULL, NULL, '', 3, '5312440', 'LC 5593526', '1950-05-03', '1947-07-03', 'DIAZ VELEZ  2929', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '4235-00040', '0', '4768-4242   INT: 120 - FAX 4768-4242  INT: 111'),
(716, '894', 'GABRIEL JORGE', 'LAZART - LISTE', '4738-9429/4849-0503', '1551556829', 'casabalzar@ciudad.com.ar', 'VIRGINIA', 'LISTE', NULL, NULL, '', 3, '13018346', '16398848', '1959-02-25', '1963-01-16', 'PACIFICO RODRIGUEZ 5185     ', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', '0', '0', NULL),
(717, '388', 'MIRTA SILVIA', 'LIBMAN - ', '4623-5869 // 5430-2062', '156-011-1273', 'arquitectalibman@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '12588322', '0', NULL, NULL, 'CAPDEVILLA 1236 ', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', '0', '0', '4627-2828'),
(718, '394', 'JUAN CARLOS', 'LOMBARDO - ', '4758-1367', '153-208-4939', 'lidiaderico@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '11488719', '0', '1965-09-12', NULL, 'MITRE 6074', 'CASEROS', 'BUENOS AIRES', 'ARGENTINA', '1678', 'lombardoveronica1@gmail.com', 'NO TIENE', '0', 'FAX 4758-7310'),
(719, '399', 'LUIS ALBERTO', 'LOPEZ - RODRIGUEZ', '4487-7529 (Luis)', '155-871-4710 (Elena)', 'elos52@hotmail.com', 'ELENA', 'RODRIGUEZ', NULL, NULL, '', 3, '3845195', '10632521', '1944-03-26', '1952-08-31', 'EDIFICIO 4 ENTRADA 2 3ºPISO DTO 7 BARRIO VILLA ROSSI', 'CIUDAD EVITA', 'BUENOS AIRES', 'ARGENTINA', '1778', '0', '0', '0', '4635-6336 (Elena)'),
(720, '899', 'OBDULIO HECTOR', 'LORUSSO - ', '4432-7643', '155-766-7643', 'obduliolorusso@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '4408056', '0', '1943-01-18', NULL, 'ACHAVAL 631 ', 'CABA', 'CABA', 'ARGENTINA', '1406', 'mari_l83@hotmail.com // paula_l85@hotmail.com', '0', '0', 'Garay. 4306-7940 - FAX Mariana: 156-6003993'),
(721, '903', 'GUILLERMO ERNESTO (FALLECIÓ)', 'MAGNANI - TRESPALACIOS', '02241-475-289', '1559-367407', 'aletrespa@hotmail.com', 'ALEJANDRA ISABEL', 'TRESPALACIOS', NULL, NULL, '', 3, '13228979', '13228880', '1960-03-27', '1959-08-09', 'BARRIO ARQUITECTURA- CASA 31-   ', 'RANCHOS', 'BUENOS AIRES', 'ARGENTINA', '1987', 'guillermo.magnani@mpsa.com', 'NO TIENE', '0', '0'),
(722, '989', 'LILIANA', 'MARASEO - 0', '4257-2849', '156-767-4661', 'liliana_sorsaburu@hotmail.com', '0', '0', NULL, NULL, '', 3, '17321782', '0', NULL, NULL, '0', '0', '0', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '0'),
(723, '420', 'JORGE GUILLERMO', 'MARCEL - ', '4696-2244', '155-420-5806 (Guillermo)', 'vamarcel2005@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '10130604', '0', NULL, NULL, 'CARABOBO 3072', 'SAN JUSTO', 'BUENOS AIRES', 'ARGENTINA', '1754', '0', '6353-00882', '0', 'Viviana: 4461-0503'),
(724, '422', 'ALBERTO OSCAR', 'MARCIO - COCCARO', '4251-2037', '155-389-4190', 'cristian.marcio@gmail.com', 'SILVIA BIBIANA', 'COCCARO', NULL, NULL, '', 3, '4644403', '5631945', NULL, NULL, 'JUJUY 1322 ', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', '1878', '0', NULL, '0', ' - FAX 4257-6458'),
(725, '435', 'MARIA CRISTINA', 'MARTINEZ - MARTINEZ', '4624-9084', '153-273-8151  (Jorge)', 'martinez_mariac@yahoo.com.ar', 'JORGE ALEJANDRO', 'MARTINEZ', NULL, NULL, '', 3, '16755140', '14868692', '1963-05-14', '1962-04-10', 'Segunda Rivadavia 21207', 'MORÓN', 'BUENOS AIRES', 'ARGENTINA', '1708', 'arquiesmar@yahoo.com.ar', '0', '0', 'Hijo: 156-207-2847 Jorge - FAX Cristina: 155-413-0209'),
(726, '437', 'MARIO VIDAL', 'MARTINO, MARIO VIDAL - CRUCIANELLI, ALCIRA ENRIQUETA', '4798-2281 // 4793-3657', NULL, 'mario.v.martino@hotmail.com', 'ALCIRA ENRIQUETA', 'CRUCIANELLI', NULL, NULL, '', 3, '5568289', NULL, NULL, NULL, 'DIAGONAL SALTA 1189', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', 'guillermo.javier.ricci@hotmail.com', NULL, NULL, '4545-2500 - FAX 4542-8081'),
(727, '443', 'CARLOS RUBEN', 'MASKIN - ', '4201-0046', '156-745-0919', 'carlosrmaskin@speedy.com.ar', NULL, NULL, NULL, NULL, '', 3, '5290041', '0', NULL, NULL, 'BERUTI 236', 'AVELLANEDA', 'BUENOS AIRES', 'ARGENTINA', '1870', '0', '0', '0', NULL),
(728, '908', 'CARLO MARINO', 'MASTROIANI - ', '4451-4984', '155-182-7707', 'escobillonesmastro@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '92421266', '0', '1959-08-12', NULL, 'MUÑOZ 752 ', 'SAN MIGUEL', 'BUENOS AIRES', 'ARGENTINA', '1663', '0', '4235-05034', '0', NULL),
(729, '909', 'DANIEL ERNESTO', 'MATSUYAMA - ', '0', '156-228-4361', 'danielmatsuyama@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '12514141', '0', NULL, NULL, 'BOGOTA 3725 DEPTO 4', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', '0', '0', '03489-492454 - FAX 03489-492462'),
(730, '910', 'JORGE LUIS', 'MELGAR - RODRIGUEZ ROCHA', '5063-8636', '155-063-8635', 'melgarjl@hotmail.com', 'PAOLA', 'RODRIGUEZ ROCHA', NULL, NULL, '', 3, '23087558', '92755233', '1972-12-09', '1975-03-30', 'PASAJE FACUNDO 415', 'CABA', 'CABA', 'ARGENTINA', '1408', '0', '0', '0', '0'),
(731, '456', 'EDUARDO', 'MERELLO - CABRERA', '4734-0306', '155-120-1377', 'normaecabrera@hotmail.com', 'NORMA', 'CABRERA', NULL, NULL, '', 3, '8242956', '6196340', '1944-10-25', '1949-09-13', 'Asamblea 3944', 'SANTOS LUGARES', 'BUENOS AIRES', 'ARGENTINA', '1676', '0', '4235-00646', '0', NULL),
(732, '912', 'GUSTAVO GERARDO', 'MESSORE - COMPAÑY', '4568-8145', '155-848-9695', 'gustavomessore@yahoo.com.ar', 'ALICIA HAYDEE', 'COMPAÑY', NULL, NULL, '', 3, '12453922', '13827874', '1958-10-04', '1960-01-08', 'ARREGUI 3655 CP 1417', 'CABA', 'CABA', 'ARGENTINA', '1417', '0', '0', '0', '0'),
(733, '470', 'OMAR ANTONIO', 'MONTES - SINCCIT', '4650-2715', '156-525-6604', 'omarmontes1@hotmail.com', 'EMILIA MERCEDES', 'SINCCIT', NULL, NULL, '', 3, '7826429', '4750221', NULL, NULL, 'ESMERALDA 1030 ', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00548', '0', NULL),
(734, '676', 'ANA MARIA', 'MONTI - ', '4552-8076', '156-194-1437', 'montiam@gmail.com', NULL, NULL, NULL, NULL, '', 3, '6401392', '0', '1950-05-18', NULL, 'HOLMBERG 1168', 'CABA', 'CABA', 'ARGENTINA', '1428', '0', '0', '0', '4787-1423 - FAX 4552-8076'),
(735, '472', 'GERARDO ANGEL', 'MONTIEL - HORNYAK', '(03327) 416-107', '154-047-6624 (Gerardo)', 'gmontiel@cotelnet.com.ar', 'ALBA GABRIELA', 'HORNYAK', NULL, NULL, '', 3, '14447375', '0', '1960-12-20', '1962-05-29', 'AV.DE LOS CONSTITUYENTES 4099 BARRIO ALTOS DE PACHECO', 'BENAVIDEZ (TIGRE)', 'BUENOS AIRES', 'ARGENTINA', '1621', 'gmontiel@trf.com.ar', 'NO TIENE', '0', '4736-5182/4736-5597'),
(736, '476', 'PABLO JOSE', 'MORENO - ABASTO', '0237-487-2930', '154-949-8882 (Pablo)', 'pablomoreno871@speedy.com.ar', 'PATRICIA MABEL', 'ABASTO', NULL, NULL, '', 3, '17466826', '17701413', NULL, NULL, 'CORONEL PEDRO TOSCANO  874- VILLA SARMIENTO', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00549', '0', NULL),
(737, '63', 'SUSANA HAYDEE', 'MORENO - BITENSKY', '4633-0067', '156-569-4588 (Susana)', 'NO TIENE', 'DANIEL HUGO (FALLECIO)', 'BITENSKY', NULL, NULL, '', 3, '0', '4371373', NULL, NULL, 'CURAPALIGUE 150 5º C', 'CABA', 'CABA', 'ARGENTINA', '1406', '0', '0', '0', '0'),
(738, '484', 'PEDRO JOSE', 'NICCOLI - ELISSETCHE', '4217-0308', '156-330-4517 (José)', 'jose.niccoli@yahoo.com.ar', 'SANDRA BEATRIZ', 'ELISSETCHE', NULL, NULL, '', 3, '16533283', '0', '1963-07-19', '1967-01-04', 'LA BLANQUEADA 3801', 'SARANDI', 'BUENOS AIRES', 'ARGENTINA', '1872', 'sandra.elissetche@yahoo.com.ar', '4235-03710', '0', '4208-7871 - FAX Trabajo Sandra: 4227-9384'),
(739, '919', 'JAIME ENRIQUE', 'NOGUERA, JAIME ENRIQUE - IRIGOITI, GRACIELA', '0221-473-0621', '154-282372', 'jenvillaelisa2000@yahoo.com.ar', 'GRACIELA', 'IRIGOITI', NULL, NULL, '', 3, '12466935', '13909563', '1958-06-02', '1959-02-24', 'CALLE 422 BIS Nº CH35 ENTRE 2 Y 3', 'VILLA ELISA', 'BUENOS AIRES', 'ARGENTINA', '1894', '0', 'NO TIENE', '0', NULL),
(740, '920', 'OLGA ESTHER', 'NOVELINO - BARRERA', '02241-481059', '02241-15541394', 'elpalenqueranchos@gmail.com', 'RICARDO', 'BARRERA', NULL, NULL, '', 3, '14961907', '14961997', '1962-07-31', '1963-04-05', 'RIVADAVIA 4270-   ', 'RANCHOS', 'BUENOS AIRES', 'ARGENTINA', '1987', '0', '4235-02755', '0', '02241-482-356'),
(741, '487', 'JUAN CARLOS', 'OCCHIUTO - ', '0220-48-33755', '154-426-0477', 'gertomar@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '13906465', '0', NULL, NULL, 'GUEMES 3560 ', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1722', '0', '0', '0', NULL),
(742, '492', 'FELIX JOSE', 'OJEDA - BOSCO', '4296-0721', '1569576003', 'felix_ojeda2003@yahoo.com.ar', 'MARTA ALICIA', 'BOSCO', NULL, NULL, '', 3, '0', '11169845', '1950-01-09', '1954-06-27', 'JORGE MILES 792', 'MONTE GRANDE', 'BUENOS AIRES', 'ARGENTINA', '1842', '0', '4235-00550', '0', '4346-6860 - FAX 4346-6869'),
(743, '493', 'IRMA GLORIA', 'OLEINK - 0', '4774-8593', '153-353-2760', 'igoleink@hotmail.com', '0', '0', NULL, NULL, '', 3, '3548402', '0', '1936-11-04', NULL, 'OLLEROS 1811 3º B', 'CABA', 'CABA', 'ARGENTINA', '1426', '0', '4235-00551', '0', '0'),
(744, '497', 'RODOLFO MIGUEL', 'OREIRO - ', '4613-6606', '154-073-6198', 'oflodorleugim@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '8290456', '0', NULL, NULL, 'CULPINA 91 6ºB', 'CABA', 'CABA', 'ARGENTINA', '0', '0', 'NO TIENE', '0', '4343-5882'),
(745, '927', 'ELBA MARIA', 'PAGANO - GARAY', '4787-2965', '156-950-4243 (Elba)', 'epagano2013@gmail.com', 'JUAN JOSE', 'GARAY', NULL, NULL, '', 3, '11421448', '5222057', '1955-04-08', '1947-11-17', 'USARES 2280 1º PISO \"C\"', 'CABA', 'CABA', 'ARGENTINA', '1428', 'jotajotage@yahoo.com', '0', '0', '154049-5645 (Juan Jose) '),
(746, '513', 'STELLA MARIS', 'PAGLIERI - CAPRIOLI', '0232-4425043', '(02324) 156-49-705', 'stellamaris-pc@hotmail.com', 'RODOLFO CARLOS', 'CAPRIOLI', NULL, NULL, '', 3, '5941454', 'LE 4928587', '1948-12-30', '1946-04-06', 'CALLE 27 Nª 1002', 'MERCEDES', 'BUENOS AIRES', 'ARGENTINA', '6600', '0', 'NO TIENE', '0', ' - FAX 02324-15649705'),
(747, '516', 'SUSANA', 'PANDOLFI - ', '4687-2857', '156-195-8315', 'susipandolfi@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '11154019', '0', '1953-10-28', NULL, 'GUAMINI 1718', 'CABA', 'CABA', 'ARGENTINA', '1440', '0', '0', '0', NULL),
(748, '525', 'WALTER JAVIER', 'PAREDES - ', '03461-43-6798', '03461-15642151', 'paredes4c@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '17733174', '0', NULL, NULL, 'RONDÓ 135', 'SAN NICOLAS', 'BUENOS AIRES', 'ARGENTINA', '2900', '0', '4235-03450', '0', NULL),
(749, '486', 'MABEL CESAREA', 'PASQUINI - ', '(03465)497-022', '0', 'cacho887@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '3676669', '0', '1938-08-24', NULL, 'GARAY 498', 'BOMBAL', 'SANTA FÉ', 'ARGENTINA', '2179', '0', '4235-00006', '0', '03465-497666 - FAX Sobrina miriam: 0341-498-7551'),
(750, '530', 'MARCELO DANIEL', 'PAVON - DAVILE', '2076-2167', '154-046-4280', 'marcelopavon_7@hotmail.com', 'SANDRA', 'DAVILE', NULL, NULL, '', 3, '16626244', '18394227', '1963-08-14', '1967-03-11', 'NECOCHEA 3507', 'LOMAS DEL MIRADOR', 'BUENOS AIRES', 'ARGENTINA', '1752', '0', '0', '0', '4682-9114'),
(751, '536', 'CARLOS ALBERTO', 'PEREIRAS - NUNES DA FONSECA', '03488-638-032', '154-147-8104 (Nancy)', 'familiapereiras@hotmail.com', 'NANCY', 'NUNES DA FONSECA', NULL, NULL, '', 3, '4433292', '0', NULL, NULL, 'CAMPO GRANDE CLUB DE CAMPO LOTE 32 ', 'PILAR', 'BUENOS AIRES', 'ARGENTINA', '1629', 'caperei12@gmail.com', '4235-00286', '0', NULL),
(752, '548', 'LUIS ALBERTO', 'PEREZ - FERNANDEZ', '4635-3356', '0', 'perez.luis.alberto@hotmail.com', 'CARMEN', 'FERNANDEZ', NULL, NULL, '', 3, '13302192', '0', NULL, NULL, 'PIERES  1173 DEP. B', 'CABA', 'CABA', 'ARGENTINA', '1440', '0', '0', '0', '4635-3078'),
(753, '550', 'ALICIA BEATRIZ', 'PICATTO - ', '(03496)423-312', '(03496) 154-64-935 (Alicia)', 'romigonzalez_83@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '14760947', '0', NULL, NULL, 'ALMAFUERTE 625', 'ESPERANZA', 'SANTA FÉ', 'ARGENTINA', '3080', '0', '*4235-00655', '0', '(03496)420-507 (Alicia)'),
(754, '563', 'JOSÉ MARÍA', 'PINTOS - CARRICONDO', '4257-0175', '155-018-2654', 'info.pintos@gmail.com', 'GLADYS', 'CARRICONDO', NULL, NULL, '', 3, '10932003', '0', '1953-03-28', '1959-05-12', 'LIBERTAD 344', 'QUILMES', 'BUENOS AIRES', 'ARGENTINA', '1878', '0', '4235-03499', '0', '4343-3134'),
(755, '916', 'CLAUDIA', 'PIRRI - ', '4665-4331', '115-669-0524', 'claudia_pirri@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '16379038', '0', '1963-04-20', NULL, 'LOS PATOS 2255 -    ', 'HURLINGHAM', 'BUENOS AIRES', 'ARGENTINA', '1686', '0', '0', '0', '155-669-0524 - FAX 4822-4705'),
(756, '570', 'SILVIA NOEMI', 'PREGAL - BOUJON', '4295-9529', '153-638-2406 (Marcelo) // 155-925-6633 (Silvia)', 'silviapregal@gmail.com', 'MARCELO NESTOR', 'BOUJON', NULL, NULL, '', 3, '16321109', '14482132', '1963-03-06', '1961-03-10', 'RUTA 52 Nº 3350', 'CANNING', 'BUENOS AIRES', 'ARGENTINA', '1804', 'mboujon@datamarkets.com.ar', '4235-00553', '0', NULL),
(757, '978', 'MARIA CRISTINA', 'PUENTE - ', '4724-4924', '154-973-1948', 'puentemc812@gmail.com', NULL, NULL, NULL, NULL, '', 3, '10108126', '0', '1951-12-08', NULL, 'VENEZUELA 3170 PISO 1º DTO \"B\"', 'FLORIDA', 'BUENOS AIRES', 'ARGENTINA', '1602', '0', '4235-01971', '0', '4724-4925'),
(758, '578', 'DIEGO FERNANDO', 'QUEIMALIÑOS, DIEGO FERNANDO - BATTINI, OLGA ROSANA', '4763-5194', '155-228-5263', 'diegofer68@live.com.ar', 'OLGA ROSANA', 'BATTINI', NULL, NULL, '', 3, '20251715', '21538335', NULL, NULL, 'LOS CEIBOS 1264', 'VILLA ADELINA', 'BUENOS AIRES', 'ARGENTINA', '1607', '0', NULL, '0', '154-940-8354 Rosana'),
(759, '579', 'DESIDERIO ANTONIO', 'QUIROGA - TOSTICARELLI', '(0358) 472-7152', '(0266) 154-65-3095 (Eduardo) / 02664 15653095 (eduardo)', 'desiderio777@hotmail.com', 'MARTA RAQUEL', 'TOSTICARELLI', NULL, NULL, '', 3, '0', '2390196', '1927-07-07', '1935-10-01', 'Ayacucho Nro. 866 ', 'CAPITAL', 'SAN LUIS ', 'ARGENTINA', '5700', 'escribaniaquiroga@gmail.com', '4235-00082', '0', '(0266) 443-5897 (Escribanía)'),
(760, '454', 'Hipólito Enrique', 'RAMIREZ - ', '0237-487-3138', '156-094-4240', 'henriqueramirez@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '14193667', '0', NULL, NULL, 'LEGUIZAMÓN 1359 ', 'FRANCISCO ALVAREZ', 'BUENOS AIRES', 'ARGENTINA', '1746', '0', 'NO TIENE', '0', '0237-487-2292'),
(761, '934', 'JORGE OMAR', 'RAPONI - CUCCO', '(03465)470-418', '(0341)156-16-0804', 'raponijo@gmail.com', 'VIVIANA TERESA', 'CUCCO', NULL, NULL, '', 3, '10944856', '10944891', '1953-11-02', '1954-06-22', 'A. ANGELOZZI 766', 'ALCORTA', 'SANTA FÉ', 'ARGENTINA', '2117', '0', '*4235-01436', '0', 'Banco: 03462-480188'),
(762, '609', 'CARLOS ALBERTO', 'RODRIGUEZ - ', '(02241)42-3041', '(02241)156-72-898', 'cacho54rodrigue@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '10854329', '0', NULL, NULL, 'JACARANDÁ 21 ENTRE HIPÓLITO IRIGOYEN Y CASALINS', 'CHASCOMUS', 'BUENOS AIRES', 'ARGENTINA', '7130', '0', 'NO TIENE', '0', NULL),
(763, '938', 'ANIBAL D', 'RODRIGUEZ - MOSCOLONI', '(02271)420-455', '02226-1560-5654', 'anibal_rodriguez460@yahoo.com.ar', 'MARIA INES', 'MOSCOLONI', NULL, NULL, '', 3, '11961836', '12735052', '1956-01-18', '1956-12-02', 'AVDA SAN MARTIN 460', 'SAN MIGUEL DEL MONTE', 'BUENOS AIRES', 'ARGENTINA', '7220', '0', 'NO TIENE', '0', '02226-15442540'),
(764, '633', 'DANIEL ALBERTO', 'RUETTER - ', '(0264)423-5255', '264155047090', 'danielruetter91@gmail.com', 'GLADYS', NULL, NULL, NULL, '', 3, '10 370191', '0', NULL, NULL, 'HERMOGENES RUIZ SUR 1230', 'SAN JUAN', 'SAN JUAN', 'ARGENTINA', '5400', '0', 'NO TIENE', '0', NULL),
(765, '605', 'María Pía', 'RUFFOLO - RIVOTTA', '4245-0251', '155-311-9846 // 154-472-0148 (Preferente)', 'mariapiaruffolo@hotmail.com', 'ABEL RAUL (FALLECIÓ)', 'RIVOTTA', NULL, NULL, '', 3, '93869757', '0', NULL, NULL, 'RAMÓN FALCÓN 1450', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', '0', 'NO TIENE', '0', NULL),
(766, '649', 'ANA MARIA TENAGLIA', 'SANTILLAN - TENAGLIA', '4764-1803', '153-244-4889 (Juan Carlos) // 153-244-5311 (Ana) // 153-416-7002 (Lucía)', 'lucia_tenaglia@yahoo.com.ar', 'JUAN CARLOS', 'TENAGLIA', NULL, NULL, '', 3, '10823474', '8626893', NULL, NULL, 'BOULEVARD BALLESTER 6063', 'VILLA BALLESTER', 'BUENOS AIRES', 'ARGENTINA', '1653', '0', 'NO TIENE', '0', ' - FAX 4781-1746 (hermano)'),
(767, '653', 'EDUARDO', 'SARTO - RODRIGUEZ DURAZZO', '4703-3783', '1540-743040', 'eduardosarto@yahoo.com.ar', 'VIVIANA', 'RODRIGUEZ DURAZZO', NULL, NULL, '', 3, '17635321', '18122554', '1966-03-06', '1966-10-07', 'AVDA CABILDO 4392 P.B. 6', 'CABA', 'CABA', 'ARGENTINA', '1429', '0', '4235-01940', '0', 'Nextel: 159*2829'),
(768, '656', 'PABLO', 'SCHAER - 0', '0', '(0388)154-73-0588 (Pablo)', 'pablo.schaer@aromasdelaquebrada.com.ar', '0', '0', NULL, NULL, '', 3, '26359162', '0', '1977-11-19', NULL, 'BELGRANO 417', 'TILCARA', 'JUJUY', 'ARGENTINA', 'Y4624AFI', '0', '4235-04052', '0', NULL),
(769, '944', 'ORLANDO HECTOR', 'SCHIAFFINO - GONZALEZ', '4902-5866', '1556629033', 'mrgonzalezds@hotmail.com', 'MARIA ROSA', 'GONZALEZ', NULL, NULL, '', 3, '8104751', '12093102', '1949-12-26', '1955-04-16', 'RIVADAVIA 4687  3ER PISO DTO \"B\" ', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', '4235-02873', '0', '4127-7202'),
(770, '665', 'CARLOS', 'SCOTTI - RODRIGUEZ', '4658-0799/4209-9596', '154-472-2233', 'scotticarlos@yahoo.com.ar', 'BEATRIZ AMELIA', 'RODRIGUEZ', NULL, NULL, '', 3, '4607125', '4738889', '1944-07-21', '1943-12-18', 'PARAGUAY 762 ', 'HAEDO', 'BUENOS AIRES', 'ARGENTINA', '1706', '0', '4235-00557', '0', NULL);
INSERT INTO `clientes` (`id`, `identificacion`, `nombre`, `apellido`, `telefono`, `celular`, `email`, `nombre_conyugue`, `apellido_conyugue`, `celular_conyugue`, `email_conyugue`, `password`, `hotel`, `dni`, `dni_conyugue`, `fecha_nacimiento`, `fecha_naciemiento_conyugue`, `domicilio`, `localidad`, `provincia`, `pais`, `cod_postal`, `email2`, `id_rci`, `id_interval`, `telefono_laboral`) VALUES
(771, '139', 'JOSE ANTONIO', 'SEGUNDO COLLADOS - BARBORINI', '4237-7883', '155-651-6961', 'segundo.jose@hotmail.com', 'LILIANA INES', 'BARBORINI', NULL, NULL, '', 3, '93397652', '12760408', '1945-01-02', '1958-11-29', 'URQUIZA 341 ', 'FLORENCIO VARELA', 'BUENOS AIRES', 'ARGENTINA', '1888', 'claudia_pirri@hotmail.com // moro_antonella@hotmail.com', 'NO TIENE', '0', '4355-1620'),
(772, '945', 'JAVIER FRANCISCO', 'SENIN - MARTINEZ SIERRA', '0', '153-790-0559 (María)', 'soki52@hotmail.com', 'María del Socorro', 'MARTINEZ SIERRA', NULL, NULL, '', 3, '12821653', '0', '1958-09-19', NULL, 'AVDA.DEL LIBERTADOR 2491   PISO 19 DEPTO \"D\"', 'OLIVOS', 'BUENOS AIRES', 'ARGENTINA', '1636', '0', '*4235-01989', '0', '0'),
(773, '680', 'MIRTHA', 'SIGNORELLO - PAGANELLI', '4682-9363', '156-753-4424', 'mirtamsignorello@gmail.com', 'ANGEL ALBERTO', 'PAGANELLI', NULL, NULL, '', 3, '0', '13222718', '1956-10-20', '1957-05-05', 'SEVERO G. GRANDE DE ZEQUEIRA 5875', 'CABA', 'CABA', 'ARGENTINA', '1440', 'rmbarrera164@hotmail.com', '*4235-00665', '0', '4864-6883'),
(774, '947', 'LILIA SILVIA', 'SILVERA - DIAZ', '4741-3658', '152-322-2751', 'liliasilvera@gmail.com', 'SERGIO NORBERTO', 'DIAZ', NULL, NULL, '', 3, '13610708', '12441630', '1959-12-30', '1958-06-23', 'TRIUNVIRATO 1612-    ', 'DON TORCUATO', 'BUENOS AIRES', 'ARGENTINA', '1611', 'martinplast@hotmail.com', 'NO TIENE', '0', ' - FAX Sergio: 154-9143381'),
(775, '686', 'JUAN DOMINGO RAMON', 'SOSA - ', '(0220)494-2039', '156-304-6123', 'jrintegral@hotmail.com', 'ELISA', NULL, NULL, NULL, '', 3, '7829702', '0', NULL, NULL, 'ALBERTO ECHAGÜE 2335', 'MERLO', 'BUENOS AIRES', 'ARGENTINA', '1716', '0', '4235-00666', '0', '(0220)494-2065'),
(776, '690', 'CARLOS ENRIQUE', 'SPECTOR - RESCH', '4632-4946', '155-793-3058  Sofía', 'cspector@cponline.org.ar', 'SOFIA', 'RESCH', NULL, NULL, '', 3, '13566912', '0', '1959-09-24', '1960-08-11', 'MORELLOS 794  PISO 7º  DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1406', 'soresch@hotmail.com', 'NO TIENE', '0', '5300-4007'),
(777, '695', 'HILDA ELIZABETH', 'STEINBAUM - ', '4855-8505', '154-928-0106 (Gustavo Smeke)', 'elmotivo23@hotmail.com', NULL, NULL, NULL, NULL, '', 3, '4461311', '0', NULL, NULL, 'SCALABRINI ORTIZ 258 9º \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', '*4235-00753', '0', '155-939-1960 (Hilda)'),
(778, '699', 'RAUL ANTONIO', 'SUAREZ, RAUL ANTONIO - ALBA, MONICA', '4661-0877', '15656561882', 'malba@estudiocontablealba.com.ar', 'MONICA', 'ALBA', NULL, NULL, '', 3, '0', '12228491', NULL, NULL, 'ALVAREZ JONTE 2095', 'CASTELAR', 'BUENOS AIRES', 'ARGENTINA', '1712', '0', NULL, '0', '5777-4523 - FAX 4623-5764'),
(779, '706', 'GRACIELA', 'SULE - MARTINEZ', '0', '1558-145975', 'graciela_sule@hotmail.com', 'JUAN CARLOS', 'MARTINEZ', NULL, NULL, '', 3, '5075501', '7667485', '1956-09-10', '1949-10-29', 'SAENZ PEÑA 1157', 'TIGRE', 'BUENOS AIRES', 'ARGENTINA', '1648', '0', '*4235-00560', '0', '4749-6915 - FAX 4731-0022'),
(780, '713', 'DANIEL PEDRO', 'TARDIVO - CALLES', '0', '11 - 5061-2883', 'danielpedrotardivo@gmail.com', 'MARINA', 'CALLES', NULL, NULL, '', 3, '13881743', '22115370', '1959-10-30', '1971-04-13', 'CORRIENTES 2032  3 PISO 2A2', 'CABA', 'CABA', 'ARGENTINA', '1045', '0', 'NO TIENE', '0', NULL),
(781, '714', 'JORGE ALBERTO', 'TAYLOR, JORGE ALBERTO - OCHOA, SILVIA MARIEL', '(0221)469-3684', '0221-15428-4625', 'jorge_a_taylor@hotmail.com', 'SILVIA MARIEL', 'OCHOA', NULL, NULL, '', 3, '17291520', NULL, NULL, NULL, 'SAN MARTIN 138 ', 'ENSENADA', 'BUENOS AIRES', 'ARGENTINA', '1925', NULL, 'NO TIENE', NULL, NULL),
(782, '715', 'BEATRIZ', 'TEDESCO - GIOVANELLI', '(02323) 492-380', '(02323) 153-63-225 (Beatriz)', 'viveromundodeplantas@gmail.com', 'ARMANDO', 'GIOVANELLI', NULL, NULL, '', 3, '0', '4750652', NULL, NULL, 'MATEO S. CASO 809', 'CAPILLA DEL SEÑOR', 'BUENOS AIRES', 'ARGENTINA', '2914', 'kukytedesco50@gmail.com', '0', '0', NULL),
(783, '730', 'IRENE BEATRIZ', 'COUFFIGNAL - TORRES', '(03329)424-920', '0', 'gediamante@yahoo.com.ar', 'ORFILIO', 'TORRES', NULL, NULL, '', 3, '2388250', '2588250', '1936-09-29', '1929-06-14', '3 DE FEBRERO 180', 'SAN PEDRO', 'BUENOS AIRES', 'ARGENTINA', '2930', 'gdiamante59@gmail.com', '4235-02789', '0', NULL),
(784, '954', 'NELIDA HAYDEE', 'TUMANE - STELLATO', '4302-0404', '155-804-9683 / 155-6591736 (NELIDA)', 'nellytumane@hotmail.com', 'MIGUEL ANGEL', 'STELLATO', NULL, NULL, '', 3, '6201430', '5530661', '1949-09-17', '1948-12-20', 'CALIFORNIA 1518 PB DEPTO A- CAP FED  CP 1289', 'CABA', 'CABA', 'ARGENTINA', '1289', '0', 'NO TIENE', '0', ' - FAX 4205-5144'),
(785, '752', 'GUILLERMO', 'VALIMBRI - VALIMBRI', '0221-4711519', '0221-155919450', 'guillermovalimbri@hotmail.com', 'GUILLERMO LUIS', 'VALIMBRI', NULL, NULL, '', 3, '0', '0', NULL, NULL, 'CALLE 26 Nº 3894 ENTRE 493 Y 494', 'GONET', 'BUENOS AIRES', 'ARGENTINA', '1897', '0', '0', '0', NULL),
(786, '757', 'MARTHA', 'VALSAMAKIS - ', '4641-4431 // 4641-4943', '156-271-3374 // 156-270-4431 (Silvina ella se ocupa)', 'marthavals10@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '3617576', '0', NULL, NULL, 'MONTIEL 316 1º PISO DTO 4', 'CABA', 'CABA', 'ARGENTINA', '1408', 'silvinaven@yahoo.com', '4235-04147', '0', '4661-2123 '),
(787, '106', 'VERÓNICA JORGELINA', 'VAZQUEZ - 0', '4793-7352', '153-642-4591', 'Veronica.J.Vazquez@walmart.com', '0', '0', NULL, NULL, '', 3, '22655035', '0', NULL, NULL, 'EDUARDO COSTA 1850 1ºA', 'MARTINEZ', 'BUENOS AIRES', 'ARGENTINA', '1640', '0', '4235-03785', '0', '0'),
(788, '764', 'HECTOR OSCAR', 'VAZQUEZ - RIOS', '4236-5743', '154-939-4696 (Gricelda)', 'griceldarios@gmail.com', 'GRICELDA BEATRIZ', 'RIOS', NULL, NULL, '', 3, '0', '10816152', '1951-01-15', '1953-02-24', 'ALTAMIRA 1580', 'RAFAEL CALZADA', 'BUENOS AIRES', 'ARGENTINA', '1847', '0', '4235-00565', '0', '4291-0355/4291-0975 - FAX 4291-0975'),
(789, '776', 'FERNANDO', 'VIDAL - PAZZELLI', '(03462)450-837 ', '154-477-3771 // (03462)154-11-601 ', 'alepazper60@gmail.com', 'ALEJANDRA', 'PAZZELLI', NULL, NULL, '', 3, '12078031', '0', '1958-02-09', NULL, 'CALLE 53 1193', 'VILLA CAÑAS', 'SANTA FÉ', 'ARGENTINA', '0', '0', '4235-04903', '0', '0'),
(790, '779', 'ESTEBAN RAMON', 'VILLAMAYOR - CACERES', '4922-8730', '154-0308347', 'villamayoresteban@gmail.com', 'ESTELA MARIA', 'CACERES', NULL, NULL, '', 3, '6613958', '6287123', '1949-04-25', '1950-03-09', 'RIGLOS 569 2º PISO', 'CABA', 'CABA', 'ARGENTINA', '1424', '0', 'NO TIENE', '0', '4348-3500 int: 2067'),
(791, '780', 'EDMUNDO', 'VILLAMIL - ANGIO', '2060-2767', '156-052-9703 (Daina) // 116-749-1641 (Elsa)', 'elsaangio@gmail.com', 'ELSA NORMA', 'ANGIO', NULL, NULL, '', 3, '0', '2733029', '1933-07-01', '1936-03-30', 'BAIGORRIA 5571 DTO \"A\"', 'CABA', 'CABA', 'ARGENTINA', '1408', '0', 'NO TIENE', '0', '4978-1480'),
(792, '782', 'ANIBAL GUSTAVO', 'VISUS - GARCIA', '(0221) 482-7955', '156-786-2505', 'fliavisus@gmail.com', 'ELSA SOLEDAD', 'GARCIA', NULL, NULL, '', 3, '5065216', '4503995', NULL, NULL, 'CALLE 39 Nº 715 ENTRE CALLE 9 Y 10', 'LA PLATA', 'BUENOS AIRES', 'ARGENTINA', '1900', '0', 'NO TIENE', '0', '4275-1280  INT. 110'),
(793, '962', 'RICARDO ENRIQUE', 'WILSON - ', '4244-6361', '154-195-4052', 'ricardoewilson@gmail.com', NULL, NULL, NULL, NULL, '', 3, '10740528', '0', NULL, NULL, 'BOEDO 916', 'LOMAS DE ZAMORA', 'BUENOS AIRES', 'ARGENTINA', '1832', '0', '4235-01672', '0', ' - FAX 4202-0298'),
(794, '964', 'RICARDO CRUZ', 'ZABALZA - ', '4375-4286', '153-236-3653 (Alexandra) / 153-236-3654 (Ricardo)', 'alexandrabzabalza@yahoo.com.ar', NULL, NULL, NULL, NULL, '', 3, '4417259', '0', '1943-06-06', NULL, 'PARANA 690 PISO 4 DTO \"B\"', 'CABA', 'CABA', 'ARGENTINA', '1017', 'jczabalza@gmail.com', 'NO TIENE', '0', '4374-5454/4371-3189 // 153-236-3650 (Juan Cruz)'),
(795, '965', 'JUAN ANTONIO AD', 'ZAFFARONI - BONOMO', '3965-4742', '156-902-6826', 'jzaffaroni@gmail.com', 'SANDRA', 'BONOMO', NULL, NULL, '', 3, '18231302', '0', '1967-08-23', NULL, 'REMEDIOS DE ESCALADA DE SAN MARTIN 4948 ', 'CABA', 'CABA', 'ARGENTINA', '1407', '0', 'NO TIENE', '0', 'Sandra: 155-645-0176'),
(796, '798', 'JORGE RICARDO', 'ZANIRATTO - GARCIA', '4242-2605', '1560-538940 (no puede recibir llam)', 'stellammgarcia@hotmail.com', 'STELLA MARIS', 'GARCIA', NULL, NULL, '', 3, '14851133', '10814276', NULL, NULL, 'ACEVEDO 1335', 'BANFIELD', 'BUENOS AIRES', 'ARGENTINA', '1828', '0', '4235-00571', '0', '4248-4115'),
(797, '808', 'EDUARDO', 'ZIMERMAN - ', '4854-3613', '154-0558473', 'emzimerman@fibertel.com.ar', NULL, NULL, NULL, NULL, '', 3, '12949920', '0', NULL, NULL, 'LUIS MARÍA DRAGO 349', 'CABA', 'CABA', 'ARGENTINA', '1414', '0', 'NO TIENE', '0', NULL),
(798, '526', 'PATRICIA LOURDES', 'PARES - PARES', '03488-440986', '155-565-4225 (Patricia)', 'patolourdes@hotmail.com', 'MIGUEL ALFREDO', 'PARES', NULL, NULL, '', 3, '17201265', '0', '1964-07-29', NULL, 'ACHEGA 3174', 'CABA', 'BUENOS AIRES', 'ARGENTINA', '0', '0', '4235-04942', '0', NULL),
(799, '906', 'DOMINGO', 'MAONE - ', '0341-421-7036', '(0341)153-427-677 (Ezequiel)', 'cannonrosario@yahoo.com.ar', 'EZEQUIEL MATIAS', NULL, NULL, NULL, '', 3, '4859539', '0', NULL, NULL, 'MENDOZA 120 2º PISO', 'ROSARIO', 'SANTA FÉ', '0', '2000', '0', '0', '0', NULL),
(800, '973', 'CARLOS', 'QUINTEROS - PEREZ', '4451-3282', '155-591-3438', 'cquinteros79@hotmail.com', 'SUSANA NOEMI', 'PEREZ', NULL, NULL, '', 3, '0', '5477455', '1947-02-14', '1947-01-30', 'JUAN JOSE PASO 1234 ', 'MUÑIZ', 'BUENOS AIRES', 'ARGENTINA', '1663', '0', 'NO TIENE', '0', '4469-8800 int: 229');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `clientes_con_saldo`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `clientes_con_saldo` (
`id` int(11)
,`identificacion` varchar(10)
,`apellido` varchar(200)
,`nombre` varchar(200)
,`telefono` varchar(200)
,`celular` varchar(200)
,`email` varchar(200)
,`nombre_conyugue` varchar(200)
,`apellido_conyugue` varchar(200)
,`celular_conyugue` varchar(20)
,`email_conyugue` varchar(200)
,`password` varchar(10)
,`hotel` int(11)
,`dni` varchar(45)
,`dni_conyugue` varchar(45)
,`fecha_nacimiento` date
,`fecha_naciemiento_conyugue` date
,`domicilio` varchar(500)
,`localidad` varchar(200)
,`provincia` varchar(45)
,`pais` varchar(45)
,`cod_postal` varchar(45)
,`email2` varchar(200)
,`id_rci` varchar(45)
,`id_interval` varchar(45)
,`telefono_laboral` varchar(255)
,`saldo` decimal(42,0)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultas`
--

CREATE TABLE `consultas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `asunto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consulta` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `respuesta` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado_consulta_id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `consultas`
--

INSERT INTO `consultas` (`id`, `asunto`, `consulta`, `respuesta`, `estado_consulta_id`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, '23423', 'dsfsf', 'respuesta, de consulta por hora , probando endpoint a ver si anda bien', 2, 74, '2021-07-13 23:56:20', '2021-07-15 19:13:12'),
(2, '23423', 'dsfsf', NULL, 1, 74, '2021-07-14 15:50:08', '2021-07-14 15:50:08'),
(3, 'asunto', 'consulta', 'respuesta nueva este es el texto', 2, 74, '2021-07-14 20:11:19', '2021-07-15 18:24:36'),
(4, 'asunto', 'consulta', 'contestada desde la web', 2, 458, '2021-07-26 13:48:44', '2021-07-26 14:10:16'),
(5, 'asunto 2da postman', 'consulta 2da postman', 'hola', 2, 458, '2021-07-26 13:49:02', '2021-08-18 19:59:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas_corrientes`
--

CREATE TABLE `cuentas_corrientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `importe` int(11) NOT NULL,
  `expensa_id` bigint(20) UNSIGNED DEFAULT NULL,
  `recibo_id` bigint(20) UNSIGNED DEFAULT NULL,
  `movimiento_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cliente_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cuentas_corrientes`
--

INSERT INTO `cuentas_corrientes` (`id`, `fecha`, `fecha_vencimiento`, `importe`, `expensa_id`, `recibo_id`, `movimiento_id`, `created_at`, `updated_at`, `cliente_id`) VALUES
(1, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 1, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 541),
(2, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 2, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 541),
(3, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 3, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 541),
(4, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 4, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 541),
(5, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 5, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 545),
(6, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 6, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 549),
(7, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 7, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 550),
(8, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 8, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 551),
(9, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 9, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 552),
(10, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 10, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 552),
(11, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 11, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 552),
(12, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 12, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 552),
(13, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 13, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 559),
(14, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 14, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 569),
(15, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 15, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 577),
(16, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 16, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 585),
(17, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 17, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 585),
(18, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 18, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 585),
(19, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 10, 19, NULL, 1, '2021-08-10 13:59:52', '2021-08-18 15:15:09', 592),
(20, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 10, 20, NULL, 1, '2021-08-10 13:59:52', '2021-08-18 15:15:09', 592),
(21, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 21, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 596),
(22, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 22, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 596),
(23, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 23, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 597),
(24, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 24, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 611),
(25, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 25, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 611),
(26, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 26, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 503),
(27, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 27, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 615),
(28, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 28, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 483),
(29, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 29, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 513),
(30, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 30, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 513),
(31, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 31, NULL, 1, '2021-08-10 13:59:52', '2021-08-13 19:29:19', 526),
(32, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 32, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 526),
(33, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 33, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 535),
(34, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 34, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 568),
(35, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 35, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 599),
(36, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 5, 36, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 599),
(37, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 37, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 460),
(38, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 38, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 464),
(39, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 39, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 464),
(40, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 40, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 471),
(41, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 41, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 471),
(42, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 42, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 471),
(43, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 43, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 491),
(44, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 44, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 491),
(45, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 45, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 493),
(46, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 46, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 494),
(47, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 47, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 494),
(48, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 48, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 506),
(49, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 49, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 518),
(50, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 50, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 519),
(51, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 51, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 519),
(52, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 52, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 531),
(53, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 53, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 534),
(54, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 54, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 534),
(55, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 55, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 544),
(56, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 56, NULL, 1, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 544),
(57, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 15, 57, NULL, 1, '2021-08-10 13:59:53', '2021-08-18 15:16:40', 560),
(58, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 58, NULL, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 624),
(59, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 59, NULL, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 458),
(60, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 60, NULL, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 459),
(61, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 61, NULL, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:09', 461),
(62, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 62, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 473),
(63, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 63, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 474),
(64, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 64, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 478),
(65, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 65, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 480),
(66, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 66, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 481),
(67, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 67, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 482),
(68, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 68, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 485),
(69, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 69, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 486),
(70, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 70, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 488),
(71, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 71, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 489),
(72, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 72, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 511),
(73, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 73, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 512),
(74, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 74, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 523),
(75, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 75, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 528),
(76, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 76, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 529),
(77, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 77, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 540),
(78, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 78, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 543),
(79, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 79, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 543),
(80, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 80, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 545),
(81, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 81, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 550),
(82, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 82, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 551),
(83, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 83, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 553),
(84, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 84, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 559),
(85, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 85, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 572),
(86, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 86, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 573),
(87, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 87, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 574),
(88, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 88, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 575),
(89, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 89, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 577),
(90, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 90, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 583),
(91, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 91, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 584),
(92, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 92, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 586),
(93, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 93, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 498),
(94, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 94, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 592),
(95, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 95, NULL, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 594),
(96, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 96, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 594),
(97, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 97, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 596),
(98, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 98, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 596),
(99, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 99, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 605),
(100, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 100, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 607),
(101, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 101, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 613),
(102, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 102, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 613),
(103, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 103, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 503),
(104, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 104, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 617),
(105, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 105, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 623),
(106, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 106, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 490),
(107, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 8, 107, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 490),
(108, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 108, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 455),
(109, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 109, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 455),
(110, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 110, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 465),
(111, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 111, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 465),
(112, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 112, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 563),
(113, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 113, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 469),
(114, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 114, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 470),
(115, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 115, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 472),
(116, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 116, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 475),
(117, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 117, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 475),
(118, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 118, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 495),
(119, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 119, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 495),
(120, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 120, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 496),
(121, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 121, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 501),
(122, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 122, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 513),
(123, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 123, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 513),
(124, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 124, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 515),
(125, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 125, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 516),
(126, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 126, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 521),
(127, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 127, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 525),
(128, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 128, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 525),
(129, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 129, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 535),
(130, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 130, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 564),
(131, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 131, NULL, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 567),
(132, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 132, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 570),
(133, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 133, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 576),
(134, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 134, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 578),
(135, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 135, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 588),
(136, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 136, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 588),
(137, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 137, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 604),
(138, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 138, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 606),
(139, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 9, 139, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 618),
(140, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 140, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 453),
(141, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 141, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 457),
(142, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 142, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 463),
(143, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 143, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 477),
(144, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 144, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 491),
(145, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 145, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 494),
(146, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 146, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 500),
(147, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 147, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 504),
(148, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 148, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 505),
(149, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 149, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 506),
(150, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 150, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 508),
(151, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 151, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 514),
(152, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 152, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 514),
(153, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 153, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 517),
(154, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 154, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 518),
(155, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 155, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 520),
(156, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 156, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 530),
(157, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 157, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 531),
(158, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 158, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 532),
(159, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 159, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 544),
(160, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 160, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 557),
(161, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 161, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 561),
(162, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 162, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 562),
(163, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 163, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 579),
(164, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 164, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 580),
(165, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 165, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 582),
(166, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 166, NULL, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 591),
(167, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 167, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 591),
(168, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 168, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 601),
(169, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 169, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 601),
(170, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 170, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 603),
(171, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 171, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 608),
(172, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 172, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 609),
(173, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 173, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 612),
(174, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 174, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 612),
(175, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 10, 175, NULL, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 616),
(176, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 231, 176, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 160),
(177, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 231, 177, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 425),
(178, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 123, 178, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 165),
(179, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 123, 179, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 165),
(180, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 123, 180, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 439),
(181, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5445, 181, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 83),
(182, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5445, 182, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 161),
(183, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5445, 183, NULL, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 164),
(184, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5445, 184, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 167),
(185, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 185, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 148),
(186, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 186, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 156),
(187, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 187, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 160),
(188, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 188, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 152),
(189, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 189, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 152),
(190, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 190, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 150),
(191, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 191, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 196),
(192, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 192, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 204),
(193, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 455145, 193, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 150),
(194, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 455145, 194, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 150),
(195, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 195, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 93),
(196, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 196, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 151),
(197, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 197, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 163),
(198, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 198, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 169),
(199, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 48414, 199, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 105),
(200, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 48414, 200, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 426),
(201, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 201, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 31),
(202, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 202, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 70),
(203, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 203, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 70),
(204, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 204, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 370),
(205, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 205, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 141),
(206, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 206, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 39),
(207, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 207, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 77),
(208, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 208, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 78),
(209, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 209, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 386),
(210, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 210, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 440),
(211, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 211, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 126),
(212, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 212, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 443),
(213, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 213, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 125),
(214, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 214, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 258),
(215, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 54, 215, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 392),
(216, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 216, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 19),
(217, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 217, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 121),
(218, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 545, 218, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 33),
(219, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 545, 219, NULL, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 33),
(220, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 545, 220, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 48),
(221, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 545, 221, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 61),
(222, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 545, 222, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 106),
(223, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45, 223, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 79),
(224, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 224, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 22),
(225, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 225, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 52),
(226, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 226, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 67),
(227, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 227, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 451),
(228, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 228, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 426),
(229, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 4, 229, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 447),
(230, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15556141, 230, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 57),
(231, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15556141, 231, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 72),
(232, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15556141, 232, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 112),
(233, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15556141, 233, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 118),
(234, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 234, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 59),
(235, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 235, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 129),
(236, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 5454, 236, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 172),
(237, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 237, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 2),
(238, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 238, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 3),
(239, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 239, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 15),
(240, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 240, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 17),
(241, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 241, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 18),
(242, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 242, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 25),
(243, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 243, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 96),
(244, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 244, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 113),
(245, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 245, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 142),
(246, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 15, 246, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 428),
(247, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45615, 247, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 13),
(248, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45615, 248, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 46),
(249, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45615, 249, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 49),
(250, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45615, 250, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 135),
(251, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 45615, 251, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 195),
(252, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 6, 252, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 85),
(253, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 6, 253, NULL, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 449),
(254, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 6, 254, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 449),
(255, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 2369, 255, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 51),
(256, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 2369, 256, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 74),
(257, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 2369, 257, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 186),
(258, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 2369, 258, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 212),
(259, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 2369, 259, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 419),
(260, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 3963936, 260, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 104),
(261, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 3963936, 261, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 116),
(262, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 3963936, 262, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 174),
(263, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 954, 263, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 100),
(264, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 954, 264, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 110),
(265, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 954, 265, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 134),
(266, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66, 266, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 101),
(267, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66, 267, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 102),
(268, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66, 268, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 102),
(269, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66, 269, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 108),
(270, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66, 270, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 201),
(271, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 3, 271, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 157),
(272, '2021-08-10 00:00:00', '2021-08-20 00:00:00', 66955, 272, NULL, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 168),
(273, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 273, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 1),
(274, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 274, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 86),
(275, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 275, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 97),
(276, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 276, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 250),
(277, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 277, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 323),
(278, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 278, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 365),
(279, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 85, 279, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 379),
(280, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 5, 280, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 5),
(281, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 5, 281, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 94),
(282, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 5, 282, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 76),
(283, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 283, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 9),
(284, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 284, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 16),
(285, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 285, NULL, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 35),
(286, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 286, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 36),
(287, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 287, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 44),
(288, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 288, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 54),
(289, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 289, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 58),
(290, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 290, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 63),
(291, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 291, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 75),
(292, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 292, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 83),
(293, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 293, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 86),
(294, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 294, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 98),
(295, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 295, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 117),
(296, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 296, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 124),
(297, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 297, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 166),
(298, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 298, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 200),
(299, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 299, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 219),
(300, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 300, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 222),
(301, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 301, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 226),
(302, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 302, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 253),
(303, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 303, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 259),
(304, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 304, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 263),
(305, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 305, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 288),
(306, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 306, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 305),
(307, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 307, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 307),
(308, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 308, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 314),
(309, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 309, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 316),
(310, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 310, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 317),
(311, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 311, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 318),
(312, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 312, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 322),
(313, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 313, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 330),
(314, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 314, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 332),
(315, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 315, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 336),
(316, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 316, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 351),
(317, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 317, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 352),
(318, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 318, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 358),
(319, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 319, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 375),
(320, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 320, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 383),
(321, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 321, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 385),
(322, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 322, NULL, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 389),
(323, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 323, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 393),
(324, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 324, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 396),
(325, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 325, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 400),
(326, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 326, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 403),
(327, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 327, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 403),
(328, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 328, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 406),
(329, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 329, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 415),
(330, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 330, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 421),
(331, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 331, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 422),
(332, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 332, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 429),
(333, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 333, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 435),
(334, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 334, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 445),
(335, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 335, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 20),
(336, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 336, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 34),
(337, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 337, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 45),
(338, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 338, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 66),
(339, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 339, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 76),
(340, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 340, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 109),
(341, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 341, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 115),
(342, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 342, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 124),
(343, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 343, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 147),
(344, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 344, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 232),
(345, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 345, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 260),
(346, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 346, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 260),
(347, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 347, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 275),
(348, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 348, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 281),
(349, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 349, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 299),
(350, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 350, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 300),
(351, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 351, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 302),
(352, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 352, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 308),
(353, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 353, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 320),
(354, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 354, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 321),
(355, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 355, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 324),
(356, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 356, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 325),
(357, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 357, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 341),
(358, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 358, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 369),
(359, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 359, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 387),
(360, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 360, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 389),
(361, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 361, NULL, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 394),
(362, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 362, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 413),
(363, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 363, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 414),
(364, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 364, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 416),
(365, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 365, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 424),
(366, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 366, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 427),
(367, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 367, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 432),
(368, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 368, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 438),
(369, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 369, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 442),
(370, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 370, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 423),
(371, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 371, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 423),
(372, '2021-08-10 00:00:00', '2023-10-11 00:00:00', 45, 372, NULL, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 452),
(373, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 373, NULL, 1, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 458),
(374, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 374, NULL, 1, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 466),
(375, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 375, NULL, 1, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 473),
(376, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 376, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 473),
(377, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 377, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 479),
(378, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 378, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 479),
(379, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 379, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 486),
(380, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 380, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 488),
(381, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 381, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 497),
(382, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 382, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 497),
(383, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 383, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 507),
(384, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 384, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 507),
(385, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 385, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 511),
(386, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 386, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 512),
(387, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 387, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 523),
(388, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 388, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 529),
(389, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 389, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 538),
(390, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 390, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 538),
(391, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 391, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 539),
(392, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 4, 392, NULL, 1, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 540),
(393, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 393, NULL, 1, '2021-08-13 19:27:53', '2021-08-13 19:29:19', 493),
(394, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 394, NULL, 1, '2021-08-13 19:27:53', '2021-08-13 19:29:19', 493),
(395, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 15, 395, NULL, 1, '2021-08-13 19:27:54', '2021-08-18 15:16:40', 560),
(396, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 396, NULL, 1, '2021-08-13 19:27:54', '2021-08-13 19:29:19', 579),
(397, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 18, 397, NULL, 1, '2021-08-13 19:27:54', '2021-08-18 15:18:48', 614),
(398, '2021-08-18 00:00:00', '2021-08-28 00:00:00', 18, 398, NULL, 1, '2021-08-13 19:27:54', '2021-08-18 15:18:48', 614),
(399, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 399, NULL, 1, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 622),
(400, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 400, NULL, 1, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 622),
(401, '2021-08-13 00:00:00', '2021-08-23 00:00:00', 6, 401, NULL, 1, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 622);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente', NULL, NULL),
(2, 'Aceptado', NULL, NULL),
(3, 'Anulado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados_semana_uso`
--

CREATE TABLE `estados_semana_uso` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estados_semana_uso`
--

INSERT INTO `estados_semana_uso` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente', NULL, NULL),
(2, 'Solicitado', NULL, NULL),
(3, 'Guardado', NULL, NULL),
(4, 'Depositado', NULL, NULL),
(5, 'Usado', NULL, NULL),
(6, 'Confirmado', '2021-08-13 20:38:39', '2021-08-13 20:38:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_consulta`
--

CREATE TABLE `estado_consulta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `estado_consulta`
--

INSERT INTO `estado_consulta` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Pendiente', NULL, NULL),
(2, 'Respondido', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expensas`
--

CREATE TABLE `expensas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `unidades_por_cliente_id` bigint(20) UNSIGNED NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `anio` bigint(20) UNSIGNED DEFAULT NULL,
  `observaciones` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `importe` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `temporada_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `expensas`
--

INSERT INTO `expensas` (`id`, `unidades_por_cliente_id`, `cliente_id`, `anio`, `observaciones`, `importe`, `created_at`, `updated_at`, `temporada_id`) VALUES
(1, 587, 541, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(2, 588, 541, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(3, 589, 541, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(4, 590, 541, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(5, 596, 545, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(6, 598, 549, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(7, 599, 550, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(8, 601, 551, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(9, 603, 552, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(10, 604, 552, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(11, 605, 552, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(12, 606, 552, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(13, 609, 559, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(14, 618, 569, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(15, 626, 577, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(16, 634, 585, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(17, 635, 585, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(18, 636, 585, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(19, 643, 592, 2021, NULL, 10, '2021-08-10 13:59:52', '2021-08-18 15:15:09', 1),
(20, 644, 592, 2021, NULL, 10, '2021-08-10 13:59:52', '2021-08-18 15:15:09', 1),
(21, 648, 596, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(22, 649, 596, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(23, 652, 597, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(24, 664, 611, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(25, 665, 611, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(26, 672, 503, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(27, 674, 615, 2021, NULL, 4, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(28, 516, 483, 2021, NULL, 5, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(29, 550, 513, 2021, NULL, 5, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(30, 551, 513, 2021, NULL, 5, '2021-08-10 13:59:52', '2021-08-13 19:29:18', 1),
(31, 569, 526, 2021, NULL, 5, '2021-08-10 13:59:52', '2021-08-13 19:29:19', 1),
(32, 570, 526, 2021, NULL, 5, '2021-08-10 13:59:52', '2021-08-13 19:29:19', 1),
(33, 580, 535, 2021, NULL, 5, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(34, 617, 568, 2021, NULL, 5, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(35, 653, 599, 2021, NULL, 5, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(36, 654, 599, 2021, NULL, 5, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(37, 488, 460, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(38, 491, 464, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(39, 492, 464, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(40, 499, 471, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(41, 500, 471, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(42, 501, 471, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(43, 523, 491, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(44, 524, 491, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(45, 526, 493, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(46, 529, 494, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(47, 530, 494, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(48, 543, 506, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(49, 559, 518, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(50, 561, 519, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(51, 562, 519, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(52, 575, 531, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(53, 578, 534, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(54, 579, 534, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(55, 593, 544, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(56, 594, 544, 2021, NULL, 6, '2021-08-10 13:59:53', '2021-08-13 19:29:19', 1),
(57, 611, 560, 2021, NULL, 15, '2021-08-10 13:59:53', '2021-08-18 15:16:40', 1),
(58, 480, 624, 2021, NULL, 8, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 2),
(59, 486, 458, 2021, NULL, 8, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 2),
(60, 487, 459, 2021, NULL, 8, '2021-08-10 18:00:37', '2021-08-13 19:23:08', 2),
(61, 489, 461, 2021, NULL, 8, '2021-08-10 18:00:37', '2021-08-13 19:23:09', 2),
(62, 505, 473, 2021, NULL, 8, '2021-08-10 18:00:37', '2021-08-13 19:23:09', 2),
(63, 506, 474, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(64, 512, 478, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(65, 513, 480, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(66, 514, 481, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(67, 515, 482, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(68, 517, 485, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(69, 519, 486, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(70, 521, 488, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(71, 522, 489, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(72, 547, 511, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(73, 549, 512, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(74, 566, 523, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(75, 571, 528, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(76, 573, 529, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(77, 586, 540, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(78, 591, 543, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(79, 592, 543, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(80, 597, 545, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(81, 600, 550, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(82, 602, 551, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(83, 607, 553, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(84, 610, 559, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(85, 620, 572, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:09', 2),
(86, 621, 573, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(87, 622, 574, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(88, 623, 575, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(89, 625, 577, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(90, 632, 583, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(91, 633, 584, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(92, 637, 586, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(93, 638, 498, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(94, 645, 592, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(95, 646, 594, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(96, 647, 594, 2021, NULL, 8, '2021-08-10 18:00:38', '2021-08-13 19:23:10', 2),
(97, 650, 596, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(98, 651, 596, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(99, 659, 605, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(100, 661, 607, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(101, 670, 613, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(102, 671, 613, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(103, 673, 503, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(104, 676, 617, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(105, 681, 623, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(106, 682, 490, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(107, 683, 490, 2021, NULL, 8, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(108, 482, 455, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(109, 483, 455, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(110, 493, 465, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(111, 494, 465, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:10', 2),
(112, 495, 563, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(113, 497, 469, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(114, 498, 470, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(115, 502, 472, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(116, 507, 475, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(117, 508, 475, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(118, 532, 495, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(119, 533, 495, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(120, 534, 496, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(121, 538, 501, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(122, 552, 513, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(123, 553, 513, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(124, 556, 515, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(125, 557, 516, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(126, 564, 521, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(127, 567, 525, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(128, 568, 525, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(129, 581, 535, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(130, 615, 564, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(131, 616, 567, 2021, NULL, 9, '2021-08-10 18:00:39', '2021-08-13 19:23:11', 2),
(132, 619, 570, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(133, 624, 576, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(134, 627, 578, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(135, 639, 588, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(136, 640, 588, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(137, 658, 604, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:11', 2),
(138, 660, 606, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(139, 677, 618, 2021, NULL, 9, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(140, 481, 453, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(141, 484, 457, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(142, 490, 463, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(143, 509, 477, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(144, 525, 491, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(145, 531, 494, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(146, 537, 500, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(147, 539, 504, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(148, 540, 505, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(149, 544, 506, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(150, 545, 508, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(151, 554, 514, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(152, 555, 514, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(153, 558, 517, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(154, 560, 518, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(155, 563, 520, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(156, 574, 530, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(157, 576, 531, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(158, 577, 532, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(159, 595, 544, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(160, 608, 557, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:12', 2),
(161, 613, 561, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(162, 614, 562, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(163, 629, 579, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(164, 630, 580, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(165, 631, 582, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(166, 641, 591, 2021, NULL, 10, '2021-08-10 18:00:40', '2021-08-13 19:23:13', 2),
(167, 642, 591, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(168, 655, 601, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(169, 656, 601, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(170, 657, 603, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(171, 662, 608, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(172, 663, 609, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(173, 666, 612, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(174, 667, 612, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(175, 675, 616, 2021, NULL, 10, '2021-08-10 18:00:41', '2021-08-13 19:23:13', 2),
(176, 441, 160, 2021, NULL, 231, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(177, 476, 425, 2021, NULL, 231, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(178, 446, 165, 2021, NULL, 123, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(179, 447, 165, 2021, NULL, 123, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(180, 468, 439, 2021, NULL, 123, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(181, 409, 83, 2021, NULL, 5445, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(182, 443, 161, 2021, NULL, 5445, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(183, 445, 164, 2021, NULL, 5445, '2021-08-10 19:57:13', '2021-08-10 19:57:13', 12),
(184, 448, 167, 2021, NULL, 5445, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(185, 434, 148, 2021, NULL, 5454, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(186, 439, 156, 2021, NULL, 5454, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(187, 442, 160, 2021, NULL, 5454, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(188, 474, 152, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(189, 475, 152, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(190, 435, 150, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(191, 455, 196, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(192, 457, 204, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(193, 436, 150, 2021, NULL, 455145, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(194, 437, 150, 2021, NULL, 455145, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(195, 411, 93, 2021, NULL, 15, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(196, 438, 151, 2021, NULL, 15, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(197, 444, 163, 2021, NULL, 15, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(198, 450, 169, 2021, NULL, 15, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(199, 418, 105, 2021, NULL, 48414, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(200, 466, 426, 2021, NULL, 48414, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(201, 389, 31, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(202, 402, 70, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(203, 403, 70, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(204, 461, 370, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(205, 432, 141, 2021, NULL, 5454, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(206, 392, 39, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(207, 406, 77, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(208, 407, 78, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(209, 462, 386, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(210, 469, 440, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(211, 428, 126, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(212, 470, 443, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(213, 427, 125, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(214, 459, 258, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(215, 463, 392, 2021, NULL, 54, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(216, 386, 19, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(217, 426, 121, 2021, NULL, 4, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(218, 390, 33, 2021, NULL, 545, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(219, 391, 33, 2021, NULL, 545, '2021-08-10 19:57:14', '2021-08-10 19:57:14', 12),
(220, 394, 48, 2021, NULL, 545, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(221, 400, 61, 2021, NULL, 545, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(222, 419, 106, 2021, NULL, 545, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(223, 408, 79, 2021, NULL, 45, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(224, 387, 22, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(225, 397, 52, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(226, 401, 67, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(227, 460, 451, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(228, 465, 426, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(229, 471, 447, 2021, NULL, 4, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(230, 398, 57, 2021, NULL, 15556141, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(231, 404, 72, 2021, NULL, 15556141, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(232, 422, 112, 2021, NULL, 15556141, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(233, 425, 118, 2021, NULL, 15556141, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(234, 399, 59, 2021, NULL, 5454, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(235, 429, 129, 2021, NULL, 5454, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(236, 451, 172, 2021, NULL, 5454, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(237, 380, 2, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(238, 381, 3, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(239, 383, 15, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(240, 384, 17, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(241, 385, 18, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(242, 388, 25, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(243, 412, 96, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(244, 423, 113, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(245, 433, 142, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(246, 467, 428, 2021, NULL, 15, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(247, 382, 13, 2021, NULL, 45615, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(248, 393, 46, 2021, NULL, 45615, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(249, 395, 49, 2021, NULL, 45615, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(250, 431, 135, 2021, NULL, 45615, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(251, 454, 195, 2021, NULL, 45615, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(252, 410, 85, 2021, NULL, 6, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(253, 472, 449, 2021, NULL, 6, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(254, 473, 449, 2021, NULL, 6, '2021-08-10 19:57:15', '2021-08-10 19:57:15', 12),
(255, 396, 51, 2021, NULL, 2369, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(256, 405, 74, 2021, NULL, 2369, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(257, 453, 186, 2021, NULL, 2369, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(258, 458, 212, 2021, NULL, 2369, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(259, 464, 419, 2021, NULL, 2369, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(260, 417, 104, 2021, NULL, 3963936, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(261, 424, 116, 2021, NULL, 3963936, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(262, 452, 174, 2021, NULL, 3963936, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(263, 413, 100, 2021, NULL, 954, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(264, 421, 110, 2021, NULL, 954, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(265, 430, 134, 2021, NULL, 954, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(266, 414, 101, 2021, NULL, 66, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(267, 415, 102, 2021, NULL, 66, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(268, 416, 102, 2021, NULL, 66, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(269, 420, 108, 2021, NULL, 66, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(270, 456, 201, 2021, NULL, 66, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(271, 440, 157, 2021, NULL, 3, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(272, 449, 168, 2021, NULL, 66955, '2021-08-10 19:57:16', '2021-08-10 19:57:16', 12),
(273, 1, 1, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(274, 18, 86, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(275, 21, 97, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(276, 35, 250, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(277, 57, 323, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(278, 67, 365, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(279, 70, 379, 2021, NULL, 85, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(280, 2, 5, 2021, NULL, 5, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(281, 20, 94, 2021, NULL, 5, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(282, 188, 76, 2021, NULL, 5, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(283, 3, 9, 2021, NULL, 45, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(284, 4, 16, 2021, NULL, 45, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(285, 7, 35, 2021, NULL, 45, '2021-08-10 19:59:47', '2021-08-10 19:59:47', 7),
(286, 8, 36, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(287, 9, 44, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(288, 11, 54, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(289, 12, 58, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(290, 13, 63, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(291, 15, 75, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(292, 17, 83, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(293, 19, 86, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(294, 22, 98, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(295, 25, 117, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(296, 26, 124, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(297, 29, 166, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(298, 30, 200, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(299, 31, 219, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(300, 32, 222, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(301, 33, 226, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(302, 36, 253, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(303, 37, 259, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(304, 40, 263, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(305, 43, 288, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(306, 47, 305, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(307, 48, 307, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(308, 50, 314, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(309, 51, 316, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(310, 52, 317, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(311, 53, 318, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(312, 56, 322, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(313, 60, 330, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(314, 61, 332, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(315, 62, 336, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(316, 64, 351, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(317, 65, 352, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(318, 66, 358, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(319, 69, 375, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(320, 71, 383, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(321, 72, 385, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(322, 74, 389, 2021, NULL, 45, '2021-08-10 19:59:48', '2021-08-10 19:59:48', 7),
(323, 76, 393, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(324, 78, 396, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(325, 79, 400, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(326, 80, 403, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(327, 81, 403, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(328, 82, 406, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(329, 85, 415, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(330, 87, 421, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(331, 88, 422, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(332, 91, 429, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(333, 93, 435, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(334, 96, 445, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(335, 5, 20, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(336, 6, 34, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(337, 10, 45, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(338, 14, 66, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(339, 16, 76, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(340, 23, 109, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(341, 24, 115, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(342, 27, 124, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(343, 28, 147, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(344, 34, 232, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(345, 38, 260, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(346, 39, 260, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(347, 41, 275, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(348, 42, 281, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(349, 44, 299, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(350, 45, 300, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(351, 46, 302, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(352, 49, 308, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(353, 54, 320, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(354, 55, 321, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(355, 58, 324, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(356, 59, 325, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(357, 63, 341, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(358, 68, 369, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(359, 73, 387, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(360, 75, 389, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(361, 77, 394, 2021, NULL, 45, '2021-08-10 19:59:49', '2021-08-10 19:59:49', 7),
(362, 83, 413, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(363, 84, 414, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(364, 86, 416, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(365, 89, 424, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(366, 90, 427, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(367, 92, 432, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(368, 94, 438, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(369, 95, 442, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(370, 97, 423, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(371, 98, 423, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(372, 99, 452, 2021, NULL, 45, '2021-08-10 19:59:50', '2021-08-10 19:59:50', 7),
(373, 485, 458, 2021, NULL, 4, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 1),
(374, 496, 466, 2021, NULL, 4, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 1),
(375, 503, 473, 2021, NULL, 4, '2021-08-13 19:27:50', '2021-08-13 19:29:17', 1),
(376, 504, 473, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(377, 510, 479, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(378, 511, 479, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(379, 518, 486, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(380, 520, 488, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(381, 535, 497, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(382, 536, 497, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(383, 541, 507, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(384, 542, 507, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(385, 546, 511, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(386, 548, 512, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(387, 565, 523, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(388, 572, 529, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(389, 582, 538, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:17', 1),
(390, 583, 538, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 1),
(391, 584, 539, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 1),
(392, 585, 540, 2021, NULL, 4, '2021-08-13 19:27:51', '2021-08-13 19:29:18', 1),
(393, 527, 493, 2021, NULL, 6, '2021-08-13 19:27:53', '2021-08-13 19:29:19', 1),
(394, 528, 493, 2021, NULL, 6, '2021-08-13 19:27:53', '2021-08-13 19:29:19', 1),
(395, 612, 560, 2021, NULL, 15, '2021-08-13 19:27:54', '2021-08-18 15:16:40', 1),
(396, 628, 579, 2021, NULL, 6, '2021-08-13 19:27:54', '2021-08-13 19:29:19', 1),
(397, 668, 614, 2021, NULL, 18, '2021-08-13 19:27:54', '2021-08-18 15:18:48', 1),
(398, 669, 614, 2021, NULL, 18, '2021-08-13 19:27:54', '2021-08-18 15:18:48', 1),
(399, 678, 622, 2021, NULL, 6, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 1),
(400, 679, 622, 2021, NULL, 6, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 1),
(401, 680, 622, 2021, NULL, 6, '2021-08-13 19:27:54', '2021-08-13 19:29:20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechas_semanas_uso`
--

CREATE TABLE `fechas_semanas_uso` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `semana_uso_id` bigint(20) UNSIGNED NOT NULL,
  `fecha_uso` date DEFAULT NULL,
  `dias_usado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `fechas_semanas_uso`
--

INSERT INTO `fechas_semanas_uso` (`id`, `semana_uso_id`, `fecha_uso`, `dias_usado`, `created_at`, `updated_at`) VALUES
(1, 273, '2021-07-03', 4, '2021-08-13 16:29:01', '2021-08-13 16:29:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hoteles`
--

CREATE TABLE `hoteles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hoteles`
--

INSERT INTO `hoteles` (`id`, `nombre`) VALUES
(1, 'HDC'),
(2, 'HDS1'),
(3, 'HDS2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_29_162106_create_permission_tables', 1),
(5, '2021_07_12_183755_create_consultas_table', 2),
(6, '2021_07_12_191943_create_estado_consulta_table', 2),
(29, '2021_07_19_191902_create_expensas_table', 3),
(30, '2021_07_19_194314_create_recibos_table', 3),
(31, '2021_07_19_195250_create_precio_temporada_anios_table', 3),
(32, '2021_07_19_200855_create_estados_table', 3),
(33, '2021_07_19_201340_create_cuentas_corrientes_table', 3),
(34, '2021_07_19_202053_create_movimientos_table', 3),
(36, '2021_07_20_150020_add_voucher_field_to_recibos_table', 4),
(37, '2021_07_20_175215_add_unidad_id_field_to_precio_temporada_anios', 5),
(38, '2021_07_22_130019_modified_observaciones_to_expensas_table', 6),
(40, '2021_07_22_145043_add_cliente_id_to_cuentas_corrientes_table', 7),
(41, '2021_07_22_154023_add_temporada_id_to_expensas_table', 8),
(42, '2021_07_22_173811_modify_observacion_field_to_recibos_table', 9),
(43, '2021_07_27_175926_add_field_to_movimientos_field', 10),
(44, '2021_07_29_164749_add_fecha_vencimiento_to_cuentas_corrientes_table', 11),
(45, '2021_08_03_145527_add_fraccionado_field_to_temporadas_table', 12),
(57, '2021_08_03_171206_create_estados_consultas_table', 13),
(59, '2021_08_03_150608_create_semanas_usos_table', 14),
(60, '2021_08_03_154429_create_semanas_temporadas_table', 14),
(61, '2021_08_03_172010_create_estados_semana_uso_table', 14),
(62, '2021_08_04_150251_add_dias_to_semanas_usos_table', 15),
(63, '2021_08_05_104054_add_unidad_id_to_semanas_usos_table', 16),
(64, '2021_08_05_131254_change_nombre_status_to_semanas_usos_table', 16),
(65, '2021_08_12_144135_add_observacion_field_to_semanas_usos_table', 17),
(66, '2021_08_13_104723_create_fechas_semanas_uso', 18),
(67, '2021_08_18_102906_create_tokens_clientes_table', 19);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `abreviatura` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signo` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`id`, `nombre`, `created_at`, `updated_at`, `abreviatura`, `signo`) VALUES
(1, 'Expensas', NULL, NULL, 'EXP', 1),
(2, 'Recibo', NULL, NULL, 'REC', -1),
(3, ' Ajuste de saldo positivo', NULL, NULL, 'AS+', 1),
(4, ' Ajuste de saldo negativo', NULL, NULL, 'AS-', -1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2021-06-29 19:40:54', '2021-06-29 19:40:54'),
(2, 'role-create', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(3, 'role-edit', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(4, 'role-delete', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(5, 'cliente-list', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(6, 'cliente-create', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(7, 'cliente-edit', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(8, 'cliente-delete', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(9, 'unidad-list', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(10, 'unidad-create', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(11, 'unidad-edit', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(12, 'unidad-delete', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(13, 'hotel-list', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(14, 'hotel-create', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(15, 'hotel-edit', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(16, 'hotel-delete', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(17, 'temporada-list', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(18, 'temporada-create', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(19, 'temporada-edit', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(20, 'temporada-delete', 'web', '2021-06-29 19:40:55', '2021-06-29 19:40:55'),
(21, 'consulta-list', 'web', '2021-07-14 17:20:00', '2021-07-14 17:20:00'),
(22, 'consulta-create', 'web', '2021-07-14 17:20:00', '2021-07-14 17:20:00'),
(23, 'consulta-edit', 'web', '2021-07-14 17:20:00', '2021-07-14 17:20:00'),
(24, 'consulta-delete', 'web', '2021-07-14 17:20:00', '2021-07-14 17:20:00'),
(25, 'expensa-list', 'web', '2021-07-26 15:31:12', '2021-07-26 15:31:12'),
(26, 'expensa-create', 'web', '2021-07-26 15:31:12', '2021-07-26 15:31:12'),
(27, 'expensa-edit', 'web', '2021-07-26 15:31:12', '2021-07-26 15:31:12'),
(28, 'expensa-delete', 'web', '2021-07-26 15:31:12', '2021-07-26 15:31:12'),
(29, 'recibo-list', 'web', '2021-07-26 18:36:44', '2021-07-26 18:36:44'),
(30, 'recibo-create', 'web', '2021-07-26 18:36:44', '2021-07-26 18:36:44'),
(31, 'recibo-edit', 'web', '2021-07-26 18:36:44', '2021-07-26 18:36:44'),
(32, 'recibo-delete', 'web', '2021-07-26 18:36:44', '2021-07-26 18:36:44'),
(33, 'uso-list', 'web', '2021-08-13 20:10:17', '2021-08-13 20:10:17'),
(34, 'uso-create', 'web', '2021-08-13 20:10:17', '2021-08-13 20:10:17'),
(35, 'uso-edit', 'web', '2021-08-13 20:10:17', '2021-08-13 20:10:17'),
(36, 'uso-delete', 'web', '2021-08-13 20:10:17', '2021-08-13 20:10:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_temporada_anios`
--

CREATE TABLE `precio_temporada_anios` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temporada_id` bigint(20) UNSIGNED NOT NULL,
  `anio` int(11) NOT NULL,
  `precio` bigint(20) UNSIGNED NOT NULL,
  `unidad_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `precio_temporada_anios`
--

INSERT INTO `precio_temporada_anios` (`id`, `temporada_id`, `anio`, `precio`, `unidad_id`, `created_at`, `updated_at`) VALUES
(274, 2, 2021, 8, 37, '2021-08-13 19:05:23', '2021-08-13 19:06:15'),
(275, 2, 2021, 9, 38, '2021-08-13 19:05:23', '2021-08-13 19:06:15'),
(276, 2, 2021, 10, 39, '2021-08-13 19:05:23', '2021-08-13 19:06:15'),
(277, 1, 2021, 312, 37, '2021-08-13 19:27:42', '2021-08-18 15:20:51'),
(278, 1, 2021, 4123, 38, '2021-08-13 19:27:42', '2021-08-18 15:20:51'),
(279, 1, 2021, 5435234, 39, '2021-08-13 19:27:42', '2021-08-18 15:20:51'),
(280, 1, 2018, 12, 37, '2021-08-18 19:24:14', '2021-08-18 19:24:14'),
(281, 1, 2018, 3213, 38, '2021-08-18 19:24:14', '2021-08-18 19:24:14'),
(282, 1, 2018, 3123, 39, '2021-08-18 19:24:14', '2021-08-18 19:24:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibos`
--

CREATE TABLE `recibos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `forma_pago` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `importe` int(11) NOT NULL,
  `estado_id` bigint(20) UNSIGNED NOT NULL,
  `observaciones` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `voucher` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2021-06-29 19:39:41', '2021-06-29 19:39:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semanas_temporadas`
--

CREATE TABLE `semanas_temporadas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temporada_id` bigint(20) UNSIGNED NOT NULL,
  `semana` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `semanas_temporadas`
--

INSERT INTO `semanas_temporadas` (`id`, `temporada_id`, `semana`, `created_at`, `updated_at`) VALUES
(1, 12, 28, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(2, 11, 28, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(3, 12, 29, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(4, 11, 29, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(5, 12, 30, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(6, 11, 30, '2021-08-17 13:28:06', '2021-08-17 13:28:06'),
(7, 12, 31, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(8, 11, 31, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(9, 12, 32, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(10, 11, 32, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(11, 12, 33, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(12, 11, 33, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(13, 5, 27, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(14, 7, 27, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(15, 5, 34, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(16, 7, 34, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(17, 5, 35, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(18, 7, 35, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(19, 5, 36, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(20, 7, 36, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(21, 5, 37, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(22, 7, 37, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(23, 5, 38, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(24, 7, 38, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(25, 6, 51, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(26, 1, 51, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(27, 7, 51, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(28, 8, 51, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(29, 6, 52, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(30, 1, 52, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(31, 7, 52, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(32, 8, 52, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(33, 7, 1, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(34, 6, 1, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(35, 1, 1, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(36, 3, 1, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(37, 8, 1, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(38, 7, 2, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(39, 6, 2, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(40, 1, 2, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(41, 3, 2, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(42, 8, 2, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(43, 7, 3, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(44, 6, 3, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(45, 1, 3, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(46, 3, 3, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(47, 8, 3, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(48, 7, 4, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(49, 6, 4, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(50, 1, 4, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(51, 3, 4, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(52, 8, 4, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(53, 7, 5, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(54, 6, 5, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(55, 1, 5, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(56, 3, 5, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(57, 8, 5, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(58, 7, 6, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(59, 6, 6, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(60, 1, 6, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(61, 3, 6, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(62, 8, 6, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(63, 7, 7, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(64, 6, 7, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(65, 1, 7, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(66, 3, 7, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(67, 8, 7, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(68, 7, 8, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(69, 6, 8, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(70, 1, 8, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(71, 3, 8, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(72, 8, 8, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(73, 7, 9, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(74, 6, 9, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(75, 1, 9, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(76, 3, 9, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(77, 8, 9, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(78, 9, 10, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(79, 10, 10, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(80, 9, 11, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(81, 10, 11, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(82, 9, 12, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(83, 10, 12, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(84, 9, 13, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(85, 10, 13, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(86, 9, 14, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(87, 10, 14, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(88, 9, 15, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(89, 10, 15, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(90, 9, 16, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(91, 10, 16, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(92, 9, 17, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(93, 10, 17, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(94, 9, 18, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(95, 10, 18, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(96, 9, 19, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(97, 10, 19, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(98, 9, 20, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(99, 10, 20, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(100, 9, 21, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(101, 10, 21, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(102, 9, 22, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(103, 10, 22, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(104, 9, 23, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(105, 10, 23, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(106, 9, 24, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(107, 10, 24, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(108, 9, 25, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(109, 10, 25, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(110, 9, 26, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(111, 10, 26, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(112, 9, 39, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(113, 10, 39, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(114, 9, 40, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(115, 10, 40, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(116, 9, 41, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(117, 10, 41, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(118, 9, 42, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(119, 10, 42, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(120, 9, 43, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(121, 10, 43, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(122, 9, 44, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(123, 10, 44, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(124, 9, 45, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(125, 10, 45, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(126, 9, 46, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(127, 10, 46, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(128, 9, 47, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(129, 10, 47, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(130, 9, 48, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(131, 10, 48, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(132, 9, 49, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(133, 10, 49, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(134, 9, 50, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(135, 10, 50, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(136, 2, 10, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(137, 4, 10, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(138, 2, 11, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(139, 4, 11, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(140, 2, 12, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(141, 4, 12, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(142, 2, 13, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(143, 4, 13, '2021-08-17 13:28:07', '2021-08-17 13:28:07'),
(144, 2, 14, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(145, 4, 14, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(146, 2, 15, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(147, 4, 15, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(148, 2, 16, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(149, 4, 16, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(150, 2, 17, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(151, 4, 17, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(152, 2, 18, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(153, 4, 18, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(154, 2, 19, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(155, 4, 19, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(156, 2, 20, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(157, 4, 20, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(158, 2, 21, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(159, 4, 21, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(160, 2, 22, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(161, 4, 22, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(162, 2, 23, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(163, 4, 23, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(164, 2, 24, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(165, 4, 24, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(166, 2, 25, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(167, 4, 25, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(168, 2, 26, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(169, 4, 26, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(170, 2, 27, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(171, 4, 27, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(172, 2, 28, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(173, 4, 28, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(174, 2, 29, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(175, 4, 29, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(176, 2, 30, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(177, 4, 30, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(178, 2, 31, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(179, 4, 31, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(180, 2, 32, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(181, 4, 32, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(182, 2, 33, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(183, 4, 33, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(184, 2, 34, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(185, 4, 34, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(186, 2, 35, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(187, 4, 35, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(188, 2, 36, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(189, 4, 36, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(190, 2, 37, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(191, 4, 37, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(192, 2, 38, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(193, 4, 38, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(194, 2, 39, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(195, 4, 39, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(196, 2, 40, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(197, 4, 40, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(198, 2, 41, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(199, 4, 41, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(200, 2, 42, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(201, 4, 42, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(202, 2, 43, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(203, 4, 43, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(204, 2, 44, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(205, 4, 44, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(206, 2, 45, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(207, 4, 45, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(208, 2, 46, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(209, 4, 46, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(210, 2, 47, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(211, 4, 47, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(212, 2, 48, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(213, 4, 48, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(214, 2, 49, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(215, 4, 49, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(216, 2, 50, '2021-08-17 13:28:08', '2021-08-17 13:28:08'),
(217, 4, 50, '2021-08-17 13:28:08', '2021-08-17 13:28:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semanas_usos`
--

CREATE TABLE `semanas_usos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `anio` int(11) NOT NULL,
  `fecha_vencimiento` datetime NOT NULL,
  `fecha_uso` datetime DEFAULT NULL,
  `semana` int(11) DEFAULT NULL,
  `dias_usado` int(11) NOT NULL DEFAULT 0,
  `dias` int(11) DEFAULT NULL,
  `fraccionado` tinyint(1) NOT NULL DEFAULT 0,
  `cliente_id` bigint(20) UNSIGNED DEFAULT NULL,
  `unidad_por_cliente` bigint(20) UNSIGNED DEFAULT NULL,
  `hotel_id` bigint(20) UNSIGNED DEFAULT NULL,
  `temporada_id` bigint(20) UNSIGNED DEFAULT NULL,
  `estado_semana_uso_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unidad_id` bigint(20) UNSIGNED DEFAULT NULL,
  `observacion` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `semanas_usos`
--

INSERT INTO `semanas_usos` (`id`, `anio`, `fecha_vencimiento`, `fecha_uso`, `semana`, `dias_usado`, `dias`, `fraccionado`, `cliente_id`, `unidad_por_cliente`, `hotel_id`, `temporada_id`, `estado_semana_uso_id`, `created_at`, `updated_at`, `unidad_id`, `observacion`) VALUES
(1, 2021, '2021-08-23 00:00:00', NULL, 1, 0, NULL, 0, 541, 587, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(2, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 541, 588, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(3, 2021, '2021-08-23 00:00:00', NULL, 51, 0, NULL, 0, 541, 589, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(4, 2021, '2021-08-23 00:00:00', NULL, 52, 0, NULL, 0, 541, 590, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(5, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 545, 596, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(6, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 549, 598, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(7, 2021, '2021-08-23 00:00:00', NULL, 4, 0, NULL, 0, 550, 599, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(8, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 551, 601, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(9, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 552, 603, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(10, 2021, '2021-08-23 00:00:00', NULL, 4, 0, NULL, 0, 552, 604, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:51', NULL, NULL),
(11, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 552, 605, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(12, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 552, 606, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(13, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 559, 609, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(14, 2021, '2021-08-23 00:00:00', NULL, 4, 0, NULL, 0, 569, 618, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(15, 2021, '2021-08-23 00:00:00', NULL, 4, 0, NULL, 0, 577, 626, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(16, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 585, 634, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(17, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 585, 635, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(18, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 585, 636, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(19, 2021, '2021-08-28 00:00:00', NULL, 2, 0, NULL, 0, 592, 643, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-18 15:15:09', NULL, NULL),
(20, 2021, '2021-08-28 00:00:00', NULL, 3, 0, NULL, 0, 592, 644, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-18 15:15:09', NULL, NULL),
(21, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 596, 648, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(22, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 596, 649, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(23, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 597, 652, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(24, 2021, '2021-08-23 00:00:00', NULL, 51, 0, NULL, 0, 611, 664, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(25, 2021, '2021-08-23 00:00:00', NULL, 52, 0, NULL, 0, 611, 665, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(26, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 503, 672, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(27, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 615, 674, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(28, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 483, 516, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(29, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 513, 550, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(30, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 513, 551, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(31, 2021, '2021-08-23 00:00:00', NULL, 51, 0, NULL, 0, 526, 569, NULL, 1, 1, '2021-08-10 13:59:52', '2021-08-13 19:27:52', NULL, NULL),
(32, 2021, '2021-08-23 00:00:00', NULL, 52, 0, NULL, 0, 526, 570, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(33, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 535, 580, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(34, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 568, 617, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(35, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 599, 653, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(36, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 599, 654, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(37, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 460, 488, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(38, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 464, 491, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(39, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 464, 492, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(40, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 471, 499, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(41, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 471, 500, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(42, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 471, 501, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(43, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 491, 523, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(44, 2021, '2021-08-23 00:00:00', NULL, 4, 0, NULL, 0, 491, 524, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(45, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 493, 526, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(46, 2021, '2021-08-23 00:00:00', NULL, 1, 0, NULL, 0, 494, 529, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(47, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 494, 530, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(48, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 506, 543, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(49, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 518, 559, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(50, 2021, '2021-08-23 00:00:00', NULL, 1, 0, NULL, 0, 519, 561, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(51, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 519, 562, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(52, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 531, 575, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(53, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 534, 578, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(54, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 534, 579, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(55, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 544, 593, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:53', NULL, NULL),
(56, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 544, 594, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-13 19:27:54', NULL, NULL),
(57, 2021, '2021-08-28 00:00:00', NULL, 7, 0, NULL, 0, 560, 611, NULL, 1, 1, '2021-08-10 13:59:53', '2021-08-18 15:15:09', NULL, NULL),
(58, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 624, 480, NULL, 2, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:08', NULL, NULL),
(59, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 458, 486, NULL, 2, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:08', NULL, NULL),
(60, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 459, 487, NULL, 2, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:09', NULL, NULL),
(61, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 461, 489, NULL, 2, 1, '2021-08-10 18:00:37', '2021-08-13 19:23:09', NULL, NULL),
(62, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 473, 505, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(63, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 474, 506, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(64, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 478, 512, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(65, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 480, 513, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(66, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 481, 514, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(67, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 482, 515, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(68, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 485, 517, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(69, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 486, 519, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(70, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 488, 521, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(71, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 489, 522, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(72, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 511, 547, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(73, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 512, 549, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(74, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 523, 566, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(75, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 528, 571, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(76, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 529, 573, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(77, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 540, 586, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(78, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 543, 591, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(79, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 543, 592, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(80, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 545, 597, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(81, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 550, 600, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(82, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 551, 602, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(83, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 553, 607, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(84, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 559, 610, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(85, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 572, 620, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:09', NULL, NULL),
(86, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 573, 621, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(87, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 574, 622, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(88, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 575, 623, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(89, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 577, 625, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(90, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 583, 632, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(91, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 584, 633, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(92, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 586, 637, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(93, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 498, 638, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(94, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 592, 645, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(95, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 594, 646, NULL, 2, 1, '2021-08-10 18:00:38', '2021-08-13 19:23:10', NULL, NULL),
(96, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 594, 647, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(97, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 596, 650, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(98, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 596, 651, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(99, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 605, 659, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(100, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 607, 661, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(101, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 613, 670, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(102, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 613, 671, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(103, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 503, 673, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(104, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 617, 676, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(105, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 623, 681, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(106, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 490, 682, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(107, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 490, 683, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(108, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 455, 482, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(109, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 455, 483, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(110, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 465, 493, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(111, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 465, 494, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:10', NULL, NULL),
(112, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 563, 495, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(113, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 469, 497, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(114, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 470, 498, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(115, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 472, 502, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(116, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 475, 507, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(117, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 475, 508, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(118, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 495, 532, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(119, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 495, 533, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(120, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 496, 534, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(121, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 501, 538, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(122, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 513, 552, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(123, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 513, 553, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(124, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 515, 556, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(125, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 516, 557, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(126, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 521, 564, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(127, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 525, 567, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(128, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 525, 568, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(129, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 535, 581, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(130, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 564, 615, NULL, 2, 1, '2021-08-10 18:00:39', '2021-08-13 19:23:11', NULL, NULL),
(131, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 567, 616, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(132, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 570, 619, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(133, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 576, 624, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(134, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 578, 627, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(135, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 588, 639, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(136, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 588, 640, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:11', NULL, NULL),
(137, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 604, 658, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(138, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 606, 660, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(139, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 618, 677, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(140, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 453, 481, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(141, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 457, 484, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(142, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 463, 490, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(143, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 477, 509, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(144, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 491, 525, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(145, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 494, 531, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(146, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 500, 537, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(147, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 504, 539, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(148, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 505, 540, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(149, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 506, 544, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(150, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 508, 545, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(151, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 514, 554, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(152, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 514, 555, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(153, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 517, 558, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(154, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 518, 560, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(155, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 520, 563, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(156, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 530, 574, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(157, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 531, 576, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(158, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 532, 577, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(159, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 544, 595, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:12', NULL, NULL),
(160, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 557, 608, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(161, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 561, 613, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(162, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 562, 614, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(163, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 579, 629, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(164, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 580, 630, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(165, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 582, 631, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(166, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 591, 641, NULL, 2, 1, '2021-08-10 18:00:40', '2021-08-13 19:23:13', NULL, NULL),
(167, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 591, 642, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(168, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 601, 655, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(169, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 601, 656, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(170, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 603, 657, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(171, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 608, 662, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(172, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 609, 663, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(173, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 612, 666, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(174, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 612, 667, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(175, 2021, '2021-08-23 00:00:00', NULL, 0, 0, NULL, 0, 616, 675, NULL, 2, 1, '2021-08-10 18:00:41', '2021-08-13 19:23:13', NULL, NULL),
(176, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 160, 441, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(177, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 425, 476, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(178, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 165, 446, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(179, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 165, 447, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(180, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 439, 468, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(181, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 83, 409, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(182, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 161, 443, NULL, 12, 1, '2021-08-10 19:57:13', '2021-08-10 19:57:13', NULL, NULL),
(183, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 164, 445, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(184, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 167, 448, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(185, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 148, 434, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(186, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 156, 439, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(187, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 160, 442, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(188, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 152, 474, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(189, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 152, 475, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(190, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 150, 435, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(191, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 196, 455, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(192, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 204, 457, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(193, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 150, 436, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(194, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 150, 437, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(195, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 93, 411, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(196, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 151, 438, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(197, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 163, 444, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(198, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 169, 450, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(199, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 105, 418, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(200, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 426, 466, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(201, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 31, 389, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(202, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 70, 402, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(203, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 70, 403, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(204, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 370, 461, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(205, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 141, 432, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(206, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 39, 392, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(207, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 77, 406, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(208, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 78, 407, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(209, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 386, 462, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(210, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 440, 469, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(211, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 126, 428, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(212, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 443, 470, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(213, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 125, 427, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(214, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 258, 459, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(215, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 392, 463, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(216, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 19, 386, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(217, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 121, 426, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(218, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 33, 390, NULL, 12, 1, '2021-08-10 19:57:14', '2021-08-10 19:57:14', NULL, NULL),
(219, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 33, 391, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(220, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 48, 394, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(221, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 61, 400, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(222, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 106, 419, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(223, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 79, 408, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(224, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 22, 387, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(225, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 52, 397, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(226, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 67, 401, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(227, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 451, 460, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(228, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 426, 465, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(229, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 447, 471, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(230, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 57, 398, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(231, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 72, 404, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(232, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 112, 422, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(233, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 118, 425, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(234, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 59, 399, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(235, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 129, 429, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(236, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 172, 451, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(237, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 2, 380, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(238, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 3, 381, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(239, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 15, 383, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(240, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 17, 384, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(241, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 18, 385, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(242, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 25, 388, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(243, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 96, 412, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(244, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 113, 423, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(245, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 142, 433, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(246, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 428, 467, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(247, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 13, 382, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(248, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 46, 393, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(249, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 49, 395, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(250, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 135, 431, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(251, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 195, 454, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(252, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 85, 410, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(253, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 449, 472, NULL, 12, 1, '2021-08-10 19:57:15', '2021-08-10 19:57:15', NULL, NULL),
(254, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 449, 473, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(255, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 51, 396, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(256, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 74, 405, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(257, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 186, 453, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(258, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 212, 458, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(259, 2021, '2021-08-20 00:00:00', NULL, 31, 0, NULL, 0, 419, 464, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(260, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 104, 417, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(261, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 116, 424, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(262, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 174, 452, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(263, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 100, 413, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(264, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 110, 421, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(265, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 134, 430, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(266, 2021, '2021-08-20 00:00:00', NULL, 29, 0, NULL, 0, 101, 414, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(267, 2021, '2021-08-20 00:00:00', NULL, 32, 0, NULL, 0, 102, 415, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(268, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 102, 416, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(269, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 108, 420, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(270, 2021, '2021-08-20 00:00:00', NULL, 28, 0, NULL, 0, 201, 456, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(271, 2021, '2021-08-20 00:00:00', NULL, 33, 0, NULL, 0, 157, 440, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(272, 2021, '2021-08-20 00:00:00', NULL, 30, 0, NULL, 0, 168, 449, NULL, 12, 1, '2021-08-10 19:57:16', '2021-08-10 19:57:16', NULL, NULL),
(273, 2021, '2023-10-11 00:00:00', '2021-07-03 00:00:00', 27, 7, 7, 0, 1, 1, NULL, 7, 6, '2021-08-10 19:59:47', '2021-08-17 15:57:55', NULL, NULL),
(274, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 86, 18, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(275, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 97, 21, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(276, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 250, 35, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(277, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 323, 57, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(278, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 365, 67, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(279, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 379, 70, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(280, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 5, 2, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(281, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 94, 20, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(282, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 76, 188, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(283, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 9, 3, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(284, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 16, 4, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(285, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 35, 7, NULL, 7, 1, '2021-08-10 19:59:47', '2021-08-10 19:59:47', NULL, NULL),
(286, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 36, 8, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(287, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 44, 9, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(288, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 54, 11, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(289, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 58, 12, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(290, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 63, 13, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(291, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 75, 15, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(292, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 83, 17, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(293, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 86, 19, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(294, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 98, 22, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(295, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 117, 25, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(296, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 124, 26, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(297, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 166, 29, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(298, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 200, 30, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(299, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 219, 31, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(300, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 222, 32, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(301, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 226, 33, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(302, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 253, 36, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(303, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 259, 37, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(304, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 263, 40, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(305, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 288, 43, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(306, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 305, 47, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(307, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 307, 48, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(308, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 314, 50, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(309, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 316, 51, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(310, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 317, 52, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(311, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 318, 53, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(312, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 322, 56, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(313, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 330, 60, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(314, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 332, 61, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(315, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 336, 62, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(316, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 351, 64, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(317, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 352, 65, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(318, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 358, 66, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(319, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 375, 69, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(320, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 383, 71, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(321, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 385, 72, NULL, 7, 1, '2021-08-10 19:59:48', '2021-08-10 19:59:48', NULL, NULL),
(322, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 389, 74, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(323, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 393, 76, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(324, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 396, 78, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(325, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 400, 79, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(326, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 403, 80, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(327, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 403, 81, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(328, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 406, 82, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(329, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 415, 85, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(330, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 421, 87, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(331, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 422, 88, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(332, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 429, 91, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(333, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 435, 93, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(334, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 445, 96, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(335, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 20, 5, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(336, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 34, 6, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(337, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 45, 10, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(338, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 66, 14, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(339, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 76, 16, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(340, 2021, '2023-10-11 00:00:00', NULL, NULL, 0, NULL, 0, 109, 23, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(341, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 115, 24, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(342, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 124, 27, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(343, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 147, 28, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(344, 2021, '2023-10-11 00:00:00', NULL, NULL, 0, NULL, 0, 232, 34, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(345, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 260, 38, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(346, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 260, 39, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(347, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 275, 41, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(348, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 281, 42, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(349, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 299, 44, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(350, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 300, 45, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(351, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 302, 46, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(352, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 308, 49, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(353, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 320, 54, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(354, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 321, 55, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(355, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 324, 58, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(356, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 325, 59, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(357, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 341, 63, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(358, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 369, 68, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(359, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 387, 73, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(360, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 389, 75, NULL, 7, 1, '2021-08-10 19:59:49', '2021-08-10 19:59:49', NULL, NULL),
(361, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 394, 77, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(362, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 413, 83, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(363, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 414, 84, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(364, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 416, 86, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(365, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 424, 89, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(366, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 427, 90, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(367, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 432, 92, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(368, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 438, 94, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL);
INSERT INTO `semanas_usos` (`id`, `anio`, `fecha_vencimiento`, `fecha_uso`, `semana`, `dias_usado`, `dias`, `fraccionado`, `cliente_id`, `unidad_por_cliente`, `hotel_id`, `temporada_id`, `estado_semana_uso_id`, `created_at`, `updated_at`, `unidad_id`, `observacion`) VALUES
(369, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 442, 95, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(370, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 423, 97, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(371, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 423, 98, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(372, 2021, '2023-10-11 00:00:00', NULL, 0, 0, NULL, 0, 452, 99, NULL, 7, 1, '2021-08-10 19:59:50', '2021-08-10 19:59:50', NULL, NULL),
(373, 2021, '2021-08-23 00:00:00', NULL, 3, 0, NULL, 0, 458, 485, NULL, 1, 1, '2021-08-13 19:27:50', '2021-08-13 19:27:50', NULL, NULL),
(374, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 466, 496, NULL, 1, 1, '2021-08-13 19:27:50', '2021-08-13 19:27:50', NULL, NULL),
(375, 2021, '2021-08-23 00:00:00', NULL, 51, 0, NULL, 0, 473, 503, NULL, 1, 1, '2021-08-13 19:27:50', '2021-08-13 19:27:50', NULL, NULL),
(376, 2021, '2021-08-23 00:00:00', NULL, 52, 0, NULL, 0, 473, 504, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(377, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 479, 510, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(378, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 479, 511, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(379, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 486, 518, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(380, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 488, 520, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(381, 2021, '2021-08-23 00:00:00', NULL, 1, 0, NULL, 0, 497, 535, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(382, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 497, 536, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(383, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 507, 541, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(384, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 507, 542, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(385, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 511, 546, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(386, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 512, 548, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(387, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 523, 565, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(388, 2021, '2021-08-23 00:00:00', NULL, 8, 0, NULL, 0, 529, 572, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(389, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 538, 582, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(390, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 538, 583, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(391, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 539, 584, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(392, 2021, '2021-08-23 00:00:00', NULL, 9, 0, NULL, 0, 540, 585, NULL, 1, 1, '2021-08-13 19:27:51', '2021-08-13 19:27:51', NULL, NULL),
(393, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 493, 527, NULL, 1, 1, '2021-08-13 19:27:53', '2021-08-13 19:27:53', NULL, NULL),
(394, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 493, 528, NULL, 1, 1, '2021-08-13 19:27:53', '2021-08-13 19:27:53', NULL, NULL),
(395, 2021, '2021-08-28 00:00:00', NULL, 8, 0, NULL, 0, 560, 612, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-18 15:15:09', NULL, NULL),
(396, 2021, '2021-08-23 00:00:00', NULL, 2, 0, NULL, 0, 579, 628, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-13 19:27:54', NULL, NULL),
(397, 2021, '2021-08-28 00:00:00', NULL, 7, 0, NULL, 0, 614, 668, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-18 15:18:48', NULL, NULL),
(398, 2021, '2021-08-28 00:00:00', NULL, 8, 0, NULL, 0, 614, 669, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-18 15:18:48', NULL, NULL),
(399, 2021, '2021-08-23 00:00:00', NULL, 5, 0, NULL, 0, 622, 678, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-13 19:27:54', NULL, NULL),
(400, 2021, '2021-08-23 00:00:00', NULL, 6, 0, NULL, 0, 622, 679, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-13 19:27:54', NULL, NULL),
(401, 2021, '2021-08-23 00:00:00', NULL, 7, 0, NULL, 0, 622, 680, NULL, 1, 1, '2021-08-13 19:27:54', '2021-08-13 19:27:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temporadas`
--

CREATE TABLE `temporadas` (
  `id` int(11) NOT NULL,
  `identificacion` varchar(100) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `hotel` int(11) DEFAULT NULL,
  `fraccionado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `temporadas`
--

INSERT INTO `temporadas` (`id`, `identificacion`, `nombre`, `hotel`, `fraccionado`) VALUES
(1, 'AVF', 'altas Verano fijas', 2, 0),
(2, 'MFL', 'Media Flotante', 2, 0),
(3, 'AVF', 'altas Verano fijas', 3, 0),
(4, 'MFL', 'Media Flotante', 3, 0),
(5, 'ALI', 'Alta Invierno Fija', 1, 0),
(6, 'AVF', 'altas Verano fijas', 1, 0),
(7, 'AFM', 'Alta Flotante Mixta', 1, 0),
(8, 'VFL', 'Verano Flotante', 1, 0),
(9, 'MFL', 'Media Flotante', 1, 0),
(10, 'MEF', 'Media Fija', 1, 0),
(11, 'FLI', 'Altas Flotantes Invierno', 1, 0),
(12, 'SAI', 'Super Alta Invierno Fija', 1, 0),
(13, 'MFL (Impar) / AFM (par)', 'MFL (Impar) / AFM (par)', 1, 0),
(14, 'MFL (Par) / AFM (Impar)', 'MFL (Par) / AFM (Impar)', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tokens_clientes`
--

CREATE TABLE `tokens_clientes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cliente_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tokens_clientes`
--

INSERT INTO `tokens_clientes` (`id`, `token`, `cliente_id`, `created_at`, `updated_at`) VALUES
(1, 'Bpm95emPEGm5p3lnilHaoqUgx9oI8pv5c7qhxBKm', 74, '2021-08-18 14:27:24', '2021-08-18 14:27:24'),
(2, 'OQS1BW7z3xGB2Ic0uoqZ4jKGY94W4OQZ9nrdaqiI', 74, '2021-08-18 14:28:02', '2021-08-18 14:28:02'),
(3, 'iHgR6vkWxdqLnaGROtfHelJCily9Vfvv6LwyUp0K', 74, '2021-08-18 14:28:55', '2021-08-18 14:28:55'),
(4, 'TqtN13yOAUPbQZ6GXs8MkQHx4dO4rEmDE4SYQwGA', 74, '2021-08-18 14:30:22', '2021-08-18 14:30:22'),
(5, 'UUOr9O9Wn3aGvzIDMeC7dmCbyLuogKxnMGq7E7tA', 74, '2021-08-18 14:32:31', '2021-08-18 14:32:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE `unidades` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `hotel` int(11) DEFAULT NULL,
  `capacidad` int(11) DEFAULT NULL,
  `capacidad_maxima` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id`, `nombre`, `hotel`, `capacidad`, `capacidad_maxima`) VALUES
(1, 'A01', 1, 6, 10),
(2, 'A02', 1, 4, 6),
(3, 'A03', 1, 4, 6),
(4, 'A04', 1, 8, 12),
(5, 'A05', 1, 4, 6),
(6, 'A06', 1, 6, 10),
(7, 'A07', 1, 6, 10),
(8, 'A08', 1, 6, 10),
(9, 'A09', 1, 6, 10),
(10, 'B01', 1, 6, 10),
(11, 'B02', 1, 4, 6),
(12, 'B03', 1, 4, 6),
(13, 'B04', 1, 4, 6),
(14, 'B05', 1, 4, 6),
(15, 'B06', 1, 4, 6),
(16, 'B07', 1, 6, 10),
(17, 'B08', 1, 6, 10),
(18, 'B09', 1, 6, 10),
(19, 'B10', 1, 6, 10),
(20, 'B11', 1, 4, 5),
(21, 'C01', 1, 4, 5),
(22, 'C02', 1, 4, 5),
(23, 'C03', 1, 6, 6),
(24, 'C04', 1, 4, 5),
(25, 'C05', 1, 4, 5),
(26, 'C06', 1, 4, 6),
(27, 'C07', 1, 6, 7),
(28, 'C08', 1, 4, 5),
(29, 'C09', 1, 6, 7),
(30, 'C10', 1, 4, 6),
(31, 'A20', 1, 4, 6),
(32, 'C70', 1, 6, 7),
(33, 'C90', 1, 6, 8),
(34, 'A80', 1, 6, 10),
(35, 'A40', 1, 8, 10),
(36, 'C40', 1, 4, 5),
(37, 'Capacidad 4', 2, 4, 4),
(38, 'Capacidad 5', 2, 5, 5),
(39, 'Capacidad 7', 2, 7, 7),
(40, 'Capacidad 4', 3, 4, 4),
(41, 'Capacidad 5', 3, 5, 5),
(42, 'Capacidad 7', 3, 7, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades_por_cliente`
--

CREATE TABLE `unidades_por_cliente` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cliente` int(11) NOT NULL,
  `unidad` int(11) NOT NULL,
  `temporada` int(11) NOT NULL,
  `semana` int(11) DEFAULT NULL,
  `capacidad` int(11) DEFAULT NULL,
  `hotel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `unidades_por_cliente`
--

INSERT INTO `unidades_por_cliente` (`id`, `cliente`, `unidad`, `temporada`, `semana`, `capacidad`, `hotel`) VALUES
(1, 1, 5, 7, 0, 6, 1),
(2, 5, 9, 7, 0, 10, 1),
(3, 9, 31, 7, 0, 6, 1),
(4, 16, 31, 7, 0, 6, 1),
(5, 20, 34, 7, 0, 10, 1),
(6, 34, 34, 7, 0, 10, 1),
(7, 35, 31, 7, 0, 6, 1),
(8, 36, 31, 7, 0, 6, 1),
(9, 44, 31, 7, 0, 6, 1),
(10, 45, 34, 7, 0, 10, 1),
(11, 54, 31, 7, 0, 6, 1),
(12, 58, 31, 7, 0, 6, 1),
(13, 63, 31, 7, 0, 6, 1),
(14, 66, 34, 7, 0, 10, 1),
(15, 75, 31, 7, 0, 6, 1),
(16, 76, 34, 7, 0, 10, 1),
(17, 83, 31, 7, 0, 6, 1),
(18, 86, 5, 7, 0, 6, 1),
(19, 86, 31, 7, 0, 6, 1),
(20, 94, 9, 7, 0, 10, 1),
(21, 97, 5, 7, 0, 6, 1),
(22, 98, 31, 7, 0, 6, 1),
(23, 109, 34, 7, NULL, 10, 1),
(24, 115, 34, 7, 0, 10, 1),
(25, 117, 31, 7, 0, 6, 1),
(26, 124, 31, 7, 0, 6, 1),
(27, 124, 34, 7, 0, 10, 1),
(28, 147, 34, 7, 0, 10, 1),
(29, 166, 31, 7, 0, 6, 1),
(30, 200, 31, 7, 0, 6, 1),
(31, 219, 31, 7, 0, 6, 1),
(32, 222, 31, 7, 0, 6, 1),
(33, 226, 31, 7, 0, 6, 1),
(34, 232, 34, 7, NULL, 10, 1),
(35, 250, 5, 7, 0, 6, 1),
(36, 253, 31, 7, 0, 6, 1),
(37, 259, 31, 7, 0, 6, 1),
(38, 260, 34, 7, 0, 10, 1),
(39, 260, 34, 7, 0, 10, 1),
(40, 263, 31, 7, 0, 6, 1),
(41, 275, 34, 7, 0, 10, 1),
(42, 281, 34, 7, 0, 10, 1),
(43, 288, 31, 7, 0, 6, 1),
(44, 299, 34, 7, 0, 10, 1),
(45, 300, 34, 7, 0, 10, 1),
(46, 302, 34, 7, 0, 10, 1),
(47, 305, 31, 7, 0, 6, 1),
(48, 307, 31, 7, 0, 6, 1),
(49, 308, 34, 7, 0, 10, 1),
(50, 314, 31, 7, 0, 6, 1),
(51, 316, 31, 7, 0, 6, 1),
(52, 317, 31, 7, 0, 6, 1),
(53, 318, 31, 7, 0, 6, 1),
(54, 320, 34, 7, 0, 10, 1),
(55, 321, 34, 7, 0, 10, 1),
(56, 322, 31, 7, 0, 6, 1),
(57, 323, 5, 7, 0, 6, 1),
(58, 324, 34, 7, 0, 10, 1),
(59, 325, 34, 7, 0, 10, 1),
(60, 330, 31, 7, 0, 6, 1),
(61, 332, 31, 7, 0, 6, 1),
(62, 336, 31, 7, 0, 6, 1),
(63, 341, 34, 7, 0, 10, 1),
(64, 351, 31, 7, 0, 6, 1),
(65, 352, 31, 7, 0, 6, 1),
(66, 358, 31, 7, 0, 6, 1),
(67, 365, 5, 7, 0, 6, 1),
(68, 369, 34, 7, 0, 10, 1),
(69, 375, 31, 7, 0, 6, 1),
(70, 379, 5, 7, 0, 6, 1),
(71, 383, 31, 7, 0, 6, 1),
(72, 385, 31, 7, 0, 6, 1),
(73, 387, 34, 7, 0, 10, 1),
(74, 389, 31, 7, 0, 6, 1),
(75, 389, 34, 7, 0, 10, 1),
(76, 393, 31, 7, 0, 6, 1),
(77, 394, 34, 7, 0, 10, 1),
(78, 396, 31, 7, 0, 6, 1),
(79, 400, 31, 7, 0, 6, 1),
(80, 403, 31, 7, 0, 6, 1),
(81, 403, 31, 7, 0, 6, 1),
(82, 406, 31, 7, 0, 6, 1),
(83, 413, 34, 7, 0, 10, 1),
(84, 414, 34, 7, 0, 10, 1),
(85, 415, 31, 7, 0, 6, 1),
(86, 416, 34, 7, 0, 10, 1),
(87, 421, 31, 7, 0, 6, 1),
(88, 422, 31, 7, 0, 6, 1),
(89, 424, 34, 7, 0, 10, 1),
(90, 427, 34, 7, 0, 10, 1),
(91, 429, 31, 7, 0, 6, 1),
(92, 432, 34, 7, 0, 10, 1),
(93, 435, 31, 7, 0, 6, 1),
(94, 438, 34, 7, 0, 10, 1),
(95, 442, 34, 7, 0, 10, 1),
(96, 445, 31, 7, 0, 6, 1),
(97, 423, 34, 7, 0, 10, 1),
(98, 423, 34, 7, 0, 10, 1),
(99, 452, 34, 7, 0, 10, 1),
(100, 9, 31, 5, 34, 6, 1),
(101, 10, 11, 5, 27, 6, 1),
(102, 12, 14, 5, 37, 6, 1),
(103, 23, 23, 5, 37, 7, 1),
(104, 30, 26, 5, 35, 5, 1),
(105, 41, 11, 5, 34, 6, 1),
(106, 79, 18, 5, 34, 10, 1),
(107, 82, 1, 5, 34, 10, 1),
(108, 84, 6, 5, 35, 10, 1),
(109, 90, 11, 5, 35, 6, 1),
(110, 114, 16, 5, 34, 10, 1),
(111, 119, 28, 5, 27, 5, 1),
(112, 120, 27, 5, 27, 7, 1),
(113, 122, 30, 5, 34, 6, 1),
(114, 123, 30, 5, 35, 6, 1),
(115, 128, 24, 5, 35, 5, 1),
(116, 131, 17, 5, 35, 10, 1),
(117, 132, 17, 5, 36, 10, 1),
(118, 133, 16, 5, 35, 10, 1),
(119, 137, 19, 5, 34, 10, 1),
(120, 149, 2, 5, 35, 6, 1),
(121, 153, 1, 5, 36, 10, 1),
(122, 155, 13, 5, 27, 6, 1),
(123, 158, 6, 5, 37, 10, 1),
(124, 159, 9, 5, 34, 10, 1),
(125, 162, 7, 5, 34, 10, 1),
(126, 199, 24, 5, 27, 5, 1),
(127, 202, 12, 5, 27, 6, 1),
(128, 207, 31, 5, 36, 6, 1),
(129, 209, 13, 5, 35, 6, 1),
(130, 211, 15, 5, 34, 6, 1),
(131, 214, 29, 5, 34, 7, 1),
(132, 216, 18, 5, 35, 10, 1),
(133, 224, 19, 5, 35, 10, 1),
(134, 225, 29, 5, 35, 7, 1),
(135, 233, 8, 5, 27, 10, 1),
(136, 234, 5, 5, 35, 6, 1),
(137, 240, 19, 5, 27, 10, 1),
(138, 251, 22, 5, 34, 5, 1),
(139, 360, 34, 5, 27, 10, 1),
(140, 370, 10, 5, 34, 10, 1),
(141, 388, 3, 5, 35, 6, 1),
(142, 433, 21, 5, 34, 5, 1),
(143, 448, 13, 5, 34, 6, 1),
(144, 6, 22, 6, 4, 5, 1),
(145, 6, 22, 6, 5, 5, 1),
(146, 8, 11, 6, 2, 6, 1),
(147, 11, 23, 6, 4, 7, 1),
(148, 14, 11, 6, 4, 6, 1),
(149, 32, 11, 6, 6, 6, 1),
(150, 37, 14, 6, 3, 6, 1),
(151, 38, 23, 6, 5, 7, 1),
(152, 40, 13, 6, 2, 6, 1),
(153, 43, 21, 6, 1, 5, 1),
(154, 47, 21, 6, 5, 5, 1),
(155, 50, 26, 6, 1, 5, 1),
(156, 53, 23, 6, 2, 7, 1),
(157, 56, 7, 6, 4, 10, 1),
(158, 60, 25, 6, 7, 5, 1),
(159, 62, 19, 6, 3, 10, 1),
(160, 64, 17, 6, 4, 10, 1),
(161, 69, 18, 6, 5, 10, 1),
(162, 71, 9, 6, 6, 10, 1),
(163, 72, 17, 6, 7, 10, 1),
(164, 73, 15, 6, 6, 6, 1),
(165, 73, 15, 6, 7, 6, 1),
(166, 81, 28, 6, 3, 5, 1),
(167, 87, 24, 6, 9, 5, 1),
(168, 95, 28, 6, 1, 5, 1),
(169, 95, 28, 6, 2, 5, 1),
(170, 99, 30, 6, 9, 6, 1),
(171, 103, 21, 6, 3, 5, 1),
(172, 107, 27, 6, 4, 7, 1),
(173, 111, 29, 6, 7, 7, 1),
(174, 130, 26, 6, 52, 5, 1),
(175, 136, 25, 6, 51, 5, 1),
(176, 146, 29, 6, 51, 7, 1),
(177, 177, 27, 6, 9, 7, 1),
(178, 182, 27, 6, 2, 7, 1),
(179, 188, 25, 6, 5, 5, 1),
(180, 189, 28, 6, 7, 5, 1),
(181, 191, 30, 6, 52, 6, 1),
(182, 192, 19, 6, 2, 10, 1),
(183, 197, 24, 6, 3, 5, 1),
(184, 203, 29, 6, 10, 7, 1),
(185, 230, 27, 6, 7, 7, 1),
(186, 255, 6, 6, 7, 10, 1),
(187, 441, 27, 6, 5, 7, 1),
(188, 76, 9, 7, 0, 10, 1),
(189, 65, 34, 11, 29, 10, 1),
(190, 262, 34, 11, 30, 10, 1),
(191, 265, 34, 11, 32, 10, 1),
(192, 266, 31, 11, 31, 6, 1),
(193, 267, 31, 11, 32, 6, 1),
(194, 268, 34, 11, 29, 10, 1),
(195, 273, 34, 11, 30, 10, 1),
(196, 279, 31, 11, 31, 6, 1),
(197, 280, 34, 11, 31, 10, 1),
(198, 282, 9, 11, 28, 10, 1),
(199, 282, 9, 11, 29, 10, 1),
(200, 284, 34, 11, 32, 10, 1),
(201, 287, 34, 11, 33, 10, 1),
(202, 289, 34, 11, 33, 10, 1),
(203, 290, 34, 11, 33, 10, 1),
(204, 357, 34, 11, 28, 10, 1),
(205, 357, 34, 11, 29, 10, 1),
(206, 357, 34, 11, 30, 10, 1),
(207, 359, 34, 11, 29, 10, 1),
(208, 417, 34, 11, 29, 10, 1),
(209, 436, 34, 11, 28, 6, 1),
(210, 437, 31, 11, 28, 6, 1),
(211, 434, 34, 11, 30, 10, 1),
(212, 381, 9, 11, 33, 10, 1),
(213, 328, 31, 11, 30, 6, 1),
(214, 444, 24, 11, 33, 5, 1),
(215, 42, 14, 10, 26, 6, 1),
(216, 127, 17, 10, 50, 10, 1),
(217, 138, 25, 10, 12, 5, 1),
(218, 139, 5, 10, 39, 6, 1),
(219, 143, 30, 10, 39, 6, 1),
(220, 179, 22, 10, 12, 5, 1),
(221, 185, 16, 10, 39, 10, 1),
(222, 187, 27, 10, 39, 7, 1),
(223, 193, 11, 10, 26, 6, 1),
(224, 236, 6, 10, 26, 10, 1),
(225, 237, 18, 10, 26, 10, 1),
(226, 238, 4, 10, 26, 12, 1),
(227, 243, 29, 10, 25, 7, 1),
(228, 252, 31, 10, 50, 6, 1),
(229, 4, 26, 9, 0, 5, 1),
(230, 7, 26, 9, 0, 5, 1),
(231, 21, 5, 9, 0, 6, 1),
(232, 24, 25, 9, 0, 5, 1),
(233, 26, 25, 9, 0, 5, 1),
(234, 27, 31, 9, 0, 6, 1),
(235, 28, 11, 9, 0, 6, 1),
(236, 29, 11, 9, NULL, 6, 1),
(237, 55, 25, 9, 0, 5, 1),
(238, 68, 6, 9, 0, 10, 1),
(239, 68, 6, 9, 0, 10, 1),
(240, 80, 21, 9, NULL, 5, 1),
(241, 88, 24, 9, 0, 5, 1),
(242, 89, 13, 9, 0, 6, 1),
(243, 91, 24, 9, 0, 5, 1),
(244, 92, 31, 9, 0, 6, 1),
(245, 109, 24, 9, NULL, 5, 1),
(246, 140, 12, 9, 0, 6, 1),
(247, 144, 31, 9, 0, 6, 1),
(248, 145, 31, 9, 0, 6, 1),
(249, 154, 14, 9, 0, 6, 1),
(250, 170, 3, 9, 51, 6, 1),
(251, 171, 5, 9, 0, 6, 1),
(252, 173, 36, 9, 0, 5, 1),
(253, 175, 36, 9, 0, 5, 1),
(254, 175, 36, 9, 0, 5, 1),
(255, 176, 31, 9, NULL, 6, 1),
(256, 178, 13, 9, 0, 6, 1),
(257, 180, 24, 9, 0, 5, 1),
(258, 181, 31, 9, 0, 6, 1),
(259, 183, 15, 9, 0, 6, 1),
(260, 184, 10, 9, NULL, 10, 1),
(261, 190, 13, 9, 0, 6, 1),
(262, 194, 14, 9, 0, 6, 1),
(263, 198, 26, 9, 0, 5, 1),
(264, 198, 31, 9, 0, 6, 1),
(265, 205, 13, 9, 0, 6, 1),
(266, 206, 10, 9, 0, 10, 1),
(267, 208, 24, 9, 0, 5, 1),
(268, 210, 19, 9, 0, 10, 1),
(269, 213, 13, 9, 0, 6, 1),
(270, 215, 14, 9, 0, 6, 1),
(271, 217, 14, 9, 0, 6, 1),
(272, 218, 18, 9, NULL, 10, 1),
(273, 220, 28, 9, 0, 5, 1),
(274, 223, 31, 9, 0, 6, 1),
(275, 227, 27, 9, 0, 7, 1),
(276, 228, 31, 9, NULL, 6, 1),
(277, 229, 13, 9, 0, 6, 1),
(278, 231, 6, 9, NULL, 10, 1),
(279, 235, 31, 9, 0, 6, 1),
(280, 239, 14, 9, 0, 6, 1),
(281, 241, 7, 9, 0, 10, 1),
(282, 242, 10, 9, 0, 10, 1),
(283, 244, 16, 9, 0, 10, 1),
(284, 245, 17, 9, 0, 10, 1),
(285, 246, 9, 9, 0, 10, 1),
(286, 247, 9, 9, 0, 10, 1),
(287, 248, 34, 9, 0, 10, 1),
(288, 249, 31, 9, 0, 6, 1),
(289, 256, 27, 9, 0, 7, 1),
(290, 257, 1, 9, 0, 10, 1),
(291, 263, 31, 9, 0, 6, 1),
(292, 269, 34, 9, 0, 10, 1),
(293, 270, 5, 9, 0, 6, 1),
(294, 276, 31, 9, 0, 6, 1),
(295, 277, 34, 9, 0, 10, 1),
(296, 278, 31, 9, 0, 6, 1),
(297, 292, 31, 9, 0, 6, 1),
(298, 294, 31, 9, 0, 6, 1),
(299, 296, 31, 9, 0, 6, 1),
(300, 297, 34, 9, 0, 10, 1),
(301, 298, 31, 9, 0, 6, 1),
(302, 301, 31, 9, 0, 6, 1),
(303, 303, 31, 9, 0, 6, 1),
(304, 304, 31, 9, 0, 6, 1),
(305, 306, 31, 9, 0, 6, 1),
(306, 309, 36, 9, 0, 5, 1),
(307, 310, 31, 9, 0, 6, 1),
(308, 311, 31, 9, 0, 6, 1),
(309, 312, 31, 9, 0, 6, 1),
(310, 313, 5, 9, 0, 6, 1),
(311, 315, 34, 9, 0, 10, 1),
(312, 326, 31, 9, 0, 6, 1),
(313, 327, 31, 9, 0, 6, 1),
(314, 329, 31, 9, 0, 6, 1),
(315, 331, 31, 9, 0, 6, 1),
(316, 333, 31, 9, 0, 6, 1),
(317, 334, 31, 9, 0, 6, 1),
(318, 335, 34, 9, 0, 10, 1),
(319, 337, 34, 9, 0, 10, 1),
(320, 338, 34, 9, 0, 10, 1),
(321, 339, 31, 9, 0, 6, 1),
(322, 340, 34, 9, 0, 10, 1),
(323, 342, 31, 10, 25, 10, 1),
(324, 343, 36, 9, 0, 5, 1),
(325, 344, 34, 9, 0, 10, 1),
(326, 345, 31, 9, 0, 6, 1),
(327, 346, 31, 9, 0, 6, 1),
(328, 347, 34, 9, 0, 10, 1),
(329, 348, 34, 9, 0, 10, 1),
(330, 349, 5, 9, 0, 6, 1),
(331, 350, 31, 9, 0, 6, 1),
(332, 353, 5, 9, 0, 6, 1),
(333, 354, 31, 9, 0, 6, 1),
(334, 355, 5, 9, 0, 6, 1),
(335, 356, 31, 9, 0, 6, 1),
(336, 361, 34, 9, 0, 10, 1),
(337, 362, 5, 9, 0, 6, 1),
(338, 363, 5, 9, 0, 6, 1),
(339, 364, 5, 9, 0, 6, 1),
(340, 366, 34, 9, 0, 10, 1),
(341, 367, 5, 9, 0, 6, 1),
(342, 368, 31, 9, 0, 6, 1),
(343, 371, 31, 9, 0, 6, 1),
(344, 372, 31, 9, 0, 6, 1),
(345, 373, 31, 9, 0, 6, 1),
(346, 374, 31, 9, 0, 6, 1),
(347, 376, 31, 9, 0, 6, 1),
(348, 377, 31, 9, 0, 6, 1),
(349, 378, 31, 9, 0, 6, 1),
(350, 380, 31, 9, 0, 6, 1),
(351, 382, 31, 9, 0, 6, 1),
(352, 384, 34, 9, 0, 10, 1),
(353, 390, 34, 9, 0, 10, 1),
(354, 391, 34, 9, 0, 10, 1),
(355, 395, 31, 9, 0, 6, 1),
(356, 397, 31, 9, 0, 6, 1),
(357, 398, 34, 9, 0, 10, 1),
(358, 399, 5, 9, 0, 6, 1),
(359, 401, 31, 9, 0, 6, 1),
(360, 402, 31, 9, 0, 6, 1),
(361, 404, 34, 9, 0, 10, 1),
(362, 405, 34, 9, 0, 10, 1),
(363, 406, 31, 9, 0, 6, 1),
(364, 407, 31, 9, 0, 6, 1),
(365, 409, 31, 9, 0, 6, 1),
(366, 410, 31, 9, 0, 6, 1),
(367, 411, 34, 9, 0, 10, 1),
(368, 412, 31, 9, 0, 6, 1),
(369, 418, 5, 9, 0, 6, 1),
(370, 420, 31, 9, 0, 6, 1),
(371, 420, 31, 9, 0, 6, 1),
(372, 420, 31, 9, 0, 6, 1),
(373, 430, 34, 9, 0, 10, 1),
(374, 431, 34, 9, 0, 10, 1),
(375, 446, 31, 9, 0, 6, 1),
(376, 450, 34, 9, 0, 10, 1),
(377, 254, 29, 9, 0, 7, 1),
(378, 221, 30, 9, 0, 6, 1),
(379, 285, 34, 9, 0, 10, 1),
(380, 2, 23, 12, 31, 7, 1),
(381, 3, 23, 12, 28, 7, 1),
(382, 13, 25, 12, 31, 5, 1),
(383, 15, 23, 12, 33, 7, 1),
(384, 17, 23, 12, 32, 7, 1),
(385, 18, 23, 12, 29, 7, 1),
(386, 19, 16, 12, 30, 10, 1),
(387, 22, 19, 12, 29, 10, 1),
(388, 25, 23, 12, 30, 7, 1),
(389, 31, 10, 12, 29, 10, 1),
(390, 33, 17, 12, 28, 10, 1),
(391, 33, 17, 12, 29, 10, 1),
(392, 39, 12, 12, 29, 6, 1),
(393, 46, 25, 12, 29, 5, 1),
(394, 48, 17, 12, 30, 10, 1),
(395, 49, 25, 12, 32, 5, 1),
(396, 51, 27, 12, 32, 7, 1),
(397, 52, 19, 12, 28, 10, 1),
(398, 57, 21, 12, 31, 5, 1),
(399, 59, 22, 12, 29, 5, 1),
(400, 61, 17, 12, 31, 10, 1),
(401, 67, 19, 12, 33, 10, 1),
(402, 70, 10, 12, 30, 10, 1),
(403, 70, 10, 12, 31, 10, 1),
(404, 72, 21, 12, 33, 5, 1),
(405, 74, 27, 12, 33, 7, 1),
(406, 77, 13, 12, 29, 6, 1),
(407, 78, 13, 12, 28, 6, 1),
(408, 79, 18, 12, 33, 10, 1),
(409, 83, 3, 12, 31, 6, 1),
(410, 85, 26, 12, 31, 5, 1),
(411, 93, 8, 12, 31, 10, 1),
(412, 96, 24, 12, 29, 5, 1),
(413, 100, 29, 12, 30, 7, 1),
(414, 101, 30, 12, 29, 6, 1),
(415, 102, 30, 12, 32, 6, 1),
(416, 102, 30, 12, 33, 6, 1),
(417, 104, 28, 12, 29, 5, 1),
(418, 105, 9, 12, 31, 10, 1),
(419, 106, 17, 12, 32, 10, 1),
(420, 108, 30, 12, 30, 6, 1),
(421, 110, 29, 12, 29, 7, 1),
(422, 112, 21, 12, 29, 5, 1),
(423, 113, 24, 12, 31, 5, 1),
(424, 116, 28, 12, 32, 5, 1),
(425, 118, 21, 12, 30, 5, 1),
(426, 121, 16, 12, 33, 10, 1),
(427, 125, 15, 12, 33, 6, 1),
(428, 126, 14, 12, 33, 6, 1),
(429, 129, 22, 12, 33, 5, 1),
(430, 134, 29, 12, 32, 7, 1),
(431, 135, 25, 12, 30, 5, 1),
(432, 141, 11, 12, 30, 6, 1),
(433, 142, 24, 12, 28, 5, 1),
(434, 148, 4, 12, 30, 12, 1),
(435, 150, 6, 12, 28, 10, 1),
(436, 150, 7, 12, 29, 10, 1),
(437, 150, 7, 12, 30, 10, 1),
(438, 151, 8, 12, 33, 10, 1),
(439, 156, 4, 12, 33, 12, 1),
(440, 157, 31, 12, 33, 6, 1),
(441, 160, 1, 12, 29, 10, 1),
(442, 160, 4, 12, 28, 12, 1),
(443, 161, 3, 12, 29, 6, 1),
(444, 163, 8, 12, 30, 10, 1),
(445, 164, 3, 12, 28, 6, 1),
(446, 165, 2, 12, 29, 6, 1),
(447, 165, 2, 12, 30, 6, 1),
(448, 167, 3, 12, 33, 6, 1),
(449, 168, 34, 12, 30, 10, 1),
(450, 169, 8, 12, 32, 10, 1),
(451, 172, 22, 12, 30, 5, 1),
(452, 174, 28, 12, 30, 5, 1),
(453, 186, 27, 12, 28, 7, 1),
(454, 195, 25, 12, 28, 5, 1),
(455, 196, 6, 12, 32, 10, 1),
(456, 201, 30, 12, 28, 6, 1),
(457, 204, 6, 12, 29, 10, 1),
(458, 212, 27, 12, 29, 7, 1),
(459, 258, 15, 12, 32, 6, 1),
(460, 451, 19, 12, 32, 10, 1),
(461, 370, 10, 12, 33, 10, 1),
(462, 386, 13, 12, 32, 6, 1),
(463, 392, 15, 12, 31, 6, 1),
(464, 419, 27, 12, 31, 7, 1),
(465, 426, 19, 12, 31, 10, 1),
(466, 426, 9, 12, 32, 10, 1),
(467, 428, 24, 12, 32, 5, 1),
(468, 439, 2, 12, 32, 6, 1),
(469, 440, 13, 12, 30, 6, 1),
(470, 443, 14, 12, 28, 6, 1),
(471, 447, 19, 12, 30, 10, 1),
(472, 449, 26, 12, 28, 5, 1),
(473, 449, 26, 12, 29, 5, 1),
(474, 152, 5, 12, 28, 6, 1),
(475, 152, 5, 12, 29, 6, 1),
(476, 425, 1, 12, 31, 10, 1),
(477, 295, 34, 8, 0, 10, 1),
(478, 319, 5, 8, 0, 6, 1),
(479, 408, 5, 8, 0, 6, 1),
(480, 624, 37, 2, 0, 4, 2),
(481, 453, 39, 2, 0, 7, 2),
(482, 455, 38, 2, 0, 5, 2),
(483, 455, 38, 2, 0, 5, 2),
(484, 457, 39, 2, 0, 7, 2),
(485, 458, 37, 1, 3, 4, 2),
(486, 458, 37, 2, 0, 4, 2),
(487, 459, 37, 2, 0, 4, 2),
(488, 460, 39, 1, 3, 7, 2),
(489, 461, 37, 2, 0, 4, 2),
(490, 463, 39, 2, 0, 7, 2),
(491, 464, 39, 1, 2, 7, 2),
(492, 464, 39, 1, 3, 7, 2),
(493, 465, 38, 2, 0, 5, 2),
(494, 465, 38, 2, 0, 5, 2),
(495, 563, 38, 2, 0, 5, 2),
(496, 466, 37, 1, 7, 4, 2),
(497, 469, 38, 2, 0, 5, 2),
(498, 470, 38, 2, 0, 5, 2),
(499, 471, 39, 1, 7, 7, 2),
(500, 471, 39, 1, 8, 7, 2),
(501, 471, 39, 1, 9, 7, 2),
(502, 472, 38, 2, 0, 5, 2),
(503, 473, 37, 1, 51, 4, 2),
(504, 473, 37, 1, 52, 4, 2),
(505, 473, 37, 2, 0, 4, 2),
(506, 474, 37, 2, 0, 4, 2),
(507, 475, 38, 2, 0, 5, 2),
(508, 475, 38, 2, 0, 5, 2),
(509, 477, 39, 2, 0, 7, 2),
(510, 479, 37, 1, 5, 4, 2),
(511, 479, 37, 1, 6, 4, 2),
(512, 478, 37, 2, 0, 4, 2),
(513, 480, 37, 2, 0, 4, 2),
(514, 481, 37, 2, 0, 4, 2),
(515, 482, 37, 2, 0, 4, 2),
(516, 483, 38, 1, 5, 5, 2),
(517, 485, 37, 2, 0, 4, 2),
(518, 486, 37, 1, 9, 4, 2),
(519, 486, 37, 2, 0, 4, 2),
(520, 488, 37, 1, 2, 4, 2),
(521, 488, 37, 2, 0, 4, 2),
(522, 489, 37, 2, 0, 4, 2),
(523, 491, 39, 1, 3, 7, 2),
(524, 491, 39, 1, 4, 7, 2),
(525, 491, 39, 2, 0, 7, 2),
(526, 493, 39, 1, 5, 7, 2),
(527, 493, 39, 1, 6, 7, 2),
(528, 493, 39, 1, 7, 7, 2),
(529, 494, 39, 1, 1, 7, 2),
(530, 494, 39, 1, 2, 7, 2),
(531, 494, 39, 2, 0, 7, 2),
(532, 495, 38, 2, 0, 5, 2),
(533, 495, 38, 2, 0, 5, 2),
(534, 496, 38, 2, 0, 5, 2),
(535, 497, 37, 1, 1, 4, 2),
(536, 497, 37, 1, 2, 4, 2),
(537, 500, 39, 2, 0, 7, 2),
(538, 501, 38, 2, 0, 5, 2),
(539, 504, 39, 2, 0, 7, 2),
(540, 505, 39, 2, 0, 7, 2),
(541, 507, 37, 1, 7, 4, 2),
(542, 507, 37, 1, 8, 4, 2),
(543, 506, 39, 1, 9, 7, 2),
(544, 506, 39, 2, 0, 7, 2),
(545, 508, 39, 2, 0, 7, 2),
(546, 511, 37, 1, 6, 4, 2),
(547, 511, 37, 2, 0, 4, 2),
(548, 512, 37, 1, 5, 4, 2),
(549, 512, 37, 2, 0, 4, 2),
(550, 513, 38, 1, 5, 5, 2),
(551, 513, 38, 1, 6, 5, 2),
(552, 513, 38, 2, 0, 5, 2),
(553, 513, 38, 2, 0, 5, 2),
(554, 514, 39, 2, 0, 7, 2),
(555, 514, 39, 2, 0, 7, 2),
(556, 515, 38, 2, 0, 5, 2),
(557, 516, 38, 2, 0, 5, 2),
(558, 517, 39, 2, 0, 7, 2),
(559, 518, 39, 1, 6, 7, 2),
(560, 518, 39, 2, 0, 7, 2),
(561, 519, 39, 1, 1, 7, 2),
(562, 519, 39, 1, 2, 7, 2),
(563, 520, 39, 2, 0, 7, 2),
(564, 521, 38, 2, 0, 5, 2),
(565, 523, 37, 1, 9, 4, 2),
(566, 523, 37, 2, 0, 4, 2),
(567, 525, 38, 2, 0, 5, 2),
(568, 525, 38, 2, 0, 5, 2),
(569, 526, 38, 1, 51, 5, 2),
(570, 526, 38, 1, 52, 5, 2),
(571, 528, 37, 2, 0, 4, 2),
(572, 529, 37, 1, 8, 4, 2),
(573, 529, 37, 2, 0, 4, 2),
(574, 530, 39, 2, 0, 7, 2),
(575, 531, 39, 1, 6, 7, 2),
(576, 531, 39, 2, 0, 7, 2),
(577, 532, 39, 2, 0, 7, 2),
(578, 534, 39, 1, 5, 7, 2),
(579, 534, 39, 1, 6, 7, 2),
(580, 535, 38, 1, 5, 5, 2),
(581, 535, 38, 2, 0, 5, 2),
(582, 538, 37, 1, 5, 4, 2),
(583, 538, 37, 1, 6, 4, 2),
(584, 539, 37, 1, 7, 4, 2),
(585, 540, 37, 1, 9, 4, 2),
(586, 540, 37, 2, 0, 4, 2),
(587, 541, 37, 1, 1, 4, 2),
(588, 541, 37, 1, 2, 4, 2),
(589, 541, 37, 1, 51, 4, 2),
(590, 541, 37, 1, 52, 4, 2),
(591, 543, 37, 2, 0, 4, 2),
(592, 543, 37, 2, 0, 4, 2),
(593, 544, 39, 1, 7, 7, 2),
(594, 544, 39, 1, 8, 7, 2),
(595, 544, 39, 2, 0, 7, 2),
(596, 545, 37, 1, 5, 4, 2),
(597, 545, 37, 2, 0, 4, 2),
(598, 549, 37, 1, 2, 4, 2),
(599, 550, 37, 1, 4, 4, 2),
(600, 550, 37, 2, 0, 4, 2),
(601, 551, 37, 1, 8, 4, 2),
(602, 551, 37, 2, 0, 4, 2),
(603, 552, 37, 1, 3, 4, 2),
(604, 552, 37, 1, 4, 4, 2),
(605, 552, 37, 1, 5, 4, 2),
(606, 552, 37, 1, 6, 4, 2),
(607, 553, 37, 2, 0, 4, 2),
(608, 557, 39, 2, 0, 7, 2),
(609, 559, 37, 1, 2, 4, 2),
(610, 559, 37, 2, 0, 4, 2),
(611, 560, 39, 1, 7, 7, 2),
(612, 560, 39, 1, 8, 7, 2),
(613, 561, 39, 2, 0, 7, 2),
(614, 562, 39, 2, 0, 7, 2),
(615, 564, 38, 2, 0, 5, 2),
(616, 567, 38, 2, 0, 5, 2),
(617, 568, 38, 1, 3, 5, 2),
(618, 569, 37, 1, 4, 4, 2),
(619, 570, 38, 2, 0, 5, 2),
(620, 572, 37, 2, 0, 4, 2),
(621, 573, 37, 2, 0, 4, 2),
(622, 574, 37, 2, 0, 4, 2),
(623, 575, 37, 2, 0, 4, 2),
(624, 576, 38, 2, 0, 5, 2),
(625, 577, 37, 2, 0, 4, 2),
(626, 577, 37, 1, 4, 4, 2),
(627, 578, 38, 2, 0, 5, 2),
(628, 579, 39, 1, 2, 7, 2),
(629, 579, 39, 2, 0, 7, 2),
(630, 580, 39, 2, 0, 7, 2),
(631, 582, 39, 2, 0, 7, 2),
(632, 583, 37, 2, 0, 4, 2),
(633, 584, 37, 2, 0, 4, 2),
(634, 585, 37, 1, 6, 4, 2),
(635, 585, 37, 1, 7, 4, 2),
(636, 585, 37, 1, 8, 4, 2),
(637, 586, 37, 2, 0, 4, 2),
(638, 498, 37, 2, 0, 4, 2),
(639, 588, 38, 2, 0, 5, 2),
(640, 588, 38, 2, 0, 5, 2),
(641, 591, 39, 2, 0, 7, 2),
(642, 591, 39, 2, 0, 7, 2),
(643, 592, 37, 1, 2, 4, 2),
(644, 592, 37, 1, 3, 4, 2),
(645, 592, 37, 2, 0, 4, 2),
(646, 594, 37, 2, 0, 4, 2),
(647, 594, 37, 2, 0, 4, 2),
(648, 596, 37, 1, 2, 4, 2),
(649, 596, 37, 1, 3, 4, 2),
(650, 596, 37, 2, 0, 4, 2),
(651, 596, 37, 2, 0, 4, 2),
(652, 597, 37, 1, 5, 4, 2),
(653, 599, 38, 1, 7, 5, 2),
(654, 599, 38, 1, 8, 5, 2),
(655, 601, 39, 2, 0, 7, 2),
(656, 601, 39, 2, 0, 7, 2),
(657, 603, 39, 2, 0, 7, 2),
(658, 604, 38, 2, 0, 5, 2),
(659, 605, 37, 2, 0, 4, 2),
(660, 606, 38, 2, 0, 5, 2),
(661, 607, 37, 2, 0, 4, 2),
(662, 608, 39, 2, 0, 7, 2),
(663, 609, 39, 2, 0, 7, 2),
(664, 611, 37, 1, 51, 4, 2),
(665, 611, 37, 1, 52, 4, 2),
(666, 612, 39, 2, 0, 7, 2),
(667, 612, 39, 2, 0, 7, 2),
(668, 614, 39, 1, 7, 7, 2),
(669, 614, 39, 1, 8, 7, 2),
(670, 613, 37, 2, 0, 4, 2),
(671, 613, 37, 2, 0, 4, 2),
(672, 503, 37, 1, 9, 4, 2),
(673, 503, 37, 2, 0, 4, 2),
(674, 615, 37, 1, 3, 4, 2),
(675, 616, 39, 2, 0, 7, 2),
(676, 617, 37, 2, 0, 4, 2),
(677, 618, 38, 2, 0, 5, 2),
(678, 622, 39, 1, 5, 7, 2),
(679, 622, 39, 1, 6, 7, 2),
(680, 622, 39, 1, 7, 7, 2),
(681, 623, 37, 2, 0, 4, 2),
(682, 490, 37, 2, 0, 4, 2),
(683, 490, 37, 2, 0, 4, 2),
(684, 799, 41, 4, 0, 5, 3),
(685, 628, 42, 4, 0, 7, 3),
(686, 630, 42, 4, 0, 7, 3),
(687, 636, 42, 3, 4, 7, 3),
(688, 641, 40, 4, 0, 4, 3),
(689, 642, 42, 4, 0, 7, 3),
(690, 643, 42, 4, 0, 7, 3),
(691, 650, 40, 3, 6, 4, 3),
(692, 661, 42, 3, 2, 7, 3),
(693, 661, 42, 3, 3, 7, 3),
(694, 661, 42, 3, 4, 7, 3),
(695, 661, 42, 3, 5, 7, 3),
(696, 661, 42, 4, 0, 7, 3),
(697, 661, 42, 4, 0, 7, 3),
(698, 666, 41, 4, 0, 5, 3),
(699, 673, 41, 4, 0, 5, 3),
(700, 673, 40, 4, 0, 4, 3),
(701, 676, 42, 4, 0, 7, 3),
(702, 698, 42, 4, 0, 7, 3),
(703, 701, 41, 3, 3, 5, 3),
(704, 701, 41, 3, 4, 5, 3),
(705, 701, 41, 3, 2, 5, 3),
(706, 710, 40, 3, 7, 4, 3),
(707, 716, 40, 4, 0, 4, 3),
(708, 720, 41, 4, 0, 5, 3),
(709, 720, 41, 4, 0, 5, 3),
(710, 720, 41, 4, 0, 5, 3),
(711, 721, 40, 4, 0, 4, 3),
(712, 722, 41, 4, 0, 5, 3),
(713, 728, 41, 4, 0, 5, 3),
(714, 728, 41, 4, 0, 5, 3),
(715, 729, 41, 4, 0, 5, 3),
(716, 732, 40, 4, 0, 4, 3),
(717, 739, 42, 4, 0, 7, 3),
(718, 740, 41, 4, 0, 5, 3),
(719, 745, 41, 4, 0, 5, 3),
(720, 755, 41, 4, 0, 5, 3),
(721, 761, 41, 4, 0, 5, 3),
(722, 763, 42, 4, 0, 7, 3),
(723, 764, 41, 3, 3, 5, 3),
(724, 764, 41, 3, 4, 5, 3),
(725, 767, 40, 3, 5, 4, 3),
(726, 769, 42, 4, 0, 7, 3),
(727, 772, 41, 4, 0, 5, 3),
(728, 774, 40, 4, 0, 4, 3),
(729, 793, 42, 4, 0, 7, 3),
(730, 794, 42, 4, 0, 7, 3),
(731, 795, 40, 4, 0, 4, 3),
(732, 707, 40, 4, 0, 4, 3),
(733, 658, 40, 3, 7, 4, 3),
(734, 800, 40, 4, 0, 4, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'Admin admin', 'admin@admin.com', NULL, '$2y$10$WxdURHr6wRDlUA3eX5zk8OLeedpeCaTyC1bU8umSt3nUoX2Jt7adW', 'ElJ8o9MBZM5JVcbB0l0jSVH855P9hp7093Mfxv4kGooOStOdBEkztOSoPvfI', '2021-06-29 19:43:23', '2021-06-29 19:43:23');

-- --------------------------------------------------------

--
-- Estructura para la vista `clientes_con_saldo`
--
DROP TABLE IF EXISTS `clientes_con_saldo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `clientes_con_saldo`  AS SELECT `c`.`id` AS `id`, `c`.`identificacion` AS `identificacion`, `c`.`apellido` AS `apellido`, `c`.`nombre` AS `nombre`, `c`.`telefono` AS `telefono`, `c`.`celular` AS `celular`, `c`.`email` AS `email`, `c`.`nombre_conyugue` AS `nombre_conyugue`, `c`.`apellido_conyugue` AS `apellido_conyugue`, `c`.`celular_conyugue` AS `celular_conyugue`, `c`.`email_conyugue` AS `email_conyugue`, `c`.`password` AS `password`, `c`.`hotel` AS `hotel`, `c`.`dni` AS `dni`, `c`.`dni_conyugue` AS `dni_conyugue`, `c`.`fecha_nacimiento` AS `fecha_nacimiento`, `c`.`fecha_naciemiento_conyugue` AS `fecha_naciemiento_conyugue`, `c`.`domicilio` AS `domicilio`, `c`.`localidad` AS `localidad`, `c`.`provincia` AS `provincia`, `c`.`pais` AS `pais`, `c`.`cod_postal` AS `cod_postal`, `c`.`email2` AS `email2`, `c`.`id_rci` AS `id_rci`, `c`.`id_interval` AS `id_interval`, `c`.`telefono_laboral` AS `telefono_laboral`, ifnull(sum(`cc`.`importe` * `m`.`signo`),0) AS `saldo` FROM ((`clientes` `c` left join `cuentas_corrientes` `cc` on(`cc`.`cliente_id` = `c`.`id`)) left join `movimientos` `m` on(`m`.`id` = `cc`.`movimiento_id`)) GROUP BY `c`.`id`, `c`.`identificacion`, `c`.`apellido`, `c`.`nombre`, `c`.`telefono`, `c`.`celular`, `c`.`email`, `c`.`nombre_conyugue`, `c`.`apellido_conyugue`, `c`.`celular_conyugue`, `c`.`email_conyugue`, `c`.`password`, `c`.`hotel`, `c`.`dni`, `c`.`dni_conyugue`, `c`.`fecha_nacimiento`, `c`.`fecha_naciemiento_conyugue`, `c`.`domicilio`, `c`.`localidad`, `c`.`provincia`, `c`.`pais`, `c`.`cod_postal`, `c`.`email2`, `c`.`id_rci`, `c`.`id_interval`, `c`.`telefono_laboral` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consultas`
--
ALTER TABLE `consultas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cuentas_corrientes`
--
ALTER TABLE `cuentas_corrientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estados_semana_uso`
--
ALTER TABLE `estados_semana_uso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_consulta`
--
ALTER TABLE `estado_consulta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expensas`
--
ALTER TABLE `expensas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `fechas_semanas_uso`
--
ALTER TABLE `fechas_semanas_uso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hoteles`
--
ALTER TABLE `hoteles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `precio_temporada_anios`
--
ALTER TABLE `precio_temporada_anios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recibos`
--
ALTER TABLE `recibos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `semanas_temporadas`
--
ALTER TABLE `semanas_temporadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `semanas_usos`
--
ALTER TABLE `semanas_usos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temporadas`
--
ALTER TABLE `temporadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tokens_clientes`
--
ALTER TABLE `tokens_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades_por_cliente`
--
ALTER TABLE `unidades_por_cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=801;

--
-- AUTO_INCREMENT de la tabla `consultas`
--
ALTER TABLE `consultas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `cuentas_corrientes`
--
ALTER TABLE `cuentas_corrientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estados_semana_uso`
--
ALTER TABLE `estados_semana_uso`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estado_consulta`
--
ALTER TABLE `estado_consulta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `expensas`
--
ALTER TABLE `expensas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `fechas_semanas_uso`
--
ALTER TABLE `fechas_semanas_uso`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `hoteles`
--
ALTER TABLE `hoteles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `precio_temporada_anios`
--
ALTER TABLE `precio_temporada_anios`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=283;

--
-- AUTO_INCREMENT de la tabla `recibos`
--
ALTER TABLE `recibos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `semanas_temporadas`
--
ALTER TABLE `semanas_temporadas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT de la tabla `semanas_usos`
--
ALTER TABLE `semanas_usos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=402;

--
-- AUTO_INCREMENT de la tabla `temporadas`
--
ALTER TABLE `temporadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `tokens_clientes`
--
ALTER TABLE `tokens_clientes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `unidades`
--
ALTER TABLE `unidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `unidades_por_cliente`
--
ALTER TABLE `unidades_por_cliente`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=735;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
